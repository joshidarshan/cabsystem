<?php

if (!isset($_SESSION))
    session_start();

function BPath() {
    $path = $_SERVER['DOCUMENT_ROOT'];
    // Differenciate the path for local and web host. Includes database connect file.
    if ($_SERVER['HTTP_HOST'] == "localhost" || $_SERVER['HTTP_HOST'] == "localhost:8080")
        $path .= '/Cabsystem';
    return $path;
}

include_once BPath() . '/bizop/views/bookedseatlayout.php';
include_once BPath() . '/dbop/data/booking.php';
include_once BPath() . '/dbop/data/cab.php';
include_once BPath() . '/dbop/data/location.php';
include_once BPath() . '/dbop/data/sharedfare.php';
include_once BPath() . '/dbop/data/globalperameter.php';
include_once BPath() . '/dbop/data/client.php';
include_once BPath() . '/dbop/data/cabroute.php';

if (isset($_POST['Book'])) {
    include_once BPath() . '/dbop/data/client.php';
    $Id = $_POST['ClientId'];
    $Name = $_POST['firstname'];
    $Email = $_POST['email'];
    $Phone = $_POST['telephone'];
    $pwdEmail = substr($Email, 0, 3);
    $pwdPhone = substr($Phone, strlen($Phone) - 3, 3);
    $Pwd = $pwdEmail . $pwdPhone;
    $RoleId = 3;
    $clientId = CheckUser($Id, $Email, $Phone, $Name, $Pwd, $RoleId);
    $ClientId = $clientId;
    $TravelerType = $_POST['TravelerType'];
    $Gander = $_POST['Gander'];
    $Designation = $_POST['Desiganation'];
    $Occupation = $_POST['Occupation'];
    $Department = $_POST['Department'];
    $Company = $_POST['Company'];
    $ClientProfileAdd = ClientProfileInsert($Id, $ClientId, $TravelerType, $Gander, $Designation, $Occupation, $Department, $Company);

    // $Pickup = $_POST['Pickup'];
    if ($_POST['Type'] == 'Virtualchartered') {

        $fromDate = $_POST['FromDate'];
        $toDate = $_POST['ToDate'];
        $source = $_POST['Source'];
        $destination = $_POST['Destination'];
        $pickupLocation = $_POST['Source'];
        $fare = $_POST['fare'];
        $amountReceived = 00;
        $carModelId = $_POST['CarModelId'];
        $paymentStatus = 0;
        "<input type='hidden' name='Amount' value='$fare'/>";

        if (isset($_POST['isPaid']) && !empty($_POST['isPaid']))
            $paymentStatus = 1;
        $isRoundTrip = 0;
        // commted below code: Virtual cab booking will always reflects 
        /* if(isset($_POST['isRoundTrip']) && !empty($_POST['isRoundTrip']))
          $isRoundTrip = 1; */

        // Insert into cabroute table
        $cabRouteId = AddVirtualCabRoute($fromDate, $source, $destination, $toDate, $isRoundTrip, $carModelId, 1);

        // Insert into booking table
        //$ClientId, $IsPaid,$SLocationId,$DLocationId,$IsShare,$fare, $AmountReceived,$Date,$IsMaharaja,$BidRate,$IsBidAwarded,$CabRouteId,$PickupLocation,$SeatsBooked,$TicketCount,$IsBookedByClient,$Status
        $bookId = InsertBooking($clientId, $paymentStatus, $source, $destination, 0, $fare, $amountReceived, $fromDate, $toDate, 0, 0, 0, $cabRouteId, $pickupLocation, 1, 1, 0, 1);
        if ($bookId)

        //header ("location: index.php");
            header("location: paymentinfo.php?Amount=$fare");
    }
    if ($_POST['Type'] == 'chartered') {
        $CabRouteId = $_REQUEST['CabRouteId'];
        $source = $_POST['Source'];
        // $destination = $_POST['Destination'];
        $travelDateTime = $_POST['ToDate'];
        $amountReceived = 00;
        $pickupLocation = $_POST['Pickup'];
        $paymentStatus = 0;
        if (isset($_POST['isPaid']) && !empty($_POST['isPaid']))
            $paymentStatus = 1;
        $fare = $_POST['fare'];
        $FromDate = $_POST['FromDate'];
        $ToDate = $_POST['ToDate'];
        "<input type='hidden' name='Amount' value='$fare'/>";
        $bookId = InsertBooking($clientId, $paymentStatus, $source, $source, 0, $fare, $amountReceived, $FromDate, $ToDate, 0, 0, 0, $CabRouteId, $pickupLocation, 1, 1, 1, 1);
        //This will be handeled by corn job.
        //$isAffected = AddCharterCabToRoute($cabRouteId, $toDateTime, $destination);

        if ($bookId)
            header("location: paymentinfo.php?Amount=$fare");
//        
    }
    if ($_POST['Type'] == 'shared') {
        include_once BPath() . '/dbop/data/sublocation.php';

        $CabRouteId = $_REQUEST['CabRouteId'];
        $CarModelId = $_REQUEST['CarModelId'];
//$Type = $_REQUEST['Type'];
//$CabId = $_REQUEST['cr_cabid'];
        $source = $_POST['Source'];
        $destination = $_POST['Destination'];
        $travelDateTime = $_POST['ToDateTime'];
        // $sublocationid = $_POST['SubLocationId'];
        $seatLayoutData = SeatLayoutGenerator($CabRouteId, $CarModelId);
//        

        $amountReceived = 00;
        $pickupLocation = $_POST['Pickup'];
//    $seatBookedList = $_POST['seat'];
        // $SubId = $_POST['SubLocationId'];
        $seatBooked = "";
        $ticketBookedCount = 0;
        $isMaharaja = 0;
        $paymentStatus = 0;
        $FromDate = $_POST['FromDate'];
        $ToDate = $_POST['ToDate'];
        //$Rate = GetFareByRoute($source, $destination);
        //$MaharajaRate = GetMaharajaFare($Rate);
        //$ProperLocationRate = SubLocationFare($SubId);
        //$Rate = $_POST['Rate'];
        //$MaharajaRate = $_POST['MaharajaRate'];
        //$ProperLocationRate = $_POST['ProperLocationRate'];
        //$fare = $Rate + $MaharajaRate + $ProperLocationRate;
        $fare = $_POST['Fare'];

        if (isset($_POST['isPaid']) && !empty($_POST['isPaid']))
            $paymentStatus = 1;
        $seatBookedList = $_POST['seat'];
        foreach ($seatBookedList as $sbk => $sbd) {
            if (!empty($sbd)) {
                $row = chr($sbk + 65) . "x" . $sbd;
                if ($sbk == 0)
                    $isMaharaja = 1;
                if (empty($seatBooked))
                    $seatBooked .= $row;
                else
                    $seatBooked .= "," . $row;
                $ticketBookedCount+=$sbd;
            }
        }
        "<input type='hidden' name='Amount' value='$fare'/>";
        $bookId = InsertBooking($clientId, $paymentStatus, $source, $destination, 1, $fare, $amountReceived, $FromDate, $ToDate, $isMaharaja, 0, 0, $CabRouteId, $pickupLocation, $seatBooked, $ticketBookedCount, 1, 1) or die('error' . mysql_error());
        if ($bookId)
            header("location: paymentinfo.php?Amount=$fare");
    }
    if ($_POST['Type'] == 'OneWay') {
        $CabRouteId = $_REQUEST['CabRouteId'];
        $source = $_POST['Source'];
        $destination = $_POST['Destination'];
        $travelDateTime = $_POST['ToDateTime'];
        $amountReceived = 00;
        $pickupLocation = $_POST['Pickup'];
        if (isset($_POST['isPaid']) && !empty($_POST['isPaid']))
            $paymentStatus = 1;
        $fare = $_POST['fare'];
        $FromDate = $_POST['FromDate'];
        $ToDate = $_POST['ToDate'];

        $bookId = InsertBooking($clientId, $paymentStatus, $source, $destination, 0, $fare, $amountReceived, $FromDate, $ToDate, 0, 0, 0, $CabRouteId, $pickupLocation, 1, 1, 1, 1);
        //This will be handeled by corn job.
        //$isAffected = AddCharterCabToRoute($cabRouteId, $toDateTime, $destination);
        "<input type='hidden' name='Amount' value='$fare'/>";
        if ($bookId)
            header("location: paymentinfo.php?Amount=$fare");
    }
}
$Str = "";
if ($_POST['Type'] == 'Bid') {
    include_once BPath() . '/dbop/data/booking.php';
    $ClientId = $clientId;
    $SLocationId = $_POST['FromLocation'];
    $DLocationId = $_POST['ToLocation'];
    $Date1 = $_POST['FromDate-1'];
    $Date2 = $_POST['FromDate-2'];
    $Date3 = $_POST['FromDate-2'];
    $NoPerson = $_POST['NoPerson'];
    $ModelCollection = $_SESSION['ModelId'];
    $BidCollection = $_SESSION['BidAmount'];
//    var_dump($BidCollection);
//    var_dump($ModelCollection);

    $Str = "";
    $BidCounter = 0;
    foreach ($ModelCollection AS $Mk => $Mv) {
        $Str .= "M" . $Mv . "X" . $BidCollection[$BidCounter] . ",";
        $BidCounter++;
    }
    $Str = substr($Str, 0, (strlen($Str) - 1));
//    echo $Str;
    $ModelBid = $Str;
    $IsAwarded = 0;
    $Status = 1;
    $bookId = InsertBid($ClientId, $SLocationId, $DLocationId, $Date1, $Date2, $Date3, $NoPerson, $ModelBid, $IsAwarded, $Status);
    //This will be handeled by corn job.
    //$isAffected = AddCharterCabToRoute($cabRouteId, $toDateTime, $destination);
//    if($bookId)
//        header ("location: index.php");
//       
//   "<input type='hidden' name='Amount' value='$fare'/>";
    header("location: paymentinfo.php?Amount=$BidCollection");
}
?>
