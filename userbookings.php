<?php
if (!isset($_SESSION))
    session_start();

function Path() {
    $path = $_SERVER['DOCUMENT_ROOT'];
    // Differenciate the path for local and web host. Includes database connect file.
    if ($_SERVER['HTTP_HOST'] == "localhost" || $_SERVER['HTTP_HOST'] == "localhost:8080")
        $path .= '/Cabsystem';
    return $path;
}
function SliderImageUrl() {
    $url = "http://" . $_SERVER['HTTP_HOST'];
    if ($_SERVER['HTTP_HOST'] == "localhost" || $_SERVER['HTTP_HOST'] == "localhost:8080")
        $url .= '/Cabsystem';

    return $url;
}
function GetLocalDateTime(){
if (function_exists('date_default_timezone_set')) {
    date_default_timezone_set('Asia/Calcutta');
}
$date = date('Y-m-d h:i:s', time());
return $date;
}
$CurrentDate = GetLocalDateTime();

include_once Path() . '/templates/header.php';
include_once Path() . '/dbop/data/location.php';
include_once Path() . '/dbop/data/hourcharges.php';
include_once Path() . '/dbop/data/booking.php';
//include_once Path() . '/pop/index.php';
?><form action="#">
<table align="center" border="3">
    <h1 align="center"> User Bookings   </h1>
    <tr>
        <th>From Date</th>
        <th>To Date</th>
        <th>From Location</th>
        <th>To Location</th>
        <th>Cancel</th>
    </tr>
    
    <?php 
    $ClientId = $_SESSION['UID'];
    $GetBooking = BookingByClient($ClientId);
    while($FetchBooking = mysql_fetch_array($GetBooking)){?>
    <tr>
        <td><?php echo $FetchBooking['FromDate'];?> </td>
        <td><?php echo $FetchBooking['ToDate'];?></td>
    <td><?php echo $FetchBooking['Source'];?></td>
    <td><?php echo $FetchBooking['Destination'];?></td>
     <?php $FromDate =  $FetchBooking['FromDate'];
          $ToDate =  $FetchBooking['ToDate'];
          if($CurrentDate < $FromDate  ) { 
    ?>  
    <td><a href="CancelationByClient.php?bookingId=<?php echo $FetchBooking['Id'];?>">Cancel</a></td>
          <?php } 
          else { ?>
    <td id="NoCancel">Cancel</td>
          <?php } ?>
    </tr>
        <?php } ?>
</table>
</form>
