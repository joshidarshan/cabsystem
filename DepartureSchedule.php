<?php
if (!isset($_SESSION))
    session_start();

function DPath() {
    $path = $_SERVER['DOCUMENT_ROOT'];
    // Differenciate the path for local and web host. Includes database connect file.
    if ($_SERVER['HTTP_HOST'] == "localhost" || $_SERVER['HTTP_HOST'] == "localhost:8080")
        $path .= '/Cabsystem';
    return $path;
}

include_once DPath() . '/templates/header.php';
include_once DPath() . '/dbop/data/listingutility.php';
include_once DPath() . '/dbop/data/location.php';
?>
<h1 align="center">Departure Schedule</h1>
<form action="DepartureSchedule.php" method="post">
    <table align="center">
        <tr>
            <td>Enter Departure Date</td>
            <td><input type="text" name="FromDate" class="dttm"/></td>
            <td><input type="submit" name="go" value="Go"/></td>
        </tr>
    </table>
    <script>
    $(function() {
        $('.dttm').appendDtpicker({
            "dateOnly": true,
            "futureOnly": true
        });
    });

    function SetToLocation() {
        $("#");
    }
</script>
</form>
    <div class="container" >
        <!--<marquee behavour="left"><img src="Upload/images/taxi-icon.png" height="100px" width="100px"/><br/><h3>Select a Car</h3></marquee>-->
        <div class="span10 searchliine" style="background-image: url('Upload/Images/main-bg.jpg'); margin-top: 2%"><p style="text-align: center;">Cabs Departure Schedule</p></div>
        <div class="row listingform span10">
            <table id="listing" class="table table-striped table-bordered  tablesorter" >
                <style>
                    #listing td{
        vertical-align: middle;
    }
                </style>
                <thead>
                <tr>
                    <th>
                        From
                    </th>
                    <th>
                        To
                    </th>
                    <th>
                        Departure-On
                    </th>
                    <th>
                        Cabs
                    </th>
                    <th>
                        Make
                    </th>
                    <th>
                        Available Seats
                    </th>
                </tr>
                </thead>
                <tbody>
                <tr>
                
                    <?php 
                    if(isset($_POST['go'])){
        
    
        include_once DPath() . '/bizop/Views/DepartureScheduleListing.php';
        $SubId = NULL;
       $FromLocalDate = $_POST['FromDate'];
        //$FromLocalDate = $_POST['FromDate'];
      //  $SourceName = $_POST['SourceName'];
       // $DestName = $_POST['DestName'];
        $List = GetClientAllSharedFree($FromLocalDate);
        $charterView = "";
// hiddenfields
        //echo "<input type='hidden' id='hFromDate' name='hFromDate' value='$FromLocalDate' />";
                                                    //Pre_List_charted($List, $days_between, $FromLocationId, $ToLocationId, $SubId, $FromLocalDate, $ToLocalDate, &$CCount) {
         $charterView = Pre_List_shared_all($List, $FromLocalDate); 
        //echo $CabCount;
         if($charterView)
                 echo $charterView;
         else
             echo "<font color='Red'><h1>Sorry , There Is No Cab According To Your Choice,<br/>Enter Correct Data</h1></font>";
                    }
                ?>  
                </tr>
               <!--<tr><td colspan="5"><div class="paging"><a>1</a> 2 3 ... 14 15 16</div></td></tr>-->                </tbody>
            </table>
        </div>
    </div>
<script>
    var isCalled = false;
    setTimeout(function(){
        callSorter();
    }, 5000);
    
    if(!isCalled)
        
    $(document).ready(function(){ 
        $("#listing").tablesorter();
    }); 
    
    function callSorter(){
        $("table#listing").trigger("update");
        /*var sorting = [[0,0]];
        $("table#listing").trigger("sorton",[sorting]);*/
    }
    
    $(function(){
        if ($("table#listing tbody tr").length > 0)
            $("#listing").tablesorter();
    });
</script>
<?php
include_once DPath() . '/templates/footer.php';
    
?>

