<?php
if (!isset($_SESSION))
    session_start();
$_SESSION['cat'] = "inventory";

function Path() {
    $path = $_SERVER['DOCUMENT_ROOT'];
    // Differenciate the path for local and web host. Includes database connect file.
    if ($_SERVER['HTTP_HOST'] == "localhost" || $_SERVER['HTTP_HOST'] == "localhost:8080")
        $path .= '/Cabsystem';
    return $path;
}

include_once Path() . '/templates/adminheader.php';
    include_once Path() . '/dbop/data/pages.php';
    
    
if (isset($_POST['sub'])) {


    $Id = 1;
    $AboutUs = $_POST['AboutUs'];
    $ContactUs = $_POST['ContactUs'];
    $PrivacyPolicy = $_POST['PrivacyPolicy'];
    $Disclaimer = $_POST['Disclaimer'];
    $Feedback = $_POST['Feedback'];
    $NewsFlash = $_POST['NewsFlash'];
    $Popup = $_POST['Popup'];
    
    EditPageContain($Id,$AboutUs,$ContactUs,$PrivacyPolicy,$Disclaimer,$Feedback,$NewsFlash,$Popup);
    echo "<script> alert ('Your Update SuccessFully'); window.location='Pages.php';</script>";
}
//Connect();
//$sql = mysql_query("select * from pages where Id = '3'");
$Id = $_REQUEST['Id'];
$GetPagesContain = GetPageContainById($Id);
$GetPageContain = mysql_fetch_array($GetPagesContain);
//$row = mysql_fetch_array($agentData);

?>
<script type="text/javascript" src="../js/tiny_mce/tiny_mce.js"></script>
<script type="text/javascript" src="../js/tiny_mce/myeditor.js"></script>

<form action="PagesEdit.php" method="post" enctype="multipart/form-data" name="Cabsystem" onSubmit=" return Validation();">
    <h1 style="text-align: center;">Pages Modifications </h1>
    <table align="center" class="table table-striped">
        <tr>
            <td>Edit About US</td>
            <td><textarea name="AboutUs"><?php echo $GetPageContain['AboutUs']; ?></textarea></td>
        </tr>
        <tr>
            <td>Edit Contact Us</td>
            <td><textarea name="ContactUs"><?php echo $GetPageContain['ContactUs']; ?></textarea></td>
        </tr>
        <tr>
            <td>Edit Privacy Policy</td>
            <td><textarea name="PrivacyPolicy"><?php echo $GetPageContain['PrivacyPolicy']; ?></textarea></td>
        </tr>
        <tr>
            <td>Edit Disclaimer</td>
            <td><textarea name="Disclaimer"><?php echo $GetPageContain['Disclaimer'];?></textarea></td>
        </tr>
        <tr>
            <td>Edit Feed Back</td>
            <td><textarea name="Feedback"><?php echo $GetPageContain['Feedback']; ?></textarea></td>
        </tr>
        <tr>
            <td>Edit News Flash</td>
            <td><textarea name="NewsFlash"><?php echo $GetPageContain['NewsFlash']; ?></textarea></td>
        </tr>
        <tr>
            <td>Edit PopUp</td>
            <td><textarea name="Popup"><?php  echo $GetPageContain['Popup'];?></textarea></td>
        <tr>
            <td colspan="2" style="text-align: center;"><input type="submit" name="sub" value="Update" class="btn"/></td>
        </tr>
    </table>	
</form>

<?php
include_once Path() . '/templates/adminfooter.php';
?>

