<?php
if (!isset($_SESSION))
    session_start();
$_SESSION['cat'] = "inventory";

function Path() {
    $path = $_SERVER['DOCUMENT_ROOT'];
    // Differenciate the path for local and web host. Includes database connect file.
    if ($_SERVER['HTTP_HOST'] == "localhost" || $_SERVER['HTTP_HOST'] == "localhost:8080")
        $path .= '/Cabsystem';
    return $path;
}

include_once Path() . '/templates/adminheader.php';

if (isset($_POST['submit']) && !empty($_POST['submit'])) {
// Call fucntion for shared available listing
// Call fucntion for charter available listing
// Show listing for virtual available cab.
// Calling SP for chartered cab listing + virtual cab function
// Calling SP for shared cab listing
// Calling SP for chartered cab listing + virtual cab function
    $source = $_POST['sourceLocation'];
    $destination = $_POST['destinationLocation'];
    $noSeats = $_POST['noSeats'];
    $fromDate = $_POST['FromDate'];
    $toDate = $_POST['ToDate'];
    
    $charterCabSp = "Call charteredcablist($source, $destination, $fromDate, $toDate)";

// Now how to call the virtual cab list. We will need the actual cab count and based on that we will display the code.
    ?>

<!-- here we will display the bookings -->


    <?php
} else {
    ?>
    <form action="BookingAvailability.php" method="POST">
        <table class="table table-striped">
            <tr>
                <td>Source</td>
                <td>
                    <select id="sourceLocation" name="sourceLocation">

                    </select> 
                </td>
            </tr>
            <tr>
                <td>Destination</td>
                <td>
                    <select id="destinationLocation" name="destinationLocation">

                    </select>
                </td>
            </tr>
            <tr>
                <td>No Seat/Persons</td>
                <td>
                    <select id="noSeats" name="noSeats">
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                        <option value="6">6</option>
                        <option value="7">7</option>
                        <option value="8">8</option>
                        <option value="9">9+</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td>From Date</td>
                <td><input type="text" id="FromDate" name="FromDate" /></td>
            </tr>
            <tr>
                <td>To Date</td>
                <td><input type="text" id="ToDate" name="ToDate" /></td>
            </tr>

            <tr>
                <td colspan="2" style="text-align: center;"><input type="submit" name="submit" value="Submit" class="btn btn-large" /></td>
            </tr>
        </table>
    </form>

    <script>
        $(function() {
            $('*[name=FromDate]').appendDtpicker({
                //"inline": true,
                "minuteInterval": 15
            });
            $('*[name=ToDate]').appendDtpicker({
                //"inline": true,
                "minuteInterval": 15
            });
        });
    </script>

    <?php
}
include_once Path() . '/templates/adminfooter.php';
?>