<?php
if (!isset($_SESSION))
    session_start();
$_SESSION['cat'] = "inventory";

function Path() {
    $path = $_SERVER['DOCUMENT_ROOT'];
    // Differenciate the path for local and web host. Includes database connect file.
    if ($_SERVER['HTTP_HOST'] == "localhost" || $_SERVER['HTTP_HOST'] == "localhost:8080")
        $path .= '/Cabsystem';
    return $path;
}
function CabDocUrl() {
    $url = "http://" . $_SERVER['HTTP_HOST'];
    if ($_SERVER['HTTP_HOST'] == "localhost" || $_SERVER['HTTP_HOST'] == "localhost:8080")
        $url .= '/Cabsystem';

    return $url;
}

include_once Path() . '/templates/adminheader.php';
include_once Path() . '/dbop/data/cab.php';

if (isset($_POST['sub'])) {
    $Id = $_POST['Id'];
    $CarModelId = $_POST['CarModelId'];
    $NumberPlate = $_POST['NoPlate'];
    $Rating = $_POST['Rating'];
    $MadeYear = $_POST['MadeYear'];
    $AgentId = $_POST['AgentId'];
    $KmRate = $_POST['KmRate'];
    $AcKmRate = $_POST['AcKmRate'];
    $DocumentLink = "";
    

    if (isset($_FILES['DocLink']['name']) && !empty($_FILES['DocLink']['name']) && $_FILES['DocLink']['size'] > 0) {
        //$DocumentLink = $_FILES['DocLink'];
        $path = Path(). '/Upload/document/';
        $guid = uniqid();
        $fname = $guid . '-' . $_FILES['DocLink']['name'];
        move_uploaded_file($_FILES["DocLink"]["tmp_name"], $path . $fname);
        $DocumentLink = $fname;
    } else {
        if (isset($_POST['prevdoc']) && !empty($_POST['prevdoc']))
            $DocumentLink = $_POST['prevdoc'];
    }
    $Status = "";
    if (isset($_POST['Status']) && !empty($_POST['Status']))
        $Status = $_POST['Status'];
    else
        $Status = 0;

    UpdateCabs($Id, $CarModelId, $NumberPlate, $Rating, $MadeYear, $AgentId, $KmRate, $AcKmRate, $DocumentLink, $Status);
//    $Through = $_POST['Through'];
//    if($Through = 'Agent'){
//        echo "<script> alert('Record Updated SuccessFully'); window.location='../agent/Agentcab.php';</script>";
//    }
        echo "<script> alert('Record Updated SuccessFully'); window.location='Cab.php';</script>";
//    }
    
}
//$Through = $_REQUEST['Through'];
$Id = $_GET['Id'];
$GetCabById = GetCabById($Id);
$fetchCabById = mysql_fetch_array($GetCabById);
include_once Path() . '/dbop/data/CarModel.php';
include_once Path() . '/dbop/data/Agent.php';

$GetAllCarModels = GetCarModelListing(0, 500);
$GetAllAgents = GetAllAgents($start = 0, $rpp = 500);
?>

<form action="CabEdit.php" method="post" enctype="multipart/form-data">
    <h1 align="center">Update Cab Data</h1>
    <table align="center" class="table table-striped">
        
        <tr>
            <td>Choose A Car Model</td>
            <td> <select name="CarModelId">
                    <?php
                    while ($FetchAllCarModels = mysql_fetch_array($GetAllCarModels)) {
                        if ($FetchAllCarModels['Id'] == $fetchCabById['CarModelId'])
                            echo "<option value=$FetchAllCarModels[Id] selected='selected'>$FetchAllCarModels[Name]</option>";
                        else
                            echo "<option value=$FetchAllCarModels[Id]>$FetchAllCarModels[Name]</option>";
                    }
                    ?>
                </select> <input type="hidden" name="Id" value="<?php echo $Id; ?>"/> </td>
        </tr>
        <tr>
            <td>Enter No. Plate</td>
            <td><input type="text" name="NoPlate" value="<?php echo $fetchCabById['NoPlate']; ?>" required/></td>
        </tr>
        <tr>
            <td>Enter Rating</td>
            <td><input type="text" name="Rating" value="<?php echo $fetchCabById['Rating']; ?>"/></td>
        </tr>
        <tr>
            <td>Enter Made Year</td>
            <td><input type="text" name="MadeYear" value="<?php echo $fetchCabById['MadeYear']; ?>"/></td>
        </tr>
        <tr>
            <td>Choose Agent</td>
            <td> <select name="AgentId">
                    <?php
                    while ($FetchAllAgents = mysql_fetch_array($GetAllAgents)) {
                        if ($FetchAllAgents['Id'] == $fetchCabById['AgentId'])
                            echo "<option value=$FetchAllAgents[Id] selected='selected'>$FetchAllAgents[Name]</option>";
                        else
                            echo "<option value=$FetchAllAgents[Id]>$FetchAllAgents[Name]</option>";
                    }
                    ?>
                </select></td>
        </tr>
        <tr>
            <td>Enter Km. Rate</td>
            <td><input type="text" name="KmRate" value="<?php echo $fetchCabById['KmRate']; ?>"/></td>
        </tr>
        <tr>
            <td>Enter Ac Km. Rate</td>
            <td><input type="text" name="AcKmRate" value="<?php echo $fetchCabById['AcKmRate']; ?>"/></td>
        </tr>
        <tr>
            <td>Your Current Image</td>
            <td><img src= "<?php echo CabDocUrl() . '/Upload/document/' . $fetchCabById['DocLink']; ?>" height="100" width="200"/></td>
            <?php
            if (isset($fetchCabById['DocLink']) && !empty($fetchCabById)) {
                $originfname = substr($fetchCabById['DocLink'], (stripos($fetchCabById['DocLink'], '-') + 1), strlen($fetchCabById['DocLink']));
                echo "<tr>";
                echo "<td>Current Document Name</td>";
                echo "<td>$originfname</td>";
                echo "</tr>";
//                echo "<input type='text' name='prevdoc' id='prevdoc' value='$fetchCabById[DocLink]' />";
            }
            ?>
            <tr>
            <td>Choose An Image</td>
            <td><input type="file" name="DocLink"/></td>
        </tr>
        </tr>	
            <td>Current Status</td>
            <td>
                <?php if ($fetchCabById['Status'] == 1) { ?>
                    <input type="checkbox" name="Status" value="1" checked="checked"/>
                <?php } else {
                    ?>

                    <input type="checkbox" name="Status" value="1"/> <?php } ?>
            </td>
        </tr>
        <tr>
            <td colspan="2" style="text-align: center"><input type="submit" name="sub" value="Update" class="btn-large"/></td>
        </tr>
    </table>
</form>
<?php
include_once Path() . '/templates/adminfooter.php';
?>