<?php
if (!isset($_SESSION))
    session_start();
$_SESSION['cat'] = "home";

function BPath() {
    $path = $_SERVER['DOCUMENT_ROOT'];
    // Differenciate the path for local and web host. Includes database connect file.
    if ($_SERVER['HTTP_HOST'] == "localhost" || $_SERVER['HTTP_HOST'] == "localhost:8080")
        $path .= '/Cabsystem';
    return $path;
}

include_once BPath() . '/bizop/views/bookedseatlayout.php';
include_once BPath() . '/dbop/data/booking.php';
include_once BPath() . '/dbop/data/cab.php';
include_once BPath() . '/dbop/data/location.php';
include_once BPath() . '/dbop/data/sharedfare.php';
include_once BPath() . '/dbop/data/globalperameter.php';
include_once BPath() . '/dbop/data/client.php';
include_once BPath().'/dbop/data/cabroute.php';

// POST handling
if (isset($_POST) && !empty($_POST)) {
    // Shared
    if($_POST['type'] == 'shared')
        SharedBooking();
    if($_POST['type'] == 'chartered')
        CharteredBooking ();
    if($_POST['type'] == 'virtual')
        VirtualBooking ();
}

include_once BPath() . '/templates/adminheader.php';



// date
if (function_exists('date_default_timezone_set')) {
    date_default_timezone_set('Asia/Calcutta');
}
$fromYear = date('Y');
$tday = date('d');
$tmonth = date('m');
$toYear = date('Y', strtotime('+1 year'));

// locations
$locations = GetLocationArray();


?>
<h2 class="text-center">Booking</h2>
<form action="booking.php" method="POST">
    <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
    <?php
    if (isset($_GET) && !empty($_GET)) {
        if ($_GET['type'] == 'shared') {
            $cabRouteId = $_GET['cabRouteId'];
            $source = $_GET['source'];
            $destination = $_GET['destination'];
            $travelDateTime = $_GET['travelDateTime'];
            $carModelId = $_GET['carModelId'];
            $seatLayoutData = SeatLayoutGenerator($cabRouteId, $carModelId);
            $fare = GetFareByRoute($source, $destination);
            $maharajaFare = GetMaharajaFare($fare);

            // hidden field
            echo "<input type='hidden' name='type' id='type' value='shared' />";
            echo "<input type='hidden' name='cabRouteId' value='$cabRouteId' />";
            echo "<input type='hidden' name='source' id='source' value='$source' />";
            echo "<input type='hidden' name='destination' id='destination' value='$destination' />";
            echo "<input type='hidden' name='travelDateTime' value='$travelDateTime' />";
            echo "<input type='hidden' name='carModelId' value='$carModelId' />";
            echo "<input type='hidden' name='hdfare' id='hdfare' value='$fare' />";
            echo "<input type='hidden' name='hdmaharaFare' id='hdmaharaFare' value='$maharajaFare' />";
        }
        if ($_GET['type'] == 'chartered') {
            $cabRouteId = $_GET['cabRouteId'];
            /*$source = $_GET['source'];
            $destination = $_GET['destination'];
            $fromDate = $_GET['fromDate'];
            $toDate = $_GET['toDate'];*/
            $acRate = 0;
            $nonAcRate = 0;
            GetCabRateByCabRouteId($cabRouteId, $acRate, $nonAcRate); // function will set the $acRate, $nonAcRate variable.

            echo "<input type='hidden' name='type' id='type' value='chartered' />";
            echo "<input type='hidden' name='cabRouteId' value='$cabRouteId' />";
//            echo "<input type='hidden' name='source' value='$source' />";
//            echo "<input type='hidden' name='destination' value='$destination' />";
//            echo "<input type='hidden' name='fromDate' value='$fromDate' />";
//            echo "<input type='hidden' name='toDate' value='$toDate' />";
            echo "<input type='hidden' name='fare' value='' />";
            echo "<input type='hidden' name='acRate' id='acRate' value='$acRate' />";
            echo "<input type='hidden' name='nonAcRate' id='nonAcRate' value='$nonAcRate' />";
        }
        if ($_GET['type'] == 'virtual') {
            $carModelId = $_GET['carModelId'];
            $acRate = 0;
            $nonAcRate = 0;
            GetVirtualCabRateByModelId($carModelId, $acRate, $nonAcRate); // function will set the $acRate, $nonAcRate variable.

            echo "<input type='hidden' name='type' id='type' value='virtual' />";
            echo "<input type='hidden' name='carModelId' value='$carModelId' />";
            echo "<input type='hidden' name='fare' value='' />";
            echo "<input type='hidden' name='acRate' id='acRate' value='$acRate' />";
            echo "<input type='hidden' name='nonAcRate' id='nonAcRate' value='$nonAcRate' />";
        }
        ?>

        <table class="table table-striped">

            <?php
            if ($_GET['type'] != 'shared') {
                ?>
                <tr>
                    <td>Is Round trip</td>
                    <td><input type="checkbox" id="isRoundTrip" name="isRoundTrip" onclick="return GetDistanceFare();"/></td>
                </tr>
                <!-- Charter & Virtual from+to location -->
                <tr>
                    <td>From Location</td>
                    <td>
                        <select id="fromLocation" name="fromLocation" >
        <?php
        foreach ($locations as $blsk => $blsd)
            echo "<option value=$blsd[Id]>$blsd[Name]</option>";
        ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>To Location</td>
                    <td>
                        <select id="toLocation" name="toLocation" onchange="return GetDistanceFare();">
        <?php
        foreach ($locations as $blsk => $blsd)
            echo "<option value=$blsd[Id]>$blsd[Name]</option>";
        ?>
                        </select>
                        &nbsp;&nbsp;&nbsp;&nbsp;
                        <a onclick="return GetDistanceFare()" style="cursor: pointer;">Calculate Fare</a>
                    </td>
                </tr>
                
                
                
                <tr>
                    <td>From Date & Time</td>
                    <td><input type="text" id="fromDateTime" name="fromDateTime" class="Date" /></td>
                </tr>
                <tr>
                    <td>To Date & Time</td>
                    <td><input type="text" id="toDateTime" name="toDateTime" class="Date" onblur="return GetDistanceFare();"/></td>
                </tr>
                <!-- End: Charter & Virtual from+to location -->
        <?php
    }
    ?>

            <!-- Shared cab -->
    <?php
    if ($_GET['type'] == 'shared') {
        ?>
                <tr>
                    <td>Seat Selection</td>
                    <td>
        <?php
        echo SeatViewGenerator($seatLayoutData);
        ?>
                    </td>
                </tr>
                
                <tr>
                    <td>Sub Location</td>
                    <td>
                        <select id="subLocation" name="subLocation" onfocus="return GetSubLocation();">
                            <option>Select</option>
                        </select>
                    </td>
                </tr>
                <!-- end: Shared Cab -->
        <?php
    }
    ?>

            <tr>
                <td>Name</td>
                <td><input type="text" id="name" name="name" required /></td>
            </tr>
            <tr>
                <td>Email</td>
                <td><input type="email" id="email" name="email" required /></td>
            </tr>
            <tr>
                <td>Phone</td>
                <td><input type="text" id="phone" name="phone" required onblur="return IdentifyClient();"/></td>
            </tr>
            <tr>
                <td>Pickup Location</td>
                <td><textarea id="pickupLocation" name="pickupLocation" required></textarea></td>
            </tr>

            <tr>
                <td>Fare</td>
                <td><input type="text" id="fare" name="fare" onfocus="return CalculateFare()" onblur="return FillAmount()" /></td>
            </tr>
            
            <tr>
                <td>Amount Received</td>
                <td><input type="text" id="amountReceived" name="amountReceived" /></td>
            </tr>
            
            <tr>
                <td>Is Paid?</td>
                <td><input type="checkbox" id="isPaid" name="isPaid" /></td>
            </tr>
            <tr>
                <td colspan="2" style="text-align: center;"><input type="submit" name="submit" value="Submit" class="btn btn-large" /></td>
            </tr>
        </table>
        <input type="hidden" id="distance"  name="distance" />
        <input type="hidden" id="ordinarySeatsCount" />
        <input type="hidden" id="maharajaSeatsCount" />
    <?php
}
?>
</form>
<script>
            function check(id, val) {
                if(val == 0){
                    alert("All seats are full");
                    return false;
                }
                else{
                    var bk = $("#"+id).val();
                    if(bk > val){
                        alert("Booking seats are more then available seats");
                        $("#"+id).val("");
                        $("#"+id).focus();
                        return false;
                    }
                    else{
                        if(id == 'seat-a'){
                            var seatCount = $("#"+id).val();
                            $("#maharajaSeatsCount").val(seatCount);
                        }
                        else{
                            var seatCount = $("#"+id).val();
                            var ordinaryCount = '';
                        }
                    }
                }
            }
            
            function CalculateFare(){
                var type = $("#type").val();
                
                if(type == 'shared'){
                    var fare = $("#hdfare").val();
                    var maharajaFare = $("#hdmaharaFare").val();

                    var maharajaSeatCount = $("#seat-a").val();
                    if(!maharajaSeatCount)
                        maharajaSeatCount = 0;
                    
                    var ordinarySeatCount = 0;
                    $("#seatLayout").find('.seat').each(function(){
                        if(this.value)
                            ordinarySeatCount += parseInt(this.value);
                    });
                    if(!ordinarySeatCount)
                        ordinarySeatCount = 0;
                    
                    
                    var total = ((maharajaFare * maharajaSeatCount) + (fare * ordinarySeatCount));
                    
                    var subLocationData = $("#subLocation").val();
                    if(subLocationData && subLocationData != 'Select'){
                        var subLocSplit = subLocationData.split('-');
                        var subLocationFare = 0;
                        if(subLocSplit)
                            subLocationFare = subLocSplit[1];
                        var totalFare = parseFloat(total) + parseFloat(subLocationFare);
                        if((maharajaSeatCount > 0 || ordinarySeatCount > 0) && total > 0)
                            $("#fare").val(totalFare);
                        else
                            $("#fare").val(0);
                    }
                    else{
                        if((maharajaSeatCount > 0 || ordinarySeatCount > 0) && total > 0)
                            $("#fare").val(total);
                        else
                            $("#fare").val(0);
                    }
                }
            }

            $(function() {
                $('.Date').appendDtpicker({
                    //"inline": true,
                    "minuteInterval": 15,
                    "current": "<?php echo $fromYear . '-' . $tmonth . '-' . $tday ?> 00:15"
                });
            });

            function GetDistanceFare() {
                //alert("In function");
                if($("#type").val() != "shared"){
                var source = $("#fromLocation option:selected").text();
                var destination = $("#toLocation option:selected").text();

                if ($("#isRoundTrip").prop('checked') == false) {
                    if (source == destination) {
                        //alert("Choose destination");
                        return false;
                    }
                }
                else {
                    if (source == destination) {
                        $("#fare").val("");
                        $("#distance").val("");
                        return true;
                    }
                    else if (source != destination) {
                        alert("For return journey Both the location should be same.\n OR \n Uncheck the return journey checkbox");
                        return false;
                    }
                }

                service = new google.maps.DistanceMatrixService();
                service.getDistanceMatrix(
                        {
                            origins: [source],
                            destinations: [destination],
                            travelMode: google.maps.TravelMode.DRIVING,
                            avoidHighways: false,
                            avoidTolls: false
                        },
                callback
                        );
                }
                else{
                    //alert("Called");
                    var source = $("#fromLocation option:selected").text();
                    var destination = $("#toLocation option:selected").text();
                    if(source == destination){
                        var oneDay = 24*60*60*1000;	// hours*minutes*seconds*milliseconds
                        
                        var fromDate = $("#fromDateTime").val();
                        var toDate = $("#toDateTime").val();
                        
                        if(fromDate && toDate){
                            var fromDateSourceSplit = fromDate.split(" ");
                            var fromDateSplit = fromDateSourceSplit[0].split("-");
                            
                            var toDateSourceSplit = toDate.split(" ");
                            var toDateSplit = toDateSourceSplit[0].split("-");
                            
                            var firstDate = new Date(fromDateSplit[0], fromDateSplit[1], fromDateSplit[2]);
                            var secondDate = new Date(toDateSplit[0], toDateSplit[1], toDateSplit[2]);
                            var diffDays = Math.abs((firstDate.getTime() - secondDate.getTime())/(oneDay));
                            $("#fare").val(diffDays);
                        }
                    }
                }
            }

            function callback(response, status) {
                if (status == "OK") {
                    var fare = parseInt($("#acRate").val()) * parseInt(response.rows[0].elements[0].distance.text);
                    $("#distance").val(fare);
                    $("#fare").val(fare);
                } else {
                    alert("Error: " + status);
                }
            }
            
            $(function(){
                var source = $("#source").val();
                var destination = $("#destination").val();
                
                if(source){
                    $("#fromLocation").val(source);
                }
                if(destination){
                    $("#toLocation").val(destination);
                }
            });
            
            function FillAmount(){
                var fare = $("#fare").val();
                if(fare)
                    $("#amountReceived").val(fare);
                else{
                    $("#amountReceived").val();
                    alert("Kindly fill fare amount");
                    return false;
                }
            }
            
            function GetSubLocation() {
                var toLocationId = $("#destination").val();
                $.ajax({
                    type: "GET",
                    url: "../ajax/location.php?type=location&required=sublocation&location=" + toLocationId,
                    //data: "std=" + std,
                    success: function(data) {
                        var parseData = JSON.parse(data);
                        $("#subLocation").find('option').remove().end();
                        var opt = '<option>Select</option>';
                        for(i=0; i<parseData.length; i++){
                            opt += '<option value="'+parseData[i]['Id']+'-'+parseData[i]['Fare']+'">'+parseData[i]['Name']+'</option>';
                        }
                        $("#subLocation").append(opt);
                    }
                });
            }
            
            // Identify client by email/phone and find clientId, creditAmount
            function IdentifyClient(){
                var email = $("#email").val();
                var phone = $("#phone").val();
                
            }
</script>

<?php

// POST function
function SharedBooking() {
    $cabRouteId = $_POST['cabRouteId'];
    $source = $_POST['source'];
    $destination = $_POST['destination'];
    $subLocationExtraFare = 0;
    if(isset($_POST['subLocation']) && !empty($_POST['subLocation'])){
        $subLocation = $_POST['subLocation'];
        $subLocationSplit = explode('-', $subLocation);
        if(is_array($subLocationSplit) && isset($subLocationSplit[1]) && empty($subLocationSplit[1]))
            $subLocationExtraFare = $subLocationSplit[1];
    }
    $travelDateTime = $_POST['travelDateTime'];
    $name = $_POST['name'];
    $email = $_POST['email'];
    $phone = $_POST['phone'];
    $fare = $_POST['fare'];
    $amountReceived = $_POST['amountReceived'];
    $pickupLocation = $_POST['pickupLocation'];
    $seatBookedList = $_POST['seat'];
    $seatBooked = "";
    $ticketBookedCount = 0;
    $isMaharaja = 0;
    $paymentStatus = 0;
    if(isset($_POST['isPaid']) && !empty($_POST['isPaid']))
        $paymentStatus = 1;

    foreach ($seatBookedList as $sbk => $sbd) {
        if (!empty($sbd)) {
            $row = chr($sbk + 65) . "x" . $sbd;
            if ($sbk == 0)
                $isMaharaja = 1;
            if (empty($seatBooked))
                $seatBooked .= $row;
            else
                $seatBooked .= "," . $row;
            $ticketBookedCount+=$sbd;
        }
    }

    $clientId = GetUserByPhoneEmail($email, $phone, $name);
    //$ClientId, $IsPaid,$SLocationId,$DLocationId,$IsShare,$fare, $AmountReceived,$Date,$IsMaharaja,$BidRate,$IsBidAwarded,$CabRouteId,$PickupLocation,$SeatsBooked,$TicketCount,$IsBookedByClient,$Status
    $bookId = InsertBooking($clientId, $paymentStatus, $source, $destination, 1, $fare, $amountReceived, $travelDateTime, $travelDateTime, $isMaharaja, 0, 0, $cabRouteId, $pickupLocation, $seatBooked, $ticketBookedCount, 0, 1);
    if($bookId)
        header ("location: index.php");
}

function CharteredBooking() {
    $cabRouteId = $_POST['cabRouteId'];
    $source = $_POST['fromLocation'];
    $destination = $_POST['toLocation'];
    $travelDateTime = $_POST['fromDateTime'];
    $toDateTime = $_POST['toDateTime'];
    $name = $_POST['name'];
    $email = $_POST['email'];
    $phone = $_POST['phone'];
    $fare = $_POST['fare'];
    $amountReceived = $_POST['amountReceived'];
    $pickupLocation = $_POST['pickupLocation'];
    $paymentStatus = 0;
    if(isset($_POST['isPaid']) && !empty($_POST['isPaid']))
        $paymentStatus = 1;

    $clientId = GetUserByPhoneEmail($email, $phone, $name);
    //`ClientId`, `IsPaid`,`SLocationId`,`DLocationId`,`IsShare`, `Fare`, `AmountReceived`,`Date`,`IsMaharaja`,`BidRate`,`IsBidAwarded`,`CabRouteId`,`PickupLocation`,`SeatsBooked`,`TicketCount`,`IsBookedByClient`,`Status`
    $bookId = InsertBooking($clientId, $paymentStatus, $source, $destination, 0, $fare, $amountReceived, $travelDateTime, $toDateTime, 0, 0, 0, $cabRouteId, $pickupLocation, 1, 1, 0, 1 );
    // code to modify cabroute todatetime, destination with booking todate time for shared cab.
    // This will be handeled by corn job.  
    //$isAffected = AddCharterCabToRoute($cabRouteId, $toDateTime, $destination);
    
    if($bookId)
        header ("location: index.php");
}

function VirtualBooking() {
    
    $name = $_POST['name'];
    $email = $_POST['email'];
    $phone = $_POST['phone'];
    $fromDate = $_POST['fromDateTime'];
    $toDate = $_POST['toDateTime'];
    $source = $_POST['fromLocation'];
    $destination = $_POST['toLocation'];
    $pickupLocation = $_POST['pickupLocation'];
    $fare = $_POST['fare'];
    $amountReceived = $_POST['amountReceived'];
    $carModelId = $_POST['carModelId'];
    $paymentStatus = 0;
    if(isset($_POST['isPaid']) && !empty($_POST['isPaid']))
        $paymentStatus = 1;
    $isRoundTrip = 0;
    // commted below code: Virtual cab booking will always reflects 
    /*if(isset($_POST['isRoundTrip']) && !empty($_POST['isRoundTrip']))
        $isRoundTrip = 1;*/

    $clientId = GetUserByPhoneEmail($email, $phone, $name);
    // Insert into cabroute table
    $cabRouteId = AddVirtualCabRoute($fromDate, $source, $destination, $toDate, $isRoundTrip, $carModelId, 1);
    
    // Insert into booking table
    //$ClientId, $IsPaid,$SLocationId,$DLocationId,$IsShare,$fare, $AmountReceived,$Date,$IsMaharaja,$BidRate,$IsBidAwarded,$CabRouteId,$PickupLocation,$SeatsBooked,$TicketCount,$IsBookedByClient,$Status
    $bookId = InsertBooking($clientId, $paymentStatus, $source, $destination, 0, $fare, $amountReceived, $fromDate, $toDate, 0, 0, 0, $cabRouteId, $pickupLocation, 1, 1, 0, 1);
    if($bookId)
        header ("location: index.php");
}
?>
