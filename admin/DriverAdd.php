<?php
if (!isset($_SESSION))
    session_start();
$_SESSION['cat'] = "inventory";

function Path() {
    $path = $_SERVER['DOCUMENT_ROOT'];
    // Differenciate the path for local and web host. Includes database connect file.
    if ($_SERVER['HTTP_HOST'] == "localhost" || $_SERVER['HTTP_HOST'] == "localhost:8080")
        $path .= '/Cabsystem';
    return $path;
}

include_once Path() . '/templates/adminheader.php';
include_once Path() . '/dbop/data/driver.php';
include_once Path() . '/dbop/data/agent.php';

if (isset($_POST['sub'])) {
    $adAgentId = $_POST['agentId'];
    $adName = $_POST['Name'];
    $adPhones = $_POST['Phones'];
    $adNotes = $_POST['Notes'];
    $image = $_FILES['file'];
    $adStatus = 1;
    
    if (($_FILES["file"]["type"] == "image/gif") || ($_FILES["file"]["type"] == "image/jpeg") || ($_FILES["file"]["type"] == "image/png" )) {
        $path = Path().'/upload/images/';
        $guid = uniqid();
        move_uploaded_file($_FILES["file"]["tmp_name"], $path . $guid .'-'. $_FILES['file']["name"]);
        $imgnam = $guid .'-'. $image['name'];
        $imgtype = $image['type'];
        AddDriver($adAgentId, $adName, $adPhones, $adNotes, $imgnam, $adStatus);
        echo "<div class='alert alert-success'><a class='close' data-dismiss='alert'>x</a><strong>Driver Added!</strong></div>";
    } else {
        AddDriver($adAgentId, $adName, $adPhones, $adNotes, '', $adStatus);
        echo "<div class='alert alert-success'><a class='close' data-dismiss='alert'>x</a><strong>Driver Added!</strong></div>";
    }
}
?>

<h1 class="text-center">Add Driver</h1>
<form action="DriverAdd.php" method="post" enctype="multipart/form-data">
    <table align="center" class="table table-striped">
        <tr>
            <td>Select Agent</td>
            <td>
                <select name="agentId">
                    <?php
                    $select = GetAllAgents(0, 500);
                    while ($agentInfo = mysql_fetch_array($select)) {
                        ?>
                        <option value="<?php echo $agentInfo['Id']; ?>"><?php echo $agentInfo['Name'];?>-<?php echo $agentInfo['Phones']; ?></option>
                        <?php
                    }
                    ?>
                </select>
            </td>
        </tr>
        <tr>
            <td>Driver Name</td>
            <td><input type="text" name="Name" /></td>
        </tr>
        <tr>
            <td>Driver Contact No</td>
            <td><input type="text" name="Phones" /></td>
        </tr>
        <tr>
            <td>Enter Notes</td>
            <td><textarea name="Notes"></textarea></td>
        </tr>
        <tr>
            <td>Image/Document</td>
            <td><input type="file" name="file" class="btn" /></td>
        </tr>
        <tr>
            <td colspan="2" style="text-align: center;"><input type="submit" name="sub" value="Add" class="btn-success btn"/></td>
        </tr>
    </table>
</form>

<?php
include_once Path() . '/templates/adminfooter.php';
?>
