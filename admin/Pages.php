<?php
if (!isset($_SESSION))
    session_start();
$_SESSION['cat'] = "inventory";

function Path() {
    $path = $_SERVER['DOCUMENT_ROOT'];
    // Differenciate the path for local and web host. Includes database connect file.
    if ($_SERVER['HTTP_HOST'] == "localhost" || $_SERVER['HTTP_HOST'] == "localhost:8080")
        $path .= '/Cabsystem';
    return $path;
}

include_once Path() . '/templates/adminheader.php';
include_once Path() . '/dbop/data/pages.php';

$GetAllPages = GetAlPageContain();
?>

<h1 class="text-center">Agent Listing</h1>
<table class="table table-striped table-bordered">
    <tr>
        <th>Action</th>
        <th>AboutUs</th>
        <th>ContactUs</th>
        <th>PrivacyPolicy</th>
        <th>Disclaimer</th>
        <th>FeedBack</th>
        <th>NewsFlash</th>
        <th>Popup</th>
    </tr>
    <?php
    while ($FetchAllPages = mysql_fetch_array($GetAllPages)) {
        
        ?>
        <tr>
            <td>
                <a href="PagesEdit.php?Id=<?php echo $FetchAllPages['Id']; ?>">Edit</a> &nbsp; | &nbsp;
            </td>
            <td><?php echo $FetchAllPages['AboutUs']; ?></td>
            <td><?php echo $FetchAllPages['ContactUs']; ?></td>
            <td><?php echo $FetchAllPages['PrivacyPolicy']; ?></td>
            <td><?php echo $FetchAllPages['Disclaimer']; ?></td>
            <td><?php echo $FetchAllPages['Feedback']; ?></td>
            <td><?php echo $FetchAllPages['NewsFlash']; ?></td>
            <td><?php echo $FetchAllPages['Popup']; ?></td>
        </tr>
        <?php
    }
    ?>
</table>
    <?php
        include_once Path() . '/templates/adminfooter.php';
     ?>   