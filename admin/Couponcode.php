<?php
if (!isset($_SESSION))
    session_start();
$_SESSION['cat']="";

function Path() {
    $path = $_SERVER['DOCUMENT_ROOT'];
    // Differenciate the path for local and web host. Includes database connect file.
    if ($_SERVER['HTTP_HOST'] == "localhost" || $_SERVER['HTTP_HOST'] == "localhost:8080")
        $path .= '/Cabsystem';
    return $path;
}

include_once Path().'/templates/adminheader.php';
include_once Path().'/dbop/data/Couponcode.php' ;

?>

<?php

        $page  = 1;
        $start= 0;
	$rpp = 2;
	

	$no = CouponcodePagination();
        $totalpage = ceil($no / $rpp);
        
if(isset($_GET['action'])){
            if($_GET['action']== "delete"){
			
			$Id = $_GET['Id'];
			DeleteCouponcode($Id);
                        
			echo "<script> alert('Your Record Delete SuccessFully');</script>";
	
                        }
	}         
            
       
        if(isset($_GET['page']))
		{
                    $page = $_REQUEST['page'];
                    $a = $page * $rpp;
                    $start = $a - $rpp;
		}
        $GetAllCouponcode = GetAllCouponcode( $start, $rpp);
        
?>

        <script language="javascript">
		function del()
			{
                                var x;
				var r=confirm("Really Want to Delete A Coupon Code");
				if(!r)
					return false;
			}		
	</script>

        <form action="Couponcode.php" method="post">
            <h1 align="center">Coupon Codes Listing</h1>
            <table align='center' class="table table-striped">
                    <tr>
                        
                        <th>Name</th>
                        <th>Valid From</th>
                        <th>Valid To</th>
                        <th>Limit Count</th>
                        <th>Discount Amount</th>
                        <th>Discount Percent</th>
                        <th>Status</th>
                        <th>Edit</th>
                        <th>Delete</th>
                        
                        
                    </tr>
                    <?php
                     
                    while ($FetchAllCouponcode = mysql_fetch_array($GetAllCouponcode)) {
                        ?>
                        <tr>
                            
                            <td><?php echo $FetchAllCouponcode['Name']; ?></td>
                            <td><?php echo $FetchAllCouponcode['ValidFrom']; ?></td>
                            <td><?php echo $FetchAllCouponcode['ValidTo']; ?></td>
                            <td><?php echo $FetchAllCouponcode['LimitCount']; ?></td>
                            <td><?php echo $FetchAllCouponcode['DiscountAmount']; ?></td>
                            <td><?php echo $FetchAllCouponcode['DiscountPercent']; ?></td>
                            <td><?php echo $FetchAllCouponcode['Status']; ?></td>
                        
                            
                            <td><a href="CouponcodeEdit.php?Id=<?php echo $FetchAllCouponcode['Id'];?>">Edit</a></td>
                            <td><a href="Couponcode.php?action=delete&Id=<?php echo $FetchAllCouponcode['Id'];?>" onClick="return del();">Delete</a>
                        </tr>
                        <?php
                    }

                    ?>
                    <table align='center'>
                        <tr>
                            <?php
                            
                            for ($i = 1; $i <= $totalpage; $i++) {
                                if($i==$page)
                                    echo '<td>'.$i.'</td>';
                                else
                                    echo '<td><a href="Couponcode.php?page='.$i.'">'.$i.'</a></td>';
                            }

                            ?>
                        </tr>
                    </table>
            </table>
        </form>
        
        <?php
        include_once Path() . '/templates/adminfooter.php';
        ?>