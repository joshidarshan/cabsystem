<?php

function Path() {
    $path = $_SERVER['DOCUMENT_ROOT'];
    // Differenciate the path for local and web host. Includes database connect file.
    if ($_SERVER['HTTP_HOST'] == "localhost" || $_SERVER['HTTP_HOST'] == "localhost:8080")
        $path .= '/Cabsystem';
    return $path;
}

include_once Path() ."/templates/adminheader.php";
include_once Path() ."/dbop/data/promotion.php";

   
$Page = 1;
if(isset($_GET['Page']) && !empty($_GET['Page']))
    $Page = $_GET['Page'];
    

    $rpp = 2;
    $start = ($Page - 1 * $rpp);
    $NumberOfPage = GetAllPromotions($start, $rpp);
     $totalpage = ceil($NumberOfPage / $rpp);
     
     
  if(isset($_GET) && !empty($_GET)){
       $Id = $_GET['Id'];
        GetPromotion($Id);
      
  }
?>
<script language="javascript">
    function del()
    {
        var x;
        var r = confirm("Really Want to Delete A Promotion");
        if (!r)
            return false;
    }
</script>
        <form>
            <h1 align="center">Promotion Listing</h1>
            <table class="table table-striped">
                
                <th>Action</th>
                <th>Name</th>
                <th>Description</th>
                <th>Status</th>
               
        <?php
        $PromotionData = GetAllPromotionsData();
        while($FetchAllPromotionData = mysql_fetch_assoc($PromotionData)){
        ?>
                 <tr>
                <td>
                    <a href="PromotionEdit.php?Id=<?php echo $FetchAllPromotionData['Id']; ?>">Edit</a> &nbsp; | &nbsp;
                    <a href="Promotion.php?action=delete&Id=<?php echo $FetchAllPromotionData['Id']; ?>" onClick="return del();">Delete</a>
                </td>
                <td>
                    <?php echo $FetchAllPromotionData['Name'];?>
                </td>
                <td>
                    <?php echo $FetchAllPromotionData['Description'];?>
                </td>
                <td>
                    <?php echo $FetchAllPromotionData['Status'];?>
                </td>
            </tr>
        <?php
        }
        ?>
            </table>
        </form>
        <div class="pagination pagination-centered">
         <ul>
               <?php
               for ($i = 1; $i <= $totalpage; $i++) {
                  if ($i == $Page)
                     echo '<li class = "active"><a href="Promotion.php?Page=' . $i . '">' . $i . '</a></li>';
                 else
                        echo '<li><a href="Promotion.php?Page=' . $i . '">' . $i . '</a></li>';
              }
               ?>
         </ul>
        </div>

  <?php
             include_once Path() . '/templates/adminfooter.php';
           ?> 