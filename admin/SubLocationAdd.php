<?php
if (!isset($_SESSION))
    session_start();
$_SESSION['cat'] = "sublocation";

function Path() {
    $path = $_SERVER['DOCUMENT_ROOT'];
    // Differenciate the path for local and web host. Includes database connect file.
    if ($_SERVER['HTTP_HOST'] == "localhost" || $_SERVER['HTTP_HOST'] == "localhost:8080")
        $path .= '/Cabsystem';
    return $path;
}
include_once Path() . '/dbop/data/Location.php';
include_once Path() . '/dbop/data/sublocation.php';
include_once Path() . '/templates/adminheader.php';

if (isset($_POST['sub'])) {
    
$LocationId = $_POST['LocationId'];
$SubLocation = $_POST['SubLocation'];
$Fare = $_POST['Fare'];
$Status = 1;
InsertSubLocation($LocationId, $SubLocation,$Fare,$Status);
    echo "<div class='alert alert-success'><a class='close' data-dismiss='alert'>x</a><strong>Sub Location Added!</strong></div>";
}
?>

<form action="SubLocationAdd.php" method="post" id ="LocationAdd" onsubmit=" return SubmitCheck();">
            <h1 align="center" >Add New Sub Location</h1>
            <table align="center" class="table table-striped" >
                <tr>
                    <td>Select Main Location</td>
                    <td><select name="LocationId">
                            <?php $locations = GetAllLocations();
                                    while($fetchlocation = mysql_fetch_array($locations)){
                                ?>
                            <option value="<?php echo $fetchlocation['Id'];?>"><?php echo $fetchlocation['Name'];?></option>
                                        <?php
                                    }
?>
                        </select></td>
                </tr>
                <tr>
                    <td>Enter Sub Location Name</td>
                    <td><input type="text" name="SubLocation"/></td>
                </tr>
                
                <tr>
                    <td>Enter Fare Name</td>
                    <td><input type="text" name="Fare"/></td>
                </tr>
                <tr>
                    <td colspan="2" style="text-align: center;"><input type="submit" name="sub" value="Add" class="btn-large"/></td>
                </tr>
            </table>
        </form>
  <?php
             include_once Path() . '/templates/adminfooter.php';
           ?> 