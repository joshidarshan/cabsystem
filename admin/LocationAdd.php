<?php
if (!isset($_SESSION))
    session_start();
$_SESSION['cat'] = "";

function Path() {
    $path = $_SERVER['DOCUMENT_ROOT'];
    // Differenciate the path for local and web host. Includes database connect file.
    if ($_SERVER['HTTP_HOST'] == "localhost" || $_SERVER['HTTP_HOST'] == "localhost:8080")
        $path .= '/Cabsystem';
    return $path;
}

include_once Path() . '/templates/adminheader.php';

if (isset($_POST['sub'])) {
    include_once Path() . '/dbop/data/Location.php';
    $LocationName = $_POST['Source'];
    //$strval = explod(",", $strvar);
    //$strval[0];
    //echo $LocationName;
    $location = explode(",", $LocationName);
    //print_r($location);
    $location = $location[0];
    //echo $location;
      //$location = substr($LocationName, (stripos($LocationName, ',') + 1), strlen($LocationName));
    
    $LocationStatus = 1;
   // InsertLocation($location, $LocationStatus);
    echo "<div class='alert alert-success'><a class='close' data-dismiss='alert'>x</a><strong>Location Added!</strong></div>";
}
?>
<script>
function CheckLocation(){
    //alert('Ajax1 Call');
    var Location = $("#Location").val();
    $.ajax({
        type: "GET",
        url:"../ajax/user.php/?type=LocationType&required=LocationRequired&Location=" +Location,
        success: function(data){
            message = "";
            if(data==0){
                message = "Already Exist";
                $("#ChkLocation").css({"color":"Red"});
            }
            else{
                message = "Available";
                $("#ChkLocation").css({"color":"Green"});
            }
            $("#ChkLocation").text(message);
        }
    });
}
function SubmitCheck(){
    var get = $("#ChkLocation").text();

    if(get.toLowerCase() == "already exist")
        return false;
    else
        return true;
    }
</script>
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
<script type="text/javascript">

function vnull(){
document.getElementById("orig").value = '';
document.getElementById("dest").value = '';
}

function Check() {
  //alert("In API Call");
var or = document.getElementById("Location").value+", Gujarat";
var ds = "Rajkot, Gujarat, India";

/*document.getElementById("orig").value = "";
document.getElementById("dest").value = "";*/

var origin = or,//new google.maps.LatLng(55.930385, -3.118425),
    destination = ds,//"Stockholm, Sweden",
    service = new google.maps.DistanceMatrixService();

service.getDistanceMatrix(
    {
        origins: [origin],
        destinations: [destination],
        travelMode: google.maps.TravelMode.DRIVING,
        avoidHighways: false,
        avoidTolls: false
    }, 
    callback
);

function callback(response, status) {
    var orig = document.getElementById("Location");
        //dest = document.getElementById("dest"),
        //dist = document.getElementById("dist");

    if(status=="OK") {
        //dest.value = response.destinationAddresses[0];
        split_city =  (response.originAddresses[0]).split(",");
        orig.value = split_city[0];
        
        //alert(split_city[0]);
        CheckLocation();
       // dist.value = response.rows[0].elements[0].distance.text;
    } else {
        alert("Error: " + status);
    }
}
return true;
}

function dummy(){
    check();
    CheckLocation();
}
</script>

<form action="LocationAdd.php" method="post" id ="LocationAdd" onsubmit=" return SubmitCheck();">
            <h1 align="center" >Add New Location</h1>
            <table align="center" class="table table-striped" >
                <tr>
                    <td>Enter Location Name</td>
                    <td><input type="text" name="Source" id="Location" onblur="Check(); "/>
                        <span id="ChkLocation" style="position: absolute; font-size: 24px"></span></td>
                </tr>
                <tr>
                    <td colspan="2" style="text-align: center;"><input type="submit" name="sub" value="Add" class="btn-large"/></td>
                </tr>
            </table>
        </form>
  <?php
             include_once Path() . '/templates/adminfooter.php';
           ?> 