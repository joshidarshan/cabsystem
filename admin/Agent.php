<?php
if (!isset($_SESSION))
    session_start();
$_SESSION['cat'] = "inventory";

function Path() {
    $path = $_SERVER['DOCUMENT_ROOT'];
    // Differenciate the path for local and web host. Includes database connect file.
    if ($_SERVER['HTTP_HOST'] == "localhost" || $_SERVER['HTTP_HOST'] == "localhost:8080")
        $path .= '/Cabsystem';
    return $path;
}

include_once Path() . '/templates/adminheader.php';
include_once Path() . '/dbop/data/agent.php';
$page = 1;
$start = 0;
$rpp = 20;

if (isset($_GET['action'])) {
    if ($_GET['action'] == "delete") {
        $id = $_GET['Id'];
        DeleteAgent($id);
        echo "<script> alert('Your Record Delete SuccessFully');</script>";
    }
    }
$no = AgentPagination();
$totalpage = ceil($no / $rpp);
if (isset($_GET['page'])) {
    $page = $_REQUEST['page'];
    $a = $page * $rpp;
    $start = $a - $rpp;
}
$select = GetAllAgents($start, $rpp);

?>
<script language="javascript">
    function del() {
        var r = confirm("Really Want to Delete An Agent");
        if (!r)
            return false;
    }
</script>

<h1 class="text-center">Agent Listing</h1>
<table class="table table-striped table-bordered">
    <tr>
        <th>Action</th>
        <th>Name</th>
        <th>Address</th>
        <th>Phone</th>
        <th>Email</th>
        <th>Note</th>
        <th>Status</th>
        <th>Image</th>
    </tr>
    <?php
    while ($row = mysql_fetch_array($select)) {
        $img = $row['ImageLink'];
        if ($img == "NoImage")
            $image = "No";
        else
            $image = "Yes";
        ?>
        <tr>
            <td>
                <a href="AgentEdit.php?Id=<?php echo $row['Id']; ?>">Edit</a> &nbsp; | &nbsp;
                <a href="Agent.php?action=delete&Id=<?php echo $row['Id']; ?>" onClick="return del();">Delete</a>
            </td>
            <td><?php echo $row['Name']; ?></td>
            <td><?php echo $row['Address']; ?></td>
            <td><?php echo $row['Phones']; ?></td>
            <td><?php echo $row['Email']; ?></td>
            <td><?php echo $row['Notes']; ?></td>
            <td><?php echo $row['Status']; ?></td>
            <td><?php echo $image; ?></td>
        </tr>
        <?php
    }
    ?>
</table>
<div class="pagination pagination-centered">
    <ul>
        <?php
        for ($i = 1; $i <= $totalpage; $i++) {
            if ($i == $page)
                echo '<li class = "active"><a href="agent.php?page=' . $i . '">' . $i . '</a></li>';
            else
                echo '<li><a href="Agent.php?page=' . $i . '">' . $i . '</a></li>';
        }
        ?>
    </ul>
</div>
    <?php
        include_once Path() . '/templates/adminfooter.php';
     ?>   