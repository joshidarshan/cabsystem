<?php
if (!isset($_SESSION))
    session_start();
$_SESSION['cat'] = "";

function Path() {
    $path = $_SERVER['DOCUMENT_ROOT'];
    // Differenciate the path for local and web host. Includes database connect file.
    if ($_SERVER['HTTP_HOST'] == "localhost" || $_SERVER['HTTP_HOST'] == "localhost:8080")
        $path .= '/Cabsystem';
    return $path;
}

include_once Path() . '/templates/adminheader.php';
include_once Path() . '/dbop/data/role.php';
include_once Path() . '/dbop/data/user.php';

if (isset($_POST['sub'])) {
    $Id = $_POST['Id'];
    $Email = $_POST['Email'];
    $Phones = $_POST['Phones'];
    $RoleId = $_POST['RoleId'];
    $Status = "";
    if (isset($_POST['Status']) && !empty($_POST['Status']))
        $Status = $_POST['Status'];
    else
        $Status = 0;

    EditUser($Id, $Email, $Phones, $RoleId, $Status);
    echo "<script> alert ('Record Updated SuccessFully'); window.location='User.php'; </script>";
}
$Id = $_REQUEST['Id'];
$GetUserById = GetUserById($Id);
$FetchUserById = mysql_fetch_array($GetUserById);
$GetAllRoles = GetAllUsersRoles($start = 0, $rpp = 500);
?>

<form action="UserEdit.php" method="post">
    <h1 align="center">Update User</h1>
    <table align="center" class="table table-striped">
        <tr>
            <td>User Id</td>
            <td><input type="hidden" name="Id" required value="<?php echo $Id; ?>" /></td>
        </tr>
        <tr>
            <td>Enter Email</td>
            <td><input type="text" name="Email" required value="<?php echo $FetchUserById['Email']; ?>" /></td>
        </tr>
        <tr>
            <td>Enter Contact No</td>
            <td><input type="text" name="Phones" required value="<?php echo $FetchUserById['Phones']; ?>" /></td>
        </tr>
        <tr>
            <td>Chose A Role</td>
            <td>
                <select name="RoleId">
                    <<?php
                    while ($FetchAllRoles = mysql_fetch_array($GetAllRoles)) {
                        if ($FetchUserById['RoleId'] == $FetchAllRoles['Id'])
                            echo "<option value=$FetchAllRoles[Id] selected='selected'>$FetchAllRoles[Role]</option>";
                        else
                            echo "<option value=$FetchAllRoles[Id]>$FetchAllRoles[Role]</option>";
                    }
                    ?>
                </select>
            </td>
        </tr>
        <tr>
            <td>Current Status</td>
            <?php
            if ($FetchUserById['Status'] == 1) {
                ?>
                <td>Click for Active<input type="checkbox" name="Status" value="1"  checked="checked"/></td>
                <?php
            }
            ?>
            <?PHP if ($FetchUserById['Status'] == 0) { ?>
                <td>Click for Active<input type="checkbox" name="Status" value="1"/></td>
            <?php } ?>
        </tr>
        <tr>
            <td colspan="2" style="text-align: center;"><input type="submit" name="sub" value="Update" class="btn-large"/></td>
        </tr>
    </table>
</form>

<?php include_once Path() . "/templates/adminfooter.php"; ?>

