<?php
if (!isset($_SESSION))
    session_start();
$_SESSION['cat'] = "";

function Path() {
    $path = $_SERVER['DOCUMENT_ROOT'];
    // Differenciate the path for local and web host. Includes database connect file.
    if ($_SERVER['HTTP_HOST'] == "localhost" || $_SERVER['HTTP_HOST'] == "localhost:8080")
        $path .= '/Cabsystem';
    return $path;
}

include_once Path() . '/templates/adminheader.php';
include_once Path() . '/dbop/data/Location.php';

if (isset($_POST['sub'])) {
    $LocationId = $_POST['Id'];
    $LocationName = $_POST['Name'];
    if (isset($_POST['Status']) && !empty($_POST['Status']))
        $LocationStatus = $_POST['Status'];
    else
        $LocationStatus = 0;
    UpdateLocation($LocationId, $LocationName, $LocationStatus);
    echo "<script> alert ('Your Update SuccessFully'); window.location='Location.php';</script>";
}
$Id = $_REQUEST['Id'];
$GetSingleLocation = GetSingleLocation($Id);
$FetchSingleLocation = mysql_fetch_array($GetSingleLocation);
?>
<script>
function CheckLocation(){
    var Location = $("#Location").val();
    $.ajax({
        type: "GET",
        url:"../ajax/user.php/?type=LocationType&required=LocationRequired&Location=" +Location,
        success: function(data){
            message = "";
            if(data==0){
                message = "Already Exist";
                $("#ChkLocation").css({"color":"Red"});
            }
            else{
                message = "Available";
                $("#ChkLocation").css({"color":"Green"});
            }
            $("#ChkLocation").text(message);
        }
    });
}
function SubmitCheck(){
    var get = $("#ChkLocation").text();

    if(get.toLowerCase() == "already Exist")
        return false;
    else
        return true;
    }
</script>

        <form action="LocationEdit.php" method="post">
            <table align="center" class="table table-striped">
                <tr>
                    <td>Location Id</td>
                    <td><input type="hidden" name="Id" value="<?php echo $Id; ?>"/></td>
                </tr>
                <tr>
                    <td>Enter Location Name</td>
                    <td><input type="text" name="Name" value="<?php echo $FetchSingleLocation['Name']; ?>" id="Location" onblur="return CheckLocation();"/>
                        &nbsp;&nbsp;&nbsp;<span id="ChkLocation" style="position: absolute; font-size: 24px"></span></td>
                </tr>
                <tr>
                    <td>Current Status</td>
                    <?php
                    if ($FetchSingleLocation['Status'] == 1) {
                        ?>
                        <td>Click for Active<input type="checkbox" name="Status" value="1"  checked="checked"/></td>
                        <?php
                    }
                    ?>
                    <?PHP if ($FetchSingleLocation['Status'] == 0) { ?>
                        <td>Click for Active<input type="checkbox" name="Status" value="1"/></td>
                    <?php } ?>
                <tr>
                    <td><input type="submit" name="sub" value="Update"/></td>
                </tr>

            </table>
        </form>


  <?php
             include_once Path() . '/templates/adminfooter.php';
           ?> 