<?php
if (!isset($_SESSION))
    session_start();
$_SESSION['cat'] = "Route";

function Path() {
    $path = $_SERVER['DOCUMENT_ROOT'];
    // Differenciate the path for local and web host. Includes database connect file.
    if ($_SERVER['HTTP_HOST'] == "localhost" || $_SERVER['HTTP_HOST'] == "localhost:8080")
        $path .= '/Cabsystem';
    return $path;
}       

include_once Path() . '/templates/adminheader.php';
include_once Path() . '/dbop/data/Location.php';
include_once Path() . '/dbop/data/BreakPoint.php';

if (isset($_POST['sub'])) {
    $Id = $_POST['Id'];
    $SlocationId = $_POST['SLocationId'];
    $DlocationId = $_POST['DLocationId'];
    $Name = $_POST['Name'];
    $Description = $_POST['Description'];
    $Maplocation = $_POST['MapLocation'];
    if (isset($_POST['Status']) && !empty($_POST['Status']))
        $Status = $_POST['Status'];
    else
        $Status = 0;

    UpdateBreakpoint($Id, $SlocationId, $DlocationId, $Name, $Description, $Maplocation, $Status);
    echo "<div class='alert alert-success'><a class='close' data-dismiss='alert'>x</a><strong>Breakpoint edited.</strong></div>";
}
$Id = $_REQUEST['Id'];
$GetBreakPointById = GetBreakpointById($Id);
$FetchBreakPointById = mysql_fetch_array($GetBreakPointById);
?>

<form action="BreakPointEdit.php" method="post">
  <!--  <input type="hidden" name="Id" id="Id" value="<?php echo $_GET['Id']; ?>" /-->
    <table align="center" class="table table-striped">
        <tr>
            <td>Break Point Id</td>
            <td><input type="hidden" name="Id" value="<?php echo $Id; ?>"/></td>
        </tr>
        <tr>
            <td>Select Source Location</td>
            <td>
                <select name="SLocationId">
                    <?php
                    $GetAllLocations = GetAllLocations($locationStart = 0, $locationRpp = 500);
                    while ($FetchAllLocations = mysql_fetch_array($GetAllLocations)) {
                        if ($FetchAllLocations['Id'] == $FetchBreakPointById['SLocationId'])
                            echo "<option value=$FetchAllLocations[Id] selected='selected'>$FetchAllLocations[Name]</option>";
                        else
                            echo "<option value=$FetchAllLocations[Id]>$FetchAllLocations[Name]</option>";
                    }
                    ?>
                </select>
            </td>
        </tr>
        <tr>
            <td>Select Destination Name</td>
            <td><select name="DLocationId">
                    <?php
                    $GetAllLocations = GetAllLocations($locationStart = 0, $locationRpp = 500);
                    while ($FetchAllLocations = mysql_fetch_array($GetAllLocations)) {

                        if ($FetchAllLocations['Id'] == $FetchBreakPointById['DLocationId'])
                            echo "<option value=$FetchAllLocations[Id] selected='selected'>$FetchAllLocations[Name]</option>";
                        else
                            echo "<option value=$FetchAllLocations[Id]>$FetchAllLocations[Name]</option>";
                    }
                    ?>

                    }
                    ?>
                </select>
            </td>
        </tr>
        <tr>
            <td>Enter Name</td>
            <td><input type="text" name="Name" value="<?php echo $FetchBreakPointById['Name']; ?>" required/></td>
        </tr>
        <tr>
            <td>Enter Description</td>
            <td><input type="text" name="Description" value="<?php echo $FetchBreakPointById['Description']; ?>"/></td>
        </tr>
        <tr>
            <td>Enter Map location</td>
            <td><input type="text" name="MapLocation" value="<?php echo $FetchBreakPointById['Maplocation']; ?>"/></td>
        </tr>

        <tr>
            <td>Current Status</td>
            <td>
                <?php if ($FetchBreakPointById['Status'] == 1) { ?>
                    <input type="checkbox" name="Status" value="1" checked="checked"/>
                <?php } else {
                    ?>

                    <input type="checkbox" name="Status" value="1"/> <?php } ?>
            </td>
        </tr>
        <tr>
            <td colspan="2" style="text-align: center;"><input type="submit" name="sub" value="Update" class="btn-success btn"/></td>
        </tr>
    </table>
</form>

<?php include_once Path() . "/templates/adminfooter.php"; ?>
