<?php
if (!isset($_SESSION))
    session_start();
$_SESSION['cat'] = "inventory";

function Path() {
    $path = $_SERVER['DOCUMENT_ROOT'];
    // Differenciate the path for local and web host. Includes database connect file.
    if ($_SERVER['HTTP_HOST'] == "localhost" || $_SERVER['HTTP_HOST'] == "localhost:8080")
        $path .= '/Cabsystem';
    return $path;
}

include_once Path() . '/templates/adminheader.php';
include_once Path() . '/dbop/data/slider.php';
$page = 1;
$start = 0;
$rpp = 2;


$no = ImagesPagination();
$totalpage = ceil($no / $rpp);
if (isset($_GET['page'])) {
    $page = $_REQUEST['page'];
    $a = $page * $rpp;
    $start = $a - $rpp;
}
$select = GetAllImages($start, $rpp);

if (isset($_GET['action'])) {
    if ($_GET['action'] == "delete") {
        echo $_GET['action'];
        $id = $_GET['Id'];
        DeleteImage($id);
        echo "<script> alert('Your Record Delete SuccessFully');</script>";
    }
}
?>
<script language="javascript">
    function del() {
        var r = confirm("Really Want to Delete An Image");
        if (!r)
            return false;
    }
</script>

<h1 class="text-center">Slider Images Listing</h1>
<table class="table table-striped table-bordered">
    <tr>
        <th>Action</th>
        <th>Name</th>
        <th>Image</th>
        <th>Description</th>
        <th>Status</th>
        
    </tr>
    <?php
    while ($FetchAllImages = mysql_fetch_array($select)) {
       $Image = substr($FetchAllImages['Image'], (stripos($FetchAllImages['Image'], '-') + 1), strlen($FetchAllImages['Image']));
        ?>
        <tr>
            <td style="min-width: 100px;">
                <a href="SliderImageEdit.php?Id=<?php echo $FetchAllImages['Id']; ?>">Edit</a> &nbsp; | &nbsp;
                <a href="SliderImages.php?action=delete&Id=<?php echo $FetchAllImages['Id']; ?>" onClick="return del();">Delete</a>
            </td>
            <td><?php echo $FetchAllImages['Name']; ?></td>
            <td><?php echo $Image; ?></td>
            <td><?php echo $FetchAllImages['Description']; ?></td>
            <td><?php echo $FetchAllImages['Status']; ?></td>
        </tr>
        <?php
    }
    ?>
</table>
<div class="pagination pagination-centered">
    <ul>
        <?php
        for ($i = 1; $i <= $totalpage; $i++) {
            if ($i == $page)
                echo '<li class = "active"><a href="SliderImages.php?page=' . $i . '">' . $i . '</a></li>';
            else
                echo '<li><a href="SliderImages.php?page=' . $i . '">' . $i . '</a></li>';
        }
        ?>
    </ul>
</div>
    <?php
        include_once Path() . '/templates/adminfooter.php';
     ?>   