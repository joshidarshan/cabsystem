<?php
if (!isset($_SESSION))
    session_start();
$_SESSION['cat'] = "car";

function Path() {
    $path = $_SERVER['DOCUMENT_ROOT'];
    // Differenciate the path for local and web host. Includes database connect file.
    if ($_SERVER['HTTP_HOST'] == "localhost" || $_SERVER['HTTP_HOST'] == "localhost:8080")
        $path .= '/Cabsystem';
    return $path;
}

include_once Path(). '/templates/adminheader.php';
include_once Path(). '/dbop/data/cartype.php';

$CarStart = 0;
$CarRpp = 2;
$no = CarTypesPagination();
$totalpage = ceil($no / $CarRpp);

if (isset($_GET['action'])) {
    if ($_GET['action'] == "delete") {
        $Id = $_GET['Id'];
        DeleteCarType($Id);
        echo "<script> alert('Your Record Delete SuccessFully');</script>";
    }
}
if (isset($_GET['page'])) {
    $page = $_REQUEST['page'];
    $a = $page * $CarRpp;
    $CarStart = $a - $CarRpp;
}
$GetCar = GetCarTypesListing($CarStart, $CarRpp);
?>

        <script language="javascript">
            function del()
            {
                var r = confirm("Really Want to Delete An Agent");
                if (!r)
                    return false;
            }
        </script>

        <form action="CarType.php" method="post">
            <h1 align="center">Car Listing</h1>
            <table align='center' class="table table-striped">
                <tr>
                    <th>Id</th>
                    <th>Name</th>
                    <th>Status</th>
                    <th>Edit</th>
                    <th>Delete</th>
                </tr>
                <?php
                while ($FetchCar = mysql_fetch_array($GetCar)) {
                    ?>
                    <tr>
                        <td><?php echo $FetchCar['Id']; ?></td>
                        <td><?php echo $FetchCar['Name']; ?></td>
                        <td><?php echo $FetchCar['Status']; ?></td>
                        <td><a href="CarTypeEdit.php?Id=<?php echo $FetchCar['Id']; ?>">Edit</a></td>
                        <td><a href="CarType.php?action=delete&Id=<?php echo $FetchCar['Id']; ?>" onClick="return del();">Delete</a>
                    </tr>
                    <?php
                }
                ?>
                <table align='center'>
                    <tr>
                        <?php
                        for ($i = 1; $i <= $totalpage; $i++) {
                            ?>
                            <td><a href="CarType.php?page=<?php echo $i; ?>"><?php echo $i; ?></a></td>
                            <?php
                        }
                        ?>
                    </tr>
                </table>
            </table>
        </form>
        
        <?php
        include_once Path() . '/templates/adminfooter.php';
        ?>