<?php

function Path() {
    $path = $_SERVER['DOCUMENT_ROOT'];
    // Differenciate the path for local and web host. Includes database connect file.
    if ($_SERVER['HTTP_HOST'] == "localhost" || $_SERVER['HTTP_HOST'] == "localhost:8080")
        $path .= '/Cabsystem';
    return $path;
}

if (isset($_GET['cabRouteId']) && !empty($_GET['cabRouteId'])) {
    include_once Path() . "/templates/adminheader.php";
    include_once Path() . "/dbop/data/listingutility.php";
    $cabPassenger = GetDriverSlip($_GET['cabRouteId']);
    $slip = "";
    while($cpr = mysql_fetch_assoc($cabPassenger)){
        
        if($cpr['cr_isreturntrip'] == 1){
            if(empty($slip)){
                $slip .= "<table class='table'><tr><td><strong>Cab</strong></td><td colspan='2'><strong>Route</strong></td><td><strong>Driver</strong></td>";
                $slip .= "<tr><td>$cpr[cb_NoPlate]</td><td colspan='2'>$cpr[lc_sourcename] To $cpr[lc_destinationname]</td><td>$cpr[dr_drivername] - $cpr[dr_phone]</td></tr>";
                $slip .= "<tr><td><span style='margin-left: 5%;'><strong>Client</strong></span></td><td><strong>Phone</strong></td><td><strong>Pickup</strong></td><td><strong>Seats</strong></td>";
            }
            $slip .= "<tr><td><span style='margin-left: 5%;'>$cpr[cl_name]</span></td><td>$cpr[cl_phone]</td><td>$cpr[bk_pickuplocation]</td><td>$cpr[bk_seatsbooked]</td></tr>";
        }
        else{
            if(empty($slip)){
                $slip .= "<table class='table'><tr><td><strong>Cab</strong></td><td><strong>Route</strong></td><td><strong>Driver</strong></td>";
                $slip .= "<tr><td>$cpr[cb_NoPlate]</td><td>$cpr[lc_sourcename] To $cpr[lc_destinationname]</td><td>$cpr[dr_drivername] - $cpr[dr_phone]</td></tr>";
                $slip .= "<tr><td><span style='margin-left: 5%;'><strong>Client</strong></span></td><td><strong>Phone</strong></td><td><strong>Pickup</strong></td>";
            }
            $slip .= "<tr><td><span style='margin-left: 5%;'>$cpr[cl_name]</span></td><td>$cpr[cl_phone]</td><td>$cpr[bk_pickuplocation]</td></tr>";
        }
    }
    $slip .= "</table>";
    echo $slip;
}
?>
