<?php

if (!isset($_SESSION))
    session_start();
$_SESSION['cat'] = "";

function Path() {
    $path = $_SERVER['DOCUMENT_ROOT'];
    // Differenciate the path for local and web host. Includes database connect file.
    if ($_SERVER['HTTP_HOST'] == "localhost" || $_SERVER['HTTP_HOST'] == "localhost:8080")
        $path .= '/Cabsystem';
        return $path;
}

include_once Path() . '/templates/adminheader.php';

if (isset($_POST['sub'])) {
    include_once Path() . '/dbop/data/sms.php';

    $From = $_POST['From'];
    $Message = $_POST['Message'];
    $Status = 1;
    SMSAdd($From, $Message, $Status);
    echo "<div class='alert alert-success'><a class='close' data-dismiss='alert'>x</a><strong>Sms Added!</strong></div>";
}
?>

<form action="SMSAdd.php" method="post" id="SMSAdd" " >
    <h1 align="center">Add SMS</h1>
    <table align="center" class="table table-striped">
        <tr>
            <td>From</td>
            <td><input type="text" name="From" required  /></td>
        </tr>
        <tr>
            <td>Enter Message</td>
            <td><textarea name="Message"></textarea></td>
        </tr>
        
        <tr>
            <td colspan="2" style="text-align: center;"><input type="submit" name="sub" value="Add" class="btn-inverse btn-large" onfocus="return CheckCarMakeName();" onclick=" return CheckCarMakeName();"/></td>
        </tr>
    </table>
</form>

<?php include_once Path() . "/templates/adminfooter.php"; ?>
