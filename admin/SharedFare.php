<?php
if (!isset($_SESSION))
    session_start();
$_SESSION['cat'] = "route";

function Path() {
    $path = $_SERVER['DOCUMENT_ROOT'];
    // Differenciate the path for local and web host. Includes database connect file.
    if ($_SERVER['HTTP_HOST'] == "localhost" || $_SERVER['HTTP_HOST'] == "localhost:8080")
        $path .= '/Cabsystem';
    return $path;
}

include_once Path() .'/templates/adminheader.php';
include_once Path() .'/dbop/data/location.php';
include_once Path() .'/dbop/data/sharedfare.php';

    
    $Page = 1;
    $rpp = 2;
    $start = 0;
    $NumberOfPage = SharedFaredPagination();
    $TotalOfPage = ceil($NumberOfPage/$rpp);

    if (isset($_GET['action'])) {
    if ($_GET['action'] == "delete") {

        $Id = $_GET['Id'];
       DeleteSharedFare($Id);
        echo "<script> alert('Your Record Delete SuccessFully');</script>";
    }
}
    
    
    if(isset($_GET['Page']) && !empty($_GET['Page'])){
    $Page = $_GET['Page'];

    $a =  $Page * $rpp;
    $start = $a - $rpp;
}
    $GetAllSharedFAre = GetAllSharedFares($start, $rpp);
?>
<script language="javascript">
    function del() {
        var r = confirm("Really Want to Delete A Shared Fare");
        if (!r)
            return false;
    }
</script>



<form action="SharedFare.php" method="POST" name="SharedFare">
    <h1 align="center">SharedFare Listing</h1>
    <table align='center' class="table table-striped">
        <tr>
            <th>Action</th>
            <th>Source-Location</th>
            <th>Destination-Location</th>
            <th>Rate</th>
            <th>Distance Km</th>
            <th>Travel TIme</th>
            <th>Status</th>
        </tr>

         <?php
        while ($FetchSharedFare = mysql_fetch_array($GetAllSharedFAre)) {
            ?>
            <tr>
                <td>
                    <a href="SharedFareEdit.php?Id=<?php echo $FetchSharedFare['Id']; ?>">Edit</a> &nbsp; | &nbsp;
                    <a href="SharedFare.php?action=delete&Id=<?php echo $FetchSharedFare['Id']; ?>" onClick="return del();">Delete</a>
                </td>
                
                <td> <?php echo $FetchSharedFare['SLocationId']; ?> </td>
                <td> <?php echo $FetchSharedFare['DLocationId']; ?> </td>
                <td> <?php echo $FetchSharedFare['Rate']; ?> </td>
                <td> <?php echo $FetchSharedFare['DistanceKm']; ?> </td>
                <td> <?php echo $FetchSharedFare['TravelTime']; ?> </td>
                <td> <?php echo $FetchSharedFare['Status']; ?> </td>
            </tr>
            <?php
        }
        ?>
</table>
    <div class="pagination pagination-centered">
        <ul>
            <?php
            for ($i = 1; $i <= $TotalOfPage; $i++) {
                if ($i == $Page)
                    echo '<li class = "active"><a href="SharedFare.php?Page=' . $i . '">' . $i . '</a></li>';
                else
                    echo '<li><a href="SharedFare.php?Page=' . $i . '">' . $i . '</a></li>';
            }
            ?>
        </ul>
    </div>
</form>

<?php include_once Path() . '/templates/adminfooter.php'; ?>
