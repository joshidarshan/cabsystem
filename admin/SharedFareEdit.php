<?php
if (!isset($_SESSION))
    session_start();
$_SESSION['cat'] = "route";

    function Path() {
    $Path = $_SERVER['DOCUMENT_ROOT'];
    // Differenciate the path for local and web host. Includes database connect file.
    if ($_SERVER['HTTP_HOST'] == "localhost" || $_SERVER['HTTP_HOST'] == "localhost:8080")
        $Path .= '/Cabsystem';
    return $Path;
}

include_once Path() .'/templates/adminheader.php';
include_once Path() . '/dbop/data/Location.php';
include_once Path() . '/dbop/data/sharedfare.php';

if (isset($_POST) && !empty($_POST)) {
    $Id = $_POST['Id'];
    $SlocationId = $_POST['SLocationId'];
    $DLocationId = $_POST['DLocationId'];
    $Rate = $_POST['Rate'];
    $DistanceKm = $_POST['DistanceKm'];
    $TravelTime = $_POST['TravelTime'];
    if (isset($_POST['Status']) && !empty($_POST['Status']))
        $Status = $_POST['Status'];
    else
        $Status = 0;

EditSharedFare($Id, $SLocationId, $DLocationId, $Rate, $DistanceKm, $TravelTime, $Status);
 echo "<script> alert ('Your Update SuccessFully'); window.location='SharedFare.php';</script>";
}

if(isset($_GET) && !empty($_GET)){
    $Id = $_GET['Id'];
}
   
?>
<form action="SharedFareEdit.php" method="post" name="SharedFareEdit">
    <input type="hidden" name="Id" id="Id" value="<?php echo $_GET['Id']; ?>" />
    <table align="center" class="table table-striped">
        <?php
        $GetSharedFareId = GetSharedFareId($Id);
          while ($FetchSharedFareId = mysql_fetch_assoc($GetSharedFareId)){
              ?>
        <tr>
            <td>Select Source Location</td>
            <td>
                <select name="SLocationId">
                    <?php
                    $GetAllLocations = GetAllLocations($locationStart = 0, $locationRpp = 500);
                    while ($FetchAllLocations = mysql_fetch_array($GetAllLocations)) {
                        if ($FetchAllLocations['Id'] == $FetchSharedFareId['SLocationId'])
                            echo "<option value=$FetchAllLocations[Id] selected='selected'>$FetchAllLocations[Name]</option>";
                        else
                            echo "<option value=$FetchAllLocations[Id]>$FetchAllLocations[Name]</option>";
                    }
                    ?>
                </select>
            </td>
        </tr>
        <tr>
            <td>Select Destination Location</td>
            <td><select name="DLocationId">
                    <?php
                    $GetAllLocations = GetAllLocations($locationStart = 0, $locationRpp = 500);
                    while ($FetchAllLocations = mysql_fetch_array($GetAllLocations)) {

                        if ($FetchAllLocations['Id'] == $FetchSharedFareId['DLocationId'])
                            echo "<option value=$FetchAllLocations[Id] selected='selected'>$FetchAllLocations[Name]</option>";
                        else
                            echo "<option value=$FetchAllLocations[Id]>$FetchAllLocations[Name]</option>";
                    }
                    ?>
                </select>
            </td>
        </tr>
        
    
        <tr>
            <td>Rate</td>
            <td><input type="text" name="Rate" value="<?php echo $FetchSharedFareId['Rate']; ?>"/></td>
        </tr>
        <tr>
            <td>Distance Km</td>
            <td><input type="text" name="DistanceKm" value="<?php echo $FetchSharedFareId['DistanceKm']; ?>"/></td>
        </tr>
        <tr>
            <td>Travel TIme</td>
            <td><input type="text" name ="TravelTime" value = "<?php echo $FetchSharedFareId['TravelTime']; ?>"/></td>
        </tr>

        <tr>
            <td>Current Status</td>
            <td>
                <?php if ($FetchSharedFareId['Status'] == 1) { ?>
                    <input type="checkbox" name="Status" value="1" checked="checked"/>
                <?php
                } 
                else
                {
                    ?>

                    <input type="checkbox" name="Status" value="1"/> <?php } ?>
            </td>
        </tr>
        <tr>
            <td colspan="2" style="text-align: center;"><input type="submit" name="submit" value="Update" class="btn-success btn"/></td>
        </tr>
        <?php
            }
            ?>
    </table>
</form>

<?php include_once Path() . "/templates/adminfooter.php"; ?>
