<?php
if (!isset($_SESSION))
    session_start();
$_SESSION['cat'] = "";

function Path() {
    $path = $_SERVER['DOCUMENT_ROOT'];
    // Differenciate the path for local and web host. Includes database connect file.
    if ($_SERVER['HTTP_HOST'] == "localhost" || $_SERVER['HTTP_HOST'] == "localhost:8080")
        $path .= '/Cabsystem';
    return $path;
}

include_once Path() . '/templates/adminheader.php';
include_once Path() . '/dbop/data/globalperameter.php';

$start = 0;
$rpp = 2;
$no = GlobalParametersPagination();
$totalpage = ceil($no / $rpp);

if (isset($_GET['action'])) {
    if ($_GET['action'] == "delete") {

        $Id = $_GET['Id'];
        DeleteCarType($Id);

        echo "<script> alert('Your Record Delete SuccessFully');</script>";
    }
}

if (isset($_GET['page'])) {
    $page = $_REQUEST['page'];
    $a = $page * $rpp;
    $start = $a - $rpp;
}
$GetAllGlobalParameters = GetAllGlobalParameters($start, $rpp);
?>
<form>
    <h1 align="center">Global Parameters Listing</h1>
    <table align="center" class="table table-striped">
        <tr>
            <th>Mh.<br/>Percentage</th>
            <th>Mh.<br/>Multipleof</th>
            <th>Cp.<br/>Minimunkm</th>
            <th>Cp.<br/>Point</th>
            <th>Cp.<br/>Maxpoints</th>
            <th>Cp.<br/>Point To Rupee Rate</th>
            <th>Cp.<br/>Discount Percent</th>
            <th>Cab <br/>Free Hours</th>
            <th>Cab <br/>Near By km</th>
            <th>Cab <br/>Average Speed</th>
            <th>Cab <br/>Redeemtion Lavel</th>
            <th>Cab <br/>Record Per Page</th>
            <th>Cab <br/>Virtual Per Model</th>
            <th>Cab <br/>Booking Confirmation Time</th>
            <th>Edit</th>

        </tr>

        </tr>
        <?php
        while ($FetchAllGlobalParameters = mysql_fetch_array($GetAllGlobalParameters)) {
            ?>
            <tr>
                <td><?php echo $FetchAllGlobalParameters['mh_percentage']; ?></td>
                <td><?php echo $FetchAllGlobalParameters['mh_multipleof']; ?></td>
                <td><?php echo $FetchAllGlobalParameters['cp_minimunkm']; ?></td>
                <td><?php echo $FetchAllGlobalParameters['cp_point']; ?></td>
                <td><?php echo $FetchAllGlobalParameters['cp_maxpoints']; ?></td>
                <td><?php echo $FetchAllGlobalParameters['cp_point_to_rupee_rate']; ?></td>
                <td><?php echo $FetchAllGlobalParameters['cp_discount_percent']; ?></td>
                <td><?php echo $FetchAllGlobalParameters['cab_freehours']; ?></td>
                <td><?php echo $FetchAllGlobalParameters['cab_nearbykm']; ?></td>
                <td><?php echo $FetchAllGlobalParameters['cab_average_speed']; ?></td>
                <td><?php echo $FetchAllGlobalParameters['cp_redeemtion_level']; ?></td>
                <td><?php echo $FetchAllGlobalParameters['recordperpage']; ?></td>
                <td><?php echo $FetchAllGlobalParameters['recordperpage']; ?></td>
                <td><?php echo $FetchAllGlobalParameters['booking_confirmation_time']; ?></td>
                <td><a href="GlobalParameterEdit.php?Id=<?php echo $FetchAllGlobalParameters['Id']; ?>">Edit</a></td>
            </tr>
            <?php
        }
        ?>
        <table align='center'>
            <tr>
                <?php
                for ($i = 1; $i <= $totalpage; $i++) {
                    ?>
                    <td><a href="GlobalParameter.php?page=<?php echo $i; ?>"><?php echo $i; ?></a></td>

                    <?php
                }
                ?>
            </tr>
        </table>
    </table>
</form>

<?php
include_once Path() . '/templates/adminfooter.php';
?>