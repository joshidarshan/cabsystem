<?php
if (!isset($_SESSION))
    session_start();
$_SESSION['cat'] = "route";

function Path() {
    $path = $_SERVER['DOCUMENT_ROOT'];
    // Differenciate the path for local and web host. Includes database connect file.
    if ($_SERVER['HTTP_HOST'] == "localhost" || $_SERVER['HTTP_HOST'] == "localhost:8080" || $_SERVER['HTTP_HOST'] == "192.168.1.100:8080")
        $path .= '/Cabsystem';
    return $path;
}

if (function_exists('date_default_timezone_set')) {
    date_default_timezone_set('Asia/Calcutta');
}
$fromYear = date('Y');
$tday = date('d');
$tmonth = date('m');
$toYear = date('Y', strtotime('+1 year'));

include_once Path() . '/templates/adminheader.php';
include_once Path() . '/dbop/data/CabRoute.php';
include_once Path() . '/dbop/data/cab.php';
include_once Path() . '/dbop/data/location.php';
include_once Path() . '/dbop/data/driver.php';
include_once Path() . '/ajax/car.php';

if (isset($_POST['sub'])) {

    //check if need to add new cab or not.
    if (!isset($_POST['CabId']) || empty($_POST['CabId'])) {
        $cabName = $_POST['ecab'];
        $cabQuery = GetCabByNoPlate($cabName);
        $cabData = mysql_fetch_assoc($cabQuery);
        $CabId = $cabData['Id'];
    }
    else
        $CabId = $_POST['CabId'];

    if (!isset($_POST['hsource']) || empty($_POST['hsource'])) {
        $locationName = $_POST['Source'];
        $SourceQuery = GetLocationByName($locationName);
        $SourceData = mysql_fetch_assoc($SourceQuery);
        $Source = $SourceData['Id'];
    }
    else
        $Source = $_POST['hsource']; //$_POST['Source'];

    if (!isset($_POST['hdestination']) || empty($_POST['hdestination'])) {
        $locationName = $_POST['Destination'];
        $SourceQuery = GetLocationByName($locationName);
        $SourceData = mysql_fetch_assoc($SourceQuery);
        $Destination = $SourceData['Id'];
    }
    else
        $Destination = $_POST['hdestination']; //$_POST['Destination'];

    $Id = $_POST['Id'];
    $FromDt = $_POST['FromDateTime'];
    $ToDt = $_POST['ToDateTime'];
    $OriginalLocation = $_POST['OriginalLocation'];


    $IsReturnTrip = 0;
    if (isset($_POST['IsReturnTrip']) && !empty($_POST['IsReturnTrip']))
        $IsReturnTrip = 1;
    $AgentId = $_POST['AgentId'];
    $DriverId = $_POST['DriverId'];
    $IsVia = 0;
    if (isset($_POST['IsVia']) && !empty($_POST['IsVia']))
        $IsVia = 1;

    $CarModelId = $_POST['CarModelId'];
    $Status = 0;
    if (isset($_POST['Status']) && !empty($_POST['Status']))
        $Status = 1;

    EditCabRoute($Id, $CabId, $FromDt, $Source, $Destination, $ToDt, $OriginalLocation, $IsReturnTrip, $AgentId, $DriverId, $IsVia, $CarModelId, $Status);
    echo "<script>alert('Updated SuccessFully'); window.location = 'CabRoute.php';</script>";
}
$Id = $_REQUEST['Id'];
$GetCabRouteId = CabRoutePoint($Id);
$FetchCab = mysql_fetch_array($GetCabRouteId);
?>

<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>

<!-- Datetime picker -->
<style>
    .td
    TD
    {
        text-align:center;
    }
</style>

<form action="CabrouteEdit.php" method="post" name="cabrouteadd" id="cabrouteadd">
    <h1 style="text-align: center;">Cabroute Add</h1>
    <table align="center" class="table table-striped"  >
        <tr>
            <td class="td">Cab Number</td>
            <td><input type="text" id="ecab" name="ecab" onblur="return getCarInfo();" value="<?php echo $FetchCab['NoPlate']; ?>"/><input type="hidden" name="Id" value="<?php echo $Id; ?>"/></td>
        </tr>

        <!--  -->
       <!-- <tr>
            <td>Car Model</td>
            <td>
                <select id="model" name="model">
                /*    <?php
        include_once Path() . '/dbop/data/carmodel.php';
        $carModelData = GetCarModelListing();
        while ($carModelRow = mysql_fetch_assoc($carModelData)) {
            echo "<option value='$carModelRow[Id]'>$carModelRow[Name]</option>";
        }
        ?>
                </select>
            </td>
        </tr>-->

        <td>From Date & Time</td>
        <td><input type="text" name="FromDateTime" id="FromDateTime" class="dttm" required/></td>
        </tr>

        <tr>
            <td>Select Source</td>
            <td><input type="text" name="Source" id="Source" onblur="return SetSource();" required value="<?php echo $FetchCab['Source']; ?>"/></td>
        </tr>
        <tr>
            <td>Select Destination</td>
            <td><input type="text" name="Destination" id="Destination" onblur="return SetDestination();" required value="<?php echo $FetchCab['Destination']; ?>"/></td>
        </tr>

        <tr>
            <td>To Date & Time</td>
            <td><input type="text" name="ToDateTime" class="dttm" required/></td>
        </tr>
        <tr>
            <td>Proper Location</td>
            <td><input type="text" name="OriginalLocation" class="dttm" value="<?php echo $FetchCab['OriginalLocation']; ?>" /></td>
        </tr>

        <tr>
            <td>Is It Return Trip?</td>
            <td><?php if ($FetchCab['IsReturnTrip'] == 1) { ?> Click For Return Trip&nbsp;&nbsp;=>&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" name="IsReturnTrip" checked="checked" /><?php } if ($FetchCab['IsReturnTrip'] == 0) { ?> Click For Return Trip&nbsp;&nbsp;=>&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" name="IsReturnTrip" /> <?php } ?></td>
        </tr>
        <tr>
            <td>Select Agent Id</td>
            <td>
                <?php
                include_once Path() . '/dbop/data/Agent.php';
                $Agentid = $FetchCab['AgentId'];

                $GetAllAgents = GetAgentById($Agentid);
                $FetchAgent = mysql_fetch_array($GetAllAgents);
                ?>
                <label><?php echo $FetchAgent['Name']; ?></label>
                <input type="hidden" value="<?php echo $Agentid; ?>" name="AgentId">
            </td>
        </tr>
        <tr>
            <td>Select Driver Id</td>
            <td>
                <select id="DriverId" name="DriverId" >
                    <?php
                    $driverStart = 0;
                    $driverRpp = 500;
                    $AgentId = $Agentid;
                    $DriverId = $FetchCab['DriverId'];
                    echo $DriverId;
                    $GetAgent = GetAgentsListing($AgentId, $driverStart, $driverRpp);
                    while ($FetchDriver = mysql_fetch_array($GetAgent)) {
                        if ($FetchDriver['Id'] == $DriverId) {
                            ?>
                            <option value="<?php echo $FetchDriver['Id']; ?>" selected="selected"><?php echo $FetchDriver['Name']; ?></option>
                            <?php
                        } else {
                            ?>
                            <option value="<?php echo $FetchDriver['Id']; ?>" ><?php echo $FetchDriver['Name']; ?></option>
                            <?php
                        }
                    }
                    ?>

                </select>
            </td>
        </tr>
        <tr>
            <td>Make Sure Is Via Or Not</td>            
            <td><?php if ($FetchCab['IsVia'] == 0) { ?>Click For Via&nbsp;&nbsp;=>&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" name="IsVia" value="1"  /><?php } if ($FetchCab['IsVia'] == 1) { ?>Click For Via&nbsp;&nbsp;=>&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" name="IsVia" value="1" checked="checked" /><?php } ?></td>
        </tr>
        <tr>
            <td>Choose A CarModel</td>
            <td>
                <select name="CarModelId">
<?php
$GetAllModels = GetCarModelListing();
while ($FetchAllModels = mysql_fetch_array($GetAllModels)) {
    if ($FetchAllModels['Id'] == $FetchCab['CarModelId']) {
        ?>
                            <option value="<?php echo $FetchAllModels['Id']; ?>" selected="selected"> <?php echo $FetchAllModels['Name'] . "-" . $FetchAllModels['NoSeats'] . "  Seater"; ?></option>
                            <?php
                        } else {
                            ?>
                            <option value="<?php echo $FetchAllModels['Id']; ?>"> <?php echo $FetchAllModels['Name'] . "-" . $FetchAllModels['NoSeats'] . "  Seater"; ?></option>          
                            <?php
                        }
                    }
                    ?>
            </td>
            </select>
        </tr>
        <tr>
            <td>Current Status</td>
            <td><?php if ($FetchCab['Status'] == 0) { ?>Click For Via&nbsp;&nbsp;=>&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" name="Status" value="1"  /><?php } if ($FetchCab['Status'] == 1) { ?>Click For Via&nbsp;&nbsp;=>&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" name="Status" value="1" checked="checked" /><?php } ?></td>
        </tr>
        <tr>
            <td colspan="2" style="text-align: center;"><input type="submit" name="sub" value="Add" class="btn-large"/></td>
        </tr>
        <tr>
            <td colspan="2" style="text-align: center;"><a href="CabAdd.php">Cab not listed? Register new cab here</a></td>
        </tr>
    </table>
    <input type="hidden" name="isnew" id="isnew" value="0" />
    <input type="hidden" name="hsource" id="hsource" value="" />
    <input type="hidden" name="hdestination" id="hdestination" value="" />
    <input type="hidden" name="CabId" id="CabId" value="" />
</form>

<?php include_once Path() . "/templates/adminfooter.php"; ?>

<script>
                function GetDriver() {
                    var agentId = $("#AgentId").val();
                    $.ajax({
                        type: "GET",
                        url: "../ajax/car.php?type=cabroute&required=drivers&agentId=" + agentId,
                        //data: "std=" + std,
                        success: function(data) {
                            //alert(data);
                            $("#DriverId").find('option').remove().end();
                            $("#DriverId").html(data);
                        }
                    });
                }

                $(function() {
                    $.ajax({
                        type: "GET",
                        url: "../ajax/car.php?type=cabroute",
                        data: "required=cablist",
                        success: function(data) {
                            obtainData = JSON.parse(data);
                            var availableCabs = obtainData;
                            $("#ecab").autocomplete({
                                source: availableCabs
                            });
                        }
                    });
                });

                // Location
                $(function() {
                    $.ajax({
                        type: "GET",
                        url: "../ajax/location.php?type=location",
                        data: "required=locationlist",
                        success: function(data) {
                            //alert(data);
                            obtainData = JSON.parse(data);
                            var availableRoutes = obtainData;
                            $("#Source").autocomplete({
                                source: availableRoutes
                            });
                            $("#Destination").autocomplete({
                                source: availableRoutes
                            });
                        }
                    });
                });

                function SetSource() {
                    source = $("#Source").val();
                    // alert(source);

                    $.ajax({
                        type: "GET",
                        url: "../ajax/location.php?type=location&required=locationid&location=" + source,
                        success: function(data) {
                            //alert(data);
                            if (data) {
                                data = JSON.parse(data);
                                $("#hsource").val(data['Id']);
                            }
                            else
                                $("#hsource").val(0);
                        }
                    });
                }

                function SetDestination() {
                    source = $("#Destination").val();
                    // alert(source);

                    $.ajax({
                        type: "GET",
                        url: "../ajax/location.php?type=location&required=locationid&location=" + source,
                        success: function(data) {
                            //alert(data);
                            if (data) {
                                data = JSON.parse(data);
                                $("#hdestination").val(data['Id']);
                            }
                            else
                                $("#hdestination").val(0);
                        }
                    });
                }

                // Datetime picker
                $(function() {
                    $('*[name=FromDateTime]').appendDtpicker({
                        //"inline": true,
                        "minuteInterval": 15,
                        "current": "<?php echo $fromYear . '-' . $tmonth . '-' . $tday ?> 00:15"
                    });
                    $('*[name=ToDateTime]').appendDtpicker({
                        //"inline": true,
                        "minuteInterval": 15
                    });
                });

                // Car Info
                function getCarInfo() {
                    var carno = $("#ecab").val();
                    $.ajax({
                        type: "GET",
                        url: "../ajax/car.php?type=cabs&required=cablist&carno=" + carno,
                        //data: "std=" + std,
                        success: function(data) {
                            data = JSON.parse(data);
                            //alert(data);
                            //{"Id":"1","CarModelId":"1","NoPlate":"gj-03-av-1546","Rating":"10","MadeYear":"2012","AgentId":"1","KmRate":"12.00","AcKmRate":"14.00","Status":"1"}
                            $("#madeyear").val(data['MadeYear']);
                            $("#ackmrate").val(data['AcKmRate']);
                            $("#nackmrate").val(data['KmRate']);
                            $("#model").val(data['CarModelId']);

                            if (data['Id']) {
                                $("#isnew").val(0);
                                $("#CabId").val(data['Id']);
                            }
                            else
                                $("#isnew").val(1);
                        }
                    });
                }

</script>
