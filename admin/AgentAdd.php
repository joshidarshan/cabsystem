<?php

if (!isset($_SESSION))
    session_start();
$_SESSION['cat'] = "inventory";

function Path() {
    $path = $_SERVER['DOCUMENT_ROOT'];
    // Differenciate the path for local and web host. Includes database connect file.
    if ($_SERVER['HTTP_HOST'] == "localhost" || $_SERVER['HTTP_HOST'] == "localhost:8080")
        $path .= '/Cabsystem';
    return $path;
}

include_once Path() . '/templates/adminheader.php';

if (isset($_POST['sub'])) {

    include_once Path() . '/dbop/data/agent.php';
    include_once Path() . '/dbop/data/user.php';

    $Name = $_REQUEST['Name'];
    $Address = $_REQUEST['Address'];
    $Phones = $_REQUEST['Phones'];
    $Email = $_REQUEST['Email'];
    $Notes = $_REQUEST['Notes'];
    $Status = 1;
    $RoleId = 2;
    $Password = $_POST['Password'];

    if (($_FILES["file"]["type"] == "image/gif") || ($_FILES["file"]["type"] == "image/jpeg") || ($_FILES["file"]["type"] == "image/png" )) {
        $path = Path() . '/Upload/images/';
        $guid = uniqid();
        $fname = $guid . '-' . $_FILES['file']['name'];
        move_uploaded_file($_FILES["file"]["tmp_name"], $path . $fname);
        $imgnam = $fname;

        $Id = InsertAgent($Name, $Address, $Phones, $Email, $Notes, $imgnam, $Status);
        AddUser($Id, $Email, $Phones, $Password, $RoleId, $Status);
        echo "<div class='alert alert-success'><a class='close' data-dismiss='alert'>x</a><strong>Agent Added!</strong></div>";
    } else {
        $Id = InsertAgent($Name, $Address, $Phones, $Email, $Notes, '', $Status);
        AddUser($Id, $Email, $Phones, $Password, $RoleId, $Status);
        echo "<div class='alert alert-success'><a class='close' data-dismiss='alert'>x</a><strong>Agent Added!</strong></div>";
    }
}
?>
<script>
    add_record_email = 0;
    add_record_no = 0;
    function CheckNumeric(e)
    {
        var key = e.which;
        if (key >= 48 && key <= 57 || key == 8)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    function CheckNumbers() {
        var Phones = $("#Phones").val();
        if (Phones) {
            $.ajax({
                type: "GET",
                url: "../ajax/user.php?type=ContactNo&required=AgentContact&Contact=" + Phones,
                success: function(data) {
                    //alert(data);
                    message = "";
                    if (data == 0) {
                        message = "Already Exist";
                        $("#CheckContact").css({"color": "Red"});
                        add_record_no = 0;
                    }
                    else {
                        message = "Available";
                        $("#CheckContact").css({"color": "Green"});
                        add_record_no = 1;
                    }
                    $("#CheckContact").text(message);
                }
            });
        }

        else
        {
            return false;
        }

    }

    function ChkEmail() {
        var Email = $("#Email").val();
        if (Email) {
            var isCorrect = CheckEmail();
            if (isCorrect) {
                $.ajax({
                    type: "GET",
                    url: "../ajax/user.php?type=EmailId&required=AgentEmail&Email=" + Email,
                    success: function(data) {
                        //alert(data);
                        message = "";
                        if (data == 0) {
                            message = "Already Exist";
                            $("#ChkEmail").css({"color": "Red"});
                            add_record_email = 0;
                        }
                        else {
                            message = "Available";
                            $("#ChkEmail").css({"color": "Green"});
                                             add_record_email = 1;           

                        }
                        $("#ChkEmail").text(message);
                    }
                });
            }
            else
                document.getElementById("ChkEmail").innerHTML = "Email Is Not Correct";
                add_record_email = 0;
        }
        else
        {
            return false;
        }
    }

    function CheckEmail() {
        var Email = $("#Email").val();
        var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
        if (reg.test(Email)) {
            return true;
        }
        else {
            return false;
        }
    }
   function Validation()
   {
       alert(add_record_email);
       if (add_record_email == 0 || add_record_no == 0) {
            return false;
        }
        return true;
   }

</script>
<style>
  
</style>
<form action="AgentAdd.php" method="post" enctype="multipart/form-data" name="Cabsystem" id="Cabsystem" onSubmit=" return Validation();">
    <h1 style="text-align: center;">Agent Add</h1>
    <table align="center" class="table table-striped">
        <tr>
            <td>Name</td>
            <td><input type="text" name="Name"/></td>
        </tr>
        <tr>
            <td>Address</td>
            <td><textarea name="Address"></textarea></td>
        </tr>
        <tr>
            <td>Phone Numbers</td>
            <td>
                <input type="text" id="Phones" name="Phones" maxlength="10" onkeypress="return CheckNumeric(event);" onblur="return CheckNumbers();" placeholder="Enter Number Only"/>
                <span id="CheckContact"></span>
            </td>
        </tr>
        <tr>
            <td>Email-Id</td>
            <td><input type="text" name="Email" id="Email" required onblur="return ChkEmail();" /><span id="ChkEmail" style="color: red"></span><span id="val"></span></td>
        </tr>
        <tr>
            <td>PassWord</td>
            <td><input type="password" name="Password" onfocus="return Email_filter();" /></td>
        </tr>
        <tr>
            <td>Notes</td>
            <td><textarea name="Notes"></textarea></td>
        </tr>
        <tr>
            <td>Image/Document</td>
            <td><input type="file" name="file" class="btn"/></td>
        <tr>
            <td colspan="2" style="text-align: center;"><input type="submit" name="sub" value="Add" class="btn"/></td>
        </tr>
    </table>	
</form>

<?php

include_once Path() . '/templates/adminfooter.php';
?>

