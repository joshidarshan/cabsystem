<?php
if (!isset($_SESSION))
    session_start();
$_SESSION['cat'] = "car";

function Path() {
    $path = $_SERVER['DOCUMENT_ROOT'];
    // Differenciate the path for local and web host. Includes database connect file.
    if ($_SERVER['HTTP_HOST'] == "localhost" || $_SERVER['HTTP_HOST'] == "localhost:8080")
        $path .= '/Cabsystem';
    return $path;
}


include_once Path() . '/templates/adminheader.php';
include_once Path() . '/dbop/data/carmodel.php';
include_once Path() . '/dbop/data/carmake.php';
include_once Path() . '/dbop/data/cartype.php';

function CarModelImageUrl() {
    $url = "http://" . $_SERVER['HTTP_HOST'];
    if ($_SERVER['HTTP_HOST'] == "localhost" || $_SERVER['HTTP_HOST'] == "localhost:8080")
        $url .= '/Cabsystem';

    return $url;
}

if (isset($_POST['sub'])) {
    $Id = $_POST['Id'];
    $CarMakeId = $_POST['CarMakeId'];
    $CarTypeId = $_POST['CarTypeId'];
    $CarModelName = $_POST['CarModelName'];
    $NoSeats = $_POST['NoSeats'];
    if (isset($_POST['CarModelIsVirtual']) && !empty($_POST['CarModelIsVirtual']))
        $CarModelIsVirtual = $_POST['CarModelIsVirtual'];
    else
        $CarModelIsVirtual = 0;
    $SeatLayout = "";
    //if($_POST['RowA'])&&!empty($_POST['RowA'])
    //if(isset($_POST['Row']))&& !empty($_POST['Row'])
    $tp = implode(',', $_POST['Row']);
    $seatData = explode(",", $tp);

    if (isset($seatData[0]) && !empty($seatData[0]))
        $SeatLayout .= "Ax$seatData[0]";
    if (isset($seatData[1]) && !empty($seatData[1]))
        $SeatLayout .= ",Bx$seatData[1]";
    if (isset($seatData[2]) && !empty($seatData[2]))
        $SeatLayout .= ",Cx$seatData[2]";
    if (isset($seatData[3]) && !empty($seatData[3]))
        $SeatLayout .=",Dx$seatData[3]";
    if (isset($seatData[4]) && !empty($seatData[4]))
        $SeatLayout .=",Ex$seatData[4]";
    if (isset($seatData[5]) && !empty($seatData[5]))
        $SeatLayout .=",Fx$seatData[5]";

    $DefaultKmRate = $_POST['DefaultKmRate'];
    $DefaultAcKmRate = $_POST['DefaultKmRate'];
    if (isset($_POST['CarModelStatus']) && !empty($_POST['CarModelStatus']))
        $CarModelStatus = $_POST['CarModelStatus'];
    else
        $CarModelStatus = 0;

    if (isset($_FILES['file']['name']) && !empty($_FILES['file']['name']) && $_FILES['file']['size'] > 0) {
        //$DocumentLink = $_FILES['DocLink'];
        $path = Path() . '/Upload/images/';
        $guid = uniqid();
        $fname = $guid . '-' . $_FILES['file']['name'];
        move_uploaded_file($_FILES["file"]["tmp_name"], $path . $fname);
        $imgnam = $fname;
    } else {
        if (isset($_POST['prevdoc']) && !empty($_POST['prevdoc']))
            $imgnam = $_POST['prevdoc'];
    }

    CarModelEdit($Id, $CarMakeId, $CarTypeId, $CarModelName, $NoSeats, $CarModelIsVirtual, $SeatLayout, $DefaultKmRate, $DefaultAcKmRate, $imgnam, $CarModelStatus);
    echo "<script> alert ('Your Update SuccessFully'); window.location='CarModel.php';</script>";
}
$Id = $_REQUEST['Id'];
$GetCarModel = GetCarModelById($Id);
$FetchCarModel = mysql_fetch_array($GetCarModel);
$GetCarMake = GetCarsListing($CarStart = 0, $CarRpp = 500);
$GetCarType = GetCarTypesListing($CarStart = 0, $CarRpp = 500);
$makeId = $FetchCarModel['CarMakeId'];

$seatArray = array();
$SeatLayout = $FetchCarModel['SeatLayout'];
$seatExplodLevel1 = explode(",", $SeatLayout);
foreach ($seatExplodLevel1 as $sseat => $sseatVal) {
    if (empty($sseatVal))
        $seatArray[] = 0;
    else {
        $lexp = explode("x", $sseatVal);
        $seatArray[] = $lexp[1];
    }
}
?>

<form action="CarModelEdit.php" method="post" enctype="multipart/form-data">
    <h1 align="center">Edit Car Model Here</h1>
    <table align="center" class="table table-striped">
        <tr>
            <td>Car Model Id</td>
            <td><input type="hidden" name="Id"  value="<?php echo $Id; ?>"/></td>
        </tr>
        <tr>
            <td>Choose A Made Car</td>
            <td><select name="CarMakeId">
<?php
while ($fetchCarMake = mysql_fetch_array($GetCarMake)) {
    if ($FetchCarModel['CarMakeId'] == $fetchCarMake['Id'])
        echo "<option value=$fetchCarMake[Id] selected='selected'>$fetchCarMake[Name]</option>";
    else
        echo "<option value=$fetchCarMake[Id]>$fetchCarMake[Name]</option>";
}
?>
                </select></td>
        </tr>
        <tr>
            <td>Choose A Car Type</td>
            <td><select name="CarTypeId">
<?php
while ($fetchCarType = mysql_fetch_array($GetCarType)) {
    if ($FetchCarModel['CarTypeId'] == $fetchCarType['Id'])
        echo "<option value=$fetchCarType[Id] selected='selected'>$fetchCarType[Name]</option>";
    else
        echo "<option value=$fetchCarType[Id]>$fetchCarType[Name]</option>";
}
?>
                </select></td>
        </tr>
        <tr>
            <td>Enter Model Name</td>
            <td><input type="text" name="CarModelName" value="<?php echo $FetchCarModel['Name']; ?>" required/></td>
        </tr>

        <tr>
            <td>No Of Seats</td>
            <td><input type="text" name="NoSeats" value="<?php echo $FetchCarModel['Seats']; ?>"</td>
        </tr>
        <tr>
            <td> IsVirtual</td>
<?php
if ($FetchCarModel['IsVirtual'] == 1) {
    ?>
                <td>Click for Active<input type="checkbox" name="CarModelIsVirtual" value="1"  checked="checked"/></td>
                <?php
            }
            ?>
            <?PHP if ($FetchCarModel['IsVirtual'] == 0) { ?>
                <td>Click for Active<input type="checkbox" name="CarModelIsVirtual" value="1"/></td>
            <?php } ?> 
        </tr>
        <tr>
            <td>Enter Seat Layout</td>
            <td>
                A &nbsp;<input  type="text" name="Row[]" class="input-mini" value="<?php echo $seatArray[0]; ?>"/><br/>
                B &nbsp;<input  type="text" name="Row[]" class="input-mini" value="<?php if (!isset($seatArray[1])) echo 0;
            else echo $seatArray[1]; ?>"/><br/>
                C &nbsp;<input  type="text" name="Row[]" class="input-mini" value="<?php if (!isset($seatArray[2])) echo 0;
            else echo $seatArray[2]; ?>"/><br/>
                D &nbsp;<input  type="text" name="Row[]" class="input-mini" value="<?php if (!isset($seatArray[3])) echo 0;
            else echo $seatArray[3]; ?>"/><br/>
                E &nbsp;<input  type="text" name="Row[]" class="input-mini" value="<?php if (!isset($seatArray[4])) echo 0;
            else echo $seatArray[4]; ?>"/><br/>
                F &nbsp;<input  type="text" name="Row[]" class="input-mini" value="<?php if (!isset($seatArray[5])) echo 0;
            else echo $seatArray[5]; ?>"/><br/>
            </td>
        </tr>
        <tr>
            <td>Enter Default Km. Rate</td>
            <td><input type="text" name="DefaultKmRate" value="<?php echo $FetchCarModel['DefaultKmRate']; ?>"/></td>
        </tr>
        <tr>
            <td>Enter Default Ac Km. Rate</td>
            <td><input type="text" name="DefaultAcKmRate" value="<?php echo $FetchCarModel['DefaultAcKmRate']; ?>"/></td>
        </tr>
        <tr>
            <td>Current Status</td>
            <?php
            if ($FetchCarModel['Status'] == 1) {
                ?>
                <td>Click for Active<input type="checkbox" name="CarModelStatus" value="1"  checked="checked"/></td>
                <?php
            }
            ?>
<?PHP if ($FetchCarModel['Status'] == 0) { ?>
                <td>Click for Active<input type="checkbox" name="CarModelStatus" value="1"/></td>
<?php } ?>
        </tr>
        <tr>
            <td>Your Current Image</td>
            <td><img src= "<?php echo CarModelImageUrl() . '/Upload/images/' . $FetchCarModel['CarImage']; ?>" height="100" width="200"/></td>
            <?php
            if (isset($FetchCarModel['CarImage']) && !empty($FetchCarModel)) {
                $originfname = substr($FetchCarModel['CarImage'], (stripos($FetchCarModel['CarImage'], '-') + 1), strlen($FetchCarModel['CarImage']));
                echo "<tr>";
                echo "<td>Current Image Name</td>";
                echo "<td>$originfname</td>";
                echo "</tr>";
                echo "<input type='text' name='prevdoc' id='prevdoc' value='$FetchCarModel[CarImage]' />";
            }
            ?>
        </tr>	
        <tr>
            <td>Choose An Image</td>
            <td><input type="file" name="file"/></td>
        </tr>
        <tr>
            <td colspan="2" style="text-align: center;"><input type="submit" name="sub" value="Update" class="btn-success btn"/></td>
        </tr>
    </table>
</form>

<?php include_once Path() . "/templates/adminfooter.php"; ?>
