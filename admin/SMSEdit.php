<?php

if (!isset($_SESSION))
    session_start();
$_SESSION['cat'] = "";

function Path() {
    $path = $_SERVER['DOCUMENT_ROOT'];
    // Differenciate the path for local and web host. Includes database connect file.
    if ($_SERVER['HTTP_HOST'] == "localhost" || $_SERVER['HTTP_HOST'] == "localhost:8080")
        $path .= '/Cabsystem';
        return $path;
}

include_once Path() . '/templates/adminheader.php';
include_once Path() . '/dbop/data/sms.php';
if (isset($_POST['sub'])) {
    
    $Id = $_POST['Id'];
    $From = $_POST['From'];
    $Message = $_POST['Message'];
    $Status = "";
    if (isset($_POST['Status']) && !empty($_POST['Status']))
        $Status = $_POST['Status'];
    else
        $Status = 0;
    
    SMSEdit($Id, $From,$Message,$Status);
    echo "<script> alert ('Your Update SuccessFully'); window.location='SMS.php';</script>";
}
$Id = $_REQUEST['Id'];
$GetSMSById = GetSMSById($Id) ;
$FetchSMSById = mysql_fetch_array($GetSMSById);
?>

<form action="SMSEdit.php" method="post" id="SMSAdd" >
    <h1 align="center">Update SMS</h1>
    <table align="center" class="table table-striped">
        <tr>
            <td>From <input type="hidden" name="Id" value="<?php echo $Id;?>"/></td>
            <td><input type="text" name="From" required value="<?php echo $FetchSMSById['From']; ?>" /></td>
        </tr>
        <tr>
            <td>Enter Message</td>
            <td><textarea name="Message"><?php echo $FetchSMSById['Message'];?></textarea></td>
        </tr>
        <tr>
            <td>Current Status</td>
            <td>
                <?php if($FetchSMSById['Status']==1) {?>
                <input type="checkbox" name="Status" checked="checked" value="1"/>
                <?php } else {?>
                <input type="checkbox" name="Status"  value="1"/>
                <?php } ?>
            </td>
        </tr>
        <tr>
            <td colspan="2" style="text-align: center;"><input type="submit" name="sub" value="Update" class="btn-inverse btn-large" onfocus="return CheckCarMakeName();" onclick=" return CheckCarMakeName();"/></td>
        </tr>
    </table>
</form>

<?php include_once Path() . "/templates/adminfooter.php"; ?>
