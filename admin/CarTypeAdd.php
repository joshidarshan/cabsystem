<?php
if (!isset($_SESSION))
    session_start();
$_SESSION['cat'] = "car";

function Path() {
    $path = $_SERVER['DOCUMENT_ROOT'];
    // Differenciate the path for local and web host. Includes database connect file.
    if ($_SERVER['HTTP_HOST'] == "localhost" || $_SERVER['HTTP_HOST'] == "localhost:8080")
        $path .= '/Cabsystem';
    return $path;
}

include_once Path() . '/templates/adminheader.php';
include_once Path() . '/dbop/DbConnect.php';

if (isset($_POST['sub'])) {
    include_once Path() . '/dbop/data/cartype.php';
    $CarName = $_POST['CarName'];
    $CarStatus = 1;
    CarTypeAdd($CarName, $CarStatus);
    echo "<div class='alert alert-success'><a class='close' data-dismiss='alert'>x</a><strong>Car Type Added!</strong></div>";
}
?>
<script>
    function CheckCarTypeName() {
        var CarTypeAdd = $("#CarTypeAdd").val();
        if(CarTypeAdd){
        $.ajax({
            type: "GET",
            url: "../ajax/car.php?type=Car&required=CarType&Type=" + CarTypeAdd,
            success: function(data) {
                //alert(data);
                message = "";
                if (data == 0) {
                    message = "Already Exist";
                    $("#ChkCarType").css({"color": "Red"});
                }
                else {
                    message = "Available";
                    $("#ChkCarType").css({"color": "Green"});
                }
                $("#ChkCarType").text(message);
            }
        });
    }
    else
        return false;
    }
    function submitCheck(){
    var get = $("#ChkCarType").text();

    if(get.toLowerCase() == "already exist")
        return false;
    else
        return true;
    }
    
</script>
<form action="CarTypeAdd.php" method="post" onsubmit="return submitCheck();">
    <h1 align="center">Add Car Type</h1>                            
    <table align="center"class="table table-striped">
        <tr>
            <td style="text-align: right">Enter Car Type Name </td>
            <td style="text-align"><input type="text" name="CarName" id="CarTypeAdd" required onblur="return CheckCarTypeName();"/>
                &nbsp;&nbsp;&nbsp;<span id="ChkCarType" style="position: absolute;"></span></td>;
        </tr>
        <tr>
            <td colspan="2" style="text-align: center;"><input type="submit" name="sub" value="Add" class="btn-large"/></td>
        </tr>
    </table>
</form>

<?php
include_once Path() . '/templates/Adminfooter.php';
?>