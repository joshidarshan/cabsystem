<?php
if (!isset($_SESSION))
    session_start();
$_SESSION['cat'] = "inventory";

function Path() {
    $path = $_SERVER['DOCUMENT_ROOT'];
    // Differenciate the path for local and web host. Includes database connect file.
    if ($_SERVER['HTTP_HOST'] == "localhost" || $_SERVER['HTTP_HOST'] == "localhost:8080")
        $path .= '/Cabsystem';
    return $path;
}

include_once Path() . '/templates/adminheader.php';
include_once Path() . '/dbop/data/driver.php';
include_once Path() . '/dbop/data/agent.php';

$driverStart = 0;
$driverRpp = 2;
$page = 1;

if (isset($_GET['page']) && !empty($_GET)) {
    $page = $_REQUEST['page'];
    $a = $page * $driverRpp;
    $driverStart = $a - $driverRpp;
    $Id = $_SESSION['Page_AgentId'];
    $no = PaginationDriver($Id);
    $totalpage = ceil($no / $driverRpp);
    $GetAgent = GetAgentsListing($Id, $driverStart, $driverRpp);
}
if (isset($_GET['action'])) {
    if ($_GET['action'] == "delete") {
        echo $_GET['action'];
        $Id = $_GET['Id'];
        DeleteDriver($Id);
        echo "<script> alert('Your Record Delete SuccessFully');</script>";
    }
}
?>
<script language="javascript">
    function del()
    {
        var r = confirm("Really Want to Delete A Driver");
        if (!r)
            return false;
    }
</script>

<h1 class="text-center">Driver Listing</h1>

<form action="Driver.php" method="post">
    <table class="table" >
        <tr>
            <td>Select Agent</td>
            <td>
                <select name="Agents">
                    <?php
                    $select = GetAllAgents($start = 0, $rpp = 500);

                    while ($agentInfo = mysql_fetch_array($select)) {
                        ?>
                        <option value="<?php echo $agentInfo['Id']; ?>"><?php echo $agentInfo['Name']; ?></option>
                        <?php
                    }
                    ?>
                </select>
                &nbsp;
                <input type="submit" name="sub" value="Go" class="btn-large"/>
            </td>
        </tr>
    </table>
    <?php
    if (isset($_POST['sub'])) {
        $Id = $_POST['Agents'];
        $no = PaginationDriver($Id);
        $totalpage = ceil($no / $driverRpp);
        $_SESSION['Page_AgentId'] = $Id;
        $GetAgent = GetAgentsListing($Id, $driverStart, $driverRpp);
    }
    if (isset($GetAgent) && !empty($GetAgent)) {
        ?>
        <table align='center' class="table table-striped">
            <tr>
                <th>Action</th>
                <th>Agent</th>
                <th>Name</th>
                <th>Contacts</th>
                <th>Note</th>
                <th>Document</th>
                <th>Status</th>
            </tr>
            <?php
            while ($FetchAgent = mysql_fetch_array($GetAgent)) {
                $doc = substr($FetchAgent['DocumentLink'], (stripos($FetchAgent['DocumentLink'], '-') + 1), strlen($FetchAgent['DocumentLink']));
                ?>
                <tr>
                    <td><a href="driveredit.php?Id=<?php echo $FetchAgent['Id']; ?>&Through=Admin">Edit</a> &nbsp;| &nbsp;
                        <a href="driver.php?action=delete&Id=<?php echo $FetchAgent['Id']; ?>" onClick="return del();">Delete</a> </td>
                    <td><?php echo $FetchAgent['AgName']; ?></td>
                    <td><?php echo $FetchAgent['Name']; ?></td>
                    <td><?php echo $FetchAgent['Phones']; ?></td>
                    <td><?php echo $FetchAgent['Note']; ?></td>
                    <td><?php echo $doc; ?></td>
                    <td><?php echo $FetchAgent['Status']; ?></td>
                </tr>
                <?php
            }
            ?>
        </table>
    <?php } ?>

    <div class="pagination pagination-centered">
        <ul>
            <?php
            if (isset($totalpage) && !empty($totalpage)) {
                for ($i = 1; $i <= $totalpage; $i++) {
                    if ($i == $page)
                        echo '<li class = "active"><a href="driver.php?page=' . $i . '">' . $i . '</a></li>';
                    else
                        echo '<li><a href="driver.php?page=' . $i . '">' . $i . '</a></li>';
                }
            }
            ?>
        </ul>
    </div>
</form>

<?php
include_once Path() . '/templates/adminfooter.php';
?>