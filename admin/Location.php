<?php
if (!isset($_SESSION))
    session_start();
$_SESSION['cat'] = "";

function Path() {
    $path = $_SERVER['DOCUMENT_ROOT'];
    // Differenciate the path for local and web host. Includes database connect file.
    if ($_SERVER['HTTP_HOST'] == "localhost" || $_SERVER['HTTP_HOST'] == "localhost:8080")
        $path .= '/Cabsystem';
    return $path;
}


include_once Path() . '/templates/adminheader.php';
$page = 1;
$locationStart = 0;
$locationRpp = 2;

include_once Path() . '/dbop/data/Location.php';

$no = LocationPagination();
$totalpage = ceil($no / $locationRpp);

if (isset($_GET['page'])) {
    $page = $_REQUEST['page'];
    $a = $page * $locationRpp;
    $locationStart = $a - $locationRpp;
}
$GetAllLocation = GetAllLocations($locationStart, $locationRpp);
if (isset($_GET['action'])) {
    if ($_GET['action'] == "delete") {
        echo $_GET['action'];
        $Id = $_GET['Id'];
        DeleteLocation($Id);
        echo "<script> alert('Your Record Delete SuccessFully');</script>";
    }
}
?>
<script language="javascript">
    function del()
    {
        var x;
        var r = confirm("Really Want to Delete Location");
        if (!r)
            return false;
    }
</script>
     
        <table align="center" class="table table-striped">
            <tr>
                <th>Id</th>
                <th>Name</th>
                <th>Status</th>
                <th>Edit</th>
                <th>Delete</th>
            </tr>
<?php
while ($FetchLocation = mysql_fetch_array($GetAllLocation)) {
    ?>
                <tr>
                    <td><?php echo $FetchLocation['Id']; ?></td>
                    <td><?php echo $FetchLocation['Name']; ?></td>
                    <td><?php echo $FetchLocation['Status']; ?></td>
                    <td><a href="LocationEdit.php?Id=<?php echo $FetchLocation['Id']; ?>">Edit</a>
                    <td><a href="Location.php?action=delete&Id=<?php echo $FetchLocation['Id']; ?>" onClick="return del();">Delete</a>
                </tr>
    <?php
}
?>
                </table>
                <div class="pagination pagination-centered">
        <ul>
            <?php
            for ($i = 1; $i <= $totalpage; $i++) {
                if ($i == $page)
                    echo '<li class = "active"><a href="Location.php?page=' . $i . '">' . $i . '</a></li>';
                else
                    echo '<li><a href="Location.php?page=' . $i . '">' . $i . '</a></li>';
            }
            ?>
        </ul>
    </div>
        
        
        <?php
             include_once Path() . '/templates/adminfooter.php';
           ?> 