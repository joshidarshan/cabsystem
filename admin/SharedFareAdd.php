<?php
if (!isset($_SESSION))
    session_start();
$_SESSION['cat'] = "route";

function Path() {
    $path = $_SERVER['DOCUMENT_ROOT'];
    // Differenciate the path for local and web host. Includes database connect file.
    if ($_SERVER['HTTP_HOST'] == "localhost" || $_SERVER['HTTP_HOST'] == "localhost:8080")
        $path .= '/Cabsystem';
    return $path;
}

include_once Path() . '/templates/adminheader.php';
include_once Path() . '/dbop/data/sharedfare.php';
include_once Path() . '/dbop/data/location.php';

if (isset($_POST) && !empty($_POST)) {
    $SLocationId = $_POST['SLocationId'];
    $DLocationId = $_POST['DLocationId'];
    $Rate = $_POST['Rate'];
    $DistanceKm = $TravelTime = 0;
    if(isset($_POST['DistanceKm']) && !empty($_POST['DistanceKm']))
        $DistanceKm = intval($_POST['DistanceKm']);
    if(isset($_POST['TravelTime']) && !empty($_POST['TravelTime']))
        $TravelTime = $_POST['TravelTime'];
    $Status = 1;

    AddSharedFare($SLocationId, $DLocationId, $Rate, $DistanceKm, $TravelTime, $Status);
}
?>
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
<script type="text/javascript">
function Check() {
  //alert("In API Call");
//var or = document.getElementById("SLocationId").value+", Gujarat";
or = $("#SLocationId option:selected").text();
//var ds = document.getElementById("DLocationId").value+", Gujarat";
//alert(ds);
ds = $("#DLocationId option:selected").text();
/*document.getElementById("orig").value = "";
document.getElementById("dest").value = "";*/

var origin = or,//new google.maps.LatLng(55.930385, -3.118425),
    destination = ds,//"Stockholm, Sweden",
    service = new google.maps.DistanceMatrixService();

service.getDistanceMatrix(
    {
        origins: [origin],
        destinations: [destination],
        travelMode: google.maps.TravelMode.DRIVING,
        avoidHighways: false,
        avoidTolls: false
    }, 
    callback
);

function callback(response, status) {
    var orig = document.getElementById("Loc");
        dest = document.getElementById("dest"),
        dist = document.getElementById("dist");

    if(status=="OK") {
        orig.value = response.destinationAddresses[0];
        dest.value = response.originAddresses[0];
        dist.value = response.rows[0].elements[0].distance.text;
    } else {
        alert("Error: " + status);
    }
}
return true;
}
function Time()
{
    var Avg_Speed = document.getElementById("speed").value;
    var Dist = document.getElementById("dist").value;
    //alert(Dist);
    var Avg_Time = "";
        
        Avg_Time = Math.round(parseInt(Dist) / parseInt(Avg_Speed));
        //alert (parseInt(Avg_Time));
        $("#Avg").val(Avg_Time);
      return true;  
}
</script>
<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>

<form name="SharedFare" method="POST" action="SharedFareAdd.php">
    <h1 align="center">Add Shared Fare </h1>  
    <table align="center" class="table table-striped" id="fareData" name="fareData">
        <tr>
            <td>From Location</td>
            <td>
                <select name="SLocationId" id="SLocationId" onchange=" return  Check();">
                    <?php
                    $GetAllLocations = GetAllLocations(0, 500);
                    while ($FetchAllLocations = mysql_fetch_array($GetAllLocations)) {
                        ?>
                        <option value="<?php echo $FetchAllLocations['Id']; ?>"><?php echo $FetchAllLocations['Name']; ?></option>
                        
                            <?php
                    }
                    ?>
                </select>
            </td>
        </tr>
        <tr>
            <td>Destination Location</td>
            <td><select name="DLocationId" id="DLocationId"  onchange=" return  Check();">
                    <?php
                    $GetAllLocations = GetAllLocations(0, 500);
                    while ($FetchAllLocations = mysql_fetch_array($GetAllLocations)) {
                        ?>
                        <option value="<?php echo $FetchAllLocations['Id']; ?>"><?php echo $FetchAllLocations['Name']; ?></option>
                        
                            <?php
                    }
                    ?>
                </select>
            </td>
        </tr>
        
        <tr>
            <td>Rate</td>
            <td><input type="text" id="rate" name="Rate" /> </td>
        </tr>
        <tr>
            <td>Your Distance</td><?php $speed = GetAvgSpeed(); ?>
            <td><input type="text" name="DistanceKm" id="dist"/><input type="hidden" name="speed" id="speed"  value="<?php echo $speed;?>" /></td>
        </tr>
        <tr>
            <td>Your Average Time Of Travel In Hours</td>
            <td><input type="text" name="TravelTime" id="Avg" onfocus="return Time();"/></td>
        </tr>
        <tr><td><input type="hidden" id="Loc"/></td>
            <td><input type="hidden" id="dest"/></td>
        </tr>
        <!--<tr id="viaheader" name='viaheader'>
            <td>Via Location name</td>
            <td>Name</td>
            <td>Price</td>
        </tr>

        <tr>
            <td>Via Location</td>
            <td><input type="text" name="via[]" style="width: 100px;" class="via"/></td>
            <td><input type="text" name="price[]" style="width: 50px;" /></td>
        </tr>-->
    </table>

    <div><hr /></div>

    <!--<div style="clear: both;" class="text-center"><a href="#" onclick="return AddMore();"  >+ Add More</a></div>-->

    <div style="clear: both; margin-top: 50px;" class="text-center"><input type="submit" name="submit"  value="submit" class="btn btn-large"/></div>

</form>
<?php
include_once Path() .'/templates/adminfooter.php';
?>
<script>
    /*var availableRoutes;
        function AddMore() {
            var newItem = '<tr><td>Via Location</td><td><input type="text" name="via[]" style="width: 100px;" class="via ui-autocomplete-input" autocomplete="off" /></td><td><input type="text" name="price[]" style="width: 50px;" /></td>';
            $("#fareData tbody").append(newItem);
            $('.via').autocomplete({source:availableRoutes});
        }*/

        // Location
        /*function GetCities() {
            var locationA = $("#SLocationId").val();
            var locationB = $("#DLocationId").val();
            $.ajax({
                type: "GET",
                url: "../ajax/location.php?type=location",
                data: "required=loglatlocation&locationA="+locationA+"&locationB="+locationB,
                success: function(data) {
                    alert(data);
                    obtainData = JSON.parse(data);
                    availableRoutes = obtainData;
                    $(".via").autocomplete({
                        source: availableRoutes
                    });
                    $(".via").bind("keydown.automomplete");
                }
            });
        }*/
</script>
