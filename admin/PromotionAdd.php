<?php

function Path() {
    $path = $_SERVER['DOCUMENT_ROOT'];
    // Differenciate the path for local and web host. Includes database connect file.
    if ($_SERVER['HTTP_HOST'] == "localhost" || $_SERVER['HTTP_HOST'] == "localhost:8080")
        $path .= '/Cabsystem';
    return $path;
}

include_once Path() ."/templates/adminheader.php";
include_once Path() ."/dbop/data/promotion.php";

if(isset($_POST) && !empty($_POST)){
    $Name = $_POST['Name'];
    $Description = $_POST['Description'];
    $Status = 1;       

    AddPromotion($Name, $Description, $Status);
    echo "<div class='alert alert-success'><a class='close' data-dismiss='alert'>x</a><strong>Promotion Added!</strong></div>";
}
?>
<form action="PromotionAdd.php" method="POST" name="promotionadd" >
    <table class="table table-striped">
        <tr>
            <th>Name</th>
            <td>
                <input type="text" name="Name">
            </td>
        </tr>
        <tr>
            <th>Description</th>
            <td>
                <textarea name="Description"></textarea>
            </td>
        </tr>
        <tr>
         <tr>
            <td colspan="2" style="text-align:center;">
                 <input type="submit" name="submit" value="Add" class="btn-success btn"/>
            </td>
         </tr>
</table>
</form>    

<?php
include_once Path() . '/templates/adminfooter.php';
?>
  
