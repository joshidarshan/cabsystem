<?php
if (!isset($_SESSION))
    session_start();
$_SESSION['cat'] = "";

function Path() {
    $path = $_SERVER['DOCUMENT_ROOT'];
    // Differenciate the path for local and web host. Includes database connect file.
    if ($_SERVER['HTTP_HOST'] == "localhost" || $_SERVER['HTTP_HOST'] == "localhost:8080")
        $path .= '/Cabsystem';
    return $path;
}

include_once Path() . '/templates/adminheader.php';
include_once Path() . '/dbop/data/role.php';
?>
<?php
if (isset($_POST['sub'])) {
    $Id = $_POST['Id'];
    $Role = $_POST['Role'];
    $Status = "";
    if (isset($_POST['Status']) && !empty($_POST['Status']))
        $Status = $_POST['Status'];
    else
        $Status = 0;
    EditUserRole($Id, $Role, $Status);
    echo "<script> alert ('Your Update SuccessFully'); window.location='Role.php';</script>";
}
$Id = $_REQUEST['Id'];
$GetRoleById = GetUsersRole($Id);
$FetchRoleById = mysql_fetch_array($GetRoleById);
?>

<form action="RoleEdit.php" method="post">
    <h1 align="center">Update Role</h1>
    <table align="center" class="table table-striped">
        <tr>
            <td>Role Id</td>
            <td><input type="hidden" name="Id" required value="<?php echo $Id; ?>"/></td>
        </tr>
        <tr>
            <td>Create A Role</td>
            <td><input type="text" name="Role" required value="<?php echo $FetchRoleById['Role']; ?>"/></td>
        </tr>
        <tr>
            <td>Current Status</td>
            <td>
                <?php if ($FetchRoleById['Status'] == 1) { ?>
                    <input type="checkbox" name="Status" value="1" checked="checked"/>
                <?php } else {
                    ?>
                    <input type="checkbox" name="Status" value="1"/> <?php } ?>
            </td>
        </tr>
        <tr>
            <td colspan="2" style="text-align: center;"><input type="submit" name="sub" value="Update" class="btn-large"/></td>
        </tr>
    </table>
</form>

<?php
include_once Path() . '/templates/adminfooter.php';
?>