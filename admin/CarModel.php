<?php
if (!isset($_SESSION))
    session_start();
$_SESSION['cat'] = "car";

function Path() {
    $path = $_SERVER['DOCUMENT_ROOT'];
    // Differenciate the path for local and web host. Includes database connect file.
    if ($_SERVER['HTTP_HOST'] == "localhost" || $_SERVER['HTTP_HOST'] == "localhost:8080")
        $path .= '/Cabsystem';
    return $path;
}

include_once Path() . '/templates/adminheader.php';
include_once Path() . '/dbop/data/carmodel.php';

$page = 1;
$CarModelStart = 0;
$CarModelRpp = 2;
$no = CarModelPagination();
$totalpage = ceil($no / $CarModelRpp);

if (isset($_GET['action'])) {
    if ($_GET['action'] == "delete") {

        $Id = $_GET['Id'];
        DeleteCarModel($Id);
        echo "<div class='alert alert-success'><a class='close' data-dismiss='alert'>x</a><strong>CarModel entered successfully.</strong></div>";
    }
}
if (isset($_GET['page'])) {
    $page = $_REQUEST['page'];
    $a = $page * $CarModelRpp;
    $CarModelStart = $a - $CarModelRpp;
}
$GetCarModel = GetCarModelListing($CarModelStart, $CarModelRpp);
?>
<script language="javascript">
    function del()
    {
        if (!confirm("Really Want to Delete A Car Model "))
            return false;
    }
</script>
<form action="CarModel.php" method="post">
    <h1 align="center">Car Listing</h1>
    <table align='center' class="table table-striped">
        <tr>
            <th>Action</th>
            <th>MakeId</th>
            <th>TypeId</th>
            <th>Name</th>
            <th>No Of Seats</th>
            <th>IsVirtual</th>
            <th>Seat Layout</th>
            <th>Default Km Rate</th>
            <th>Default Ac Km Rate</th>
            <th>Image</th>
            <th>Status</th>
        </tr>
        <?php
        while ($FetchCarModel = mysql_fetch_assoc($GetCarModel)) {
           
             $Image = substr($FetchCarModel['CarImage'], (stripos($FetchCarModel['CarImage'], '-') + 1), strlen($FetchCarModel['CarImage']));
            ?>
            <tr>
                <td><a href="CarModelEdit.php?Id=<?php echo $FetchCarModel['Id']; ?>">Edit</a> &nbsp; | &nbsp;
                    <a href="CarModel.php?action=delete&Id=<?php echo $FetchCarModel['Id']; ?>" onClick="return del();">Delete</a></td>
                <td><?php echo $FetchCarModel['cmName']; ?></td>
                <td><?php echo $FetchCarModel['ctName']; ?></td>
                <td><?php echo $FetchCarModel['Name']; ?></td>
                <td><?php echo $FetchCarModel['NoSeats']; ?></td>
                <?php if ($FetchCarModel['IsVirtual'] == 1){ ?> 
                <td><?php echo "Yes";?></td>
                <?php } else { ?>
                <td><?php echo "No";?></td><?php } ?>
                <td><?php echo $FetchCarModel['SeatLayout'];?></td>
                <td><?php echo $FetchCarModel['DefaultKmRate'];?></td>
                <td><?php echo $FetchCarModel['DefaultAcKmRate'];?></td>
                <td><?php echo $Image;?></td>
                <td><?php echo $FetchCarModel['Status']; ?></td>
            </tr>
            <?php
        }
        ?>
    </table>
    
    <div class="pagination pagination-centered">
            <ul>
                <?php
                for ($i = 1; $i <= $totalpage; $i++) {
                    if ($i == $page)
                        echo '<li class = "active"><a href="CarModel.php?page=' . $i . '">' . $i . '</a></li>';
                    else
                        echo '<li><a href="CarModel.php?page=' . $i . '">' . $i . '</a></li>';
                }
                ?>
            </ul>
        </div>
</form>

<?php include_once Path() . "/templates/adminfooter.php"; ?>

