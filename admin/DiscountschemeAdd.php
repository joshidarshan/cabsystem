<?php
if (!isset($_SESSION))
    session_start();
$_SESSION['cat'] = "";

function Path() {
    $path = $_SERVER['DOCUMENT_ROOT'];
    // Differenciate the path for local and web host. Includes database connect file.
    if ($_SERVER['HTTP_HOST'] == "localhost" || $_SERVER['HTTP_HOST'] == "localhost:8080")
        $path .= '/Cabsystem';
    return $path;
}

include_once Path() . '/templates/adminheader.php';
?>
<?php
if (isset($_POST['sub'])) {
    include_once Path() . '/dbop/data/discountscheme.php';

    $Name = $_POST['Name'];
    $ValidFrom = $_POST['ValidFrom'];
    $ValidTo = $_POST['ValidTo'];
    $LimitCount = $_POST['LimitCount'];
    $DiscountAmount = $_POST['DiscountAmount'];
    $DiscountPercent = $_POST['DiscountPercent'];
    $Status = 1;

    AddDiscountScheme($Name, $ValidFrom, $ValidTo, $LimitCount, $DiscountAmount, $DiscountPercent, $Status);
    echo "<div class='alert alert-success'><a class='close' data-dismiss='alert'>x</a><strong>Discount Scheme Added!</strong></div>";
}
?>

<form action="DiscountschemeAdd.php" method="post">
    <h1 align="center">Add Client Here</h1>
    <table align="center" class="table table-striped">
        <tr>
            <td>Enter Client Name</td>
            <td><input type="text" name="Name"/></td>

        </tr>

        <tr>
            <td>Valid From</td>
            <td><input type="text" name="ValidFrom"/></td>
        </tr>
        <tr>
            <td>Valid To</td>
            <td><input type="text" name="ValidTo"/></td>
        </tr>
        <tr>
            <td>Enter Client Limit Count</td>
            <td><textarea name="LimitCount"></textarea></td>
        </tr>
        <tr>
            <td>Enter Discount Amount</td>
            <td><input type="text" name="DiscountAmount"/></td>
        </tr>
        <tr>
            <td>Enter Discount Percent</td>
            <td><input type="text" name="DiscountPercent"/></td>
        </tr>



        <tr>
            <td colspan="2" style="text-align: center;"><input type="submit" name="sub" value="Add" class="btn-success btn"/></td>
        </tr>
    </table>
</form>

<?php
include_once Path() . '/templates/adminfooter.php';
?>