<?php
if (!isset($_SESSION))
    session_start();
$_SESSION['cat'] = "inventory";

function Path() {
    $path = $_SERVER['DOCUMENT_ROOT'];
    // Differenciate the path for local and web host. Includes database connect file.
    if ($_SERVER['HTTP_HOST'] == "localhost" || $_SERVER['HTTP_HOST'] == "localhost:8080")
        $path .= '/Cabsystem';
    return $path;
}

include_once Path() . '/templates/adminheader.php';

if (isset($_POST['sub'])) {

    include_once Path() . '/dbop/data/slider.php';

    $Name = $_REQUEST['Name'];
   
    $Description = $_REQUEST['Description'];
    $Status = 1;

    if (($_FILES["Image"]["type"] == "image/gif") || ($_FILES["Image"]["type"] == "image/jpeg") || ($_FILES["Image"]["type"] == "image/png" )) {
        $path = Path() . '/Upload/SliderImages/';
        $guid = uniqid();
       $Image = $guid . '-' . $_FILES['Image']['name'];
        move_uploaded_file($_FILES["Image"]["tmp_name"], $path . $Image);

        InsertImage($Name, $Image, $Description, $Status);
        echo "<div class='alert alert-success'><a class='close' data-dismiss='alert'>x</a><strong>Agent Added!</strong></div>";
    } 
    
else {
        echo "<div class='alert-error'><a class='close' data-dismiss='alert'>x</a><strong>Image Type InValid!</strong></div>";
        }
}
?>

<form action="SliderImageAdd.php" method="post" enctype="multipart/form-data" id="SliderImageAdd" >
    <h1 style="text-align: center;">Agent Add</h1>
    <table align="center" class="table table-striped">
        <tr>
            <td>Enter Image Name</td>
            <td><input type="text" name="Name"/></td>
        </tr>
        <tr>
            <td>Choose Image</td>
            <td><input type="file" name="Image"/></td>
        </tr>
        <tr>
            <td>Description</td>
            <td>
                <textarea name="Description"></textarea>
            </td>
        </tr>
        <td colspan="2" style="text-align: center;"><input type="submit" name="sub" value="Add" class="btn"/></td>
        </tr>
    </table>	
</form>

<?php
include_once Path() . '/templates/adminfooter.php';
?>

