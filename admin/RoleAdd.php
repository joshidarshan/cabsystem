<?php
if (!isset($_SESSION))
    session_start();
$_SESSION['cat'] = "";

function Path() {
    $path = $_SERVER['DOCUMENT_ROOT'];
    // Differenciate the path for local and web host. Includes database connect file.
    if ($_SERVER['HTTP_HOST'] == "localhost" || $_SERVER['HTTP_HOST'] == "localhost:8080")
        $path .= '/Cabsystem';
    return $path;
}

include_once Path() . '/templates/adminheader.php';
include_once Path() . '/dbop/data/role.php';

?>
<?php
if (isset($_POST['sub'])) {
    $Role = $_POST['Role'];
    $Status = 1;
    AddUserRole($Role, $Status);
   echo "<div class='alert alert-success'><a class='close' data-dismiss='alert'>x</a><strong>Role Added!</strong></div>";
}

?>
<script>
    function CheckRole() {
        var Roles = $("#Role").val();
        $.ajax({
            type: "GET",
            url: "../ajax/user.php?type=Role&required=UserRole&Role=" + Roles,
            success: function(data) {
                //alert(data);
                message = "";
                if (data == 0) {
                    message = "Already Exist";
                    $("#ChkRole").css({"color": "Red"});
                }
                else {
                    message = "Available";
                    $("#ChkRole").css({"color": "Green"});
                }
                $("#ChkRole").text(message);
            }
        });
    }
    function SubmitCheck(){
    var get = $("#ChkRole").text();

    if(get.toLowerCase() == "already exist")
        return false;
    else
        return true;
    }
</script>
<form action="RoleAdd.php" method="post" onsubmit="return SubmitCheck();">
    <h1 align="center">Add Role</h1>
    <table align="center" class="table table-striped">
        <tr>
            <td>Create A Role</td>
            <td><input type="text" name="Role" id="Role" required onblur="return CheckRole();"/> &nbsp;&nbsp;&nbsp
                <span id="ChkRole" style="position: absolute; font-size: 24px"></span></td>
        </tr>
        <tr>
            <td colspan="2" style="text-align: center;"><input type="submit" name="sub" value="Add" class="btn-large"  
                                                    onfocus="return CheckRole();" onclick="return CheckRole();"/></td>
        </tr>
    </table>
</form>

<?php
include_once Path() . '/templates/adminfooter.php';
?>