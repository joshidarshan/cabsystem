<?php
if (!isset($_SESSION))
    session_start();
$_SESSION['cat'] = "Route";

function Path() {
    $path = $_SERVER['DOCUMENT_ROOT'];
    // Differenciate the path for local and web host. Includes database connect file.
    if ($_SERVER['HTTP_HOST'] == "localhost" || $_SERVER['HTTP_HOST'] == "localhost:8080")
        $path .= '/Cabsystem';
    return $path;
}

include_once Path() . '/templates/adminheader.php';
include_once Path() . '/dbop/data/location.php';
include_once Path() . '/dbop/data/breakpoint.php';

if (isset($_POST['sub'])) {
    $slocationId = $_POST['SLocationId'];
    $dlocationId = $_POST['DLocationId'];   
    $name = $_POST['Name'];
    $description = $_POST['Description'];
    $maplocation = $_POST['MapLocation'];
    $Status = 1;
    InsertBreakpoint($slocationId, $dlocationId, $name, $description, $maplocation, $Status);
    echo "<div class='alert alert-success'><a class='close' data-dismiss='alert'>x</a><strong>BreakPoint Added.</strong></div>";
}
?>
<form action="BreakPointAdd.php" method="post">
    <table align="center" class="table table-striped">
        <tr>
            <td>Select Source Location</td>
            <td>
                <select name="SLocationId">
                    <?php
                    $GetAllLocations = GetAllLocations(0, 500);
                    while ($FetchAllLocations = mysql_fetch_array($GetAllLocations)) {
                        ?>
                        <option value="<?php echo $FetchAllLocations['Id']; ?>"><?php echo $FetchAllLocations['Name']; ?></option>
                        <?php
                    }
                    ?>
                </select>
            </td>
        </tr>
        <tr>
            <td>Enter Driver Name</td>
            <td><select name="DLocationId">
                    <?php
                    $GetAllLocations = GetAllLocations(0, 500);
                    while ($FetchAllLocations = mysql_fetch_array($GetAllLocations)) {
                        ?>
                        <option value="<?php echo $FetchAllLocations['Id']; ?>"><?php echo $FetchAllLocations['Name']; ?></option>
                        <?php
                    }
                    ?>
                </select>
            </td>
        </tr>
        <tr>
            <td>Enter Name</td>
            <td><input type="text" name="Name" required/></td>
        </tr>
        <tr>
            <td>Enter Description</td>
            <td><input type="text" name="Description"/></td>
        </tr>
        <tr>
            <td>Enter Map location</td>
            <td><input type="text" name="MapLocation"/></td>
        </tr>
        <tr>
            <td colspan="2" style="text-align: center;"><input type="submit" name="sub" value="Add" class="btn-success btn"/></td>
        </tr>
    </table>
</form>

<?php include_once Path() . "/templates/adminfooter.php"; ?>

