<?php

if (!isset($_SESSION))
    session_start();
$_SESSION['cat'] = "";

function Path() {
    $path = $_SERVER['DOCUMENT_ROOT'];
    // Differenciate the path for local and web host. Includes database connect file.
    if ($_SERVER['HTTP_HOST'] == "localhost" || $_SERVER['HTTP_HOST'] == "localhost:8080")
        $path .= '/Cabsystem';
    return $path;
}

include_once Path() . '/templates/adminheader.php';

if (isset($_POST['sub'])) {
    include_once Path() . '/dbop/data/carmake.php';

    $CarName = $_POST['CarName'];
    $CarStatus = 1;
    CarAdd($CarName, $CarStatus);
    echo "<div class='alert alert-success'><a class='close' data-dismiss='alert'>x</a><strong>Car Make Added!</strong></div>";
}
?>
<script>
    function CheckCarMakeName() {
        //alert("In Check");
        var CarName = $("#CarName").val();
        if (CarName) {
            $.ajax({
                type: "GET",
                url: "../ajax/car.php?type=Car&required=CarMakeName&Name=" + CarName,
                success: function(data) {
                    //alert(data);
                    message = "";
                    if (data == 0) {
                        message = "Already Exist";
                        $("#ChkCarName").css({"color": "Red"});
                        $("#ChkCarName").text(message);
                        return false;
                    }
                    else {
                        message = "Available";
                        $("#ChkCarName").css({"color": "Green"});
                        $("#ChkCarName").text(message);
                    }


                }
            });
        }
        else
        {
            return false;
        }
    }
    function submitCheck(){
    var get = $("#ChkCarName").text();

    if(get.toLowerCase() == "already exist")
        return false;
    else
        return true;
    }
    
</script>
<form action="CarMakeAdd.php" method="post" id="CarMakeAdd" onsubmit="return submitCheck();" >
    <h1 align="center">Add Car</h1>
    <table align="center" class="table table-striped">
        <tr>
            <td>Enter Car Name</td>
            <td><input type="text" name="CarName" id="CarName" required  onblur="return CheckCarMakeName();"/>
                <span id="ChkCarName" style="position: absolute"></span></td>
        </tr>
        <tr>
        <tr>
            <td colspan="2" style="text-align: center;"><input type="submit" name="sub" value="Add" class="btn-inverse btn-large" onfocus="return CheckCarMakeName();" onclick=" return CheckCarMakeName();"/></td>
        </tr>
    </table>
</form>

<?php include_once Path() . "/templates/adminfooter.php"; ?>
