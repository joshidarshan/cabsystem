<?php
if (!isset($_SESSION))
    session_start();
$_SESSION['cat'] = "";

function Path() {
    $path = $_SERVER['DOCUMENT_ROOT'];
    // Differenciate the path for local and web host. Includes database connect file.
    if ($_SERVER['HTTP_HOST'] == "localhost" || $_SERVER['HTTP_HOST'] == "localhost:8080")
        $path .= '/Cabsystem';
    return $path;
}

include_once Path() . '/templates/adminheader.php';
include_once Path() . '/dbop/data/carmake.php';

if (isset($_POST['sub'])) {
    $Id = $_POST['Id'];
    $CarName = $_POST['CarName'];
    $CarStatus = "";
    if (isset($_POST['CarStatus']) && !empty($_POST['CarStatus']))
        $CarStatus = $_POST['CarStatus'];
    else
        $CarStatus = 0;
    CarEdit($Id, $CarName, $CarStatus);
    echo "<script> alert ('Your Update SuccessFully'); window.location='CarMake.php';</script>";
}
$Id = $_REQUEST['Id'];
$Select = GetCarById($Id);
$CarFetch = mysql_fetch_array($Select);
?>
<script>
    function CheckCarMakeName() {
        var CarName = $("#CarName").val();
        if(CarName){
        $.ajax({
            type: "GET",
            url: "../ajax/car.php?type=Car&required=CarMakeName&Name=" + CarName,
            success: function(data) {
                //alert(data);
                message = "";
                if (data == 0) {
                    message = "Already Exist";
                    $("#ChkCarName").css({"color": "Red"});
                }
                else {
                    message = "Available";
                    $("#ChkCarName").css({"color": "Green"});
                }
                $("#ChkCarName").text(message);
            }
        });
        }
        else
        {
        return false;
        }
    }
</script>
<form action="CarMakeEdit.php" method="post">
    <h1 align="center">Add Car Here</h1>
    <table align="center" class="table table-striped">
        <tr>
            <td>Car Id</td>
            <td><input type="hidden" name="Id" value="<?php echo $Id; ?>"/></td>
        </tr>
        <tr>
            <td>Enter Car Name</td>
            <td><input type="text" name="CarName" value="<?php echo $CarFetch['Name']; ?>" 
                       id="CarName" required  onblur="return CheckCarMakeName();"/>
                       <span id="ChkCarName" style="position: absolute"></span>
            </td>
        </tr>
        <tr>
            <td>Current Status</td>
            <?php
            if ($CarFetch['Status'] == 1) {
                ?>
                <td>Click for Active<input type="checkbox" name="CarStatus" value="1"  checked="checked"/></td>
                <?php
            }
            ?>
            <?PHP if ($CarFetch['Status'] == 0) { ?>
                <td>Click for Active<input type="checkbox" name="CarStatus" value="1"/></td>
            <?php } ?>
        <tr>
        <tr>
            <td colspan="2" style="text-align: center;"><input type="submit" name="sub" value="Update" class="btn-success btn"/></td>
        </tr>
    </table>
</form>

<?php include_once Path() . "/templates/adminfooter.php"; ?>