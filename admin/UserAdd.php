<?php
if (!isset($_SESSION))
    session_start();
$_SESSION['cat'] = "";

function Path() {
    $path = $_SERVER['DOCUMENT_ROOT'];
    // Differenciate the path for local and web host. Includes database connect file.
    if ($_SERVER['HTTP_HOST'] == "localhost" || $_SERVER['HTTP_HOST'] == "localhost:8080")
        $path .= '/Cabsystem';
    return $path;
}

include_once Path() . '/templates/adminheader.php';
include_once Path() . '/dbop/data/role.php';
    
if (isset($_POST['sub'])) {
include_once Path() . '/dbop/data/user.php';
    $Id = MaxId();
    $Email = $_POST['Email'];
    $Phones = $_POST['Phones'];
    $Password = md5($_POST['Password']);
    $RoleId = 1;
    $Status = 1;
    AddUser($Id,$Email,$Phones, $Password, $RoleId,$Status);
  echo "<div class='alert alert-success'><a class='close' data-dismiss='alert'>x</a><strong>User Added!</strong></div>";
}
$GetAllRoles = GetAllUsersRoles($start = 0, $rpp = 500);


?>
<script>
    function CheckNumeric(e)
    {
                        var key=e.which;
			if(key>=48 && key<=57 || key==8)
			{
				return true;
			}
			else
			{
				return false;
			}
    }
</script>
<form action="UserAdd.php" method="post">
    <h1 align="center">Add User</h1>
    <table align="center" class="table table-striped">
        <tr>
            <td>Enter Email</td>
            <td><input type="text" name="Email" id="Email" required onblur="return ChkEmail();"  />
                <span id="ChkEmail"></span>
            </td>
            
        </tr>
        <tr>
            <td>Enter Contact No</td>
            <td><input type="text" name="Phones" id="Phones" required  onkeypress="return CheckNumeric(event);" maxlength="10" onblur="return ChkPhones();"/>
                <span id="ChkPhones"></span>
            </td>
        </tr>
        <tr>
            <td>Enter PassWord</td>
            <td><input type="password" name="Password" required /></td>
        </tr>
        <tr>
            <td colspan="2" style="text-align: center;"><input type="submit" name="sub" value="Add" class="btn-success btn"/></td>
        </tr>
    </table>
</form>
<script>
    function ChkPhones(){
     var Phones = $("#Phones").val();
     $.ajax({
         type:"GET",
         url:"../ajax/user.php?type=ContactNo&required=AdminContact&Contact="+Phones,
         success: function(data) {
                //alert(data);
                message = "";
                if(data == 0){
                    message = "Already Exist";
                    $("#ChkPhones").css({"color":"Red" });
                }
                else{
                    message = "Available";
                    $("#ChkPhones").css({"color":"Green" });
                }
                $("#ChkPhones").text(message);
        }
     });
    }
    
    function ChkEmail(){
     var Email = $("#Email").val();
     $.ajax({
         type:"GET",
         url:"../ajax/user.php?type=EmailId&required=AdminEmail&Email="+Email,
         success: function(data) {
                //alert(data);
                message = "";
                if(data == 0){
                    message = "Already Exist";
                    $("#ChkEmail").css({"color":"Red" });
                }
                else{
                    message = "Available";
                    $("#ChkEmail").css({"color":"Green" });
                }
                $("#ChkEmail").text(message);
        }
     });
    }
</script>    
<?php include_once Path() . "/templates/adminfooter.php"; ?>

