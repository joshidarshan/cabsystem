<?php
if (!isset($_SESSION))
    session_start();
$_SESSION['cat'] = "inventory";

function Path() {
    $path = $_SERVER['DOCUMENT_ROOT'];
    // Differenciate the path for local and web host. Includes database connect file.
    if ($_SERVER['HTTP_HOST'] == "localhost" || $_SERVER['HTTP_HOST'] == "localhost:8080")
        $path .= '/Cabsystem';
    return $path;
}

function AgentImageUrl() {
    $url = "http://" . $_SERVER['HTTP_HOST'];
    if ($_SERVER['HTTP_HOST'] == "localhost" || $_SERVER['HTTP_HOST'] == "localhost:8080")
        $url .= '/Cabsystem';
   
    return $url;
    
}


include_once Path() . '/dbop/data/Agent.php';
include_once Path() . '/dbop/data/user.php';

if (isset($_POST['sub'])) {
    $Id = $_REQUEST['id'];
    $Name = $_REQUEST['Name'];
    $Address = $_REQUEST['Address'];
    $Phones = $_REQUEST['Phones'];
    $Email = $_REQUEST['Email'];
    $Notes = $_REQUEST['Notes'];
    if (isset($_POST['status']) && !empty($_POST['status']))
        $Status = $_REQUEST['status'];
    else
        $Status = 0;
    $RoleId = 2;
    
    if (isset($_FILES['file']['name']) && !empty($_FILES['file']['name']) && $_FILES['file']['size'] > 0) {
        //$DocumentLink = $_FILES['DocLink'];
        $path = Path() . '/Upload/images/';
        $guid = uniqid();
        $fname = $guid . '-' . $_FILES['file']['name'];
        move_uploaded_file($_FILES["file"]["tmp_name"], $path . $fname);
        $imgnam = $fname;
    } else {
        if (isset($_POST['prevdoc']) && !empty($_POST['prevdoc']))
            $imgnam = $_POST['prevdoc'];
    }

    EditAgent($Id, $Name, $Address, $Phones, $Email, $Notes, $imgnam, $Status);
    EditUser($Id, $Email, $Phones, $RoleId, $Status);
    echo "<div class='alert alert-success'><a class='close' data-dismiss='alert'>x</a><strong>Agent edited.</strong></div>";
    header('location: agent.php');
}

include_once Path() . '/templates/adminheader.php';

if (isset($_GET['Id']) && !empty($_GET['Id'])) {
    $id = $_GET['Id'];
    $agentData = GetAgentById($id);
    $row = mysql_fetch_array($agentData);
    ?>

    <form action="AgentEdit.php" method="post" enctype="multipart/form-data">
        <input type="hidden" name="id" value="<?php echo $id; ?>"/>
        <table align="center" class="table table-striped">
            <tr>
                <td>Enter Your Name</td>
                <td><input type="text" name="Name" value="<?php echo $row['Name']; ?>" required/></td>
            </tr>
            <tr>
                <td>Enter Your Address</td>
                <td><textarea name="Address"><?php echo $row['Address']; ?></textarea></td>
            </tr>
            <tr>
                <td>Enter Your Contact Numbers</td>
                <td><input type="text" name="Phones"  value="<?php echo $row['Phones']; ?>"/></td>
            </tr>
            <tr>
                <td>Enter Your Email Id</td>
                <td><input type="text" name="Email"  value="<?php echo $row['Email']; ?>"/></td>
            </tr>
            <tr>
                <td>Enter Your Notes</td>
                <td><textarea name="Notes"><?php echo $row['Notes']; ?></textarea></td>
            </tr>
            <tr>
                <td>Previous Image</td>
                <td><?php $imgPath = AgentImageUrl()."/Upload/images/$row[ImageLink]"; if(!empty($row['ImageLink'])) echo "<img src='$imgPath'></img>"; else echo "No Image found";?></td>
            </tr>	
            <tr>
                <td>Add New</td>
                <td><input type="file" name="file"/></td>
                <td></td>
            </tr>
            <tr>
                <td>Choose Agent Active/Deactive</td>
                <?php
                if ($row['Status'] == 1) {
                    ?>
                    <td><input type="checkbox" name="status" value="1"  checked="checked" /></td>
                    <?php
                }
                ?>
                <?PHP if ($row['Status'] == 0) { ?>
                    <td><input type="checkbox" name="status" value="1" /></td>
                    <?php
                }
                ?>
            </tr>
            <tr>
                <td colspan="2" style="text-align: center;"><input type="submit" name="sub" value="Add" class="btn-large"/></td>
            </tr>
            <?php
        }
        ?>
    </table>	
    </form>
    <?php
    include_once Path() . '/templates/adminfooter.php';
    ?>
