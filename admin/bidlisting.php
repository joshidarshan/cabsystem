<?php
if (!isset($_SESSION))
    session_start();
$_SESSION['cat'] = "bid";

function BPath() {
    $path = $_SERVER['DOCUMENT_ROOT'];
    // Differenciate the path for local and web host. Includes database connect file.
    if ($_SERVER['HTTP_HOST'] == "localhost" || $_SERVER['HTTP_HOST'] == "localhost:8080")
        $path .= '/Cabsystem';
    return $path;
}

include_once BPath() . '/templates/adminheader.php';
include_once BPath() . '/dbop/data/bidcab.php';
include_once BPath() . '/dbop/data/carmodel.php';

if (function_exists('date_default_timezone_set')) {
    date_default_timezone_set('Asia/Calcutta');
}
$fromYear = date('Y');
$tday = date('d');
$tmonth = date('m');
$toYear = date('Y', strtotime('+1 year'));
?>
<h2 style="text-align: center;">Bid Listing</h2>

<form action="bidlisting.php" method="POST">
    <table class="table table-striped">
        <tr>
            <td>From DateTime</td>
            <td><input type="text" id="FromDate" name="FromDate" class="Date"/></td>

            <td>To DateTime</td>
            <td><input type="text" id="ToDate" name="ToDate" class="Date"/></td>

            <td colspan="2" style="text-align: center;"><input type="submit" class="btn-large" name="submit" value="Submit" /></td>
        </tr>
    </table>
</form>

<?php
if (isset($_POST['FromDate']) && !empty($_POST['FromDate']) && isset($_POST['ToDate']) && !empty($_POST['ToDate'])) {
    $FromDate = $_POST['FromDate'];
    $ToDate = $_POST['ToDate'];
    echo "<input type='hidden' id='from' value='$FromDate' /><input type='hidden' id='to' value='$ToDate' />";
    $getBidBookingForDate = GetBidBookingTemp($FromDate, $ToDate);
    $tblBuilder = "<table class='table table-striped'>"
            . "<tr>"
            . "<th>Client</th>"
            . "<th>Route</th>"
            . "<th>Modle=Bid</th>"
            . "<th>Prefered-Date-1</th>"
            . "<th>Prefered-Date-2</th>"
            . "</tr>";
    echo $tblBuilder;
    if ($getBidBookingForDate) {
        while ($rb = mysql_fetch_assoc($getBidBookingForDate)) {
            // Splitting car model bid for client
//cl.Name as client, sl.Name as Source, dl.Name as Destination, bc.`DateTime-1` as DT1, bc.`DateTime-2` as DT2, bc.NoPassenger as NoPassengers, bc.ModelBid as ModelBid            
            $carModelId = $rb['ModelBid'];

            // modelId to name string builder
            $clientCarModelNameBid = array();
            $splitBid = split(",", $rb['ModelBid']);
            foreach ($splitBid as $sb) {
                $splitData = split("x", strtolower($sb));
                if (!empty($splitData[1])) {
                    $modelQuery = GetCarModelById(substr($splitData[0], 1, strlen($splitData[0])));
                    $modelData = mysql_fetch_assoc($modelQuery);
                    $clientCarModelNameBid[] = $modelData['Name'] . "=" . $splitData[1];
                }
            }
            $cm = implode(", ", $clientCarModelNameBid);
            echo "<tr>";
            echo "<td>$rb[client]</td>";
            echo "<td>$rb[Source] -> $rb[Destination]</td>";
            echo "<td>$cm</td>";
            echo "<td>$rb[DT1]</td>";
            echo "<td>$rb[DT2]</td>";
        }
    } else {
        echo "No data available";
    }
    echo "</table>";
}
?>

<script>
    $(function() {
        $('.Date').appendDtpicker({
            "minuteInterval": 15,
            "current": "<?php echo $fromYear . '-' . $tmonth . '-' . $tday ?> 00:15"
        });
    });
    
    $(document).ready(function(){
        from = $("#from").val();
        to = $("#to").val();
        
        if(from && to){
            $("#FromDate").val(from);
            $("#ToDate").val(to);
        }
    });
</script>