<?php
if (!isset($_SESSION))
    session_start();
$_SESSION['cat'] = "";

function Path() {
    $path = $_SERVER['DOCUMENT_ROOT'];
    // Differenciate the path for local and web host. Includes database connect file.
    if ($_SERVER['HTTP_HOST'] == "localhost" || $_SERVER['HTTP_HOST'] == "localhost:8080")
        $path .= '/Cabsystem';
    return $path;
}

include_once Path() . '/templates/adminheader.php';
include_once Path() . '/dbop/data/Couponcode.php';
?>
<?php
if (isset($_POST['sub'])) {

    $Id = $_POST['Id'];
    $Name = $_POST['Name'];
    $ValidFrom = $_POST['ValidFrom'];
    $ValidTo = $_POST['ValidTo'];
    $LimitCount = $_POST['LimitCount'];
    $DiscountAmount = $_POST['DiscountAmount'];
    $DiscountPercent = $_POST['DiscountPercent'];
    $Status = "";

    if (isset($_POST['Status']) && !empty($_POST['Status']))
        $Status = $_POST['Status'];
    else
        $Status = 0;

    EditCouponcode($Id, $Name, $ValidFrom, $ValidTo, $LimitCount, $DiscountAmount, $DiscountPercent, $Status);
    echo "<script> alert ('Your Update SuccessFully'); window.location='Couponcode.php';</script>";
}
$Id = $_REQUEST['Id'];
$GetCouponcode = GetCouponcode($Id);
$FetchCOuponcode = mysql_fetch_array($GetCouponcode);
?>

<form action="CouponcodeEdit.php" method="post">
    <h1 align="center">Add Client Here</h1>
    <table align="center" class="table table-striped">
        <tr>
            <td>Enter Client Id</td>
            <td><input type="hidden" name="Id" value="<?php echo $Id; ?>"/></td>
        </tr>
        <tr>
            <td>Enter Client Name</td>
            <td><input type="text" name="Name" value="<?php echo $FetchCOuponcode['Name']; ?>" required/></td>
        </tr>
        <tr>
            <td>Valid From</td>
            <td><input type="text" name="ValidFrom" value="<?php echo $FetchCOuponcode['ValidFrom']; ?>" required/></td>
        </tr>
        <tr>
            <td>Valid To</td>
            <td><input type="text" name="ValidTo" value="<?php echo $FetchCOuponcode['ValidTo']; ?>" required/></td>
        </tr>
        <tr>
            <td>Enter Client Limit Count</td>
            <td><textarea name="LimitCount" required><?php echo $FetchCOuponcode['LimitCount']; ?></textarea></td>
        </tr>
        <tr>
            <td>Enter Discount Amount</td>
            <td><input type="text" name="DiscountAmount" value="<?php echo $FetchCOuponcode['DiscountAmount']; ?>"/></td>
        </tr>
        <tr>
            <td>Enter Discount Percent</td>
            <td><input type="text" name="DiscountPercent" value="<?php echo $FetchCOuponcode['DiscountPercent']; ?>"/></td>
        </tr>
        <tr>
            <td>Current Status</td>
            <?php
            if ($FetchCOuponcode['Status'] == 1) {
                ?>

                <td>Click for Active<input type="checkbox" name="Status" value="1"  checked="checked"/></td>
                <?php
            }
            ?>
            <?PHP if ($FetchCOuponcode['Status'] == 0) { ?>
                <td>Click for Active<input type="checkbox" name="Status" value="1"/></td>
            <?php } ?>
        </tr>

        <tr>
            <td colspan="2" style="text-align: center;"><input type="submit" name="sub" value="Update" class="btn btn-success btn"/></td>
        </tr>
    </table>
</form>

<?php
include_once Path() . '/templates/adminfooter.php';
?>