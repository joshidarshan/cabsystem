<?php
if (!isset($_SESSION))
    session_start();
$_SESSION['cat'] = "Route";

function Path() {
    $path = $_SERVER['DOCUMENT_ROOT'];
    // Differenciate the path for local and web host. Includes database connect file.
    if ($_SERVER['HTTP_HOST'] == "localhost" || $_SERVER['HTTP_HOST'] == "localhost:8080")
        $path .= '/Cabsystem';
    return $path;
}
$val1 ="";
$val2 = "";
if (function_exists('date_default_timezone_set'))
{
  date_default_timezone_set('Asia/Calcutta');
}

$fromYear = date('Y');
$tday = date('d');
$tmonth = date('m');
$toYear = date('Y', strtotime('+1 year'));

include_once Path() . '/dbop/data/cabroute.php';
include_once Path() . '/templates/adminheader.php';

$totalpage = 0;
if (isset($_GET['action'])) {
    if ($_GET['action'] == "delete") {

        $Id = $_GET['Id'];
        DeleteCabRoute($Id);
        echo "<script> alert('Your Record Delete SuccessFully');</script>";
    }
}

$page = 1;
$start = 0;
$rpp = 10;
if (isset($_GET['page'])) {
    $page = $_REQUEST['page'];
    $a = $page * $rpp;
    $start = $a - $rpp;
$val1= $_SESSION['FromDateTime'];
   $val2= $_SESSION['ToDateTime'] ;
    $AllCabRoutes =GetAllCabRoutes($val1,$val2, $start, $rpp);
}
if(empty($_SESSION['FromDateTime']) && !isset($_GET['page'])){
    $tomorrow = date("Y-m-d", time() + (86400));
    $today = date("Y-m-d");
    $AllCabRoutes = GetAllCabRoutes($today, $tomorrow);
}
if (function_exists('date_default_timezone_set')) {
    date_default_timezone_set('Asia/Calcutta');
}
$fromYear = date('Y');
$tday = date('d');
$tmonth = date('m');
$toYear = date('Y', strtotime('+1 year'));
?>
<?php
    if(isset($_POST['sub']))
    {
        $start = $_POST['FromDateTime'];
        $end = $_POST['ToDateTime'];
        //echo $start.",".$end;
    }
    ?>
<script language="javascript">
    function del() {
        var r = confirm("Really Want to Delete A CabRoute");
        if (!r)
            return false;
    }
     $(function() {
        var fromdt = $("#hfromdt").val();
        var todt = $("#htodt").val();
        if(fromdt && todt){
            $("#fromDate").val(fromdt);
            $("#toDate").val(todt);
            $('.Date').appendDtpicker({
                //"inline": true,
                "minuteInterval": 15,
                "current": "<?php echo $fromYear . '-' . $tmonth . '-' . $tday ?> 00:15"
            });
        }
        else{
            $('.Date').appendDtpicker({
                //"inline": true,
                "minuteInterval": 15,
                "current": "<?php echo $fromYear . '-' . $tmonth . '-' . $tday ?> 00:15"
            });
        }
    });
    
</script>
<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
  <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
  <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<form action="CabRoutePanding.php" method="post">
    <table style="margin-left: 460px">
            <tr>
            <td>From Date & Time</td>
            <td><input type="text" name="FromDateTime" id="fromDate" class="Date" required/></td>
            <td>To Date & Time</td>
            <td><input type="text" name="toDate" class="Date" required/></td>
        </tr>
        
        <tr>
            <td colspan="2" style="text-align: center"><input type="submit" name="go" value="Go" class="btn large"/></td>
        </tr>
        <script>
            $(function(){
				$('.dttm').datepicker({dateFormat: 'yy-mm-dd'});
				
			});
        </script>
    </table>
    
    <h1 align="center">CabRoute Listing</h1>
    <table align='center' class="table table-striped">
        <tr>
            <th>Action</th>
            <th>Cab</th>
            <th>From Date Time</th>
            <th>Source</th>
            <th>Destination</th>
            <th>To Date Time</th>
            <th>Proper Location</th>
            <th>Is Return Trip</th>
            <th>Agent Name</th>
            <th>Driver Name</th>
            <th>Is Via</th>
            <th>Car Model</th>
            <th>Status</th>
        </tr>
        <?php
        if(isset($_POST['go'])){
            //$val1 = $_POST['FromDateTime'];
            //$val2 = $_POST['ToDateTime'];
       $page = 1;
$start = 0;
$rpp = 2;
 $val1 = $_POST['FromDateTime'];
            $val2 = $_POST['ToDateTime'];
           
$no = CabRoutesPagination($val1,$val2);
$totalpage = ceil($no / $rpp);

            $_SESSION['FromDateTime'] = $val1;
        $_SESSION['ToDateTime'] = $val2;
     $AllCabRoutes = GetAllPandingCabRoutes($val1, $val2,$start, $rpp);
        
        }
        if(isset($AllCabRoutes)&& !empty($AllCabRoutes)){
            
        
        while ($FetchAllCabRoutes = mysql_fetch_array($AllCabRoutes)) {
            ?>
        <tr>
                <td>
                    <a href="CabRouteEdit.php?Id=<?php echo $FetchAllCabRoutes['Id']; ?>">Edit</a> &nbsp; | &nbsp;
                    <a href="CabRoutePanding.php?action=delete&Id=<?php echo $FetchAllCabRoutes['Id']; ?>" onClick="return del();">Delete</a>&nbsp; | &nbsp;
                    <a href="CabRouteConform.php?Id=<?php echo $FetchAllCabRoutes['Id']; ?>">Conform</a>
                </td>
                <td><?php echo $FetchAllCabRoutes['CName']; ?></td>
                <td><?php echo $FetchAllCabRoutes['FromDateTime']; ?></td>
               <td><?php echo $FetchAllCabRoutes['LName']; ?></td>
 <td><?php echo $FetchAllCabRoutes['DsName']; ?></td>
                <td><?php echo $FetchAllCabRoutes['ToDateTime']; ?></td>
                <td><?php echo $FetchAllCabRoutes['OriginalLocation']; ?></td>
                <?php if( $FetchAllCabRoutes['IsReturnTrip'] == 1) {?>
                <td><?php echo "Yes"; ?></td>
                <?php } else { ?>
                <td><?php echo "No"; ?></td> <?php } ?>
                <td><?php echo $FetchAllCabRoutes['ANam']; ?></td>
                <td><?php echo $FetchAllCabRoutes['DName']; ?></td>
                <?php if($FetchAllCabRoutes['IsVia']==1) $IsVia = "Yes"; else $IsVia = "No"; ?> 
                <td><?php echo $IsVia; ?></td>
                <td><?php echo $FetchAllCabRoutes['ModelName']; ?></td>
                <td><?php echo $FetchAllCabRoutes['Status']; ?></td>
            </tr>
            <?php
        }
        }
        ?>
    
    </table>
    
    <div class="pagination pagination-centered">
        <ul>
            <?php
            for ($i = 1; $i <= $totalpage; $i++) {
                if ($i == $page)
                    echo '<li class = "active"><a href="CabRoutePanding.php?page=' . $i . '">' . $i . '</a></li>';
                else
                    echo '<li><a href="CabRoutePanding.php?page=' . $i . '">' . $i . '</a></li>';
            }
            ?>
        </ul>
    </div>
</form>

<?php 
include_once Path() . '/templates/adminfooter.php'; 
?>
