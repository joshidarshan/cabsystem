<?php
if (!isset($_SESSION))
    session_start();
$_SESSION['cat'] = "car";

function Path() {
    $path = $_SERVER['DOCUMENT_ROOT'];
    // Differenciate the path for local and web host. Includes database connect file.
    if ($_SERVER['HTTP_HOST'] == "localhost" || $_SERVER['HTTP_HOST'] == "localhost:8080")
        $path .= '/Cabsystem';
    return $path;
}

include_once Path() . '/templates/adminheader.php';
include_once Path() . '/dbop/data/carmodel.php';
include_once Path() . '/dbop/data/carmake.php';
include_once Path() . '/dbop/data/cartype.php';
?>
<?php
if (isset($_POST['sub'])) {
    $CarMakeId = $_POST['CarMakeId'];
    $CarTypeId = $_POST['CarTypeId'];
    $CarModelName = $_POST['CarModelName'];
    $Seats = $_POST['Seats'];
    if (isset($_POST['IsVirtual']))
        $Isvirtual = '1';
    else
        $Isvirtual = '0';
    
    $SeatLayout = "";
   //if($_POST['RowA'])&&!empty($_POST['RowA'])
    //if(isset($_POST['Row']))&& !empty($_POST['Row'])
    $tp= implode(',', $_POST['Row']);
    $seatData = explode(",", $tp);
    
    if(isset($seatData[0]) && !empty($seatData[0]))
        $SeatLayout .= "Ax$seatData[0]";
    if(isset($seatData[1]) && !empty($seatData[1]))
        $SeatLayout .= ",Bx$seatData[1]";
    if(isset($seatData[2]) && !empty($seatData[2]))
        $SeatLayout .= ",Cx$seatData[2]";
    if(isset($seatData[3]) && !empty($seatData[3]))
        $SeatLayout .=",Dx$seatData[3]";
    if(isset($seatData[4]) && !empty($seatData[4]))
        $SeatLayout .=",Ex$seatData[4]";
    if(isset($seatData[5]) && !empty($seatData[5]))
        $SeatLayout .=",Fx$seatData[5]";
    //echo $SeatLayout;
    $DefaultKmRate = $_POST['DefaultKmRate'];
    $DefaultAcKmRate = $_POST['DefaultKmRate'];
    $Status = 1;
    
     if (($_FILES["file"]["type"] == "image/gif") || ($_FILES["file"]["type"] == "image/jpeg") || ($_FILES["file"]["type"] == "image/png" )) {
        $path = Path() . '/Upload/images/';
        $guid = uniqid();
        $fname = $guid . '-' . $_FILES['file']['name'];
        move_uploaded_file($_FILES["file"]["tmp_name"], $path . $fname);
        $CarImage = $fname;
        
    CarModelAdd($CarMakeId, $CarTypeId, $CarModelName, $Seats, $Isvirtual, $SeatLayout, $DefaultKmRate, $DefaultAcKmRate,$CarImage, $Status);
    echo "<div class='alert alert-success'><a class='close' data-dismiss='alert'>x</a><strong>Car Model Added.</strong></div>";
}
}
$GetCarMake = GetCarsListing($CarStart = 0, $CarRpp = 500);
$GetCarType = GetCarTypesListing($CarStart = 0, $CarRpp = 500);
?>

<form action="CarModelAdd.php" method="post" enctype="multipart/form-data">
    <h1 align="center">Add Car Model Here</h1>
    <table align="center" class="table table-striped">
        <tr>
            <td>Choose A Made Car</td>
            <td><select name="CarMakeId">
                    <?php while ($fetchCarMake = mysql_fetch_array($GetCarMake)) { ?>
                        <option value="<?php echo $fetchCarMake['Id']; ?>"><?php echo $fetchCarMake['Name']; ?></option>
                    <?php } ?>

                </select></td>
        </tr>

        <tr>
            <td>Choose A Car Type</td>
            <td><select name="CarTypeId">
                    <?php while ($fetchCarType = mysql_fetch_array($GetCarType)) { ?>
                        <option value="<?php echo $fetchCarType['Id']; ?>"><?php echo $fetchCarType['Name']; ?></option>
                    <?php } ?>

                </select></td>
        </tr>

        <tr>
            <td>Enter Model Name</td>
            <td><input type="text" name="CarModelName" required /></td>
        </tr>

        <tr>
            <td>Number of Seats</td>
            <td><input type="text" name="Seats" id="Seats" /> <span>Including driver</span></td>
        <tr>
            <td>Is Virtual</td>
            <td><input type="checkbox" name="IsVirtual"></td>
        </tr>

        <tr>
            <td>Enter Seat Layout</td>
            <td>
                A &nbsp;<input  type="text" name="Row[]" class="input-mini"/><br/>
                B &nbsp;<input  type="text" name="Row[]" class="input-mini"/><br/>
                C &nbsp;<input  type="text" name="Row[]" class="input-mini"/><br/>
                D &nbsp;<input  type="text" name="Row[]" class="input-mini"/><br/>
                E &nbsp;<input  type="text" name="Row[]" class="input-mini"/><br/>
                F &nbsp;<input  type="text" name="Row[]" class="input-mini"/><br/>
            </td>
        </tr>
        <tr>
            <td>Enter Default Km. Rate</td>
            <td><input type="text" name="DefaultKmRate"/></td>
        </tr>
        <tr>
            <td>Enter Default Ac Km. Rate</td>
            <td><input type="text" name="DefaultAcKmRate"/></td>
        </tr>
        <tr>
            <td>Choose Image</td>
            <td><input type="file" name="file"/></td>
        </tr>
        <tr>
            <td colspan="2" style="text-align: center;"><input type="submit" name="sub" value="Add" class="btn-success btn"/></td>
        </tr>
    </table>
</form>

<?php
include_once Path() . '/templates/adminfooter.php';
?>