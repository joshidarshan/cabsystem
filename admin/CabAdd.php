<?php
if (!isset($_SESSION))
    session_start();
$_SESSION['cat'] = "inventory";

function Path() {
    $path = $_SERVER['DOCUMENT_ROOT'];
    // Differenciate the path for local and web host. Includes database connect file.
    if ($_SERVER['HTTP_HOST'] == "localhost" || $_SERVER['HTTP_HOST'] == "localhost:8080")
        $path .= '/Cabsystem';
    return $path;
}

include_once Path() . '/templates/adminheader.php';
include_once Path() . '/dbop/data/Cab.php';

if (isset($_POST['sub'])) {
    $CarModelId = $_POST['CarModelId'];
    $NoPlate = $_POST['NoPlate'];
    $Rating = $_POST['Rating'];
    $MadeYear = $_POST['MadeYear'];
    $AgentId = $_POST['AgentId'];
    $KmRate = $_POST['KmRate'];
    $AcKmRate = $_POST['AcKmRate'];
    $DocLink = $_FILES['file'];
    $Status = 1;
    
    if (isset($_FILES["file"]["name"]) && $_FILES["file"]["name"]>0&& ($_FILES["file"]["type"] == "image/gif") || ($_FILES["file"]["type"] == "image/jpeg") || ($_FILES["file"]["type"] == "image/png" )) {
        $path = Path().'/upload/images/';
        $guid = uniqid();
        move_uploaded_file($_FILES["file"]["tmp_name"], $path . $guid .'-'. $_FILES['file']["name"]);
        $imgnam = $guid .'-'. $DocLink['name'];
        $imgtype = $DocLink['type'];
        InsertCab($CarModelId, $NoPlate, $Rating, $MadeYear, $AgentId, $KmRate, $AcKmRate, $imgnam, $Status);
        echo "<div class='alert alert-success'><a class='close' data-dismiss='alert'>x</a><strong>Cab Added!</strong></div>";
    } else {
        InsertCab($CarModelId, $NoPlate, $Rating, $MadeYear, $AgentId, $KmRate, $AcKmRate, '', $Status);
        echo "<div class='alert alert-success'><a class='close' data-dismiss='alert'>x</a><strong>Cab Added!</strong></div>";
    }
}
include_once Path() . '/dbop/data/Carmodel.php';
include_once Path() . '/dbop/data/Agent.php';

$GetAllCarModels = GetCarModelListing(0, 500);
$GetAllAgents = GetAllAgents($start = 0, $rpp = 500);
?>
<script>
    function CheckNoPlate() {
        //alert("In Check");
        var NoPlate = $("#NoPlate").val();
        if (NoPlate) {
            $.ajax({
                type: "GET",
                url: "../ajax/car.php?type=Car&required=CarNoPlate&NoPlate=" + NoPlate,
                success: function(data) {
                    //alert(data);
                    message = "";
                    if (data == 0) {
                        message = "Already Exist";
                        $("#ChkNOPlate").css({"color": "Red"});
                        $("#ChkNOPlate").text(message);
                        return false;
                    }
                    else {
                        message = "Available";
                        $("#ChkNOPlate").css({"color": "Green"});
                        $("#ChkNOPlate").text(message);
                    }


                }
            });
        }
        else
        {
            return false;
        }
    }
    function submitCheck(){
    var get = $("#ChkNOPlate").text();

    if(get.toLowerCase() == "already exist")
        return false;
    else
        return true;
    }
    
</script>

<form action="CabAdd.php" method="post" enctype="multipart/form-data" onsubmit="return submitCheck();">
    <h1 align="center">Add Cab</h1>
    <table align="center" class="table table-striped">
        <tr>
            <td>Choose A Car Model</td>
            <td> <select name="CarModelId">
                    <?php
                    while ($FetchAllCarModels = mysql_fetch_array($GetAllCarModels)) {
                        ?>
                        <option value="<?php echo $FetchAllCarModels['Id']; ?>"><?php echo $FetchAllCarModels['Name']; ?> </option>
                    <?php } ?>
                </select></td>
        </tr>
        <tr>
            <td>Enter No. Plate</td>
            <td><input type="text" name="NoPlate" id="NoPlate" required onblur="return CheckNoPlate();"/>&nbsp;&nbsp;&nbsp;<span id="ChkNOPlate" style="position: absolute"></span></td>
        </tr>
        <tr>
            <td>Enter Rating</td>
            <td>
                <select id="Rating" name="Rating">
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                </select>
            </td>
        </tr>
        <tr>
            <td>Enter Made Year</td>
            <td><input type="text" name="MadeYear" /></td>
        </tr>
        <tr>
            <td>Choose Agent</td>
            <td> <select name="AgentId">
                    <?php
                    while ($FetchAllAgents = mysql_fetch_array($GetAllAgents)) {
                        ?>
                        <option value="<?php echo $FetchAllAgents['Id']; ?>"><?php echo $FetchAllAgents['Name']; ?>-<?php echo $FetchAllAgents['Phones']; ?> </option>
                    <?php } ?>
                </select></td>
        </tr>
        <tr>
            <td>Enter Km. Rate</td>
            <td><input type="text" name="KmRate" /></td>
        </tr>
        <tr>
            <td>Enter Ac Km. Rate</td>
            <td><input type="text" name="AcKmRate" /></td>
        </tr>
        <tr>
            <td>Document Link</td>
            <td><input type="file" name="file"/></td>
        </tr>
        <tr>
            <td colspan="2" align="center"><input type="submit" name="sub" value="Add"/></td>
        </tr>
    </table>
</form>
<?php
include_once Path() . '/templates/adminfooter.php';
?>
