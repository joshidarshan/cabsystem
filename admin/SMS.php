<?php
if (!isset($_SESSION))
    session_start();
$_SESSION['cat'] = "";

function Path() {
    $path = $_SERVER['DOCUMENT_ROOT'];
    // Differenciate the path for local and web host. Includes database connect file.
    if ($_SERVER['HTTP_HOST'] == "localhost" || $_SERVER['HTTP_HOST'] == "localhost:8080")
        $path .= '/Cabsystem';
    return $path;
}

include_once Path() . '/templates/adminheader.php';
include_once Path() . '/dbop/data/sms.php';

$page = 1;
$start = 0;
$rpp = 2;
$no = SMSPagination();
$totalpage = ceil($no / $rpp);
if (isset($_GET['action'])) {
    if ($_GET['action'] == "delete") {
        echo $_GET['action'];
        $Id = $_GET['Id'];
        DeleteSMS($Id);
        echo "<script> alert('Your Record Delete SuccessFully');</script>";
    }
}
if (isset($_GET['page'])) {
    $page = $_REQUEST['page'];
    $a = $page * $rpp;
    $start = $a - $rpp;
}
$GetAllSMS = GetAllSMS($start, $rpp);
?>
<script language="javascript">
    function del()
    {
        var r = confirm("Really Want to Delete An Agent");
        if (!r)
            return false;
    }
</script>
<form action="SMS.php">
    <h1 align="center">Car Listing</h1>
    <table align='center' class="table table-striped">
        <tr>
            <th>Action</th>
            <th>From</th>
            <th>Message</th>
            <th>Status</th>
        </tr>
        <?php
        while ($FetchSMS = mysql_fetch_array($GetAllSMS)) {
            ?>
            <tr>
                <td>
                    <a href="SMSEdit.php?Id=<?php echo $FetchSMS['Id']; ?>">Edit</a> &nbsp; | &nbsp;
                    <a href="SMS.php?action=delete&Id=<?php echo $FetchSMS['Id']; ?>" onClick="return del();">Delete</a>
                </td>
                <td><?php echo $FetchSMS['From']; ?></td>
                <td><?php echo $FetchSMS['Message']; ?></td>
                <td><?php echo $FetchSMS['Status']; ?></td>
            </tr>
            <?php
        }
        ?>
    </table>
    <div class="pagination pagination-centered">
        <ul>
            <?php
            for ($i = 1; $i <= $totalpage; $i++) {
                if ($i == $page)
                    echo '<li class = "active"><a href="SMS.php?page=' . $i . '">' . $i . '</a></li>';
                else
                    echo '<li><a href="SMS.php?page=' . $i . '">' . $i . '</a></li>';
            }
            ?>
        </ul>
    </div>

</form>

<?php include_once Path() . "/templates/adminfooter.php"; ?>
