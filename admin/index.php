<?php
if (!isset($_SESSION))
    session_start();
$_SESSION['cat'] = "home";

function CPath() {
    $path = $_SERVER['DOCUMENT_ROOT'];
    // Differenciate the path for local and web host. Includes database connect file.
    if ($_SERVER['HTTP_HOST'] == "localhost" || $_SERVER['HTTP_HOST'] == "localhost:8080")
        $path .= '/Cabsystem';
    return $path;
}

include_once CPath() . "/templates/adminheader.php";
include_once CPath() . '/bizop/Views/CabListing.php';
include_once CPath() . '/dbop/data/listingutility.php';
include_once CPath() . '/bizop/VirtualCab.php';
include_once CPath() . '/dbop/data/bidcab.php';

if (function_exists('date_default_timezone_set')) {
    date_default_timezone_set('Asia/Calcutta');
}
$fromYear = date('Y');
$tday = date('d');
$tmonth = date('m');
$toYear = date('Y', strtotime('+1 year'));
?>
<script type="text/javascript" src="../js/jquery.tablesorter.min.js"></script>
<link rel="stylesheet" href="../css/blue/style.css" />
<form action="index.php" method="POST">
    <table class="table table-striped">
        <tr>
            <td>Trip Starts On</td>
            <td><input type="text" id="fromDate" name="fromDate" class="Date" /></td>
            <td>Trip Ends On</td>
            <td><input type="text" id="toDate" name="toDate" class="Date" /></td>
            <td><input type="submit" name="submit" value="Submit" class="btn" /></td>
            <td> <a href="sms.php" target="_blank" class="btn">Read SMS</a></td>
        </tr>
    </table>
</form>

<?php
// Construct sql query for shared cab booking lsit, using cablist-view.
// fetch all data from resultset into an array
// pass array to cablisitng.php function that returns the view.
// Location: lc_sourcename, lc_destinationname.
// Driver: dr_drivername.
if (function_exists('date_default_timezone_set')) {
    date_default_timezone_set('Asia/Calcutta');
}

if (!isset($_POST['fromDate']) && empty($_POST['fromDate'])) {
    $FromLocalDate = date('Y-m-d');
    $FromLocalDate .= " 00:00:01";
    echo "<input type='hidden' id='hfromdt' name='hfromdt' />";
}
else{
    $FromLocalDate = $_POST['fromDate'];
    echo "<input type='hidden' id='hfromdt' name='hfromdt' value='$FromLocalDate' />";
}

if (!isset($_POST['toDate']) && empty($_POST['toDate'])) {
    $ToLocalDate = date('Y-m-d', strtotime($FromLocalDate . '+ 5 day'));
    $ToLocalDate .= " 23:59:59";
    echo "<input type='hidden' id='htodt' name='htodt' />";
}
else{
    $ToLocalDate = $_POST['toDate'];
    echo "<input type='hidden' id='htodt' name='htodt' value ='$ToLocalDate' />";
}


$dumy = 0;
$sharedBookedSqlQuery = GetSharedBooking($FromLocalDate, $ToLocalDate);
//$sharedBookedView = GetListingView($sharedBookedSqlQuery, TRUE, $dumy);
$sharedBookedView = NewGetListingView($sharedBookedSqlQuery, TRUE);

$sharedCabCount = 0;
$sharedAvailableSqlQuery = GetSharedFree($FromLocalDate, $ToLocalDate);
//$sharedAvailableView = GetListingView($sharedAvailableSqlQuery, FALSE, $sharedCabCount);
$sharedAvailableView = NewGetListingView($sharedAvailableSqlQuery, FALSE);

// Call sp for chartered booking list
// fetch all data from resultset into an array
// pass array to cablisitng.php function that returns the view.
$charterBookedSqlQuery = GetCharterBooking($FromLocalDate, $ToLocalDate);
//$charterBookedView = GetListingView($charterBookedSqlQuery, TRUE, $dumy);
$charterBookedView = NewGetListingView($charterBookedSqlQuery, TRUE);

$charterCabCount = 0;
$charterAvailableSqlQuery = GetCharterFree($FromLocalDate, $ToLocalDate);
//$charterFreeView = GetListingView($charterAvailableSqlQuery, FALSE, $charterCabCount);
$charterFreeView = NewGetListingView($charterAvailableSqlQuery, False);

// Call sp for virtual booking list
// fetch all data from resultset into an array
// pass array to cablisitng.php function that returns the view.
$virtualBookingSqlQuery = GetVirtualBooking($FromLocalDate, $ToLocalDate);
//$virtualBookedView = GetListingView($virtualBookingSqlQuery, TRUE, $dumy);
$virtualBookedView = NewGetListingView($virtualBookingSqlQuery, TRUE);

// getting data for bid based booking
// commented on 26-10 due to new bid logic.
// $bidBookingSqlQuery = GetBidBooking($FromLocalDate, $ToLocalDate);
$bidBookingSqlQuery = GetNewBidBookings($FromLocalDate, $ToLocalDate);
//$bidBookedView = GetListingView($bidBookingSqlQuery, True, $dumy);
$bidBookedView = NewGetListingView($bidBookingSqlQuery, TRUE);

// getting data for bid award booking
$bidAwardeBookingSqlQuery = GetBidAwardedBooking($FromLocalDate, $ToLocalDate);
$bidAwardedView = NewGetListingView($bidAwardeBookingSqlQuery, TRUE);
?>

<div>&nbsp;</div>

<div class="tabbable"> <!-- Only required for left/right tabs -->
    <ul class="nav nav-tabs">
        <li class="active" style="width: 33%; text-align: center;">
            <a href="#charter" data-toggle="tab">Charter</a>
        </li>
        <li style="width: 33%; text-align: center;">
            <a href="#shared" data-toggle="tab">Shared</a>
        </li>
        <li style="width: 33%; text-align: center;">
            <a href="#bid" data-toggle="tab">Bid</a>
        </li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane active" id="charter">
            <h3 class="text-center">Charter Cab Status</h3>
            <div class='tabbable tabs-left'>
                <ul class="nav nav-tabs">
                    <li class="active" style="width: 49%;">
                        <a href="#charter-free" data-toggle="tab">Free</a>
                    </li>
                    <li style="width: 49%;">
                        <a href="#charter-booked" data-toggle="tab">Booked</a>
                    </li>
                </ul>

                <div class="tab-content">
                    <div class="tab-pane active" id="charter-free">
                        <table id="charter-free-table" class="tablesorter table">
                            <thead>
                            <tr>
                                <th>Id</th>
                                <th>Cab Number</th>
                                <th>Departure</th>
                                <th>Source</th>
                                <th>Destination/Free time</th>
                                <th>Driver Name - Number</th>
                                <th>Car Model</th>
                                <th>Seat Capacity</th>
                                <th>Ticket Count</th>
                                <th>Max Bid</th>
                            </tr>
                            </thead>
                            <tbody>
<?php
echo $charterFreeView;
?>
                            </tbody>
                        </table>
                        <hr/>
                        
                        <h3 class="text-center">Virtual Cab</h3>
                        <table id="table-virtual" class="table ">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Capacity</th>
                                    <th>AC Rate</th>
                                    <th>NonAc Rate</th>
                            </thead>
                            <tbody>
<?php
echo GetVirtualCabView();
?>
                            </tbody>
                        </table>
                            <?php echo $charterCabCount; ?>
                    </div>

                    <div class="tab-pane" id="charter-booked">
<!--                        <div style="clear: both; float: left; width: 49%; text-align: center; font-size: large; margin-bottom: 10px;">Actual Booking</div>
                        <div style="float: left; width: 49%; text-align: center; font-size: large; margin-bottom: 10px;">Virtual Booking</div>-->

                        <!-- content for Actual Booking -->
                        <div style="clear: both; float: left; width: 99%; overflow: auto; text-align: justify; max-height: 600px;">
                            <table id="charter-booked-table" class="tablesorter table ">
                                <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>Cab</th>
                                        <th>Departure</th>
                                        <th>Source</th>
                                        <th>Destination</th>
                                        <th>Driver</th>
                                        <th>Model</th>
                                        <th>Capacity</th>
                                        <th>Available</th>
                                        <th>Max. Bid</th>
                                    </tr>
                                </thead>
                                <tbody>
<?php
echo $charterBookedView;
?>
                                </tbody>
                            </table>
                        </div>

                        <!--content for virtual booking-->
                        
                        <div style="float: left; width: 99%; overflow: auto; text-align: justify; max-height: 600px; margin-top: 15px;">
                            <h3 class="text-center">Virtual Booking</h3>
                            <table class="table ">
                                <thead>
                                    <tr>
                                        <th colspan='2'>Action</th>
                                        <th>Departure</th>
                                        <th colspan='2'>Route</th>
                                        <th>Model</th>
                                        <th>Seats</th>
                                        <th colspan='3'></th>
                                    </tr>
                                </thead>
<?php
echo $virtualBookedView;
?>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="tab-pane" id="shared">
            <div><hr /></div>
            <div class='tabbable tabs-left'>
                <ul class="nav nav-tabs">
                    <li class="active" style="width: 49%;">
                        <a href="#shared-free" data-toggle="tab">Free</a>
                    </li>
                    <li style="width: 49%;">
                        <a href="#shared-booked" data-toggle="tab">Booked</a>
                    </li>
                </ul>

                <div class="tab-content">
                    <div class="tab-pane active" id="shared-free">
                        <table id="shared-free-table" class="tablesorter table">
                            <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Cab</th>
                                    <th>Departure</th>
                                    <th>Source</th>
                                    <th>Destination</th>
                                    <th>Driver</th>
                                    <th>Car</th>
                                    <th>Capacity</th>
                                    <th>Bookings</th>
                                    <th>MaxBid</th>
                                </tr>
                            </thead>
                            <tbody>
<?php
echo $sharedAvailableView;
?>
                            </tbody>
                        </table>
                    </div>

                    <div class="tab-pane" id="shared-booked">
                        <table class="tablesorter table ">
                            
                            <thead>
                                <tr>
                                    <th colspan='2'>Cab</th>
                                    <th>Departure</th>
                                    <th colspan='2'>Route</th>
                                    <th>Driver</th>
                                    <th>Model</th>
                                    <th>Seats</th>
                                    <th>Bookings</th>
                                    <th>Bid</th>
                                </tr>
                            </thead>
<?php
echo $sharedBookedView;
?>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="tab-pane" id="bid">
            <div><hr /></div>
            <div class='tabbable tabs-left'>
                <ul class="nav nav-tabs">
                    <li class="active" style="width: 49%;">
                        <a href="#bidbooking" data-toggle="tab">Booking</a>
                    </li>
                    <li style="width: 49%;">
                        <a href="#bidawarded" data-toggle="tab">Awarded</a>
                    </li>
                </ul>
                
                <div class="tab-content">
                    <div class="tab-pane active" id="bidbooking">
                        <table id="bid-booking-table" class="tablesorter table">
                            <thead>
                                <tr>
                                    <th colspan='2'>Cab</th>
                                    <th>Departure</th>
                                    <th colspan='2'>Route</th>
                                    <th>Driver</th>
                                    <th>Model</th>
                                    <th>Seats</th>
                                    <th>Bookings</th>
                                    <th>Bid</th>
                                </tr>
                            </thead>
                            <tbody>
            
<?php
echo $bidBookedView;
?>
                                </tbody>
            </table>
                    </div>
                    
                    <div class="tab-pane" id="bidawarded">
                        <table id="bid-awarded-table" class="tablesorter table">
                            <thead>
                                <tr>
                                    <th colspan='2'>Cab</th>
                                    <th>Departure</th>
                                    <th colspan='2'>Route</th>
                                    <th>Driver</th>
                                    <th>Model</th>
                                    <th>Seats</th>
                                    <th>Bookings</th>
                                    <th>Bid</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php echo $bidAwardedView; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
include_once Path() . '/templates/adminfooter.php';
?>

<script>
    $(document).ready(function() 
    { 
        $("#charter-free-table").tablesorter(); 
        $("#shared-free-table").tablesorter();
        
        var fromdt = $("#hfromdt").val();
        var todt = $("#htodt").val();
        
        if(fromdt && todt){
            $("#fromDate").val(fromdt);
            $("#toDate").val(todt);
        }
    } 
); 
    $(function() {
        var fromdt = $("#hfromdt").val();
        var todt = $("#htodt").val();
        if(fromdt && todt){
            $("#fromDate").val(fromdt);
            $("#toDate").val(todt);
            $('.Date').appendDtpicker({
                //"inline": true,
                "minuteInterval": 15,
                "current": "<?php echo $fromYear . '-' . $tmonth . '-' . $tday ?> 00:15"
            });
        }
        else{
            $('.Date').appendDtpicker({
                //"inline": true,
                "minuteInterval": 15,
                "current": "<?php echo $fromYear . '-' . $tmonth . '-' . $tday ?> 00:15"
            });
        }
    });
</script>
