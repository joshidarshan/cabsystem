<?php

function Path() {
    $path = $_SERVER['DOCUMENT_ROOT'];
    // Differenciate the path for local and web host. Includes database connect file.
    if ($_SERVER['HTTP_HOST'] == "localhost" || $_SERVER['HTTP_HOST'] == "localhost:8080")
        $path .= '/Cabsystem';
    return $path;
}

if (function_exists('date_default_timezone_set')) {
    date_default_timezone_set('Asia/Calcutta');
}
$date = date('Y/m/d h:i:s', time());

include_once Path() . '/dbop/data/booking.php';
include_once Path() . '/dbop/data/clientamounts.php';
include_once Path() . '/dbop/data/cancelation.php';

// An entry into Booking cancel table.
// If booking was 
// If it was paid then the amount is to be debited into clientamounts

if (isset($_GET['bookingId']) && !empty($_GET['bookingId'])) {
    $bookingId = $_GET['bookingId'];

    // Geting received amount
    $bookingQuery = GetBookingById($bookingId);
    $bookingData = mysql_fetch_assoc($bookingQuery);
    $amount = $bookingData['AmountReceived'];
    if (!$amount)
        $amount = 0;
    //SELECT `Id`, `ClientId`, `IsPaid`,`SLocationId`,`DLocationId`,`IsShare`,`AmountReceived`,`Date`,`IsMaharaja`,`BidRate`,`IsBidAwarded`,`CabRouteId`,`PickupLocation`,`SeatsBooked`,`TicketCount`,`IsBookedByClient`,`Status`
    // Getting booking information

    $cancelId = AddSimpleCancellation($bookingData['ClientId'], $bookingId);
    $isBookingCancel = CancelBooking($bookingId);
    $clientCancelAmountId = AddCancelClientAmount($date, $amount, $cancelId, $bookingData['ClientId']);
    
    header("location: index.php");
}

function CancelBookingAddRefund($bookingId) {
    if(isset($bookingId) && !empty($bookingId)){
    if (function_exists('date_default_timezone_set')) {
        date_default_timezone_set('Asia/Calcutta');
    }
    $date = date('Y/m/d h:i:s', time());

    // Geting received amount
    $bookingQuery = GetBookingById($bookingId);
    $bookingData = mysql_fetch_assoc($bookingQuery);
    $amount = $bookingData['AmountReceived'];
    if (!$amount)
        $amount = 0;

    $cancelId = AddSimpleCancellation($bookingData['ClientId'], $bookingId); // Getting booking information
    $isBookingCancel = CancelBooking($bookingId); // Updating booking status 0
    $clientCancelAmountId = AddCancelClientAmount($date, $amount, $cancelId, $bookingData['ClientId']); // Adds an entry into ClientAmount
    }
}

?>
