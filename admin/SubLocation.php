<?php
if (!isset($_SESSION))
    session_start();
$_SESSION['cat'] = "";

function Path() {
    $path = $_SERVER['DOCUMENT_ROOT'];
    // Differenciate the path for local and web host. Includes database connect file.
    if ($_SERVER['HTTP_HOST'] == "localhost" || $_SERVER['HTTP_HOST'] == "localhost:8080")
        $path .= '/Cabsystem';
    return $path;
}


include_once Path() . '/templates/adminheader.php';
$page = 1;
$locationStart = 0;
$locationRpp = 2;

include_once Path() . '/dbop/data/sublocation.php';
if (isset($_GET['action'])) {
    if ($_GET['action'] == "delete") {
        $Id = $_GET['Id'];
       DeleteSubLocation($Id);
        echo "<script> alert('Your Record Delete SuccessFully');</script>";
    }
}
$no = SubLocationPagination();
$totalpage = ceil($no / $locationRpp);

if (isset($_GET['page'])) {
    $page = $_REQUEST['page'];
    $a = $page * $locationRpp;
    $locationStart = $a - $locationRpp;
}
$GetAllSubLocation = GetAllSubLocations($locationStart, $locationRpp);

?>
<script language="javascript">
    function del()
    {
        var x;
        var r = confirm("Really Want to Delete Location");
        if (!r)
            return false;
    }
</script>
     
        <table align="center" class="table table-striped">
            <tr>
                <th>Action</th>
                <th>Location</th>
                <th>Sub Location</th>
                <th>Fare</th>
                <th>Status</th>
            </tr>
<?php
while ($FetchSub = mysql_fetch_array($GetAllSubLocation)) {
    ?>
                <tr>
                    <td><a href="SubLocationEdit.php?Id=<?php echo $FetchSub['Id']; ?>">Edit</a> &nbsp;| &nbsp;
                    <a href="SubLocation.php?action=delete&Id=<?php echo $FetchSub['Id']; ?>" onClick="return del();">Delete</a></td>
                    <td><?php echo $FetchSub['LocName']; ?></td>
                    <td><?php echo $FetchSub['SubLocation']; ?></td>
                    <td><?php echo $FetchSub['Fare']; ?></td>
                    <td><?php echo $FetchSub['Status']; ?></td>
                    
                </tr>
    <?php
}
?>
                </table>
                <div class="pagination pagination-centered">
        <ul>
            <?php
            for ($i = 1; $i <= $totalpage; $i++) {
                if ($i == $page)
                    echo '<li class = "active"><a href="SubLocation.php?page=' . $i . '">' . $i . '</a></li>';
                else
                    echo '<li><a href="SubLocation.php?page=' . $i . '">' . $i . '</a></li>';
            }
            ?>
        </ul>
    </div>
        
        
        <?php
             include_once Path() . '/templates/adminfooter.php';
           ?> 