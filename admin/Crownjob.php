<?php
function LPath() {
    $path = $_SERVER['DOCUMENT_ROOT'];
    // Differenciate the path for local and web host. Includes database connect file.
    if ($_SERVER['HTTP_HOST'] == "localhost" || $_SERVER['HTTP_HOST'] == "localhost:8080")
        $path .= '/Cabsystem';
    return $path;
}

include_once LPath().'/dbop/data/globalperameter.php';
include_once LPath().'/dbop/data/crownjob.php';
include_once LPath().'/sms/sms.php';
include_once LPath().'/email/email.php';
include_once LPath().'/dbop/data/booking.php';
include_once LPath().'/dbop/data/cabroute.php';

function BookingConfirmation(){
    // Checks the list of booking due in next 25 minuts or less.
    // Getting confirmation time from global parameter.
    $globalParameterQuery = GetAllGlobalParameters();
    $globalParameterData = mysql_fetch_assoc($globalParameterQuery);
    $confirmTime = $globalParameterData['booking_confirmation_time'];
    
    // last cron job time
    $lastCronJobDateTime = GetLastCronJobTimeStamp();
    
    // from datetime
    $departureFromDateTime = date("Y/m/d h:i:s", strtotime("+$confirmTime hours", strtotime($lastCronJobDateTime)));//$lastCronJobDateTime + $confirmTime;
    $departureToDateTime = date("Y/m/d h:i:s", strtotime("+15 minutes", strtotime($departureFromDateTime)));
    
    // getting a list of booking except bid in time range
    $bookingList = GetBookingsInTimeInterval($departureFromDateTime, $departureToDateTime);
    $bookingIdList = array();
    while($br = mysql_fetch_assoc($bookingList)){
        //cl.cr_id, cl.bk_clientid, SUM(cl.`bk_ticketCount`) AS TicketCount, MAX(cl.cl_phone) AS ClientPhone, MAX(cl.cl_email) AS ClientEmail, MAX(dr.`Name`) As DriverName, (dr.`Phones`) AS DriverPhone, MAX(ls.`Name`) as Source, MAX(ld.`Name`) as Destination, MAX(cl.`cb_NoPlate`) as NumberPlate, MAX(cl.cr_fromDateTime) AS Departure
        //$smsMessage = "Departure reminder\n$br[Source] To $br[Destination]\nTicket:$br[TicketCount]\nCab:$br[NumberPlate]\nDriver:$br[DriverName]-$br[DriverPhone]";
        //SendSms($br['ClientPhone'], $smsMessage);
        //$emailMessage = "Departure reminder <br/> $br[Source] To $br[Destination] <br/> Ticket:$br[TicketCount] <br/> Cab:$br[NumberPlate] <br/> Driver:$br[DriverName]-$br[DriverPhone]";
        //SendEmail($br['ClientEmail'], 'Departure reminder - Khaligadi.com', $emailMessage);
        $bookingIdList[] = $br['BookingId'];
    }
    
    // Generating booking id string, adding an entry into cron job table.
    $bookingIdString = implode(",", $bookingIdList);
    $lastInsertId = AddCrownJob("Booking Confirmation, SMS, Email for BookingId: $bookingIdString", GetLocalDateTime());
}

// BidConfirmation Code no longer required as of 26-10-2013 client customization need.
/*function BidConfirmation(){
    // Checks the list of booking due in next 25 minuts or less.
    // Getting confirmation time from global parameter.
    $globalParameterQuery = GetAllGlobalParameters();
    $globalParameterData = mysql_fetch_assoc($globalParameterQuery);
    $confirmTime = $globalParameterData['booking_confirmation_time'];
    
    // last cron job time
    $lastCronJobDateTime = GetLastCronJobTimeStamp();
    // from datetime
    $departureFromDateTime = date("Y/m/d H:i:s", strtotime("+$confirmTime hours", strtotime($lastCronJobDateTime)));//$lastCronJobDateTime + $confirmTime;
    $departureToDateTime = date("Y/m/d H:i:s", strtotime("+15 minutes", strtotime($departureFromDateTime)));
    
    echo "From: $departureFromDateTime, To: $departureToDateTime"."<br />";
    
    // getting a list of bid booking in time range
    $bookingList = GetBidInTimeInverval($departureFromDateTime, $departureToDateTime);
    
    // List of bookingId
    $cancelBookingIdList = array(); $confirmBookingIdList = array();
    
    // Local variable to check bidaward for cabroute bookings, if no award found then award the max bid and cancel the rest.
    $maxBidAmount = 0; $lastCabRouteId=0; $isBidAwarded = FALSE; $bidAwardBookingId = 0; $bookingCancelIdList = array();
    
    // Logic begins
    while($bar = mysql_fetch_assoc($bookingList)){
        // if current cabrouteId != lastCabRouteId
        if($bar['cr_id'] != $lastCabRouteId && !empty($lastCabRouteId)){
            if(!$isBidAwarded && $bidAwardBookingId != 0){
                //$smsMessage = "Departure reminder\n$bar[Source] To $bar[Destination]\nTicket:$bar[TicketCount]\nCab:$bar[NumberPlate]\nDriver:$bar[DriverName]-$bar[DriverPhone]";
                //SendSms($bar['ClientPhone'], $smsMessage);
                //$emailMessage = "Departure reminder <br/> $bar[Source] To $bar[Destination] <br/> Ticket:$bar[TicketCount] <br/> Cab:$bar[NumberPlate] <br/> Driver:$bar[DriverName]-$bar[DriverPhone]";
                //SendEmail($bar['ClientEmail'], 'Departure reminder - Khaligadi.com', $emailMessage);
                $confirmBookingIdList[] = $bidAwardBookingId;
                
                AwardBid($bidAwardBookingId);
            }
            foreach ($bookingCancelIdList as $bcr=>$bcd){
                //$smsMessage = "Bid Cancelation Feedback\n$bar[Source] To $bar[Destination]\nTicket:$bar[TicketCount]\nCab:$bar[NumberPlate]\nDriver:$bar[DriverName]-$bar[DriverPhone]";
                //SendSms($bar['ClientPhone'], $smsMessage);
                //$emailMessage = "Bid Cancelation Feedback <br/> $bar[Source] To $bar[Destination] <br/> Ticket:$bar[TicketCount] <br/> Cab:$bar[NumberPlate] <br/> Driver:$bar[DriverName]-$bar[DriverPhone]";
                //SendEmail($bar['ClientEmail'], 'Bid Cancelation Feedback - Khaligadi.com', $emailMessage);
                
                CancelBookingAddRefund($bcd);
            }
            
            $lastCabRouteId = $bar['cr_id'];
            $isBidAwarded = FALSE;
            $maxBidAmount = $bar['bk_bidrate'];
            $bidAwardBookingId = $bar['bk_id'];
            unset($bookingCancelIdList); $bookingCancelIdList = array();
        }
        else{
            if($bar['bk_isbidawarded']){
                $isBidAwarded = TRUE;
                $bidAwardBookingId = $bar['bk_id'];
                $maxBidAmount = $bar['bk_bidrate'];
            }
            else{
                if(!$isBidAwarded){
                    if($maxBidAmount < $bar['bk_bidrate']){
                        $cancelBookingIdList[] = $bidAwardBookingId;
                        $maxBidAmount = $bar['bk_bidrate'];
                        $bidAwardBookingId = $bar['bk_id'];
                        $lastCabRouteId = $bar['cr_id'];
                    }
                    else{
                        $cancelBookingIdList[] = $bar['bk_id'];
                    }
                }
            }
        }
    }
    AwardBid($bidAwardBookingId);
}*/

function SplitFreeTime(){
    // last cron job time
    $lastCronJobDateTime = GetLastCronJobTimeStamp();
    
    // getting list of last bookings that needs splliting.
    $lastBookings = GetSplitableCabRoutes($lastCronJobDateTime);
    
    while($bk = mysql_fetch_assoc($lastBookings)){
        $fromDateTime=""; $toDateTime=""; $source=""; $destination=""; $currentSource=""; $currentDestination="";
        
        // checking if booking date is equal to cabroute from date.
        $endHourDiff = round((strtotime($bk['cr_todatetime'])- strtotime($bk['bk_todate']))/3600, 1);
        if($bk['cr_fromdatetime'] == $bk['bk_traveldate']){
            $fromDateTime = $bk['bk_traveldate'];
            $toDateTime = $bk['bk_todate'];
            $source = $bk['bk_slocationid'];
            $destination = $bk['bk_dlocationid'];
            EditCabRoute($bk['cr_id'], $bk['cr_cabid'], $fromDateTime, $source, $destination, $toDateTime, $bk['cr_originalLocation'], $bk['cr_isreturntrip'], $bk['cr_agentid'], $bk['cr_driverid'], 0, $bk['cm_Id'], 1);
            
            if($source != $destination){
                // provision to add one way shared cab.
            }
            else{
                $fromDateTime = date("Y-m-d H:i:s", strtotime("1 days", strtotime($toDateTime)));
                echo "141: $fromDateTime, ToDate: $toDateTime, Source: $source" ;
                $toDateTime = $bk['cr_todatetime'];
                $source = $destination;
                $destination = 0;
                $hourDiff = round((strtotime($toDateTime)- strtotime($fromDateTime))/3600, 1);
                echo $hourDiff;
                if($hourDiff >= 24){
                    echo "Adding record for new cab route for cabId = $bk[cr_cabid], Source:$source";
                    
                    AddCabRoute($bk['cr_cabid'], $fromDateTime, $source, $destination, $toDateTime, $bk['cr_originalLocation'], $bk['cr_isreturntrip'], $bk['cr_agentid'], $bk['cr_driverid'], 0, $bk['cm_Id'], 1);
                }
            }
        }
        // checking if to date is equal to cabroute to date
        else if($endHourDiff <= 12){
            $fromDateTime = $bk['bk_traveldate'];
            $toDateTime = $bk['bk_todate'];
            $source = $bk['bk_slocationid'];
            $destination = $bk['bk_dlocationid'];
            EditCabRoute($bk['cr_id'], $bk['cr_cabid'], $fromDateTime, $source, $destination, $toDateTime, $bk['cr_originalLocation'], $bk['cr_isreturntrip'], $bk['cr_agentid'], $bk['cr_driverid'], 0, $bk['cm_Id'], 1);
            
            $fromDateTime = $bk['cr_fromdatetime'];
            $toDateTime = date("Y/m/d h:i:s", strtotime("-2 hours", strtotime($bk['bk_traveldate'])));
            $source = $bk['cr_source'];
            $destination = $bk['cr_destination'];
            AddCabRoute($bk['cr_cabid'], $fromDateTime, $source, $destination, $toDateTime, $bk['cr_originalLocation'], $bk['cr_isreturntrip'], $bk['cr_agentid'], $bk['cr_driverid'], 0, $bk['cm_Id'], 1);
        }
        else{
            $fromDateTime = $bk['bk_traveldate'];
            $toDateTime = $bk['bk_todate'];
            $source = $bk['bk_slocationid'];
            $destination = $bk['bk_dlocationid'];
            EditCabRoute($bk['cr_id'], $bk['cr_cabid'], $fromDateTime, $source, $destination, $toDateTime, $bk['cr_originalLocation'], $bk['cr_isreturntrip'], $bk['cr_agentid'], $bk['cr_driverid'], 0, $bk['cm_Id'], 1);
            
            $fromDateTime = $bk['cr_fromdatetime'];
            $toDateTime = date("Y/m/d h:i:s", strtotime("-2 hours", strtotime($bk['bk_traveldate'])));
            $source = $bk['cr_source'];
            $destination = $bk['cr_destination'];
            AddCabRoute($bk['cr_cabid'], $fromDateTime, $source, $destination, $toDateTime, $bk['cr_originalLocation'], $bk['cr_isreturntrip'], $bk['cr_agentid'], $bk['cr_driverid'], 0, $bk['cm_Id'], 1);
            
            $fromDateTime = date("Y/m/d h:i:s", strtotime("+24 hours", strtotime($bk['bk_todate'])));
            $toDateTime = $bk['cr_todatetime'];
            $source = $source;
            $destination="0";
            $hourDiff = round((strtotime($toDateTime)- strtotime($fromDateTime))/3600, 1);
            if($hourDiff >= 24){
                echo "Adding record for new cab route for cabId = $bk[cr_cabid]";
                //$CabId, $FromDateTime, $Source, $Destination, $ToDateTime, $OriginalLocation, $IsReturnTrip, $AgentId, $DriverId, $IsVia, $CarModelId, $Status
                AddCabRoute($bk['cr_cabid'], $fromDateTime, $source, $destination, $toDateTime, $bk['cr_originalLocation'], $bk['cr_isreturntrip'], $bk['cr_agentid'], $bk['cr_driverid'], 0, $bk['cm_Id'], 1);
            }
        }
    }
}

function ClubFreeTime(){
    // last cron job time
    $lastCronJobDateTime = GetLastCronJobTimeStamp();
    
    // local variables
    $fromDateTime=""; $toDateTime=""; $source=""; $destination=""; $cabId=""; $cabIdList = array();
    
    // Getting list of clubbale cabs
    $clubCabs = GetClubableCabId($lastCronJobDateTime);
    
    while($clrow = mysql_fetch_assoc($clubCabs)){
        // Identifing unique cab from records for mergeing.
        
        if(empty($cabId)){
            $cabId = $clrow['cr_cabid'];
            $toDateTime = $clrow['cr_todatetime'];
            $cabIdList[] = $clrow['cr_id'];
            continue;
        }
        
        if($cabId != $clrow['cr_cabid']){
            if(count($cabIdList) >= 2){
                for ($i = 0; $i < count($cabIdList); $i++) {
                    if($i == 0){
                        EditCabRouteCornJob($cabIdList[0], $toDateTime, 1);
                    }
                    else{
                        EditCabRouteCornJob($cabIdList[$i], "", 0);
                    }
                }
            }
            unset($cabIdList); $cabIdList = array();
            $cabId = $clrow['cr_cabid'];
            $cabIdList[] = $clrow['cr_id'];
            $toDateTime = $clrow['cr_todatetime'];
            continue;
        }
        
        $hourDiff = round((strtotime($clrow['cr_fromdatetime'])- strtotime($toDateTime))/3600, 1);
        if($hourDiff <= 36){
            $toDateTime = $clrow['cr_todatetime'];
            $cabIdList[] = $clrow['cr_id'];
        }
        else{
            if(count($cabIdList) >= 2){
                for ($i = 0; $i < count($cabIdList); $i++) {
                    if($i == 0){
                        EditCabRouteCornJob($cabIdList[0], $toDateTime, 1);
                    }
                    else{
                        EditCabRouteCornJob($cabIdList[$i], "", 0);
                    }
                }
            }
            unset($cabIdList); $cabIdList = array();
            $cabId = $clrow['cr_cabid'];
            $cabIdList[] = $clrow['cr_id'];
            $toDateTime = $clrow['cr_todatetime'];
        }
    }
    if(count($cabIdList) >= 2){
        echo "Border condition: <br />";
        for ($i = 0; $i < count($cabIdList); $i++) {
            if($i == 0){
                EditCabRouteCornJob($cabIdList[0], $toDateTime, 1);
            }
            else{
                EditCabRouteCornJob($cabIdList[$i], "", 0);
            }
        }
        unset($cabIdList); $cabIdList = array();
        $cabIdList[] = $clrow['cr_id'];
        $toDateTime = $clrow['cr_todatetime'];
    }
}

function GetLocalDateTime(){
if (function_exists('date_default_timezone_set')) {
    date_default_timezone_set('Asia/Calcutta');
}
$date = date('Y/m/d h:i:s', time());
return $date;
}

/* Notes
 * In corn job we first need to call splitDate function.
 * Because the cab would be available for number days larger than booking time.
 * The splitdate function will split the timespan sloat and will update the booking cabroute id limited to booked from-to dates.
 */
?>
