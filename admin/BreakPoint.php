<?php
if (!isset($_SESSION))
    session_start();
$_SESSION['cat'] = "route";

function Path() {
    $path = $_SERVER['DOCUMENT_ROOT'];
    // Differenciate the path for local and web host. Includes database connect file.
    if ($_SERVER['HTTP_HOST'] == "localhost" || $_SERVER['HTTP_HOST'] == "localhost:8080")
        $path .= '/Cabsystem';
    return $path;
}

include_once Path() . '/dbop/data/location.php';
include_once Path() . '/dbop/data/breakpoint.php';
include_once Path() . '/templates/adminheader.php';

$page = 1;
$start =0;
$rpp = 2;
$no = BreakPointsPagination();
$totalpage = ceil($no / $rpp);

if (isset($_GET['action'])) {
    if ($_GET['action'] == "delete") {

        $Id = $_GET['Id'];
        DeleteBreakPoint($Id);
        echo "<script> alert('Your Record Delete SuccessFully');</script>";
    }
}
if (isset($_GET['page'])) {
    $page = $_REQUEST['page'];
    $a = $page * $rpp;
    $start = $a - $rpp;
}
$GetAllBreakingPoints = GetBreakpoint($start, $rpp);
?>
<script language="javascript">
    function del() {
        var r = confirm("Really Want to Delete A Break Point");
        if (!r)
            return false;
    }
</script>

<form action="BreakPoint.php" method="post">
    <h1 align="center">Car Listing</h1>
    <table align='center' class="table table-striped">
        <tr>
            <th>Action</th>
            <th>Source-Location</th>
            <th>Destination-Location</th>
            <th>Name</th>
            <th>Description</th>
            <th>Map-Location</th>
            <th>Status</th>
        </tr>
        <?php
        while ($FetchAllBreakingPoints = mysql_fetch_array($GetAllBreakingPoints)) {
            ?>
            <tr>
                <td>
                    <a href="BreakPointEdit.php?Id=<?php echo $FetchAllBreakingPoints['Id']; ?>">Edit</a> &nbsp; | &nbsp;
                    <a href="BreakPoint.php?action=delete&Id=<?php echo $FetchAllBreakingPoints['Id']; ?>" onClick="return del();">Delete</a>
                </td>
                <td><?php echo $FetchAllBreakingPoints['SrcName']; ?></td>
                <td><?php echo $FetchAllBreakingPoints['DstName']; ?></td>
                <td><?php echo $FetchAllBreakingPoints['Name']; ?></td>
                <td><?php echo $FetchAllBreakingPoints['Description']; ?></td>
                <td><?php echo $FetchAllBreakingPoints['Maplocation']; ?></td>
                <td><?php echo $FetchAllBreakingPoints['Status']; ?></td>
            </tr>
            <?php
        }
        ?>
    </table>
    <div class="pagination pagination-centered">
        <ul>
            <?php
            for ($i = 1; $i <= $totalpage; $i++) {
                if ($i == $page)
                    echo '<li class = "active"><a href="BreakPoint.php?page=' . $i . '">' . $i . '</a></li>';
                else
                    echo '<li><a href="BreakPoint.php?page=' . $i . '">' . $i . '</a></li>';
            }
            ?>
        </ul>
    </div>
</form>

<?php include_once Path() . '/templates/adminfooter.php'; ?>