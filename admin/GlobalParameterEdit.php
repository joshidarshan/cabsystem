<?php
if (!isset($_SESSION))
    session_start();
$_SESSION['cat'] = "";

function Path() {
    $path = $_SERVER['DOCUMENT_ROOT'];
    // Differenciate the path for local and web host. Includes database connect file.
    if ($_SERVER['HTTP_HOST'] == "localhost" || $_SERVER['HTTP_HOST'] == "localhost:8080")
        $path .= '/Cabsystem';
    return $path;
}

include_once Path() . '/templates/adminheader.php';
include_once Path() . '/dbop/data/globalperameter.php';

if (isset($_POST['sub'])) {
    $Id = $_POST['Id'];
    $mh_percentage = $_POST['mh_percentage'];
    $mh_multipleof = $_POST['mh_multipleof'];
    $cp_minimunkm = $_POST['cp_minimunkm'];
    $cp_point = $_POST['cp_point'];
    $cp_maxpoints = $_POST['cp_maxpoints'];
    $cp_point_to_rupee_rate = $_POST['cp_point_to_rupee_rate'];
    $cp_discount_percent = $_POST['cp_discount_percent'];
    $cab_freehours = $_POST['cab_freehours'];
    $cab_nearbykm = $_POST['cab_nearbykm'];
    $cab_average_speed = $_POST['cab_average_speed'];
    $cab_redeemtion_level = $_POST['cab_redeemtion_level'];
    $virtualcab_permodel = $_POST['virtualCabPerModel'];
    $bookingConfirmTime = $_POST['bookingConfirmationTime'];
    $cabPerPage = $_POST['cab_perpage'];

    EditGlobalParameter($Id, $mh_percentage, $mh_multipleof, $cp_minimunkm, $cp_point, $cp_maxpoints, $cp_point_to_rupee_rate, $cp_discount_percent, $cab_freehours, $cab_nearbykm, $cab_average_speed, $cab_redeemtion_level, $cabPerPage, $virtualcab_permodel, $bookingConfirmTime);
    echo "<script> alert ('Your Update SuccessFully'); window.location='GlobalParameter.php';</script>";
}

$Id = $_REQUEST['Id'];
$GetGlobalParameter = GetGlobalParameter($Id);
$fetchGlobalParameter = mysql_fetch_array($GetGlobalParameter);
?>

<form action="GlobalParameterEdit.php" method="post">
    <h1 align="center">Edit Global Parameter Here</h1>
    <table align="center" class="table table-striped">
        <tr class="hidden">
            <td>Global Parameter Id</td>
            <td><input type="hidden" name="Id" value="<?php echo $Id; ?>"/></td>
        </tr>
        <tr>
            <td>Maharaja Fare Percentage</td>
            <td><input type="text" name="mh_percentage" value="<?php echo $fetchGlobalParameter['mh_percentage']; ?>"/></td>
        </tr>
        <tr>
            <td>Maharaja Fare Multiple Of</td>
            <td><input type="text" name="mh_multipleof" value="<?php echo $fetchGlobalParameter['mh_multipleof']; ?>"/></td>
        </tr>
        <tr>
            <td>Credit Point Min Km</td>
            <td><input  type="text" name="cp_minimunkm" value="<?php echo $fetchGlobalParameter['cp_minimunkm']; ?>" /></td>
        </tr>
        <tr>
            <td>Credit Point</td>
            <td><input  type="text" name="cp_point" value="<?php echo $fetchGlobalParameter['cp_point']; ?>"/></td>
        </tr>
        <tr>
            <td>Credit Point Max Km</td>
            <td><input  type="text" name="cp_maxpoints"  value="<?php echo $fetchGlobalParameter['cp_maxpoints']; ?>"/></td>
        </tr>
        <tr>
            <td>Credit Point To Rupee Rate</td>
            <td><input  type="text" name="cp_point_to_rupee_rate" value="<?php echo $fetchGlobalParameter['cp_point_to_rupee_rate']; ?>"/></td>
        </tr>
        <tr>
            <td>Credit Point Discount Percent</td>
            <td><input  type="text" name="cp_discount_percent" value="<?php echo $fetchGlobalParameter['cp_discount_percent']; ?>"/></td>
        </tr>
        <tr>
            <td>Cab Free Hours</td>
            <td><input  type="text" name="cab_freehours" value="<?php echo $fetchGlobalParameter['cab_freehours']; ?>"/></td>
        </tr>
        <tr>
            <td>Cab Near By KM</td>
            <td><input  type="text" name="cab_nearbykm" value="<?php echo $fetchGlobalParameter['cab_nearbykm']; ?>"/></td>
        </tr>
        <tr>
            <td>Average Speed</td>
            <td><input  type="text" name="cab_average_speed" value="<?php echo $fetchGlobalParameter['cab_average_speed']; ?>"/></td>
        </tr>
        <tr>
            <td>Redeemtion Level</td>
            <td><input type="text" name="cab_redeemtion_level" id="cab_redeemtion_level" value="<?php echo $fetchGlobalParameter['cp_redeemtion_level']; ?>" /></td>
        <tr>
            <td>Record Per Page</td>
            <td><input type="text" id="cab_perpage" name="cab_perpage" value="<?php echo $fetchGlobalParameter['recordperpage']; ?>" /></td>
        </tr>
        <tr> 
            <td>Virtual Cab Per Model</td>
            <td><input type="text" id="virtualCabPerModel" name="virtualCabPerModel" value="<?php echo $fetchGlobalParameter['virtual_cab_per_model']; ?>" /></td>
        </tr>
        <tr>
            <td>Booking Confirmation Time</td>
            <td><input type="text" id="bookingConfirmationTime" name="bookingConfirmationTime" value="<?php echo $fetchGlobalParameter['booking_confirmation_time']; ?>"/></td>
        </tr>
        <tr>
            <td colspan="2" style="text-align: center;"><input type="submit" name="sub" value="Update" class="btn-success btn"/></td>
        </tr>
    </table>
</form>

<?php
include_once Path().'/templates/adminfooter.php';
?>