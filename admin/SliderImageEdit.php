<?php
if (!isset($_SESSION))
    session_start();
$_SESSION['cat'] = "inventory";

function Path() {
    $path = $_SERVER['DOCUMENT_ROOT'];
    // Differenciate the path for local and web host. Includes database connect file.
    if ($_SERVER['HTTP_HOST'] == "localhost" || $_SERVER['HTTP_HOST'] == "localhost:8080")
        $path .= '/Cabsystem';
    return $path;
}

include_once Path() . '/templates/adminheader.php';
include_once Path() . '/dbop/data/slider.php';
if (isset($_POST['sub'])) {
    $Id = $_POST['Id'];
    $Name = $_REQUEST['Name'];
    $Description = $_REQUEST['Description'];
    $Status = 1;
    $Image = $_POST['prevdoc'];

    if (($_FILES["Image"]["type"] == "image/gif") || ($_FILES["Image"]["type"] == "image/jpeg") || ($_FILES["Image"]["type"] == "image/png" )) {
        $path = Path() . '/Upload/SliderImages/';
        $guid = uniqid();
        $Image = $guid . '-' . $_FILES['Image']['name'];
        move_uploaded_file($_FILES["Image"]["tmp_name"], $path . $Image);
    } else {
        if (isset($_POST['prevdoc']) && !empty($_POST['prevdoc']))
            $Iamge = $_POST['prevdoc'];
    }
    EditImage($Id, $Name, $Image, addslashes($Description), $Status);
    echo "<script> alert('Your Image Updated SuccessFully'); window.location='SliderImages.php';</script>";
}
$Id = $_REQUEST['Id'];
$GetImageById = GetImageById($Id);
$FetchImageById = mysql_fetch_array($GetImageById);
?>

<form action="SliderImageEdit.php" method="post" enctype="multipart/form-data" id="SliderImageAdd" >
    <h1 style="text-align: center;">Slider Image Modification</h1>
    <table align="center" class="table table-striped">
        <tr>
            <td>Enter Image Name</td>
            <td><input type="text" name="Name" value="<?php echo $FetchImageById['Name']; ?>"/></td>
            <td><input type="hidden" name="Id" value="<?php echo $Id; ?>"/></td>
        </tr>
        <tr>
            <td>Choose Image</td>
            <td><input type="file" name="Image"/></td>
        </tr>

<?php
if (isset($FetchImageById['Image']) && !empty($FetchImageById)) {
    $originfname = substr($FetchImageById['Image'], (stripos($FetchImageById['Image'], '-') + 1), strlen($FetchImageById['Image']));
    echo "<tr>";
    echo "<td>Previous Image</td>";
    echo "<td>$originfname</td>";
    echo "</tr>";
    echo "<input type='hidden' name='prevdoc' id='prevdoc' value='$FetchImageById[Image]' />";
}
?>
        <tr>
            <td>Description</td>
            <td>
                <textarea name="Description"><?php echo $FetchImageById['Description']; ?></textarea>
            </td>
        </tr>
        <td colspan="2" style="text-align: center;"><input type="submit" name="sub" value="Update" class="btn"/></td>
        </tr>
    </table>	
</form>

<?php
include_once Path() . '/templates/adminfooter.php';
?>

