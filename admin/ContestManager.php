<!doctype html>
 <?php
 
 function ContestPath() {  
    $path = $_SERVER['DOCUMENT_ROOT'];
    // Differenciate the path for local and web host. Includes database connect file.
    if ($_SERVER['HTTP_HOST'] == "localhost" || $_SERVER['HTTP_HOST'] == "localhost:8080")
        $path .= '/Cabsystem';
    return $path;
}
?>
<html lang="en">
<head>
  <meta charset="utf-8" />
  <title>jQuery UI Tabs - Default functionality</title>
  <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
  <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
  <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
  <link rel="stylesheet" href="/resources/demos/style.css" />
  <script>
  $(function() {
    $( "#tabs" ).tabs();
  });
  </script>
</head>
<body>
 
<div id="tabs">
  <ul>
    <li><a href="#tabs-1">Today'S Visitor</a></li>
    <li><a href="#tabs-2">All Visitor</a></li>
    <li><a href="#tabs-3">Winners</a></li>
  </ul>
  <div id="tabs-1">
      <h1 align='center'>Today's Visitor</h1>
        <?php 
        include_once ContestPath() . '/dbop/data/contest.php';
        function GetLocalDateTime(){
        if (function_exists('date_default_timezone_set')) {
        date_default_timezone_set('Asia/Calcutta');
        }
        $date = date('Y/m/d', time());
        return $date;
        }
        $Date = GetLocalDateTime();
        $GetVisitor = GetClientByDate($Date);
        $GetVisitorAll = GetClientAll();        
        ?>
      <p></p>
      <form method="post" action="#">
      <table align='center' border='3'>
          <tr>
              <th>Name</th>
              <th>Email-Id</th>
              <th>Contact No</th>
              <th>Make Winner</th>
          </tr>
          <?php
            while($FetchVisitor =  mysql_fetch_array($GetVisitor)){
                ?>
          <tr>
              <td><?php echo $FetchVisitor['CLName'];?></td>
              <td><?php echo $FetchVisitor['CLEmail'];?></td>
              <td><?php echo $FetchVisitor['CLPhone'];?></td>
              <?php if($FetchVisitor['Selected']==0){ ?>
              <td><a href='ContstWiner.php?Id=<?php echo $FetchVisitor['conId'];?>'>Choose VIsitor</a></td>
              <?php } else { ?>
              <td><a href='ContstWiner.php?RemoveId=<?php echo $FetchVisitor['conId'];?>'>Selected<br/>Remove</a></td>
              <?php } ?>
          </tr>
          <?php  
          }
          ?>
      </table>
      </form>
  </div>
  <div id="tabs-2">
    <p></p>
    <h1 align='center'>All Visitors</h1>
    <table align='center' border='3'>
          <tr>
              <th>Name</th>
              <th>Email-Id</th>
              <th>Contact No</th>
              <th>Date</th>
          </tr>
          <?php
            while($FetchVisitor =  mysql_fetch_array($GetVisitorAll)){
                ?>
          <tr>
              <td><?php echo $FetchVisitor['CLName'];?></td>
              <td><?php echo $FetchVisitor['CLEmail'];?></td>
              <td><?php echo $FetchVisitor['CLPhone'];?></td>
              <td><?php echo $FetchVisitor['ConDate'];?></td>
          </tr>
          <?php  
          }
          ?>
      </table>
  </div>
  <div id="tabs-3">
    <p>Mauris eleifend est et turpis. Duis id erat. Suspendisse potenti. Aliquam vulputate, pede vel vehicula accumsan, mi neque rutrum erat, eu congue orci lorem eget lorem. Vestibulum non ante. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Fusce sodales. Quisque eu urna vel enim commodo pellentesque. Praesent eu risus hendrerit ligula tempus pretium. Curabitur lorem enim, pretium nec, feugiat nec, luctus a, lacus.</p>
    <p>Duis cursus. Maecenas ligula eros, blandit nec, pharetra at, semper at, magna. Nullam ac lacus. Nulla facilisi. Praesent viverra justo vitae neque. Praesent blandit adipiscing velit. Suspendisse potenti. Donec mattis, pede vel pharetra blandit, magna ligula faucibus eros, id euismod lacus dolor eget odio. Nam scelerisque. Donec non libero sed nulla mattis commodo. Ut sagittis. Donec nisi lectus, feugiat porttitor, tempor ac, tempor vitae, pede. Aenean vehicula velit eu tellus interdum rutrum. Maecenas commodo. Pellentesque nec elit. Fusce in lacus. Vivamus a libero vitae lectus hendrerit hendrerit.</p>
  </div>
</div>
 
 
</body>
</html>