<?php
if (!isset($_SESSION))
    session_start();
$_SESSION['cat'] = "";

function HPath() {
    $path = $_SERVER['DOCUMENT_ROOT'];
    // Differenciate the path for local and web host. Includes database connect file.
    if ($_SERVER['HTTP_HOST'] == "localhost" || $_SERVER['HTTP_HOST'] == "localhost:8080")
        $path .= '/Cabsystem';
    return $path;
}

include_once HPath() . '/templates/adminheader.php';

if (isset($_POST['sub'])) {
    include_once HPath() . '/dbop/data/hourcharges.php';
    $Hour = $_POST['Hour'];
    $Km = $_POST['Km'];
    $Charge = $_POST['Charges'];
    InsertCharges($Hour, $Km,$Charge);
    echo "<div class='alert alert-success'><a class='close' data-dismiss='alert'>x</a><strong>Location Added!</strong></div>";
}
?>


<form action="HourChargesAdd.php" method="post" id ="LocationAdd" onsubmit=" return SubmitCheck();">
            <h1 align="center" >Add Per - Hour Charges</h1>
            <table align="center" class="table table-striped" >
                <tr>
                    <td>Enter Hours</td>
                    <td><input type="text" name="Hour" id="Location" onblur="Check(); "/></td>
                </tr>
                
                <tr>
                    <td>Enter Km</td>
                    <td><input type="text" name="Km" id="Location" onblur="Check(); "/></td>
                </tr>
                <tr>
                    <td>Enter Charges</td>
                    <td><input type="text" name="Charges" id="Location" onblur="Check(); "/></td>
                </tr>
                <tr>
                    <td colspan="2" style="text-align: center;"><input type="submit" name="sub" value="Add" class="btn-large"/></td>
                </tr>
            </table>
        </form>
  <?php
             include_once HPath() . '/templates/adminfooter.php';
           ?> 