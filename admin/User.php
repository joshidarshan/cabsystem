<?php
if (!isset($_SESSION))
    session_start();
$_SESSION['cat'] = "";

function Path() {
    $path = $_SERVER['DOCUMENT_ROOT'];
    // Differenciate the path for local and web host. Includes database connect file.
    if ($_SERVER['HTTP_HOST'] == "localhost" || $_SERVER['HTTP_HOST'] == "localhost:8080")
        $path .= '/Cabsystem';
    return $path;
}

include_once Path() . '/templates/adminheader.php';
include_once Path() . '/dbop/data/user.php';

$page = 1;
$start = 0;
$rpp = 10;
$no = PaginationUser();
$totalpage = ceil($no / $rpp);

if (isset($_GET['action'])) {
    if ($_GET['action'] == "delete") {
        echo $_GET['action'];
        $Id = $_GET['Id'];
        DeleteUser($Id);
        echo "<script> alert('Your Record Delete SuccessFully');</script>";
    }
}
if (isset($_GET['page'])) {
    $page = $_REQUEST['page'];
    $a = $page * $rpp;
    $start = $a - $rpp;
}
$GetAllUsers = GetAllUsers($start, $rpp) ;
?>
<script language="javascript">
    function del()
    {
        var r = confirm("Really Want to Delete An Agent");
        if (!r)
            return false;
    }
</script>
<form action="User.php">
    <h1 align="center">User Listing</h1>
    <table align='center' class="table table-striped">
        <tr>
            <th>Action</th>
            <th>Email Id</th>
            <th>Contact No.</th>
            <th>Role</th>
            <th>Status</th>
        </tr>
        <?php
        while ($FetchAllUsers = mysql_fetch_array($GetAllUsers)) {
            ?>
            <tr>
                <td>
                    <a href="UserEdit.php?Id=<?php echo $FetchAllUsers['Id']; ?>">Edit</a> &nbsp; | &nbsp;
                    <a href="User.php?action=delete&Id=<?php echo $FetchAllUsers['Id']; ?>" onClick="return del();">Delete</a>
                </td>
                <td><?php echo $FetchAllUsers['Email']; ?></td>
                <td><?php echo $FetchAllUsers['Phones']; ?></td>
                <td><?php echo $FetchAllUsers['Role']; ?></td>
                <td><?php echo $FetchAllUsers['Status']; ?></td>
            </tr>
            <?php
        }
        ?>
    </table>
       <div class="pagination pagination-centered">
        <ul>
            <?php
            for ($i = 1; $i <= $totalpage; $i++) {
                if ($i == $page)
                    echo '<li class = "active"><a href="User.php?page=' . $i . '">' . $i . '</a></li>';
                else
                    echo '<li><a href="User.php?page=' . $i . '">' . $i . '</a></li>';
            }
            ?>
        </ul>
    </div>
</form>

<?php include_once Path() . "/templates/adminfooter.php"; ?>
