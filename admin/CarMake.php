<?php
if (!isset($_SESSION))
    session_start();
$_SESSION['cat'] = "";

function Path() {
    $path = $_SERVER['DOCUMENT_ROOT'];
    // Differenciate the path for local and web host. Includes database connect file.
    if ($_SERVER['HTTP_HOST'] == "localhost" || $_SERVER['HTTP_HOST'] == "localhost:8080")
        $path .= '/Cabsystem';
    return $path;
}

include_once Path() . '/templates/adminheader.php';
include_once Path() . '/dbop/data/carmake.php';

$page = 1;
$CarStart = 0;
$CarRpp = 2;
$no = CarPagination();
$totalpage = ceil($no / $CarRpp);
if (isset($_GET['action'])) {
    if ($_GET['action'] == "delete") {
        echo $_GET['action'];
        $Id = $_GET['Id'];
        DeleteCar($Id);
        echo $Id;
        echo "<script> alert('Your Record Delete SuccessFully');</script>";
    }
}
if (isset($_GET['page'])) {
    $page = $_REQUEST['page'];
    $a = $page * $CarRpp;
    $CarStart = $a - $CarRpp;
}
$GetCar = GetCarsListing($CarStart, $CarRpp);
?>
<script language="javascript">
    function del()
    {
        var r = confirm("Really Want to Delete An Agent");
        if (!r)
            return false;
    }
</script>
<form action="CarMake.php">
    <h1 align="center">Car Listing</h1>
    <table align='center' class="table table-striped">
        <tr>
            <th>Action</th>
            <th>Name</th>
            <th>Status</th>
        </tr>
        <?php
        while ($FetchCar = mysql_fetch_array($GetCar)) {
            ?>
            <tr>
                <td>
                    <a href="CarMakeEdit.php?Id=<?php echo $FetchCar['Id']; ?>">Edit</a> &nbsp; | &nbsp;
                    <a href="CarMake.php?action=delete&Id=<?php echo $FetchCar['Id']; ?>" onClick="return del();">Delete</a>
                </td>
                <td><?php echo $FetchCar['Name']; ?></td>
                <td><?php echo $FetchCar['Status']; ?></td>
            </tr>
            <?php
        }
        ?>
        <table align='center'>
            <tr>
                <?php
                for ($i = 1; $i <= $totalpage; $i++) {
                    if ($i == $page)
                        echo '<td>' . $i . '</td>';
                    else
                        echo '<td><a href="CarMake.php?page=' . $i . '">' . $i . '</a></td>';
                }
                ?>
            </tr>
        </table>
    </table>
</form>

<?php include_once Path() . "/templates/adminfooter.php"; ?>
