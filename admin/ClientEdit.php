<?php
if (!isset($_SESSION))
    session_start();
$_SESSION['cat'] = "inventory";

function Path() {
    $path = $_SERVER['DOCUMENT_ROOT'];
    // Differenciate the path for local and web host. Includes database connect file.
    if ($_SERVER['HTTP_HOST'] == "localhost" || $_SERVER['HTTP_HOST'] == "localhost:8080")
        $path .= '/Cabsystem';
    return $path;
}

include_once Path() . '/templates/adminheader.php';
include_once Path() . '/dbop/data/client.php';
include_once Path() . '/dbop/data/user.php';

if (isset($_POST['sub'])) {
    $Id = $_POST['Id'];
    $Name = $_POST['Name'];
    $UserId = '21';
    $Email = $_POST['Email'];
    $Phones = $_POST['Phones'];
    $Address = $_POST['Address'];
    $Milestone = $_POST['Milestone'];
    $Note = $_POST['Note'];
    $CreditKm = $_POST['CreditKm'];
    $CreditPoint = $_POST['CreditPoint'];
    $RoleId = 3;
    if (isset($_POST['Status']) && !empty($_POST['Status']))
        $Status = $_POST['Status'];
    else
        $Status = 0;
    ClientEdit($Id, $Name, $Email, $Phones, $Address, $Milestone, $Note, $CreditKm, $CreditPoint, $Status);
    EditUser($Id, $Email, $Phones, $RoleId, $Status);
    echo "<script> alert ('Your Update SuccessFully'); window.location='Client.php';</script>";
}

$Id = $_REQUEST['Id'];
$GetClient = GetClient($Id);
$FetchClient = mysql_fetch_array($GetClient);
?>
<form action="ClientEdit.php" method="post">
    <h1 align="center">Add Client Here</h1>
    <table align="center" class="table table-striped">
        <tr>
            <td> Client ID</td>
            <td><input type="hidden" name="Id" placeholder="Client Id" value="<?php echo $Id; ?>"/></td>
        </tr>
        <tr>
            <td>Enter Client Name</td>
            <td><input type="text" name="Name" value="<?php echo $FetchClient['Name']; ?>" required/></td>
        </tr>
        <tr>
            <td>Enter Client Email</td>
            <td><input type="text" name="Email" value="<?php echo $FetchClient['Email']; ?>" required/></td>
        </tr>
        <tr>
            <td>Enter Client Contact No</td>
            <td><input type="text" name="Phones" value="<?php echo $FetchClient['Phones']; ?>" required lang="10"/></td>
        </tr>
        <tr>
            <td>Enter Client Address</td>
            <td><textarea name="Address"><?php echo $FetchClient['Name']; ?></textarea></td>
        </tr>
        <tr>
            <td>Enter Milestone</td>
            <td><input type="text" name="Milestone" value="<?php echo $FetchClient['Milestone']; ?>"/></td>
        </tr>
        <tr>
            <td>Enter Note</td>
            <td><input type="text" name="Note" value="<?php echo $FetchClient['Note']; ?>"/></td>
        </tr>
        <tr>
            <td>Enter CreditKm</td>
            <td><input type="text" name="CreditKm" value="<?php echo $FetchClient['CreditKm']; ?>"/></td>
        </tr>
        <tr>
            <td>Enter CreditPoint</td>
            <td><input type="text" name="CreditPoint" value="<?php echo $FetchClient['CreditPoints']; ?>"/></td>
        </tr>
        <tr>
            <td>Current Status</td>
            <?php
            if ($FetchClient['Status'] == 1) {
                ?>
                <td>Click for Active<input type="checkbox" name="Status" value="1"  checked="checked"/></td>
                <?php
            }
            ?>
            <?PHP if ($FetchClient['Status'] == 0) { ?>
                <td>Click for Active<input type="checkbox" name="Status" value="1"/></td>
            <?php } ?>
        </tr>
        <tr>
            <td colspan="2" style="text-align: center;"><input type="submit" name="sub" value="Update" class="btn-large"/></td>
        </tr>
    </table>
</form>

<?php
include_once Path() . '/templates/adminfooter.php';
?>