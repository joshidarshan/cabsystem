<?php
if (!isset($_SESSION))
    session_start();
$_SESSION['cat'] = "";

function Path() {
    $path = $_SERVER['DOCUMENT_ROOT'];
    // Differenciate the path for local and web host. Includes database connect file.
    if ($_SERVER['HTTP_HOST'] == "localhost" || $_SERVER['HTTP_HOST'] == "localhost:8080")
        $path .= '/Cabsystem';
    return $path;
}

include_once Path(). '/templates/adminheader.php';
include_once Path(). '/dbop/data/client.php';

$page = 1;
$clientStart = 0;
$clientRpp = 2;
$no = ClientPagination();
$totalpage = ceil($no / $clientRpp);
if (isset($_GET['action'])) {
    if ($_GET['action'] == "delete") {
        $Id = $_GET['Id'];
        ClientDelete($Id);
        echo "<script> alert('Your Record Delete SuccessFully');</script>";
    }
}
if (isset($_GET['page'])) {
    $page = $_REQUEST['page'];
    $a = $page * $clientRpp;
    $clientStart = $a - $clientRpp;
}
$GetAllClient = GetAllClient($clientStart, $clientRpp);
?>
<script language="javascript">
    function del()
    {
        var x;
        var r = confirm("Really Want to Delete An Agent");
        if (!r)
            return false;
    }
</script>
<form action="Client.php" method="post">
    <h1 align="center">Client Listing</h1>
    <div>
        <table align='center' class="table table-striped">
            <tr>
                <th>Action</th>
                <th>Name</th>
                <th>EmailId</th>
                <th>Contacts</th>
                <th>Address</th>
                <th>MileStone</th>
                <th>Note</th>
                <th>CreditKm</th>
                <th>CreditPoint</th>
                <th>Status</th>
            </tr>
            <?php
            while ($FetchAllClient = mysql_fetch_array($GetAllClient)) {
                ?>
                <tr>
                    <td>
                        <a href="ClientEdit.php?Id=<?php echo $FetchAllClient['Id']; ?>">Edit</a> &nbsp; | &nbsp;
                        <a href="Client.php?action=delete&Id=<?php echo $FetchAllClient['Id']; ?>" onClick="return del();">Delete</a>
                    </td>
                    <td><?php echo $FetchAllClient['Name']; ?></td>
                    <td><?php echo $FetchAllClient['Email']; ?></td>
                    <td><?php echo $FetchAllClient['Phones']; ?></td>
                    <td><?php echo $FetchAllClient['Address']; ?></td>
                    <td><?php echo $FetchAllClient['Milestone']; ?></td>
                    <td><?php echo $FetchAllClient['Note']; ?></td>
                    <td><?php echo $FetchAllClient['CreditKm']; ?></td>
                    <td><?php echo $FetchAllClient['CreditPoints']; ?></td>
                    <td><?php echo $FetchAllClient['Status']; ?></td>
                </tr>
                <?php
            }
            ?>
        </table>
    </div>
    <div class="pagination pagination-centered">
        <ul>
            <?php
            for ($i = 1; $i <= $totalpage; $i++) {
                if ($i == $page)
                    echo '<li class = "active"><a href="Client.php?page=' . $i . '">' . $i . '</a></li>';
                else
                    echo '<li><a href="Client.php?page=' . $i . '">' . $i . '</a></li>';
            }
            ?>
        </ul>
    </div>

</form>

<?php
        include_once Path() . '/templates/adminfooter.php';
        ?>