<?php

if (!isset($_SESSION))
    session_start();
$_SESSION['cat'] = "inventory";

function Path() {
    $path = $_SERVER['DOCUMENT_ROOT'];
    // Differenciate the path for local and web host. Includes database connect file.
    if ($_SERVER['HTTP_HOST'] == "localhost" || $_SERVER['HTTP_HOST'] == "localhost:8080")
        $path .= '/Cabsystem';
    return $path;
}

include_once Path() . '/templates/adminheader.php';

if (isset($_POST['sub'])) {
    include_once Path() . '/dbop/data/client.php';
    include_once Path() . '/dbop/data/user.php';
    $Name = $_POST['Name'];
    $UserId = '21';
    $Email = $_POST['Email'];
    $Password = $_POST['Password'];
    $Phones = $_POST['Phones'];
    if(isset($_POST['Address']) && !empty($_POST['Address']))
        $Address = $_POST['Address'];
    else
        $Address = "";
    if(isset($_POST['Milestone']) && !empty($_POST['Milestone']))
        $Milestone = $_POST['Milestone'];
    else
        $Milestone = "";
    if(isset($_POST['Note']) && !empty($_POST['Note']))
        $Note = $_POST['Note'];
    else
        $Note = "";
    $Status = 1;
    $RoleId = 3;

    $Id = ClientAdd($Name, $Email, $Phones, $Address, $Milestone, $Note, 0, 0, $Status);
    AddUser($Id, $Email, $Phones, $Password, $RoleId, $Status);
    echo "<div class='alert alert-success'><a class='close' data-dismiss='alert'>x</a><strong>Client Added!</strong></div>";
}
?>
<script>
    function CheckNumeric(e)
    {
        var key = e.which;
        if (key >= 48 && key <= 57 || key == 8)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    function CheckNumbers() {

        var Phones = $("#Phones").val();
        if (Phones) {
            $.ajax({
                type: "GET",
                url: "../ajax/user.php?type=ContactNo&required=ClientContact&Contact=" + Phones,
                success: function(data) {
                    //alert(data);
                    message = "";
                    if (data == 0) {
                        message = "Already Exist";
                        $("#CheckContact").css({"color": "Red"});
                    }
                    else {
                        message = "Available";
                        $("#CheckContact").css({"color": "Green"});
                    }
                    $("#CheckContact").text(message);
                }
            });
        }
        else
        {
            return false;
        }
    }

    function ChkEmail() {
        var Email = $("#Email").val();
        if (Email) {
            $.ajax({
                type: "GET",
                url: "../ajax/user.php?type=EmailId&required=ClientEmail&Email=" + Email,
                success: function(data) {
                    //alert(data);
                    message = "";
                    if (data == 0) {
                        message = "Already Exist";
                        $("#ChkEmail").css({"color": "Red"});
                    }
                    else {
                        message = "Available";
                        $("#ChkEmail").css({"color": "Green"});
                    }
                    $("#ChkEmail").text(message);
                }
            });
        }
        else {
            return false;
        }
    }
</script>
<form action="ClientAdd.php" method="post">
    <h1 align="center">Add Client Here</h1>
    <table align="center" class="table table-striped ">
        <tr>
            <td>Enter Client Name</td>
            <td><input type="text" name="Name" required/></td>
        </tr>
        <tr>
            <td>Enter Client Email</td>
            <td><input type="text" name="Email" id="Email" required onblur="return ChkEmail();" placeholder="example@gmail.com"/>
                <span id="ChkEmail"></span>
            </td>
        </tr>
        <tr>
            <td>Enter Client Password</td>
            <td><input type="password" name="Password"/></td>
        </tr>
        <tr>
            <td>Enter Client Contact No</td>
            <td><input type="text" name="Phones" Id="Phones" maxlength="10" onkeypress="return CheckNumeric(event);"
                       onblur="return CheckNumbers();" placeholder="Enter Number Only"/><span id="CheckContact"></span>
            </td>

        </tr>
        <tr>
            <td>Enter Client Address</td>
            <td><textarea name="Address"></textarea></td>
        </tr>
        <tr>
            <td>Enter Milestone</td>
            <td><textarea name="Milestone"></textarea></td>
        </tr>
        <tr>
            <td>Enter Note</td>
            <td><textarea id="Note" name="Note"></textarea></td>
        </tr>
        <tr>
            <td colspan="2" style="text-align: center;"><input type="submit" name="sub" value="Add" class="btn-large"/></td>
        </tr>
    </table>
</form>

<?php

include_once Path() . '/templates/Adminfooter.php';
?>