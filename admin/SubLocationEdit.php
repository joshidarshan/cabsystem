<?php
if (!isset($_SESSION))
    session_start();
$_SESSION['cat'] = "";

function Path() {
    $path = $_SERVER['DOCUMENT_ROOT'];
    // Differenciate the path for local and web host. Includes database connect file.
    if ($_SERVER['HTTP_HOST'] == "localhost" || $_SERVER['HTTP_HOST'] == "localhost:8080")
        $path .= '/Cabsystem';
    return $path;
}

include_once Path() . '/dbop/data/Location.php';
include_once Path() . '/dbop/data/sublocation.php';
include_once Path() . '/templates/adminheader.php';

if (isset($_POST['sub'])) {
    $Id = $_POST['Id'];
    $LocationId = $_POST['LocationId'];
    $SubLocation = $_POST['SubLocation'];
    $Fare = $_POST['Fare'];
    $Status = "";
    if(isset($_POST['Status'])|| !empty($_POST['Status']))
        $Status = 1;
    else 
        $Status = 0;
    
    UpdateSubLocation($Id,$LocationId, $SubLocation,$Fare,$Status);
    echo "<script> alert('Your Record Success Fully Updated'); window.location = 'SubLocation.php';</script>";
}

$Id = $_REQUEST['Id'];
$GetSubLocation = GetSingleSubLocation($Id);
$FetchSubLocations = mysql_fetch_array($GetSubLocation);
?>

<form action="SubLocationEdit.php" method="post" id ="LocationAdd" onsubmit=" return SubmitCheck();">
    <h1 align="center" >Update Sub Location Detail</h1>
    <table align="center" class="table table-striped" >
        
        <tr>
            <td>Select Main Location</td>
            <td><select name="LocationId">
                    <?php
                    $locations = GetAllLocations();
                    while ($fetchlocation = mysql_fetch_array($locations)) {
                        if ($FetchSubLocations['Id'] == $fetchlocation['Id']) {
                            ?>
                            <option value="<?php echo $fetchlocation['Id']; ?>" selected="selected"><?php echo $fetchlocation['Name']; ?></option>
                            <?php
                        } else {
                            ?>
                            <option value="<?php echo $fetchlocation['Id']; ?>"><?php echo $fetchlocation['Name']; ?></option>
                        <?php }
                    }
                    ?>
                </select></td><td><input type="hidden" name="Id" value="<?php echo $Id;?>"/></td>
        </tr>
        <tr>
            <td>Enter Sub Location Name</td>
            <td><input type="text" name="SubLocation" value="<?php echo $FetchSubLocations['SubLocation'];?> "/></td>
        </tr>

        <tr>
            <td>Enter Fare Name</td>
            <td><input type="text" name="Fare" value="<?php echo $FetchSubLocations['Fare'];?> "/></td>
        </tr>
        
        <tr>
            <td>Make Status</td>
            <td>
                <?php 
                    if($FetchSubLocations['Status']==1){
                        ?>
                <input type="checkbox" name="Status" value="1" checked="checked"/>
                <?php
                    }
                else {
                    ?>
                <input type="checkbox" name="Status" value="1"/>
                <?php } ?>
            </td>
        </tr>
        <tr>
            <td colspan="2" style="text-align: center;"><input type="submit" name="sub" value="Update" class="btn-large"/></td>
        </tr>
    </table>
</form>
<?php
include_once Path() . '/templates/adminfooter.php';
?> 