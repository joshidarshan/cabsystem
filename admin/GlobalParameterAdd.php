<?php
if (!isset($_SESSION))
    session_start();
$_SESSION['cat'] = "";

function Path() {
    $path = $_SERVER['DOCUMENT_ROOT'];
    // Differenciate the path for local and web host. Includes database connect file.
    if ($_SERVER['HTTP_HOST'] == "localhost" || $_SERVER['HTTP_HOST'] == "localhost:8080")
        $path .= '/Cabsystem';
    return $path;
}
/*Author :- Darshan Joshi,
 * Parblem :- While Adding, Query MisMatch Error
 * Change By :- Hardik Vyas
 * Change On :- 13-0--13,Friday
 */
include_once Path() . '/templates/adminheader.php';

if (isset($_POST['sub'])) {
    include_once Path() . '/dbop/data/globalperameter.php';

    $mh_percentage = $_POST['mh_percentage'];
    $mh_multipleof = $_POST['mh_multipleof'];
    $cp_minimunkm = $_POST['cp_minimunkm'];
    $cp_point = $_POST['cp_point'];
    $cp_maxpoints = $_POST['cp_maxpoints'];
    $cp_point_to_rupee_rate = $_POST['cp_point_to_rupee_rate'];
    $cp_discount_percent = $_POST['cp_discount_percent'];
    $cab_freehours = $_POST['cab_freehours'];
    $cab_nearbykm = $_POST['cab_nearbykm'];
    $cab_average_speed = $_POST['cab_average_speed'];
    $cp_redeemtion_level = $_POST['cab_redeemtion_level'];
    $recordperpage = $_POST['cab_perpage'];
    $virtual_cab_per_model = $_POST['virtualCabPerModel'];
    $booking_confirmation_time = $_POST['bookingConfirmationTime'];
    

    AddGlobalParameter($mh_percentage, $mh_multipleof, $cp_minimunkm, $cp_point, $cp_maxpoints,
            $cp_point_to_rupee_rate, $cp_discount_percent, $cab_freehours, 
            $cab_nearbykm, $cab_average_speed, $cp_redeemtion_level, $recordperpage, 
            $virtual_cab_per_model, $booking_confirmation_time);
    echo "<div class='alert alert-success'><a class='close' data-dismiss='alert'>x</a><strong>Global Parameter Added!</strong></div>";
}
?>

<form action="GlobalParameterAdd.php" method="post">
    <h1 align="center">Add Shared Fare Here</h1>
    <table align="center" class="table table-striped">
        <tr>
            <td>Maharaja Fare Percentage</td>
            <td><input type="text" name="mh_percentage"/></td>
        </tr>
        <tr>
            <td>Maharaja Fare Multiple Of</td>
            <td><input type="text" name="mh_multipleof"/></td>
        </tr>
        <tr>
            <td>Credit Point Min Km</td>
            <td><input  type="text" name="cp_minimunkm"/></td>
        </tr>
        <tr>
            <td>Credit Point</td>
            <td><input  type="text" name="cp_point"/></td>
        </tr>
        <tr>
            <td>Credit Point Max Km</td>
            <td><input  type="text" name="cp_maxpoints"/></td>
        </tr>
        <tr>
            <td>Credit Point To Rupee Rate</td>
            <td><input  type="text" name="cp_point_to_rupee_rate"/></td>
        </tr>
        <tr>
            <td>Credit Point Discount Percent</td>
            <td><input  type="text" name="cp_discount_percent"/></td>
        </tr>
        <tr>
            <td>Cab Free Hours</td>
            <td><input  type="text" name="cab_freehours"/></td>
        </tr>
        <tr>
            <td>Cab Near By KM</td>
            <td><input  type="text" name="cab_nearbykm"/></td>
        </tr>
        <tr>
            <td>Average Speed</td>
            <td><input  type="text" name="cab_average_speed"/></td>
        </tr>
        <tr>
            <td>Redeemtion Level</td>
            <td><input type="text" id="cab_redeemtion_level" name="cab_redeemtion_level" /></td>
        </tr>
        <tr>
            <td>Record Per Page</td>
            <td><input type="text" id="cab_perpage" name="cab_perpage" /></td>
        </tr>
        <tr> 
            <td>Virtual Cab Per Model</td>
            <td><input type="text" id="virtualCabPerModel" name="virtualCabPerModel" /></td>
        </tr>
        <tr>
            <td>Booking Confirmation Time</td>
            <td><input type="text" id="bookingConfirmationTime" name="bookingConfirmationTime" /></td>
        </tr>
        <tr>
            <td colspan="2" style="text-align: center;"><input type="submit" name="sub" value="Add" class="btn-success btn"/></td>
        </tr>
    </table>
</form>

<?php
include_once Path().'/templates/adminfooter.php';
?>