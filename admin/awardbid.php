<?php
function LPath() {
    $path = $_SERVER['DOCUMENT_ROOT'];
    // Differenciate the path for local and web host. Includes database connect file.
    if ($_SERVER['HTTP_HOST'] == "localhost" || $_SERVER['HTTP_HOST'] == "localhost:8080")
        $path .= '/Cabsystem';
    return $path;
}

include_once LPath().'/dbop/data/cancelation.php';
include_once LPath().'/dbop/data/booking.php';
include_once LPath().'/admin/bookingcancellation.php';

if(isset($_GET['bookingId']) && isset($_GET['cabRouteId']) && !empty($_GET['bookingId']) && !empty($_GET['cabRouteId'])){
    $bookingId = $_GET['bookingId'];
    $cabRouteId = $_GET['cabRouteId'];
    
    // Awarding bid
    $isAwarded = AwardBid($bookingId);
    
    // Cancelling other bid and bookings
    // Getting list of other bookings.
    $bookingIdList = GetRestOfBookingsForCabRoute($bookingId, $cabRouteId);
    foreach($bookingIdList AS $bk=>$bv){
        CancelBookingAddRefund($bv);
    }
}
?>
