<?php
if (!isset($_SESSION))
    session_start();

function Path() {
    $path = $_SERVER['DOCUMENT_ROOT'];
    // Differenciate the path for local and web host. Includes database connect file.
    if ($_SERVER['HTTP_HOST'] == "localhost" || $_SERVER['HTTP_HOST'] == "localhost:8080")
        $path .= '/Cabsystem';
    return $path;
}

function SliderImageUrl() {
    $url = "http://" . $_SERVER['HTTP_HOST'];
    if ($_SERVER['HTTP_HOST'] == "localhost" || $_SERVER['HTTP_HOST'] == "localhost:8080")
        $url .= '/Cabsystem';

    return $url;
}

include_once Path() . '/templates/header.php';
include_once Path() . '/dbop/data/location.php';
include_once Path() . '/dbop/data/hourcharges.php';
include_once Path() . '/dbop/data/hourcharges.php';
//include_once Path() . '/pop/index.php';
?>
<script type="text/javascript" src="js/jquery.cycle.js"></script>
<div class="container">
    <div class="marketing">
        <div id="tabs" class="hs_tabs span9" style="float: left; height: 300px; width: 200px;">
            <div class="span2" style="width: 160px; height: 67px; padding-top: 22px; cursor: pointer;"><a class="end" style="font-size: large;" title="Bid and go for any destination at your price! First time in Gujarat"><span onclick="return Bid();">Bid Cab</span></a></div>
            <div class="span2" style="width: 160px; height: 67px; padding-top: 22px; margin-top: 14px;"><a href="#shared_cab" class="end" style="font-size: large;" title="Share cab option with point to point service!">Shared Cab</a></div>
            <div class="span2" style="width: 160px; height: 67px; padding-top: 22px; margin-top: 14px;"><a href="#oneway_cab" class="end" style="font-size: large;" title="Are you in search of one way charter cab? This option is for you!">One Way Cab</a></div>

        </div>

        <div id="banner0" class="banner" style="width: 100%; max-width: 600px; float: left; max-height: 500px;">
            <?php
            include_once Path() . '/dbop/data/slider.php';
            $GetSlider = GetAllImages();
            while ($fetchImage = mysql_fetch_array($GetSlider)) {
                ?>
                <div style="width: 100%;">
                    <img src= "<?php echo SliderImageUrl() . '/Upload/SliderImages/' . $fetchImage['Image']; ?>" style="height: 300px; width: 600px; overflow: hidden;" class="img-responsive"/>
                    <p><b>Car Name&nbsp;:-</b><?php echo $fetchImage['Name'], $fetchImage['Description']; ?></p>
                </div>
            <?php } ?>
        </div>

        <div id="tabs" class="hs_tabs span9" style="float: left; height: 100px; width: 200px; margin-left: -20px;">
            <div class="span2" style="width: 160px; height: 67px; padding-top: 22px;"><a class="end" style="font-size: large; cursor: none" title="Charter cab bookings">Charter Cab</a></div>
            <div class="span2" style="width: 160px; height: 67px; padding-top: 22px; margin-top: 14px;"><a href="#outstation_cab" class="end" style="font-size: large; line-height: 27px;" title="Looking for round trip? This option is for you!">Book Outstation Cab</a></div>
            <div class="span2" style="width: 160px; height: 67px; padding-top: 22px; margin-top: 14px;"><a href="#city_cab" class="end" style="font-size: large;" title="Looking for city ride? This option is for you!">Book Local Cab</a></div>
        </div> 

        <script type="text/javascript">
            function Bid(){
                window.location = 'bidnow.php';
            }
            
            $(document).ready(function() {
                $('#banner0 div:first-child').css('display', 'block');
                $("a[class='end']").click(function() {
                $("html, body").animate({scrollTop: $(document).height()}, "slow");
                return false;
                });
            });
            var banner = function() {
                $('#banner0').cycle({
                    before: function(current, next) {
                        $(next).parent().height($(next).outerHeight());
                    }
                });
            };
            setTimeout(banner, 2000);
        </script>
        <br>


        <style>
            .textbox4{
                padding: 6px 20px;
                border: 0;
                height:25px;
                width: 121px;
                border-radius: 10px;
                -moz-border-radius: 10px;
                -moz-box-shadow: 1px 1px 0 0 #FFF, 5px 5px 40px 2px #BBB inset;
                -webkit-background-clip: padding-box;
                outline:0;
            }
        </style>

        <div class="row-fluid">
            <div class="span4" id="outstation_cab">
                <hr>
                <span style="font-size: x-large; font-weight: bold;">Book Outstation Cab</span>
                <form action="listing.php" method="post">
                    <div style="border:0px dashed gray; width: 519px; margin-left: 21%;
                         "content="width=device-width; initial-scale=0.1  " >
                        &nbsp;
                        <table  class="textbox4" style="background-color: #CACACA" >
                            <tr>
                                <td class="textbox4"><span style="color: black; font-weight:bold; ">Departure</span> <br/>
                                    <input type="text" name="FromDate"  id="FromDateTime"  required class="dttm" /></td>
                                <td class="textbox4"><span style="color: black; font-weight:bold; ">Return</span><br/>
                                    <input type="text" name="ToDate" class="dttm" required/></td>
                            </tr>
                            <?php $GetLocation = GetAllLocations(); ?>
                            <tr>
                                <td class="textbox4"><span style="color: black; font-weight:bold; ">Source Location</span> <br/>
                                    <select name="FromLocation"  >
                                        <option value="">Select</option>
                                        <?php while ($FetchLocations = mysql_fetch_array($GetLocation)) {
                                            ?>
                                            <option value="<?php echo $FetchLocations['Id']; ?>" ><?php
                                                echo $FetchLocations['Name'];
                                                $a = $FetchLocations['Name'];
                                                ?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                    <input type="hidden" name="NoPerson" value="1"/>                                
                                    <input type="hidden" id="ToLocation" name="ToLocation" />
                                    <input type="hidden" id="DestName" name="DestName" />
                                </td>
                                <td colspan="2"> <input type="submit" name="outstation" class="btn-large"  /></td>
                            </tr>
                            <tr>
                            </tr>
                        </table>
                    </div>
                </form>
            </div>

            <div class="span4" id="city_cab">
                <hr>
                <span style="font-size: x-large; font-weight: bold;">Book City Cab</span>
                <form action="listing.php" method="post">
                    <input type="hidden" id="type" name="type" value="shared" />
                    <div style=" width: 519px; margin-left: 21%;
                         "content="width=device-width; initial-scale=0.1  " >
                        &nbsp;
                        <table  class="textbox4"  style="background-color: #CACACA;">
                            <tr>
                                <td class="textbox4"><span style="color: black; font-weight:bold; ">Choose Date</span> <br/>
                                    <input type="text" name="FromDate"  id="FromDateTime"  required class="dttm" /></td>
                                <td class="textbox4"><span style="color: black; font-weight:bold; ">Choose Date</span> <br/>
                                    <input type="text" name="ToDate"  id="FromDateTime"  required class="dttm" /></td>

                            </tr>
                            <tr>
                                <?php $GetLocation = GetAllLocations(); ?>
                                <td class="textbox4"><span style="color: black; font-weight:bold; ">Choose City </span> <br/>
                                    <select name="FromLocation" id="Locations" onchange="return GetSubLocation();"  >
                                        <option>Select</option>
                                        <?php while ($FetchLocations = mysql_fetch_array($GetLocation)) {
                                            ?>
                                            <option value="<?php echo $FetchLocations['Id']; ?>" ><?php echo $FetchLocations['Name']; ?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </td>
                                <td class="textbox4"><span style="color: black; font-weight:bold; ">Choose Proper Location</span>
                                    <br/>
                                    <select id="SubLocationId" name="SubLocationId" >
                                        <option>Select</option>
                                    </select>
                                </td>

                            <script>
                                $("#bidCab").click(function() {
                                    window.location = 'bidnow.php';
                                });
                                function OpenBid() {
                                    window.location = 'bidnow.php';
                                }

                                function GetSubLocation() {
                                    //alert ('Hello');
                                    var LocationId = $("#Locations").val();
                                    //alert (LocationId);                ;
                                    $.ajax({
                                        type: "GET",
                                        url: "./ajax/location.php?type=location&required=sublocationlist&locationId=" + LocationId,
                                        success: function(data) {
                                            //alert(data);
                                            $("#SubLocationId").find('option').remove().end();
                                            $("#SubLocationId").html(data);
                                        }
                                    });
                                }
                            </script>
                            </tr>
                            <tr>
                                <td class="textbox4"><span style="color: black; font-weight:bold; ">Choose Hourly Package</span> <br/>
                                    <?php $GetAllCharges = GetAllCharges(); ?> 
                                    <select name="ChargeId">
                                        <option>Select</option>
                                        <?php while ($FetchCharges = mysql_fetch_array($GetAllCharges)) {
                                            ?>
                                            <option value="<?php echo $FetchCharges['Id']; ?>" ><?php echo $FetchCharges['Hour'] . " Hours- " . $FetchCharges['Km'] . " Km " ; ?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>

                                </td>
                                <td colspan="2" style="text-align: center"> <input type="submit" name="citycab" class="btn-large"  /></td>
                            </tr>


                        </table>
                    </div>
                </form>
            </div>
            <div class="span4" id="shared_cab">
                <hr>
                <span style="font-size: x-large; font-weight: bold;">Book Shared Cab</span>
                <form action="listing.php" method="post">
                    <input type="hidden" id="type" name="type" value="shared" />
                    <div style=" width: 519px; margin-left: 21%;
                         "content="width=device-width; initial-scale=0.1  " >
                        &nbsp;
                        <table  class="textbox4"  style="background-color: #CACACA;">
                            <tr>
                                <td class="textbox4"><span style="color: black; font-weight:bold; ">Departure</span> <br/>
                                    <input type="text" name="FromDate"  id="FromDateTime"  required class="dttm" /></td>
                                <td class="textbox4"><span style="color: black; font-weight:bold; ">To Date</span><br/>
                                    <input type="text" name="ToDate" class="dttm"   required/></td>
                            </tr>

                            <tr>
                                <?php $GetLocation = GetAllLocations(); ?>
                                <td class="textbox4"><span style="color: black; font-weight:bold; ">From Location</span> <br/>
                                    <select name="FromLocation"  >
                                        <option>Select</option>
                                        <?php while ($FetchLocations = mysql_fetch_array($GetLocation)) {
                                            ?>
                                            <option value="<?php echo $FetchLocations['Id']; ?>" ><?php echo $FetchLocations['Name']; ?></option>
                                            <?php
                                        }
                                        ?>
                                    </select><input type="hidden" name="SourceName" value="<?php echo $FetchLocations['Name']; ?>"/></td>
                                <?php $GetLocation = GetAllLocations(); ?>
                                <td class="textbox4"><span style="color: black; font-weight:bold; ">To Location</span> <br/>
                                    <select name="ToLocation" id="Locations" onchange="return GetSubLocation();"  >
                                        <option>Select</option>
                                        <?php while ($FetchLocations = mysql_fetch_array($GetLocation)) {
                                            ?>
                                            <option value="<?php echo $FetchLocations['Id']; ?>" ><?php echo $FetchLocations['Name']; ?></option>
                                            <?php
                                        }
                                        ?>
                                    </select><input type="hidden" name="DestName" value="<?php echo $FetchLocations['Name']; ?>"/></td>
                            </tr>
                            <tr>
                                <td class="textbox4"><span style="color: black; font-weight:bold; ">Choose Proper Location</span>
                                    <br/>
                                    <select id="SubLocationId" name="SubLocationId" >
                                        <option>Select</option>
                                    </select>
                                </td>

                            <script>
                                function GetSubLocation() {
                                    //alert ('Hello');
                                    var LocationId = $("#Locations").val();
                                    //alert (LocationId);                ;
                                    $.ajax({
                                        type: "GET",
                                        url: "./ajax/location.php?type=location&required=sublocationlist&locationId=" + LocationId,
                                        success: function(data) {
                                            //alert(data);
                                            $("#SubLocationId").find('option').remove().end();
                                            $("#SubLocationId").html(data);
                                        }
                                    });
                                }
                            </script>
                            <td > <input type="submit" name="shared" class="btn-large"  /></td>
                            </tr>
                        </table>
                    </div>
                </form>
            </div>
            <div class="span4" id="oneway_cab">
                <hr>
                <span style="font-size: x-large; font-weight: bold;">Book Oneway Charter Cab</span>
                <form action="listing.php" method="post">
                    <input type="hidden" id="type" name="type" value="shared" />
                    <div style=" width: 519px; margin-left: 21%;
                         "content="width=device-width; initial-scale=0.1  " >
                        &nbsp;
                        <table  class="textbox4" style="background-color:#CACACA" >
                            <tr>
                                <td class="textbox4"><span style="color: black; font-weight:bold; ">Departure</span><br/>
                                    <input type="text" name="FromDate"  id="FromDateTime"   required class="dttm" /></td>
                                <td class="textbox4"><span style="color: black; font-weight:bold; ">Return</span><br/>
                                    <input type="text" name="ToDate" class="dttm" required /></td>
                            </tr>

                            <tr>
                                <?php $GetLocation = GetAllLocations(); ?>
                                <td class="textbox4"><span style="color: black; font-weight:bold; ">Source Location</span> <br/>
                                    <select name="FromLocation" id="SLocationId"   onchange=" return  Check();">
                                        <option>Select</option>
                                        <?php while ($FetchLocations = mysql_fetch_array($GetLocation)) {
                                            ?>
                                            <option value="<?php echo $FetchLocations['Id']; ?>" ><?php echo $FetchLocations['Name']; ?></option>
                                            <?php
                                        }
                                        ?>
                                    </select><input type="hidden" name="SourceName" value="<?php echo $FetchLocations['Name']; ?>"/></td>
                                <?php $GetLocation = GetAllLocations(); ?>
                                <td class="textbox4"><span style="color: black; font-weight:bold; ">Destination Location </span><br/>
                                    <select name="ToLocation" id="DLocationId"  onchange=" return  Check();">
                                        <option>Select</option>
                                        <?php while ($FetchLocations = mysql_fetch_array($GetLocation)) {
                                            ?>
                                            <option value="<?php echo $FetchLocations['Id']; ?>" ><?php echo $FetchLocations['Name']; ?></option>
                                            <?php
                                        }
                                        ?>
                                    </select><input type="hidden" name="DestName" value="<?php echo $FetchLocations['Name']; ?>"/></td>
                            </tr>
                            <tr>

                                <td colspan="2" style="text-align: center"> <input type="submit" name="OneWay" class="btn-large"  /></td>
                            </tr>
                        </table>
                    </div>
                    <td><input type="hidden" name="DistanceKm" id="dist"/>
                </form>
                <tr><td><input type="hidden" id="Loc"/></td>
                    <td><input type="hidden" id="dest"/></td>
                </tr>


                <script type="text/javascript">
                    function Check() {
                        //alert("In API Call");
                        //var or = document.getElementById("SLocationId").value+", Gujarat";
                        or = $("#SLocationId option:selected").text();
                        //var ds = document.getElementById("DLocationId").value+", Gujarat";
                        //alert(ds);
                        ds = $("#DLocationId option:selected").text();
                        /*document.getElementById("orig").value = "";
                         document.getElementById("dest").value = "";*/

                        var origin = or, //new google.maps.LatLng(55.930385, -3.118425),
                                destination = ds, //"Stockholm, Sweden",
                                service = new google.maps.DistanceMatrixService();

                        service.getDistanceMatrix(
                                {
                                    origins: [origin],
                                    destinations: [destination],
                                    travelMode: google.maps.TravelMode.DRIVING,
                                    avoidHighways: false,
                                    avoidTolls: false
                                },
                        callback
                                );

                        function callback(response, status) {
                            var orig = document.getElementById("Loc");
                            dest = document.getElementById("dest"),
                                    dist = document.getElementById("dist");
                            if (status == "OK") {
                                orig.value = response.destinationAddresses[0];
                                dest.value = response.originAddresses[0];
                                //dist.value = response.rows[0].elements[0].distance.text;
                                var ok = response.rows[0].elements[0].distance.text;
                                dist.value = parseInt(ok);
                            } else {
                                alert("Error: " + status);
                            }
                        }
                        return true;
                    }
                </script>
            </div>
        </div>
        <script type="text/javascript"><!--
        $('#tabs a').tabs();
//--></script>
    </div>

</div>

<script>
            function GoEnd() {
                $("a[class='end']").click(function() {
                $("html, body").animate({scrollTop: $(document).height()}, "slow");
                return false;
                });
            }

            $(function() {
                $('.dttm').appendDtpicker({
                    "dateOnly": true,
                    "futureOnly": true
                });
            });
</script>

<div id="bottom"></div>
<?php include_once Path() . '/templates/footer.php'; ?>