
        <!-- Footer
        ================================================== -->
        <footer class="footer">
            <div id="ft" class="container">
<!--                <ul class="footer-links">
                    <li class="active">
                        <a href="1">Home</a>
                    </li>
                    <li class="">
                        <a href="">Share Cab</a>
                    </li>
                    <li class="">
                        <a href="4">Rent Cab</a>
                    </li>
                    <li class="">
                        <a href="6">Feed Back</a>
                    </li>
                    <li class="">
                        <a href="7">Client List</a>
                    </li>
                    <li class="">
                        <a href="8">Disclaimer</a>
                    </li>
                    <li class="">
                        <a href="9">About Us</a>
                    </li>
                    <li class="">
                        <a href="10">Contact Us</a>
                    </li>
                </ul>-->
                <p>All Rights reserved: 2009-2013</p>
            </div>
        </footer>

        <!-- Le javascript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script type="text/javascript" src="js/bootstrap-transition.js"></script>
        <script type="text/javascript" src="js/bootstrap-alert.js"></script>
        <script type="text/javascript" src="js/bootstrap-modal.js"></script>
        <script type="text/javascript" src="js/bootstrap-dropdown.js"></script>
        <script type="text/javascript" src="js/bootstrap-scrollspy.js"></script>
        <script type="text/javascript" src="js/bootstrap-tab.js"></script>
        <script type="text/javascript" src="js/bootstrap-tooltip.js"></script>
        <script type="text/javascript" src="js/bootstrap-popover.js"></script>
        <script type="text/javascript" src="js/bootstrap-button.js"></script>
        <script type="text/javascript" src="js/bootstrap-collapse.js"></script>
        <script type="text/javascript" src="js/bootstrap-carousel.js"></script>
        <script type="text/javascript" src="js/bootstrap-typeahead.js"></script>
        <script type="text/javascript" src="js/bootstrap-affix.js"></script>
        <script type="text/javascript" src="js/holder.js"></script>
        <script type="text/javascript" src="js/application.js"></script>
    </body>
</html>