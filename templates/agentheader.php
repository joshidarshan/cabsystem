<?php
if (!isset($_SESSION))
    session_start();
if(!isset($_SESSION['UID']) || empty($_SESSION['UID']))
    header ("location: AgentLogin.php");
//$selCategory = "";
//    if(isset($_SESSION) && !empty($_SESSION))
//        $selCategory = $_SESSION['cat'];
    
function LPath() {
    $path = "";
    // Differenciate the path for local and web host. Includes database connect file.
    if ($_SERVER['HTTP_HOST'] == "localhost" || $_SERVER['HTTP_HOST'] == "localhost:8080")
        $path .= '/Cabsystem';
    return $path;
}

?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <title>Cabsystem | Administration</title>

        <script src="<?php echo LPath().'/js/jquery-1.10.2.min.js';?>" type="text/javascript"></script>
        <script src="<?php echo LPath().'/bootstrap/js/bootstrap.js'?>" type="text/javascript"></script>
        <script src="<?php echo LPath().'/js/jquery.validate.min.js'?>" type="text/javascript"></script>
        
        <link href="<?php echo LPath().'/bootstrap/css/bootstrap.css';?>" rel="stylesheet" />
        <link href="<?php echo LPath().'/bootstrap/css/bootstrap-responsive.css';?>" rel="stylesheet" />

        <script src="<?php echo LPath().'/js/jquery.simple-dtpicker.js';?>" type="text/javascript"></script>
        <link href="<?php echo LPath().'/css/jquery.simple-dtpicker.css';?>" rel="stylesheet"/>
        
    </head>

    <body>
        <div class="navbar navbar-fixed-top hidden-print">  
            <div class="navbar-inner">  
                <div class="container">  
                    <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </a>
                    <a class="brand" href="#">Agent Profile</a>
                    <div class="nav-collapse">
                        <ul class="nav">  
                            <li <?php if (isset($selCategory) && !empty($selCategory) && $selCategory == 'home') {
    echo "class='active'";
} else {
    echo "class=''";
    } ?> ><a href="index.php"><i class="icon-home">&nbsp;</i>&nbsp;Home</a></li>  

                            <li <?php if (isset($selCategory) && !empty($selCategory) && $selCategory == 'inventory') {
    echo "class='dropdown active'";
} else {
    echo "class='dropdown'";
} ?> >  
                                <a class="dropdown-toggle" data-toggle="dropdown" href="#">Inventory<b class="caret"></b></a>  
                                <ul class="dropdown-menu bottom-up pull-right">  
<!--                                    <li><a href="client.php">Client Manage</a></li>  
                                    <li><a href="clientadd.php">Client Add</a></li>-->

                                    <li class="divider"></li>  

<!--                                    <li><a href="agent.php">Agent Manage</a></li>
                                    <li><a href="agentadd.php">Agent Add</a></li>-->
                                    
                                    <li class="divider"></li>
                                    
                                    <li><a href="Agentdriver.php">Driver Manage</a></li>  
                                    <li><a href="Agentdriveradd.php">Driver Add</a></li>
                                    
                                    <li class="divider"></li>
                                    
                                    <li><a href="Agentcab.php">Cab Manage</a></li>
                                    <li><a href="Agentcabadd.php">Cab Add</a></li>
                                </ul>  
                            </li>  
                            
                            <li <?php if (isset($selCategory) && !empty($selCategory) && $selCategory == 'car') {
    echo "class='dropdown active'";
} else {
    echo "class='dropdown'";
} ?> >  
<!--                                <a class="dropdown-toggle" data-toggle="dropdown" href="#">Car <b class="caret"></b></a>  
                                <ul class="dropdown-menu bottom-up pull-right">  
                                    <li><a href="cartype.php">Car Type</a></li>  
                                    <li><a href="cartypeadd.php">Car Type Add</a></li>

                                    <li class="divider"></li>  

                                    <li><a href="carmodel.php">Car Model</a></li>
                                    <li><a href="carmodeladd.php">Car Model Add</a></li>
                                    
                                    <li class="divider"></li>
                                    
                                    <li><a href="carmake.php">Car Make</a></li>  
                                    <li><a href="carmakeadd.php">Car Make Add</a></li>
                                </ul>  
                            </li> -->
                            
                            <li <?php if (isset($selCategory) && !empty($selCategory) && $selCategory == 'route') {
    echo "class='dropdown active'";
} else {
    echo "class='dropdown'";
} ?>>
                                <a href="dropdown-toggle" data-toggle="dropdown" href="#">Route<b class="caret"></b></a>
                                <ul class="dropdown-menu bottom-up pull-right">
<!--                                    <li><a href="location.php">Locations</a></li>
                                    <li><a href="locationadd.php">Location Add</a>-->
                                    
<!--                                    <li class="divider"></li>
                                    
                                    <li><a href="sharedfare.php">SharedFare</a></li>
                                    <li><a href="sharedfareadd.php">SharedFare Add</a></li>
                                    -->
<!--                                    <li class="divider"></li>
                                    
                                    <li><a href="breakpoint.php">BreakPoint</a></li>
                                    <li><a href="breakpointadd.php">BreakPoint Add</a></li>
                                    -->
                                    <li class="divider"></li>
                                    
                                    <li><a href="Agentcabroute.php">Cab Routes</a></li>
                                    <li><a href="Agentcabrouteadd.php">Cab Route Add</a></li>
                                </ul>
                            </li>


                            <li <?php if (isset($selCategory) && !empty($selCategory) && $selCategory == 'reporting') {
    echo "class='dropdown active'";
} else {
    echo "class='dropdown'";
} ?>>
                                <a href="dropdown-toggle" data-toggle="dropdown" href="#">Reporting<b class="caret bottom-up"></b></a>
                                <ul class="dropdown-menu bottom-up pull-right">
                                    <li><a href="#">Chartered Cabs</a></li>
                                    <li class="divider"></li>
                                    <li><a href="#">Shared Cabs</a></li>
                                    <li class="divider"></li>
                                    <li><a href="#">Financial Reporting</a></li>
                                </ul>
                            </li>

                            <li <?php if (isset($selCategory) && !empty($selCategory) && $selCategory == 'promotion') {
    echo "class='dropdown active'";
} else {
    echo "class='dropdown'";
} ?>>
<!--                                <a href="dropdown-toggle" data-toggle="dropdown" href="#">Promotions<b class="caret bottom-up"></b></a>
                                <ul class="dropdown-menu bottom-up pull-right">
                                     Commented because coupon code, discount are no longer required by client.
                                    <li><a href="#">Coupon Code</a></li>
                                    <li class="divider"></li>
                                    <li><a href="#">Discount Schemes</a></li>
                                    <li class="divider"></li>
                                    <li><a href="Promotion.php">Promotion Listing</a></li>
                                    <li><a href="PromotionAdd.php">Promotion By SMS/Email</a></li>
                                </ul>
                            </li>-->

                            <li <?php if (isset($selCategory) && !empty($selCategory) && $selCategory == 'tools') {
    echo "class='dropdown active'";
} else {
    echo "class='dropdown'";
} ?>>
<!--                                <a href="dropdown-toggle" data-toggle="dropdown" href="#"><i class="icon">&nbsp;</i>Tools<b class="caret bottom-up"></b></a>
                                <ul class="dropdown-menu bottom-up pull-right">
                                    <li><a href="#">Allow/Ban Users</a></li>
                                    
                                    <li class="divider"></li>
                                    
                                    <li><a href="GlobalParameter.php">Global Settings</a></li>
                                    
                                    <li class="divider"></li>
                                    
                                    <li><a href="crownjob.php">Crown Jobs</a></li>
                                </ul>
                            </li>-->

                            <li><a href="AgentLogout.php">LogOff</a></li>
                        </ul>  
                    </div>
                </div>  
            </div>  
        </div>
        
        <div id="spacer" style="height: 48px;"></div>
        