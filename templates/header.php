<?php
if(!isset($_SESSION))
    session_start ();
//function HPath() {
//    $url = "http://" . $_SERVER['HTTP_HOST'];
//    if ($_SERVER['HTTP_HOST'] == "localhost" || $_SERVER['HTTP_HOST'] == "localhost:8080")
//        $url .= '/Cabsystem';
//   
//    return $url;
//    
//}
function HPath() {
    $path = "";
    // Differenciate the path for local and web host. Includes database connect file.
    if ($_SERVER['HTTP_HOST'] == "localhost" || $_SERVER['HTTP_HOST'] == "localhost:8080")
        $path .= '/Cabsystem';
    return $path;
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        
        <meta http-equiv="content-type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">
        <title>Cab</title>
        <!--<meta name="viewport" content="width=device-width, initial-scale=1.0">-->
        <meta name="description" content="">
        <meta name="author" content="">

        <script type="text/javascript" src="<?php echo HPath(). '/js/widgets.js';?>"></script>
        <script type="text/javascript" src="<?php echo HPath(). '/js/jquery-1.7.1.min.js';?>"></script>
        <script type="text/javascript" src="<?php echo HPath(). '/js/tabs.js';?>"></script>     
        <script type="text/javascript" src="<?php echo HPath(). '/js/jquery.simple-dtpicker.js';?>"></script>
        
        <!-- Le styles -->
        <link rel="stylesheet" href="<?php echo HPath().'/css/bootstrap.css';?>" />
        <!--<link rel="stylesheet" href="css/bootstrap-responsive.css" />-->
        <link rel="stylesheet" href="<?php echo HPath().'/css/docs.css'; ?> " />
        <link rel="stylesheet" href="<?php echo HPath(). '/css/jquery.simple-dtpicker.css';?>" />
       
        
        <!-- Following File Is Used To Date & Time Picker -->
       <!-- <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>-->

        <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
          <script src="assets/js/html5shiv.js"></script>
        <![endif]-->

        <!-- Le fav and touch icons -->
    </head>

    <body data-twttr-rendered="true" data-spy="scroll" data-target=".bs-docs-sidebar">

        <div class="jumbotron masthead">
            <div class="container">
                <div class="span4">
                    <img src="Upload/images/logo.jpg">
                    <div>
<!--                        <audio controls="controls" autoplay>
                            <source src="./upload/Instrumental.ogg" />
                            <source src="./Upload/Instrumental.mp3" />
                        </audio>-->
                    </div>
                </div>
                <div class="span4 quote">
                    <img src="Upload/images/saint.jpg">
                    <br>
                    <u>"We are blessed by & indebted to"</br> Bagwan Nityanand</u>
                </div>
                <div class="span4" style="padding-top: 20px;">
                  <?php  if (isset($_SESSION['UID']) && !empty($_SESSION['UID'])) {
                      include_once HPath() . '/dbop/data/client.php';
                      $GetName = GetClient($_SESSION['UID']);
                      $FetchName = mysql_fetch_array($GetName);?>
                <ul class="masthead-links">
                    <li>
                        <font color="Black"><b>Well-Come &nbsp;:- &nbsp; <?php echo $FetchName['Name'];?> </b></font>
                    </li><br/>
                    <li>
                        <a href="UserProfile.php">User Profile</a>
                    </li>
                    <li>
                        <a href="logout.php">Logout</a>
                    </li>
                </ul>
                  <?php } else {?>
                    
                    <ul class="masthead-links">
                    <li>
                        <a href="login.php">Login</a>
                    </li>
                    <li>
                        <a href="UserRegister.php">Register</a>
                    </li>
                </ul>
                  <?php }?>
                 <ul class="masthead-links">
                    <li>
                        <font style="color: #000000;">&nbsp;&nbsp;&nbsp;&nbsp;Call Us:24X7 @9925299252<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Email: info@khaligadi.com</font>
                    </li>
                </ul>
                </div>
            </div>
        </div>
        <div class="navbar navbar-inverse navbar-fixed-top">
            <div class="navbar-inner">
                <div class="container">
                    <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <div class="nav-collapse collapse" >
                        <ul class="nav" >
                            <li class="active">
                                <a href="home.php">Home</a>
                            </li>
                            
                            <li class="">
                                 <a href="AboutUs.php">About Us</a>
                                
                            </li>
                             <li class="">
                                 <a href="DepartureSchedule.php">Departure Schedule</a>
                            </li>
                            <li class="">
                                <a href="agent/AgentLogin.php">Agent Login</a>
                            </li>
                            <li>
                                <a href="#">Our Clients</a>
                            </li>
                            
                            <li class="">
                                <a href="FeedBack.php">Feed Back</a>
                            </li>
                            
                            <li class="">
                                <a href="Disclamer.php">Disclaimer</a>
                            </li>
                            <li class="">
                               <a href="PrivacyPolicy.php">Privacy Policy</a>
                            </li>
                            <li class="">
                                <a href="">Contact Us</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
