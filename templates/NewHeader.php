<!DOCTYPE html>
<html lang="en">
    <head>
        
        <meta http-equiv="content-type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">
        <title>Cab</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">

        <script type="text/javascript" src="js/widgets.js"></script>
        <script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>
        <script type="text/javascript" src="js/tabs.js"></script>     
        <script type="text/javascript" src="js/jquery.simple-dtpicker.js"></script>
        <!-- Le styles -->
<!--        <link rel="stylesheet" href="css//bootstrap.css" />
        <link rel="stylesheet" href="css/bootstrap-responsive.css" />
        <link rel="stylesheet" href="css/docs.css" />-->
        <link rel="stylesheet" href="css/jquery.simple-dtpicker.css" />
       
        
        <!-- Following File Is Used To Date & Time Picker -->
       <!-- <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>-->

        <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
          <script src="assets/js/html5shiv.js"></script>
        <![endif]-->

        <!-- Le fav and touch icons -->
    </head>

    <body data-twttr-rendered="true" data-spy="scroll" data-target=".bs-docs-sidebar">

        <div class="jumbotron masthead">
            <div class="container">
                <div class="span4">
                    <img src="Upload/images/logo.jpg">
                    <div>
                        <audio controls="controls">
                            <source src="./upload/Instrumental.ogg" />
                            <source src="./Upload/Instrumental.mp3" />
                             fallback 
                            <embed type="application/x-shockwave-flash"                                                         
                                flashvars="./upload/Instrumental.mp3"    
                                src="http://www.google.com/reader/ui/3523697345-audio-player.swf"   
                                width="650" height="0" quality="best"></embed>
                        </audio>
                    </div>
                </div>
                <div class="span4 quote">
                    <img src="Upload/images/saint.jpg">
                    <br>
                    <u>"We are blessed by & indebted to"</br> Bagwan Nityanand</u>
                </div>
                <div class="span4" style="padding-top: 20px;">
                <ul class="masthead-links">
                    <li>
                        <a href="#">Login</a>
                    </li>
                    <li>
                        <a href="#">Register</a>
                    </li>
                </ul>
                 <ul class="masthead-links">
                    <li>
                        <font style="color: #000000;">&nbsp;&nbsp;&nbsp;&nbsp;Call Us:24X7 @9925299252<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Email: info@khaligadi.com</font>
                    </li>
                </ul>
                </div>
            </div>
        </div>
        <div class="navbar navbar-inverse navbar-fixed-top">
            <div class="navbar-inner">
                <div class="container">
                    <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <div class="nav-collapse collapse">
                        <ul class="nav">
                            <li class="active">
                                <a href="index.php">Home</a>
                            </li>
                            
                            <li class="">
                                 <a href="AboutUs.php">About Us</a>
                                
                            </li>
                             <li class="">
                                 <a href="DepartureSchedule.php">Departure Schedule</a>
                                
                            </li>
                            <li>
                                <a href="#">Our Clients</a>
                            </li>
                            
                            <li class="">
                                <a href="FeedBack.php">Feed Back</a>
                            </li>
                            
                            <li class="">
                                <a href="Disclamer.php">Disclaimer</a>
                            </li>
                            <li class="">
                               <a href="PrivacyPolicy.php">Privacy Policy</a>
                            </li>
                            <li class="">
                                <a href="">Contact Us</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
