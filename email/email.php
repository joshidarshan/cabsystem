<?php

function Path() {
    $path = $_SERVER['DOCUMENT_ROOT'];
    // Differenciate the path for local and web host. Includes database connect file.
    if ($_SERVER['HTTP_HOST'] == "localhost" || $_SERVER['HTTP_HOST'] == "localhost:8080")
        $path .= '/Cabsystem';
    return $path;
}

//include_once Path() . '/email/mailgun/Mailgun.php';

require './email/vendor/autoload.php';

use Mailgun\Mailgun;

function SendEmail($to, $subject, $message) {
//    mailgun_init("key-08pbfhr6s0a5iuprkl-477nbcdxhbn-6");
//    $message = "<html><table><tr><td>Name</td><td>Darshan</td></tr><tr><td>Address</td><td>Rajkot</td></tr></table></html>";
//    MailgunMessage::send_raw("mailer@alphansotech.com", "$to", "$subject", "$message");

    $mg = new Mailgun("key-08pbfhr6s0a5iuprkl-477nbcdxhbn-6");
    $domain = "alphansotech.com";

    $mg->sendMessage($domain, array('from' => 'mailer@alphansotech.com',
        'to' => $to,
        'subject' => $subject,
        'text' => $message,
        'html' => $message));
    
    return true;
}

?>
