<?php

if (!isset($_SESSION))
    session_start();

function CarPath() {
    $path = $_SERVER['DOCUMENT_ROOT'];
    // Differenciate the path for local and web host. Includes database connect file.
    if ($_SERVER['HTTP_HOST'] == "localhost" || $_SERVER['HTTP_HOST'] == "localhost:8080")
        $path .= '/Cabsystem';
    return $path;
}

// Check which file has called the function
if (isset($_GET['type']) && !empty($_GET['type'])) {
    $carFunctionType = $_GET['type'];

    // Checking which functionality caller needs.
    
    $cabFunctionRequired = "";
    if (isset($_GET['required']) && !empty($_GET['required']))
        $cabFunctionRequired = $_GET['required'];

    if (($carFunctionType) == 'Car') {

        if (($cabFunctionRequired) == 'CarNoPlate') {
            // Get Drive according to agent
            if (isset($_GET['NoPlate']) && !empty($_GET['NoPlate'])) {

                include_once CarPath() . '/dbop/data/cab.php';
                $NoPlate = $_GET['NoPlate'];
                $check = CheckCabByNoPlate($NoPlate);

                if ($check > 0)
                    echo 0;
                else
                    echo 1;
            }
        }
        if (($cabFunctionRequired) == 'CarType') {
            // Get Drive according to agent
            if (isset($_GET['Type']) && !empty($_GET['Type'])) {

                include_once CarPath() . '/dbop/data/cartype.php';

                $check = CheckCarType($_GET['Type']);

                if ($check > 0)
                    echo 0;
                else
                    echo 1;
            }
        }
        // If function needs cars list from car id
        if (($cabFunctionRequired) == 'CarMakeName') {
            // Get Drive according to agent
            if (isset($_GET['Name']) && !empty($_GET['Name'])) {

                include_once CarPath() . '/dbop/data/carmake.php';

                $check = CheckCarName($_GET['Name']);

                if ($check > 0)
                    echo 0;
                else
                    echo 1;
            }
        }
    }

// Cabroute
    if (strtolower($carFunctionType) == 'cabroute') {

        // If function needs drivers list from agent id
        if (strtolower($cabFunctionRequired) == 'drivers') {
            // Get Drive according to agent
            $cabFunctionAgentId = "";
            if (isset($_GET['agentId']) && !empty($_GET['agentId']))
                $cabFunctionAgentId = $_GET['agentId'];

            include_once CarPath() . '/dbop/data/driver.php';
            $cabFunctionDriver = GetDriverFromAgentId($cabFunctionAgentId);
            $cabDriverListString = "";
            while ($cabDriverRow = mysql_fetch_assoc($cabFunctionDriver))
                $cabDriverListString .= "<option value='$cabDriverRow[Id]'>$cabDriverRow[Name]</option>";
            echo $cabDriverListString;
        }

        // If function needs cablist from cabs table.
        if (strtolower($cabFunctionRequired == 'cablist')) {
            include_once CarPath() . '/dbop/data/cab.php';
            $cabResult = GetAllCab();
            $cabListCount = 1;
            $cabResultString = array();
            while ($cabListRow = mysql_fetch_assoc($cabResult)) {
                $cabResultString[] = $cabListRow['NoPlate'];
                /* if($cabListCount == 1)
                  $cabResultString .= "'".$cabListRow['NoPlate']."',";
                  else if($cabListCount == 2)
                  $cabResultString .= "'".$cabListRow['NoPlate']."'";
                  else
                  $cabResultString .= ",'".$cabListRow['NoPlate']."'";
                  $cabListCount++; */
            }
            $cabResultString = json_encode($cabResultString);
            echo $cabResultString;
        }
        if (strtolower($cabFunctionRequired == 'Agentcablist')) {
            include_once CarPath() . '/dbop/data/cab.php';
            $AgentId = $_GET['AgentId'];
            $cabResult = GetCabNoByAgentId($AgentId);
            $cabListCount = 1;
            $cabResultString = array();
            while ($cabListRow = mysql_fetch_assoc($cabResult)) {
                $cabResultString[] = $cabListRow['NoPlate'];
                /* if($cabListCount == 1)
                  $cabResultString .= "'".$cabListRow['NoPlate']."',";
                  else if($cabListCount == 2)
                  $cabResultString .= "'".$cabListRow['NoPlate']."'";
                  else
                  $cabResultString .= ",'".$cabListRow['NoPlate']."'";
                  $cabListCount++; */
            }
            $cabResultString = json_encode($cabResultString);
            echo $cabResultString;
        }
        
        
    } else if (strtolower($carFunctionType) == 'cabs') {
        $carFunctionType = $_GET['type'];

        // Checking which functionality caller needs.
        $cabFunctionRequired = "";
        if (isset($_GET['required']) && !empty($_GET['required']))
            $cabFunctionRequired = $_GET['required'];

        if (strtolower($cabFunctionRequired == 'cablist')) {
            $cabNo = $_GET['carno'];
            include_once CarPath() . '/dbop/data/cab.php';
            $cabResult = GetCabByNoPlate($cabNo);
            $cabData = mysql_fetch_assoc($cabResult);
            $cabData = json_encode($cabData);
            echo $cabData;
        }
        
    }
}
?>