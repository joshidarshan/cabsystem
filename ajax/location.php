<?php
if (!isset($_SESSION))
    session_start();

function CarPath() {
    $path = $_SERVER['DOCUMENT_ROOT'];
    // Differenciate the path for local and web host. Includes database connect file.
    if ($_SERVER['HTTP_HOST'] == "localhost" || $_SERVER['HTTP_HOST'] == "localhost:8080")
        $path .= '/Cabsystem';
    return $path;
}

// Check which file has called the function
if(isset($_GET['type']) && !empty($_GET['type'])){
    $locationFunctionType = $_GET['type'];
    
    // Checking which functionality caller needs.
    $locationFunctionRequired = "";
    if(isset($_GET['required']) && !empty($_GET['required']))
        $locationFunctionRequired = $_GET['required'];
    
    if(strtolower($locationFunctionType) == 'location'){
        
        // for location list
        if(strtolower($locationFunctionRequired) == 'locationlist'){
            include_once CarPath().'/dbop/data/location.php';
            $locationData = GetAllLocations();
            $locationArray = array();
            while($locationRow = mysql_fetch_assoc($locationData))
                    $locationArray[] = $locationRow['Name'];
            $locationArray = json_encode($locationArray);
            echo $locationArray;
        }
        if(strtolower($locationFunctionRequired) == 'sublocationlist'){
            if (isset($_GET['locationId']) && !empty($_GET['locationId']))
            include_once CarPath().'/dbop/data/sublocation.php';
            
            $locationId = $_GET['locationId'];
            $SublocationList = GetSubLocationByLocationId($locationId);
            $SubLocationStr = "<option value=''>Select</option>";
            while ($SubLocationRow = mysql_fetch_assoc($SublocationList))
                $SubLocationStr.= "<option value='$SubLocationRow[Id]'>$SubLocationRow[SubLocation]</option>";
            echo  $SubLocationStr;
        }
        
        // for location id
        if(strtolower($locationFunctionRequired) == 'locationid'){
            $locationName = $_GET['location'];
            include_once CarPath().'/dbop/data/location.php';
            $locationData = GetLocationByName($locationName);
            $locationArray = json_encode(mysql_fetch_assoc($locationData));
            echo $locationArray;
        }
        
        // for cities between the source and destination. Data will come from geolocations table.
        if(strtolower($locationFunctionRequired) == 'loglatlocation'){
            $locationA = $_GET['locationA'];
            $locationB = $_GET['locationB'];
            include_once CarPath().'/dbop/data/geolocation.php';
            $citiesData = GetCitiesBetweenLocation($locationA, $locationB);
            $locationArray = array();
            while($localLocationRow = mysql_fetch_assoc($citiesData))
                    $locationArray[] = $localLocationRow['Name'];
            $locationArray = json_encode($locationArray);
            echo $locationArray;
        }
        
        if(strtolower($locationFunctionRequired) == 'sublocation'){
            //type=cabroute&required=sublocation&location=7
            $location = $_GET['location'];
            include_once CarPath().'/dbop/data/sublocation.php';
            $subLocationQuery = GetSubLocationByLocationId($location);
            $subLocationList = array();
            while($slRow = mysql_fetch_assoc($subLocationQuery)){
                $local_sublocation['Id'] = $slRow['Id'];
                $local_sublocation['Name'] = $slRow['SubLocation'];
                $local_sublocation['Fare'] = $slRow['Fare'];
                
                array_push($subLocationList, $local_sublocation);
            }
            $subLocationList = json_encode($subLocationList);
            echo $subLocationList;
        }
    }
}
?>