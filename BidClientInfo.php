<?php

if (!isset($_SESSION))
    session_start();

function BidPath() {
    $path = $_SERVER['DOCUMENT_ROOT'];
    // Differenciate the path for local and web host. Includes database connect file.
    if ($_SERVER['HTTP_HOST'] == "localhost" || $_SERVER['HTTP_HOST'] == "localhost:8080")
        $path .= '/Cabsystem';
    return $path;
}
include_once BidPath() . '/templates/header.php';
include_once BidPath() . '/dbop/data/location.php';
include_once BidPath() . '/dbop/data/listingutility.php';
include_once BidPath() . '/dbop/data/booking.php';

$FromLocation = $_POST['FromLocation'];
$ToLocation = $_POST['ToLocation'];
$Date1 = $_POST['FromDate-1'];
$Date2 = $_POST['FromDate-2'];
$Date3 = 00;
$_SESSION['BidAmount'] = $_POST['BidAmount'];
$_SESSION['ModelId'] = $_POST['Model'];

?>
<div class="row contactform span8" style="width: 400px; height: 610PX; margin-left:2px; padding-left: 25px"  >
     <form action="BookingByClient.php" method="post">
         <?php ?>
         <input type="hidden" name="Type" value="Bid"/>
            <br/>
            <div class="span4" style="width: 200px;" id="table_manual">
                <br>
                <span class="required">*</span> Your Client Id:<br>

                <br/>  
                <div style="padding-top: 11px"><span class="required">*</span> Phone No:<br/></div>
                <input type="text" class="large-field" value="" name="telephone"  id="telephone" style="max-width: 160px;" onblur="return getClientInfo();"><br/>
                <span class="required">*</span> E-Mail:<br>
                <input type="text" class="large-field" value="" name="email" id="email" style="max-width: 160px;" onblur="return getClientInfo();"><br/>
                <hr>
                <span class="required">*</span> Full Name:<br>
                <input type="text"  value="" name="firstname" id="firstname" style="max-width: 160px;">
                <br/>  
                <span class="required">*</span> Gander:<br>
                <select style="max-width: 160px;" name="Gander" id="Gander">
                    <option value="Null">Select Gander</option>
                    <option value="Male">Male</option>
                    <option value="FeMale">FeMale</option>
                </select><br/>
                <span class="required">*</span> Occupation<br>
                <input type="text" class="large-field" value="" name="Occupation" id="Occupation" style="max-width: 160px;"><br/>
                

            </div>
            <div class="span4" style="width: 200px;"><br/>
                <input type="text"  value="" name="ClientId" id="ClientId"   style="max-width: 160px;"><br/>
                <div style="margin-top: 10px"><span class="required">*</span> Company/Org.<br>
                    <input type="text" class="large-field" value="" name="Company" id="Company" style="max-width: 160px;"><br/>
                <span class="required">*</span>Reporting Address:<br/>
                <input type="text" class="large-field" value="" name="Pickup" id="Pickup" style="max-width: 160px;"></div>
                <hr>

                <span class="required">*</span> Traveler Type:<br/>
                <select style="max-width: 160px;" name="TravelerType" id="TravelerType">
                    <option value="Null">Select Type</option>
                    <option value="Family">Family</option>
                    <option value="Coupal">Coupal</option>
                    <option value="Individual">Individual</option>
                </select><br/>
                <span class="required">*</span> Desiganation<br>
                <input type="text" class="large-field" value="" name="Desiganation" id="Desiganation" style="max-width: 160px;"><br/>
                <span class="required">*</span> Department<br>
                <input type="text" class="large-field" value="" name="Department" id="Department" style="max-width: 160px;"><br/>
                <!--   <br>
                <span class="required">*</span> Last Name:<br>
                <input type="text" class="large-field" value="" name="lastname"><br><br>
                <span class="required">*</span> Address 1:<br>
                <textarea class="large-field" name="address"></textarea><br><br>
                <span class="required">*</span> City:<br>
                <input type="text" class="large-field" value="" name="city"><br>
                
                <br>
                <span class="required">*</span> Pickup Time:<br>
                <input type="text" class="large-field" value="" name="pickuptime"><br>-->
                <br/>
                <div><input type="submit" name="Book" value="Register Bid" class="btn-large"/></div>
            </div>
    </div>
<input type="text" name="FromLocation" value="<?php echo $FromLocation;?>">
<input type="hidden" name="ToLocation" value="<?php echo $ToLocation;?>">
<input type="hidden" name="FromDate-1" value="<?php echo $Date1;?>"/>
<input type="hidden" name="FromDate-2" value="<?php echo $Date2;?>"/>
<input type="hidden"  name="FromDate-3" value="<?php echo $Date3;?>"/>
<input type="hidden" name="NoPerson" value="1"/>
<!--<input type="text" name="BidAmount[]" value="<?php echo implode($BidNow); ?>"/>-->
</form>
    <script>
        function getClientInfo() {
            
            var clientid = $("#ClientId").val();
                telephone = $("#telephone").val();
                email = $("#email").val();
                
//        alert(email );   
        $.ajax({
                
                type: "GET",
                url: "ajax/user.php?type=clients&required=clientinfo&clientid=" + clientid +"&phone="+ telephone + "&email="+ email,
                //data: "std=" + std,
                success: function(data) {
                    data = JSON.parse(data);
//                    alert(data);
//                    console.log(data);
                   if(data){
                    $("#firstname").val(data['CName']);
                    $("#email").val(data['CEmail']);
                    $("#telephone").val(data['CPhone']);
                    $("#Gander").val(data['CPGander']);
                    $("#Occupation").val(data['CPOccup']);
                    $("#Company").val(data['CPComp']);
                    $("#TravelerType").val(data['CPTravel']);
                    $("#Desiganation").val(data['CPDes']);
                    $("#Department").val(data['CPDept']);
                    $("#telephone").attr("disabled", "disabled"); 
                    $("#email").attr("disabled", "disabled"); 
//                    $("#checkbox1").attr("disabled", "disabled"); 
                   }
                }
            });
        }
    </script>
