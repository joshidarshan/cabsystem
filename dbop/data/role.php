<?php

if ($_SERVER['HTTP_HOST'] == "localhost" || $_SERVER['HTTP_HOST'] == "localhost:8080")
    include_once $_SERVER['DOCUMENT_ROOT'] . '/cabsystem/dbop/dbconnect.php';
else
    include_once $_SERVER['DOCUMENT_ROOT'] . '/dbop/dbconnect.php';

function AddUserRole($Role, $Status) {
    Connect();
    $sql = "INSERT INTO roles (Role,Status)VALUES('$Role','$Status')";
    $exe = mysql_query($sql);
    $returnId = mysql_insert_id();

    Disconnect();

    return $returnId;
}

function EditUserRole($Id, $Role, $Status) {
    Connect();
    $sql = "UPDATE roles 
            SET `Role` = '$Role',`Status` = '$Status' 
            WHERE `Id` ='$Id'";
    mysql_query($sql);
    $returnId = mysql_affected_rows();
    Disconnect();

    return $returnId;
}

function PaginationUserRoles() {
    Connect();
    $sql = "SELECT COUNT(`Id`) AS total FROM roles";
    $select = mysql_query($sql);
    $num = mysql_fetch_array($select);
    $no = $num['total'];
    Disconnect();

    return $no;
}

/*
 * Author: Harik Vyas
 * Des: Set Limit Of Record Per Page According Number Of Table Records.
 * ChangeBy: Hardik Vyas
 * Changes: Put A Condition For Limitation.
 * changedOn: 31-08-2013,SaturDay
 */

function GetAllUsersRoles($start = 0, $rpp = 500) {
    if ($rpp == 500) {
        $no = PaginationUserRoles();
        $rpp = $no;
    }
    Connect();
    $sql = "SELECT `Id`, `Role`, `Status` 
            FROM roles 
            LIMIT $start,$rpp";
    $exe = mysql_query($sql);
    Disconnect();

    return $exe;
}

function GetUsersRole($Id) {
    Connect();
    $sql = "SELECT `Id`, `Role`, `Status` 
            FROM roles 
            WHERE `Id` = '$Id'";
    $exe = mysql_query($sql);
    Disconnect();
    return $exe;
}

/*
 * Author: Harik Vyas
 * Des: Add A Function Named DeleteUserRole().
 * ChangeBy: Hardik Vyas
 * Changes: Update Status As Zero.
 * changedOn: 02-09-2013,MondayDay
 */

function DeleteUserRole($Id) {
    Connect();
    $sql = "UPDATE 
            roles 
            SET Status = 0 WHERE Id = '$Id'";
    $exe = mysql_query($sql);
    $rows = mysql_affected_rows();
    Disconnect();
    return $rows;
}

/*
 * Author: Harik Vyas
 * Des: Check User Role Available In DataBAse Or Not.
 * ChangeBy: Hardik Vyas
 * Changes: Add A New Function Name CheckRole.
 * changedOn: 5-09-2013,ThursDay
 */

function CheckRole($Role) {
    Connect();
    $sql = "SELECT
            Id FROM roles WHERE Role = '$Role'";
    $exe = mysql_query($sql);
    $count = mysql_num_rows($exe);
    Disconnect();
    return  $count;
}

?>
