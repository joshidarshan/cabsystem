<?php

if ($_SERVER['HTTP_HOST'] == "localhost" || $_SERVER['HTTP_HOST'] == "localhost:8080")
    include_once $_SERVER['DOCUMENT_ROOT'] . '/cabsystem/dbop/dbconnect.php';
else
    include_once $_SERVER['DOCUMENT_ROOT'] . '/dbop/dbconnect.php';

function AddDiscountUsed($ClientId, $DiscountId, $DateTime) {
    Connect();
    $sql = "INSERT INTO
            discountuseds(`ClientId`, `DiscountId`, `DateTime`)
            VALUES ('$ClientId','$DiscountId','$DateTime')";
    $exe = mysql_query($sql);
    $ReturnId = mysql_insert_id();
    Disconnect();

    return $ReturnId;
}

function EditDiscountUsed($Id, $ClientId, $DiscountId, $DateTime) {
    Connect();
    $sql = "UPDATE discountuseds 
            SET `ClientId`='$ClientId',`DiscountId`='$DiscountId',`DateTime`='$DateTime' 
            WHERE `Id` = '$Id'";
    mysql_query($sql);
    $ReturnRows = mysql_affected_rows();
    Disconnect();

    return $ReturnRows;
}

function DiscountUsedPagination() {
    Connect();
    $sql = "SELECT COUNT(`Id`) AS total 
            FROM discountuseds";
    $exe = mysql_query($sql);
    $fetch = mysql_fetch_array($exe);
    $no = $fetch['total'];
    Disconnect();
    
    return $no;
}

/*
 * Author: Harik Vyas
 * Des: Set Limit Of Record Per Page According Number Of Table Records.
 * ChangeBy: Hardik Vyas
 * Changes: Put A Condition For Limitation.
 * changedOn: 31-08-2013,SaturDay
 */

function GetAllDiscountUsed($start=0, $rpp=500) {
    if ($rpp == 500) {
        $no = DiscountUsedPagination();
        $rpp = $no;
    }
    Connect();
    $sql = "SELECT `Id`, `ClientId`, `DiscountId`, `DateTime` 
            FROM discountuseds 
            LIMIT $start,$rpp";
    $exe = mysql_query($sql);
    Disconnect();

    return $exe;
}

function GetDiscountUsed($Id) {
    Connect();
    $sql = "SELECT `Id`, `ClientId`, `DiscountId`, `DateTime` 
            FROM discountuseds 
            WHERE Id = '$Id'";
    $exe = mysql_query($sql);
    Disconnect();

    return $exe;
}

?>
