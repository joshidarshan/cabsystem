<?php

if ($_SERVER['HTTP_HOST'] == "localhost" || $_SERVER['HTTP_HOST'] == "localhost:8080")
    include_once $_SERVER['DOCUMENT_ROOT'] . '/cabsystem/dbop/dbconnect.php';
else
    include_once $_SERVER['DOCUMENT_ROOT'] . '/dbop/dbconnect.php';

function AddCrownJob($Description, $DateTime) {
    Connect();
    $sql = "INSERT INTO
            crownjobs(`Description`, `DateTime`)
            VALUES ('$Description','$DateTime')";
    mysql_query($sql);
    $ReturnId = mysql_insert_id();
    Disconnect();

    return $ReturnId;
}

function EditCrownJob($Id, $Description, $DateTime) {
    Connect();
    $sql = "UPDATE crownjobs 
            SET `Description` = '$Description', `DateTime` = '$DateTime' 
            WHERE `Id` = '$Id'";
    mysql_query($sql);
    $ReturnRows = mysql_affected_rows();
    Disconnect();

    return $ReturnRows;
}

function CrownJobsPagination() {
    Connect();
    $sql = "SELECT COUNT(`Id`) AS total 
            FROM crownjobs";
    $exe = mysql_query($sql);
    $fetch = mysql_fetch_array($exe);
    $no = $fetch['total'];
    Disconnect();
    return $no;
}

/*
 * Author: Harik Vyas
 * Des: Set Limit Of Record Per Page According Number Of Table Records.
 * ChangeBy: Hardik Vyas
 * Changes: Put A Condition For Limitation.
 * changedOn: 31-08-2013,SaturDay
 */

function GetAnllCrownJobs($start = 0, $rpp = 500) {
    if ($rpp == 500) {
        $no = CrownJobsPagination();
        $rpp = $no;
    }
    Connect();
    $sql = "SELECT `Id`, `Description`, `DateTime`
            FROM crownjobs
            LIMIT $start, $rpp";
    $exe = mysql_query($sql);
    Disconnect();

    return $exe;
}

function GetCrownJob($Id) {
    Connect();
    $sql = "SELECT `Description`, `DateTime` 
            FROM crownjobs 
            WHERE `Id` = '$Id'";
    $exe = mysql_query($sql);

    Disconnect();
    return $exe;
}

function GetLastCronJobTimeStamp(){
    Connect();
    $sql = "SELECT MAX(`DateTime`) AS LastDateTime
            FROM crownjobs";
    $query = mysql_query($sql);
    $data = mysql_fetch_assoc($query);
    Disconnect();
    return $data['LastDateTime'];
}

function GetBookingsInTimeInterval($fromDateTime, $toDateTime){
    Connect();
    $sql = "SELECT cl.cr_id, cl.bk_clientid, SUM(cl.`bk_ticketCount`) AS TicketCount, MAX(cl.bk_id) AS BookingId, MAX(cl.cl_phone) AS ClientPhone, MAX(cl.cl_email) AS ClientEmail, MAX(dr.`Name`) As DriverName, MAX(dr.`Phones`) AS DriverPhone, MAX(ls.`Name`) as Source, MAX(ld.`Name`) as Destination, MAX(cl.`cb_NoPlate`) as NumberPlate, MAX(cl.cr_fromDateTime) AS Departure, MAX(cl.bk_bidrate) AS ClientBid
            FROM cablist cl
            JOIN drivers dr
                ON cl.cr_driverid = dr.`Id`
            LEFT OUTER JOIN locations ls
                ON cl.cr_source = ls.`Id`
            LEFT OUTER JOIN locations ld
                ON cl.cr_destination = ld.`Id`
            WHERE cl.cr_fromdatetime >= '$fromDateTime' AND cl.cr_todatetime < '$toDateTime' AND cl.bk_status = '1' AND (cl.bk_bidrate = 0 OR cl.bk_bidrate IS NULL)
            GROUP BY cl.cr_id, cl.bk_clientid
            ORDER BY cl.cr_id, cl.bk_clientid";
    $query = mysql_query($sql);
    Disconnect();
    return $query;
}

function GetBidInTimeInverval($fromDateTime, $toDateTime){
    Connect();
    $sql = "SELECT cl.cr_id, cl.bk_id, cl.bk_clientid, cl.cl_phone AS ClientPhone, cl.cl_email AS ClientEmail, dr.`Name` As DriverName, dr.`Phones` AS DriverPhone, ls.`Name` AS Source, ld.`Name` AS Destination, cl.`cb_NoPlate` AS NumberPlate, cl.bk_isbidawarded, cl.bk_bidrate
            FROM cablist cl
            JOIN drivers dr
                ON cl.cr_driverid = dr.`Id`
            JOIN locations ls
                ON cl.cr_source = ls.`Id`
            JOIN locations ld
                ON cl.cr_destination = ld.`Id`
            WHERE cl.cr_fromdatetime >= '$fromDateTime' AND cl.cr_fromdatetime < '$toDateTime' AND cl.bk_status = '1' AND cl.bk_bidrate > 0
            ORDER BY cl.cr_id, cl.bk_id";
    echo $sql;
    $query = mysql_query($sql);
    Disconnect();
    return $query;
}

function GetSplitableCabRoutes($fromDateTime){
    Connect();
    //$OriginalLocation
    $sql = "SELECT cl.cr_id, cl.cr_agentid, cl.cr_cabid, cl.cr_destination, cl.cr_driverid, cl.cr_fromdatetime, cl.cr_isreturntrip, cl.cr_source, cl.cr_status, cl.cr_todatetime, cl.bk_traveldate, cl.bk_todate, cl.bk_slocationid, cl.bk_dlocationid, cl.cr_cabid, cl.cr_isreturntrip, cl.cr_agentid, cl.cr_driverid, cl.`cm_Id`, cl.`cr_originalLocation`
            FROM cablist cl
            WHERE cl.`bk_bookedOn` >= '$fromDateTime' AND ((cl.bk_traveldate >= cl.cr_fromdatetime AND cl.bk_todate < cl.cr_todatetime) OR (cl.bk_traveldate > cl.cr_fromdatetime AND cl.bk_todate<=cl.cr_todatetime)) AND cl.cr_cabid IS NOT NULL AND cl.bk_id IS NOT NULL AND cl.cr_isreturntrip = 0 AND cl.cr_status = '1'";
    $query = mysql_query($sql);
    Disconnect();
    return $query;
}

function GetClubableCabId($fromDateTime){
    Connect();
    $cancelBookingCabIdSql = "SELECT distinct cl.cr_cabid
            FROM cablist cl
            WHERE cl.`bk_bookedOn` >= '$fromDateTime' AND cl.bk_id = cl.`cn_bookingId`";
    $cancelBookingCabIdQuery = mysql_query($cancelBookingCabIdSql);
    $cancelBookingCabIdList = array();
    while($cr = mysql_fetch_assoc($cancelBookingCabIdQuery))
            $cancelBookingCabIdList[] = $cr['cr_cabid'];
    $cancelBookingCabIdString = implode(",", $cancelBookingCabIdList);
    if(!empty($freeListId))
        $filterCancelBookingCabId = "cl.cr_cabid in ($cancelBookingCabIdString) AND ";
    else
        $filterCancelBookingCabId = "";
    $freeCabRouteRowsSql = "SELECT cl.cr_id, cl.cr_agentid, cl.cr_cabid, cl.cr_destination, cl.cr_driverid, cl.cr_fromdatetime, cl.cr_isreturntrip, cl.cr_source, cl.cr_status, cl.cr_todatetime, cl.bk_traveldate, cl.bk_todate, cl.bk_slocationid, cl.bk_dlocationid, cl.cr_cabid, cl.cr_isreturntrip, cl.cr_agentid, cl.cr_driverid, cl.`cm_Id`, cl.`cr_originalLocation`
                            FROM cablist cl
                            LEFT OUTER JOIN (SELECT CabRouteId, SUM(CASE WHEN `Status` = '0' THEN 0 ELSE IFNULL(TicketCount,0) END) AS TotalBooking FROM bookings group by CabRouteId) tbg
                                ON tbg.CabRouteId = cl.cr_id 
                            WHERE $filterCancelBookingCabId (cl.bk_id IS NULL OR (cl.bk_id = cl.`cn_bookingId`)) AND cl.cr_isreturntrip = 0 AND IFNULL(TotalBooking,0) = 0 
                            ORDER BY cl.cr_cabid, cl.cr_fromdatetime";
    $freeCabRouteRowsQuery = mysql_query($freeCabRouteRowsSql);
    Disconnect();
    return $freeCabRouteRowsQuery;
}   

?>
