<?php

if ($_SERVER['HTTP_HOST'] == "localhost" || $_SERVER['HTTP_HOST'] == "localhost:8080")
    include_once $_SERVER['DOCUMENT_ROOT'] . '/cabsystem/dbop/dbconnect.php';
else
    include_once $_SERVER['DOCUMENT_ROOT'] . '/dbop/dbconnect.php';

function InsertLocation($LocationName, $LocationStatus) {
    Connect();
    $sql = "INSERT INTO 
            locations(`Name`,`Status`)
            values('$LocationName','$LocationStatus')";
    mysql_query($sql);
    $returnId = mysql_insert_id();
    Disconnect();

    return $returnId;
}

function LocationPagination() {
    Connect();
    $sql = "SELECT COUNT(`Id`) AS total 
            FROM locations";
    $select = mysql_query($sql);
    $num = mysql_fetch_array($select);
    $no = $num['total'];
    Disconnect();

    return $no;
}

/*
 * Author: Harik Vyas
 * Des: Set Limit Of Record Per Page According Number Of Table Records.
 * ChangeBy: Hardik Vyas
 * Changes: Put A Condition For Limitation.
 * changedOn: 31-08-2013,SaturDay
 */

function GetAllLocations($locationStart = 0, $locationRpp = 500) {
    if ($locationRpp == 500) {
        $no = LocationPagination();
        $locationRpp = $no;
    }

    Connect();
    $sql = "select `Id`, `Name`, `Status` 
            FROM locations
            WHERE `Status` = '1' order by `Name`
            LIMIT $locationStart,$locationRpp";
    $fire = mysql_query($sql);
    Disconnect();

    return $fire;
}

function GetLocationArray(){
    $location = GetAllLocations();
    $locationArray = array();
    while($lr = mysql_fetch_assoc($location)){
        $lc['Id'] = $lr['Id'];
        $lc['Name'] = $lr['Name'];
        array_push($locationArray, $lc);
    }
    return $locationArray;    
}

function DeleteLocation($Id) {
    Connect();
    $Delete = "UPDATE locations 
                SET `Status` = 0 
                WHERE `Id` = '$Id'";
    mysql_query($Delete) or die('Query Error' . mysql_error());
    $rows = mysql_affected_rows();
    Disconnect();

    return $rows;
}

function GetSingleLocation($Id) {
    Connect();
    $sql = "select `Id`, `Name`, `Status` 
            FROM locations 
            WHERE Id = '$Id'";
    $fire = mysql_query($sql);
    Disconnect();

    return $fire;
}

function GetLocationByName($locationName){
    Connect();
    $sql = "SELECT `Id`, `Name`, `Status`
            FROM locations
            WHERE `Name` = '$locationName'";
    $query = mysql_query($sql);
    Disconnect();
    return $query;
}

function UpdateLocation($LocationId, $LocationName, $LocationStatus) {
    Connect();
    $update = "UPDATE locations 
                SET `Name` ='$LocationName', `Status` = '$LocationStatus' 
                WHERE `Id` = '$LocationId'";
    mysql_query($update);
    $ReturnRows = mysql_affected_rows();
    Disconnect();

    return $ReturnRows;
}
/*
 * Author: Harik Vyas
 * Des: Check User Role Available In DataBAse Or Not.
 * ChangeBy: Hardik Vyas
 * Changes: Add A New Function Name CheckRole.
 * changedOn: 5-09-2013,ThursDay
 */

function CheckLocation($Location) {
    Connect();
    $sql = "SELECT
            Id FROM locations WHERE Name = '$Location'";
    $exe = mysql_query($sql);
    $count = mysql_num_rows($exe);
    Disconnect();
    return  $count;
}
?>
