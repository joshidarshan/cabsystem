<?php

if ($_SERVER['HTTP_HOST'] == "localhost" || $_SERVER['HTTP_HOST'] == "localhost:8080")
    include_once $_SERVER['DOCUMENT_ROOT'] . '/cabsystem/dbop/dbconnect.php';
else
    include_once $_SERVER['DOCUMENT_ROOT'] . '/dbop/dbconnect.php';

function CarAdd($CarName, $CarStatus) {

    Connect();
    $sql = "INSERT  INTO carmakes(Name,Status)
            VALUES('$CarName', '$CarStatus')";
    mysql_query($sql);
    $returnId = mysql_insert_id();
    Disconnect();

    return $returnId;
}

function CarEdit($Id, $CarName, $CarStatus) {
    Connect();
    $sql = "UPDATE carmakes 
            SET `Name` = '$CarName', `Status` = '$CarStatus' 
            WHERE Id = '$Id'";
    mysql_query($sql);
    $returnRows = mysql_affected_rows();
    Disconnect();

    return $returnRows;
}

function CarPagination() {
    Connect();
    $sql = "SELECT COUNT(Id) AS total 
            FROM carmakes";
    $select = mysql_query($sql);
    $num = mysql_fetch_array($select);
    $no = $num['total'];

    Disconnect();
    return $no;
}

/*
 * Author: Harik Vyas
 * Des: Set Limit Of Record Per Page According Number Of Table Records.
 * ChangeBy: Hardik Vyas
 * Changes: Put A Condition For Limitation.
 * changedOn: 31-08-2013,SaturDay
 */
function GetCarsListing($CarStart=0, $CarRpp=500) {
    if ($CarRpp == 500) {
        $no = CarPagination();
        $CarRpp = $no;
    }

    Connect();
    $sql = "SELECT `Id`, `Name`, `Status`
            FROM carmakes 
            LIMIT $CarStart,$CarRpp";
    $exe = mysql_query($sql);
    Disconnect();
    return $exe;
}

function GetCarById($Id) {
    Connect();
    $sql = "SELECT `Id`, `Name`, `Status`
            FROM carmakes 
            WHERE Id = '$Id'";
    $exe = mysql_query($sql);
    Disconnect();

    return $exe;
}

function DeleteCar($Id) {
    Connect();
    $sql = "UPDATE carmakes 
            SET Status = 0 
            WHERE Id = '$Id'";
    mysql_query($sql);
    $returnId = mysql_affected_rows();
    Disconnect();

    return $returnId;
}
/*
 * Author: Harik Vyas
 * Des: Made for  Check Availablity of Name with Ajax.
 * ChangeBy: Hardik Vyas
 * Changes: Return Number Of rows With Select Query.
 * changedOn: 4-09-2013,WednesDay
 */
function CheckCarName($CarName){
    Connect();
                $sql = "select 
                        Id from 
                        carmakes where Name = '$CarName'";
                $exe = mysql_query($sql);
                $rows = mysql_num_rows($exe);
   Disconnect();
   return $rows;
}
?>