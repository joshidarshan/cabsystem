<?php

// Differenciate the path for local and web host. Includes database connect file.
if ($_SERVER['HTTP_HOST'] == "localhost" || $_SERVER['HTTP_HOST'] == "localhost:8080")
    include_once $_SERVER['DOCUMENT_ROOT'] . '/cabsystem/dbop/dbconnect.php';
else
    include_once $_SERVER['DOCUMENT_ROOT'] . '/dbop/dbconnect.php';

// Add agent into the system.
function InsertAgent($Name, $Address, $Phones, $Email, $Notes, $imgnam) {
    Connect();
    $sql = "INSERT  INTO 	
            agents(`Name`,`Address`,`Phones`,`Email`,`Notes`,`ImageLink`, `Status`)
            values('$Name','$Address','$Phones','$Email','$Notes','$imgnam', '1')";
    $insert = mysql_query($sql);
    $returnId = mysql_insert_id();
    Disconnect();

    return $returnId;
}

// Update the existing agent.
function EditAgent($id, $Name, $Address, $Phones, $Email, $Notes, $imgnam, $status) {
    Connect();
    $sql = "UPDATE agents 
            SET `Name` = '$Name', `Address` = '$Address', `Phones` = '$Phones', `Email` = '$Email', `Notes` = '$Notes', `ImageLink` = '$imgnam', `Status` = '$status'
            WHERE `Id` = '$id'";
    $update = mysql_query($sql);
    $returnId = mysql_affected_rows();
    Disconnect();

    return $returnId;
}

// Set active status=0 of specific agent.
function DeleteAgent($id) {
    Connect();
    $sql = "UPDATE agents 
            SET status = 0 
            WHERE Id = '$id'";
    $delete = mysql_query($sql);
    $returnId = mysql_affected_rows();
    Disconnect();

    return $returnId;
}

// Gives total number of records of table agent.
function AgentPagination() {
    Connect();
    $sql = "SELECT 
            COUNT(Id) AS total 
            FROM agents";
    $select = mysql_query($sql);
    $num = mysql_fetch_array($select);
    $no = $num['total'];
    Disconnect();
    return $no;
}

/*
 * Author: Harik Vyas
 * Des: Set Limit Of Record Per Page According Number Of Table Records.
 * ChangeBy: Hardik Vyas
 * Changes: Put A Condition For Limitation.
 * changedOn: 31-08-2013,SaturDay
 */
function GetAllAgents($start = 0, $rpp = 500) {
    if ($rpp == 500) {
        $no = AgentPagination();
        $rpp = $no;
    }
    Connect();
    $sql = "SELECT `Id`, `Name`, `Address`, `Phones`, `Email`, `Notes`, `ImageLink` ,`Status`
            FROM agents
           
            LIMIT $start,$rpp";

    $query = mysql_query($sql);
    Disconnect();
    return $query;
}

function GetAgentById($id) {
    Connect();
    $sql = "SELECT `Id`, `Name`, `Address`, `Phones`, `Email`, `Notes`, `ImageLink`, `Status`
            FROM agents
            WHERE `Id` = '$id'";
    $query = mysql_query($sql);
    Disconnect();

    return $query;
}
/*
 * Author: Harik Vyas
 * Des: Made for  Check Availablity of Contact No with Ajax.
 * ChangeBy: Hardik Vyas
 * Changes: Return Number Of rows With Select Query.
 * changedOn: 2-09-2013,MondayDay
 */
function CheckAgentPhone($Contact){
    Connect();
                $sql = "select 
                        Id from 
                        agents where Phones = '$Contact'";
                $exe = mysql_query($sql);
                $rows = mysql_num_rows($exe);
   Disconnect();
   return $rows;
   
}
/*
 * Author: Harik Vyas
 * Des: Made for  Check Availablity of Email with Ajax.
 * ChangeBy: Hardik Vyas
 * Changes: Return Number Of rows With Select Query.
 * changedOn: 3-09-2013,TuesdayDay
 */
function CheckAgentEmail($Email){
    Connect();
                $sql = "select 
                        Id from 
                        agents where Email = '$Email'";
                $exe = mysql_query($sql);
                $rows = mysql_num_rows($exe);
   Disconnect();
   return $rows;
}

function GetOneWayAgent(){
    Connect();
                $sql = "select 
                        Id from 
                        agents where Name = 'OneWay'";
                $exe = mysql_query($sql);
   Disconnect();
   return $exe;
}
function AddOneWayAgent(){
    Connect();
                $sql = "INSERT INTO agents (`Name`) VALUES('OneWay')";
                $exe = mysql_query($sql);
                $return = mysql_insert_id();
   Disconnect();
   return $return;
}
?>
