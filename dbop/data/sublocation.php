<?php
/*
 * Author: Harik Vyas
 * Des: For Add Sub Location
 * MADE On: 19-09-2013,Thursday
 */

if ($_SERVER['HTTP_HOST'] == "localhost" || $_SERVER['HTTP_HOST'] == "localhost:8080")
    include_once $_SERVER['DOCUMENT_ROOT'] . '/cabsystem/dbop/dbconnect.php';
else
    include_once $_SERVER['DOCUMENT_ROOT'] . '/dbop/dbconnect.php';

function InsertSubLocation($LocationId, $SubLocation,$Fare,$Status) {
    Connect();
    $sql = "INSERT INTO 
            sublocations(`LocationId`,`SubLocation`,`Fare`,`Status`)
            values('$LocationId', '$SubLocation','$Fare','$Status')";
    mysql_query($sql);
    $returnId = mysql_insert_id();
    Disconnect();

    return $returnId;
}

function SubLocationPagination() {
    Connect();
    $sql = "SELECT COUNT(`Id`) AS total 
            FROM sublocations";
    $select = mysql_query($sql);
    $num = mysql_fetch_array($select);
    $no = $num['total'];
    Disconnect();

    return $no;
}


function GetAllSubLocations($locationStart = 0, $locationRpp = 500) {
    if ($locationRpp == 500) {
        $no = SubLocationPagination();
        $locationRpp = $no;
    }

    Connect();
    $sql = "select sbloc.`Id`, sbloc.`LocationId` , sbloc.`SubLocation`,sbloc.`Fare`, sbloc.`Status`,loc.Name AS LocName
            FROM sublocations sbloc
            JOIN locations  loc
            ON sbloc.`LocationId` = loc.`Id` 
            LIMIT $locationStart,$locationRpp";
    $fire = mysql_query($sql) or die('Error'.  mysql_error());
    Disconnect();

    return $fire;
}


function UpdateSubLocation($Id,$LocationId, $SubLocation,$Fare,$Status) {
    Connect();
    $update = "UPDATE sublocations 
                SET `LocationId` ='$LocationId', `SubLocation`='$SubLocation', `Fare`= '$Fare', `Status` = '$Status' 
                WHERE `Id` = '$Id'";
    mysql_query($update);
    $ReturnRows = mysql_affected_rows();
    Disconnect();

    return $ReturnRows;
}
function DeleteSubLocation($Id) {
    Connect();
    $Delete = "UPDATE sublocations 
                SET `Status` = 0 
                WHERE `Id` = '$Id'";
    mysql_query($Delete) or die('Query Error' . mysql_error());
    $rows = mysql_affected_rows();
    Disconnect();

    return $rows;
}

function GetSingleSubLocation($Id) {
    Connect();
    $sql = "select `Id`,`LocationId` , `SubLocation`,`Fare`, `Status` 
            FROM sublocations 
            WHERE Id = '$Id'";
    $fire = mysql_query($sql);
    Disconnect();

    return $fire;
}

function GetSubLocationByLocationId($Id){
    Connect();
    $sql = "SELECT `Id`, `LocationId`, `SubLocation`, `Fare`, `Status`
            FROM sublocations
            WHERE `LocationId` = '$Id'";
    $query = mysql_query($sql);
    Disconnect();
    return $query;
}
function SubLocationFare($SubId)
{
    Connect();
    $sql = "SELECT `Fare` FROM sublocations WHERE Id = '$SubId'";
    $exe = mysql_query($sql);
    $Fetch =mysql_fetch_array($exe);
        $Sub = $Fetch['Fare'];
    Disconnect();
    return $Sub;
}
?>
