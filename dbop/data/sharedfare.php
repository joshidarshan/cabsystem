<?php

if ($_SERVER['HTTP_HOST'] == "localhost" || $_SERVER['HTTP_HOST'] == "localhost:8080")
    include_once $_SERVER['DOCUMENT_ROOT'] . '/cabsystem/dbop/dbconnect.php';
else
    include_once $_SERVER['DOCUMENT_ROOT'] . '/dbop/dbconnect.php';

function AddSharedFare($SLocationId, $DLocationId, $Rate, $DistanceKm, $TravelTime, $Status) {
    Connect();
    $sql = "INSERT INTO
            sharedfare(`SLocationId`, `DLocationId`, `Rate`, `DistanceKm`, `TravelTime`, `Status`)
                        VALUES ('$SLocationId','$DLocationId','$Rate','$DistanceKm','$TravelTime','$Status')";
    $exe = mysql_query($sql);
    $ReturnId = mysql_insert_id();

    Disconnect();

    return $ReturnId;
}

function EditSharedFare($Id, $SLocationId, $DLocationId, $Rate, $DistanceKm, $TravelTime, $Status) {
    Connect();
    $sql = "UPDATE sharedfare 
            SET `SLocationId`='$SLocationId', `DLocationId`='$DLocationId', `Rate`='$Rate', `DistanceKm`='$DistanceKm', `TravelTime`='$TravelTime', `Status` ='$Status' 
            WHERE `Id` = '$Id'";
    mysql_query($sql);
    $ReturnRows = mysql_affected_rows();
    Disconnect();

    return $ReturnRows;
}

function DeleteSharedFare($Id) {
    Connect();
    $sql = "UPDATE sharedfare 
            SET `Status` = 0 
            WHERE `Id` = '$Id' ";
    mysql_query($sql);
    $ReturnRows = mysql_affected_rows();
    Disconnect();

    return $ReturnRows;
}

function SharedFaredPagination() {
    Connect();
    $sql = "SELECT COUNT(`Id`) AS total 
            FROM sharedfare";
    $exe = mysql_query($sql);
    $fetch = mysql_fetch_array($exe);
    $no = $fetch['total'];
    Disconnect();

    return $no;
}

/*
 * Author: Harik Vyas
 * Des: Set Limit Of Record Per Page According Number Of Table Records.
 * ChangeBy: Hardik Vyas
 * Changes: Put A Condition For Limitation.
 * changedOn: 31-08-2013,SaturDay
 */

function GetAllSharedFares($start = 0, $rpp = 500) {
    if ($rpp == 500) {
        $no = SharedFaredPagination();
        $rpp = $no;
    }
    Connect();
    $sql = "SELECT `Id`, `SLocationId`, `DLocationId`, `Rate`, `DistanceKm`, `TravelTime`, `Status` 
            FROM sharedfare 
            LIMIT $start,$rpp";
    $exe = mysql_query($sql);
    Disconnect();

    return $exe;
}

function GetSharedFareId($Id) {
    Connect();
    $sql = "SELECT `Id`, `SLocationId`, `DLocationId`, `Rate`, `DistanceKm`, `TravelTime`, `Status` 
            FROM sharedfare 
            WHERE `Id` = '$Id'";
    $exe = mysql_query($sql);

    Disconnect();
    return $exe;
}

function GetFareByRoute($SLocationId, $DLocationId){
    Connect();
    $sql = "SELECT `Rate`
            FROM sharedfare
            WHERE `SLocationId` in ('$SLocationId', '$DLocationId') AND `DLocationId` in ('$DLocationId', '$SLocationId')";
    $query = mysql_query($sql);
    $data = mysql_fetch_assoc($query);
    $rate = $data['Rate'];
    //echo $sql;
    Disconnect();
    //echo $rate;
    return $rate;
}
function GetAvgSpeed(){
    Connect();
    $sql = "SELECT `cab_average_speed`
            FROM globalparameters";
    $query = mysql_query($sql);
    $fetch = mysql_fetch_array($query);
    $speed = $fetch['cab_average_speed'];
    Disconnect();
    //echo $rate;
    return $speed;
}

function GetDistance($SLocationId,$DLocationId){
    Connect();
    $sql = "SELECT `DistanceKm`
            FROM sharedfare WHERE `SLocationId` = '$SLocationId' AND `DLocationId` = '$DLocationId'";
    $query = mysql_query($sql);
    Disconnect();
    //echo $rate;
    return $query;
}
?>
