<?php

if ($_SERVER['HTTP_HOST'] == "localhost" || $_SERVER['HTTP_HOST'] == "localhost:8080")
    include_once $_SERVER['DOCUMENT_ROOT'] . '/cabsystem/dbop/dbconnect.php';
else
    include_once $_SERVER['DOCUMENT_ROOT'] . '/dbop/dbconnect.php';

function AddCouponUsed($ClientId, $CouponId, $DateTime) {
    Connect();
    $sql = "INSERT INTO
            couponuseds(`ClientId`, `CouponId`, `DateTime`)
            VALUES ('$ClientId','$CouponId','$DateTime')";
    mysql_query($sql);
    $ReturnId = mysql_insert_id();
    Disconnect();

    return $ReturnId;
}

function EditCouponUsed($Id, $ClientId, $CouponId, $DateTime) {
    Connect();
    $sql = "UPDATE couponuseds 
            SET `ClientId` = '$ClientId', `CouponId` = '$CouponId', `DateTime` = '$DateTime' 
            WHERE `Id` = '$Id'";
    mysql_query($sql);
    $ReturnRows = mysql_affected_rows();
    Disconnect();

    return $ReturnRows;
}

function CouponUsedsPagination() {
    Connect();
    $sql = "SELECT COUNT(`Id`) AS total 
            FROM couponuseds";
    $exe = mysql_query($sql);
    $fetch = mysql_fetch_array($exe);
    $no = $fetch['total'];
    Disconnect();

    return $no;
}

/*
 * Author: Harik Vyas
 * Des: Set Limit Of Record Per Page According Number Of Table Records.
 * ChangeBy: Hardik Vyas
 * Changes: Put A Condition For Limitation.
 * changedOn: 31-08-2013,SaturDay
 */

function GetAllCouponUseds($start = 0, $rpp = 500) {
    if ($rpp == 500) {
        $no = CouponUsedsPagination();
        $rpp = $no;
    }
    Connect();
    $sql = "SELECT `Id`, `ClientId`, `CouponId`, `DateTime` 
            FROM couponuseds 
            LIMIT $start,$rpp";
    $exe = mysql_query($sql);
    Disconnect();

    return $exe;
}

function GetCouponUsed($Id) {
    Connect();
    $sql = "SELECT `Id`, `ClientId`, `CouponId`, `DateTime` 
            FROM couponuseds 
            WHERE Id = '$Id'";
    $exe = mysql_query($sql);

    Disconnect();
    return $exe;
}

?>
