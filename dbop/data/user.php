<?php

if ($_SERVER['HTTP_HOST'] == "localhost" || $_SERVER['HTTP_HOST'] == "localhost:8080")
    include_once $_SERVER['DOCUMENT_ROOT'] . '/cabsystem/dbop/dbconnect.php';
else
    include_once $_SERVER['DOCUMENT_ROOT'] . '/dbop/dbconnect.php';
/*
 * Author: Harik Vyas
 * Des: Modyfy All Function According to Data Base Schema
 * ChangeBy: Hardik Vyas
 * Changes: Add New Fields Phones And Email.
 * changedOn: 02-09-2013,MondayDay
 */

function AddUser($Id, $Email, $Phone, $Password, $RoleId, $Status) {
    Connect();
    $sql = "INSERT INTO 
            users(`Id`,`Email`, `Phone`,`Password`, `RoleId`,`Status`)
            VALUES('$Id','$Email','$Phone','$Password','$RoleId','$Status')";
    mysql_query($sql);
    $returnId = mysql_insert_id();
    Disconnect();

    return $returnId;
}

function DeleteUser($Id) {
    Connect();
    $sql = "UPDATE users 
            SET `Status` = 0 
            WHERE Id = '$Id'";
    mysql_query($sql);
    $returnId = mysql_affected_rows();
    Disconnect();

    return $returnId;
}

/*
 * Remove Password Field From UserEdit Frontend 
 * Changed By HArdik Vyas On 10/2/13 On Tuesday
 */

function EditUser($Id, $Email, $Phones, $RoleId, $Status) {
    Connect();
    $sql = "UPDATE users 
            SET `Email` = '$Email', `Phone` = '$Phones', `RoleId` = '$RoleId', `Status` = '$Status' 
            WHERE Id ='$Id'";
    mysql_query($sql);
    $returnId = mysql_affected_rows();
    Disconnect();
//    echo $sql;
    return $returnId;
}

function PaginationUser() {
    Connect();
    $sql = "SELECT COUNT(`Id`) AS total 
            FROM users";
    $select = mysql_query($sql);
    $num = mysql_fetch_array($select);
    $no = $num['total'];
    Disconnect();

    return $no;
}

/*
 * Author: Harik Vyas
 * Des: Set Limit Of Record Per Page According Number Of Table Records.
 * ChangeBy: Hardik Vyas
 * Changes: Put A Condition For Limitation.
 * changedOn: 31-08-2013,SaturDay
 */

function GetAllUsers($start = 0, $rpp = 500) {
    if ($rpp == 500) {
        $no = PaginationUser();
        $rpp = $no;
    }
    Connect();
    $sql = "SELECT 
            users.Id, users.Email, users.Phones, users.Password, users.RoleId, users.Status, roles.Role
            FROM users JOIN
            roles ON users.RoleId = roles.Id";
    $exe = mysql_query($sql) or die('Errorddd' . mysql_errno());
    Disconnect();
    return $exe;
}

/*
 * Auther : Hardik Vyas
 * Change : Name Of Function GetUserById() In Place Of GetUser()
 * On 02/09/13, Monday
 */

function GetUserById($Id) {
    Connect();
    $sql = "SELECT `Id`, `Email`,`Phones`, `Password`, `RoleId`, `Status` 
            FROM users 
            WHERE `Id` = '$Id' ";
    $exe = mysql_query($sql) or die('Www' . mysql_error());
    Disconnect();

    return $exe;
}
/*By Hardik Vyas Auto Take Id*/
function MaxId() {
    Connect();
    $sql = "SELECT MAX(Id) AS max  From users WHERE RoleId = '1'";
    $exe = mysql_query($sql);
    $FetchMaxId = mysql_fetch_array($exe);
    $max = $FetchMaxId['max'];
    $AdminId = $max + 1;
    Disconnect();
    return $AdminId;
}

/*
 * Author: Harik Vyas
 * Des: Made for  Check Availablity of Email & COntact  with Ajax.
 * ChangeBy: Hardik Vyas
 * Changes: Return Number Of rows With Select Query.
 * changedOn: 3-09-2013,TuesdayDay
 */

function CheckAdminPhone($Contact) {
    Connect();
    $sql = "select 
                        Id from 
                        users where Phones = '$Contact'";
    $exe = mysql_query($sql);
    $rows = mysql_num_rows($exe);
    Disconnect();
    return $rows;
}
function CheckUserEmail($Email){
    Connect();
                $sql = "select 
                        Id from 
                        users where Email = '$Email'";
                $exe = mysql_query($sql);
                $rows = mysql_num_rows($exe);
   Disconnect();
   return $rows;
}

function LoginUserByUseridPassword($userName, $password){
    Connect();
    $userDetailByRolesql = "SELECT us.`Id`, rl.`Id` AS RoleId, rl.`Role`
            FROM users us
            JOIN roles rl
                ON us.`RoleId` = rl.`Id`
            WHERE `Password` = '$password' AND (us.`Email` = '$userName' OR us.`Phone` = '$userName')";
    $userDetailByRoleQuery = mysql_query($userDetailByRolesql);
    $userDetailByRoleData = mysql_fetch_assoc($userDetailByRoleQuery);
    
    $userDetail = array();
    if($userDetailByRoleData){
        $userDetail['UID'] = $userDetailByRoleData['Id'];
        $userDetail['Role'] = $userDetailByRoleData['Role'];
        $userDetail['RoleId'] = $userDetailByRoleData['RoleId'];
        
        //For admin
        if(strtolower($userDetailByRoleData['Role']) == 'admin')
            $userDetail['IsAdmin'] = '1';
        else
            $userDetail['IsAdmin'] = '0';
        
        //For agent
        if(strtolower($userDetailByRoleData['Role']) == 'agent')
            $userDetail['IsAgent'] = '1';
        else
            $userDetail['IsAgent'] = '0';
    }
    return $userDetail;
}
?>
