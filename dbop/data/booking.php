<?php

if ($_SERVER['HTTP_HOST'] == "localhost" || $_SERVER['HTTP_HOST'] == "localhost:8080")
    include_once $_SERVER['DOCUMENT_ROOT'] . '/cabsystem/dbop/dbconnect.php';
else
    include_once $_SERVER['DOCUMENT_ROOT'] . '/dbop/dbconnect.php';

function InsertBooking($ClientId, $IsPaid,$SLocationId,$DLocationId,$IsShare,$fare, $AmountReceived,$FromDate, $ToDate,$IsMaharaja,$BidRate,$IsBidAwarded,$CabRouteId,$PickupLocation,$SeatsBooked,$TicketCount,$IsBookedByClient,$Status) {
    if (function_exists('date_default_timezone_set')) {
        date_default_timezone_set('Asia/Calcutta');
    }
    $date = date('Y/m/d H:i:s', time());

    Connect();
    $sql = "INSERT INTO bookings(`ClientId`, `IsPaid`,`SLocationId`,`DLocationId`,`IsShare`, `Fare`, `AmountReceived`,`FromDate`, `ToDate`,`IsMaharaja`,`BidRate`,`IsBidAwarded`,`CabRouteId`,`PickupLocation`,`SeatsBooked`,`TicketCount`,`IsBookedByClient`, `BookedOn`, `Status`)
            VALUES('$ClientId', '$IsPaid','$SLocationId','$DLocationId','$IsShare', $fare, '$AmountReceived','$FromDate', '$ToDate','$IsMaharaja','$BidRate','$IsBidAwarded','$CabRouteId','$PickupLocation','$SeatsBooked','$TicketCount','$IsBookedByClient', '$date', '$Status')";
    $query = mysql_query($sql);
    echo $sql;
    $returnId = mysql_insert_id();

    Disconnect();

    return $returnId;
}

function UpdateBooking($Id, $ClientId, $IsPaid,$SLocationId,$DLocationId,$IsShare,$AmountReceived,$FromDate, $ToDate, $IsMaharaja,$BidRate,$IsBidAwarded,$CabRouteId,$PickupLocation,$SeatsBooked,$TicketCount,$IsBookedByClient,$Status) {
    Connect();
    $sql = "UPDATE bookings
            SET `ClientId` = '$ClientId', `IsPaid`='$IsPaid',`SLocationId`='$SLocationId',`DLocationId`='$DLocationId',`IsShare`='$IsShare',`AmountReceived`='$AmountReceived',`FromDate`='$FromDate', `ToDate` = '$ToDate',`IsMaharaja`='$IsMaharaja',`BidRate`='$BidRate',`IsBidAwarded`='$IsBidAwarded',`CabRouteId`='$CabRouteId',`PickupLocation`='$PickupLocation',`SeatsBooked`='$SeatsBooked',`TicketCount`='$TicketCount',`IsBookedByClient`='$IsBookedByClient',`Status`='$Status'
            WHERE `Id` = '$Id'";
    $query = mysql_query($sql);
    Disconnect();
    return $query;
}

function DeleteBookig($Id) {
    Connect();
    $sql = "UPDATE bookings
            SET `Status` = 0
            WHERE `Id` = '$Id'";
    $query = mysql_query($sql);
    Disconnect();

    return $query;
}

/*
 * Author: Harik Vyas
 * Des: Add A Pagination Function For Count Number Of records From Tables.
 * ChangeBy: Hardik Vyas
 * changedOn: 31-08-2013,SaturDay
 */
function BookingPagination() {
    Connect();
    $sql = "SELECT 
            COUNT(Id) AS total 
            FROM bookings";
    $select = mysql_query($sql);
    $num = mysql_fetch_array($select);
    $no = $num['total'];
    Disconnect();
    return $no;
}

/*
 * Author: Harik Vyas
 * Des: Set Limit Of Record Per Page According Number Of Table Records.
 * ChangeBy: Hardik Vyas
 * Changes: Put A Condition For Limitation.
 * changedOn: 31-08-2013,SaturDay
 */
function GetAllBooking($start = 0, $range = 500) {
    if ($range == 500) {
        $no = BookingPagination();
        $range = $no;
    }

    Connect();
    $sql = "SELECT bk.`ClientId`, bk.`IsPaid`, bk.`SLocationId`, bk.`DLocationId`, bk.`IsShare`, bk.`AmountReceived`, bk.`fromDate`, bk.`ToDate`, bk.`IsMaharaja`, bk.`BidRate`, bk.`IsBidAwarded`, bk.`CabRouteId`, bk.`PickupLocation`, bk.`SeatsBooked`, bk.`TicketCount`, bk.`IsBookedByClient`, bk.`Status`, cl.`Name`, cl.`Phones`, cl.`Email`
            FROM bookings bk
            JOIN clients cl
                ON bk.`ClientId` = cl.`Id`
            LIMIT $start,$range";
    $query = mysql_query($sql);
    Disconnect();
    return $query;
}

function GetBookingByDateLocation($fromLocation, $toLocation, $fromTime, $toTime) {
    Connect();
    $sql = "SELECT bk.`Id`, bk.`ClientId`, bk.`IsPaid`, bk.`SLocationId`, bk.`DLocationId`, bk.`IsShare`, bk.`AmountReceived`, bk.`fromDate`, bk.`ToDate`, bk.`IsMaharaja`, bk.`BidRate`, bk.`IsBidAwarded`, bk.`CabRouteId`, bk.`PickupLocation`, bk.`SeatsBooked`, bk.`TicketCount`, bk.`IsBookedByClient`, bk.`Status`, cl.`Name`, cl.`Phones`, cl.`Email`
            FROM bookings bk
            JOIN clients cl
                ON bk.`ClientId` = cl.`Id` AND bk.`SLocationId` = '$fromLocation' AND bk.`DLocationId` = '$toLocation' AND bk.`FromDate` >= '$fromTime' AND bk.`FromDate` <= '$toTime'";
    $query = mysql_query($sql);
    Disconnect();
    return $query;
}

function GetBookingByLocation($fromLocation, $toLocation) {
    Connect();
    $sql = "SELECT bk.`Id`, bk.`ClientId`, bk.`IsPaid`, bk.`SLocationId`, bk.`DLocationId`, bk.`IsShare`, bk.`AmountReceived`, bk.`FromDate` AS Date, bk.`IsMaharaja`, bk.`BidRate`, bk.`IsBidAwarded`, bk.`CabRouteId`, bk.`PickupLocation`, bk.`SeatsBooked`, bk.`TicketCount`, bk.`IsBookedByClient`, bk.`Status`, cl.`Name`, cl.`Phones`, cl.`Email`
            FROM bookings bk
            JOIN clients cl
                ON bk.`ClientId` = cl.`Id` AND bk.`SLocationId` = '$fromLocation' AND bk.`DLocationId` = '$toLocation'";
    $query = mysql_query($sql);
    Disconnect();
    return $query;
}

function GetBookingByDate($fromDate, $toDate) {
    Connect();
    $sql = "SELECT bk.`Id`, bk.`ClientId`, bk.`IsPaid`, bk.`SLocationId`, bk.`DLocationId`, bk.`IsShare`, bk.`AmountReceived`, bk.`FromDate` AS Date, bk.`IsMaharaja`, bk.`BidRate`, bk.`IsBidAwarded`, bk.`CabRouteId`, bk.`PickupLocation`, bk.`SeatsBooked`, bk.`TicketCount`, bk.`IsBookedByClient`, bk.`Status`, cl.`Name`, cl.`Phones`, cl.`Email`
            FROM bookings bk
            JOIN clients cl
                ON bk.`ClientId` = cl.`Id` AND bk.`FromDate` >= '$fromDate' AND bk.`FromDate` <= '$toDate'";
    $query = mysql_query($sql);
    Disconnect();
    return $query;
}

function GetBookingById($Id) {
    Connect();
    $sql = "SELECT `Id`, `ClientId`, `IsPaid`,`SLocationId`,`DLocationId`,`IsShare`,`AmountReceived`,`FromDate` AS Date,`IsMaharaja`,`BidRate`,`IsBidAwarded`,`CabRouteId`,`PickupLocation`,`SeatsBooked`,`TicketCount`,`IsBookedByClient`,`Status` 
            FROM bookings 
            WHERE Id = '$Id'";
    $query = mysql_query($sql);
    Disconnect();

    return $query;
}

function AwardBid($bookingId){
    // Getting booking details
    $bookingQuery = GetBookingById($bookingId);
    $bookingData = mysql_fetch_assoc($bookingQuery);
    
    // Bid award code starts
    Connect();
    $sql = "UPDATE bookings
            SET `Fare` = $bookingData[BidRate], `IsBidAwarded` = '1', `Status` = '1'
            WHERE `Id` = '$bookingId'";
    echo $sql;
    $query = mysql_query($sql);
    $effectedRows = mysql_affected_rows();
    Disconnect();
    return $effectedRows;
}

function GetRestOfBookingsForCabRoute($bookingId, $cabRouteId){
    Connect();
    $sql = "SELECT `Id`
            FROM bookings
            WHERE Id NOT IN ($bookingId) AND `CabRouteId` = '$cabRouteId'";
    $query = mysql_query($sql);
    $idList = array();
    while($row = mysql_fetch_assoc($query)){
        $idList[] = $row['Id'];
    }
    Disconnect();
    return $idList;
}

function CancelBooking($bookingId){
    Connect();
    $sql = "UPDATE bookings
            SET `Status` = 0 
            WHERE `Id` = '$bookingId'";
    $query = mysql_query($sql);
    $affectedRows = mysql_affected_rows();
    Disconnect();
    return $affectedRows;
}
function InsertBid($ClientId,$SLocationId,$DLocationId,$Date1,$Date2,$Date3,$NoPerson,$ModelBid,$IsAwarded,$Status){
    Connect();
    $sql = "INSERT INTO `bidcab` 
            (`ClientId`,`SLocationId`,`DLocationId`,`DateTime-1`,`DateTime-2`,`DateTime-3`,`NoPassenger`,`ModelBid`,`IsAwarded`,`Status`)
             VALUES ('$ClientId','$SLocationId','$DLocationId','$Date1','$Date2','$Date3','$NoPerson','$ModelBid','$IsAwarded','$Status')";
    $exe = mysql_query($sql) or die ('Error'.  mysql_error());
//    echo $sql;
    $ReturnId = mysql_insert_id();
    Disconnect();
    return $ReturnId;
}    
function BookingByClient($ClientId){
    Connect();
    $sql = "SELECT bk.`Id`,bk.`FromDate`,bk.`ToDate`,Sloc.`Name` AS Source,Dloc.`Name`AS Destination,bk.Status
            FROM bookings bk
            JOIN locations Sloc
            ON bk.SLocationId = Sloc.Id
            JOIN locations Dloc
            ON bk.DLocationId = Dloc.Id
            WHERE ClientId = '$ClientId' AND bk.Status = '1'";
    //echo $sql;
    $query = mysql_query($sql);
//    $effectedRows = mysql_affected_rows();
    Disconnect();
    return $query;
}

?>
