<?php

if ($_SERVER['HTTP_HOST'] == "localhost" || $_SERVER['HTTP_HOST'] == "localhost:8080")
    include_once $_SERVER['DOCUMENT_ROOT'] . '/cabsystem/dbop/dbconnect.php';
else
    include_once $_SERVER['DOCUMENT_ROOT'] . '/dbop/dbconnect.php';

function CarModelAdd($CarMakeId, $CarTypeId, $CarModelName, $Seats, $Isvirtual, $SeatLayout, $DefaultKmRate, $DefaultAcKmRate,$CarImage, $Status) {
    Connect();
    $sql = "INSERT INTO 
            carmodels(`CarMakeId`, `CarTypeId`, `Name`, `Seats`, `IsVirtual`, `SeatLayout`,`DefaultKmRate`,`DefaultAcKmRate`,`CarImage`, `Status`)
            VALUES('$CarMakeId', '$CarTypeId', '$CarModelName', '$Seats', '$Isvirtual', '$SeatLayout', '$DefaultKmRate','$DefaultAcKmRate', '$CarImage','$Status')";
    $exe = mysql_query($sql) or die('Errp'.  mysql_error());
    $returnId = mysql_insert_id();
    Disconnect();

    return $returnId;
}

function CarModelEdit($Id, $CarMakeId, $CarTypeId, $CarModelName, $Seats, $Isvirtual, $SeatLayout, $DefaultKmRate, $DefaultAcKmRate,$CarImage, $Status) {
    Connect();
    $sql = "UPDATE carmodels 
            SET `CarMakeId` = '$CarMakeId', `CarTypeId` = '$CarTypeId', `Name` = '$CarModelName', `Seats` = '$Seats', `Isvirtual` = '$Isvirtual', `SeatLayout` = '$SeatLayout', `DefaultKmRate`= '$DefaultKmRate', `DefaultAcKmRate` = '$DefaultAcKmRate', `CarImage` = '$CarImage', `Status` = '$Status' 
            WHERE `Id` = '$Id'";
    $exe = mysql_query($sql);
    $returnId = mysql_affected_rows();
    Disconnect();

    return $returnId;
}

function DeleteCarModel($Id) {
    Connect();
    $sql = "UPDATE carmodels 
            SET `Status` = 0 
            WHERE Id = '$Id'";
    $exe = mysql_query($sql);
    $returnId = mysql_affected_rows();
    Disconnect();

    return $returnId;
}

function CarModelPagination() {
    Connect();
    $sql = "SELECT COUNT(`Id`) AS total 
            FROM carmodels";
    $select = mysql_query($sql);
    $num = mysql_fetch_array($select);
    $no = $num['total'];
    Disconnect();

    return $no;
}

/*
 * Author: Harik Vyas
 * Des: Set Limit Of Record Per Page According Number Of Table Records.
 * ChangeBy: Hardik Vyas
 * Changes: Put A Condition For Limitation.
 * changedOn: 31-08-2013,SaturDay
 */

function GetCarModelListing($CarModelStart = 0, $CarModelRpp = 500) {
    if ($CarModelRpp == 500) {
        $no = CarModelPagination();
        $CarModelRpp = $no;
    }

    Connect();
    $sql = "SELECT cm.`Id`, cm.`CarMakeId`, cm.`CarTypeId`, cm.`Name`, cm.`Status`, cm.`Seats` as NoSeats, cm.`IsVirtual`, cm.`SeatLayout`,cm.`DefaultKmRate`, cm.`DefaultAcKmRate`, cm.`CarImage`, crm.`Name` as cmName, ct.`Name` as ctName
            FROM carmodels  cm
            JOIN carmakes crm
                ON cm.`CarMakeId` = crm.`Id`
            JOIN cartypes ct
                ON cm.`CarTypeId` = ct.`Id`
            LIMIT $CarModelStart,$CarModelRpp";
    $exe = mysql_query($sql);
    Disconnect();
    return $exe;
}

function GetCarModelById($Id) {
    Connect();
    $sql = "SELECT `CarMakeId`, `CarTypeId`, `Name`, `Seats`, `IsVirtual`, `SeatLayout`, `DefaultKmRate`, `DefaultAcKmRate`, `CarImage`,`Status`
            FROM carmodels 
            WHERE Id = '$Id'";
    $exe = mysql_query($sql);
    Disconnect();
    return $exe;
}

function GetVirtualCarModels(){
    Connect();
    $sql = "SELECT cm.`Id`, cm.`Name`, cm.`CarMakeId`, cm.`CarTypeId`, cm.`Seats`, cm.`SeatLayout`, cm.`DefaultKmRate`, cm.`DefaultAcKmRate`, cm.`CarImage`, ct.`Name` as Type
            FROM carmodels cm
            JOIN cartypes ct
                ON cm.`CarTypeId` = ct.`Id`
            WHERE `IsVirtual` = '1'";
    $query = mysql_query($sql);
   // echo $sql;
    Disconnect();
    return $query;
}

?>
