<?php

if ($_SERVER['HTTP_HOST'] == "localhost" || $_SERVER['HTTP_HOST'] == "localhost:8080"){
    include_once $_SERVER['DOCUMENT_ROOT'] . '/cabsystem/dbop/dbconnect.php';
    include_once $_SERVER['DOCUMENT_ROOT'].'/cabsystem/dbop/data/globalperameter.php';
    include_once $_SERVER['DOCUMENT_ROOT'].'/cabsystem/dbop/data/crownjob.php';
}
else{
    include_once $_SERVER['DOCUMENT_ROOT'] . '/dbop/dbconnect.php';
    include_once $_SERVER['DOCUMENT_ROOT'].'/dbop/data/globalperameter.php';
    include_once $_SERVER['DOCUMENT_ROOT'].'/dbop/data/crownjob.php';
}

$globalParameterQuery = GetAllGlobalParameters();
$globalParameterData = mysql_fetch_assoc($globalParameterQuery);
$confirmTime = $globalParameterData['booking_confirmation_time'];

// last cron job time
$lastCronJobDateTime = GetLastCronJobTimeStamp();
$lastCronJobDateTime = "2013-10-03 04:00:43";
//
//echo "Last corn job time:".$lastCronJobDateTime."<br/>"    ;

// from datetime
$departureFromDateTime = date("Y/m/d h:i:s", strtotime("+$confirmTime hours", strtotime($lastCronJobDateTime)));//$lastCronJobDateTime + $confirmTime;

$departureFromDateTime = date("Y/m/d h:i:s", strtotime("+15 minutes", strtotime($departureFromDateTime)));

function GetSharedBooking($FromLocalDate, $ToLocalDate) {
    Connect();
    $sql = "SELECT cablist.*, cl.Name AS cl_name, cl.Phones AS cl_phone, sl.`Name` AS lc_sourcename, dl.`Name` AS lc_destinationname, dr.`Name` AS dr_drivername, dr.`Phones` AS dr_phone
            FROM cablist
            LEFT OUTER JOIN clients cl
                ON bk_clientid = cl.Id
            LEFT OUTER JOIN locations sl
                ON bk_slocationid = sl.`Id`
            LEFT OUTER JOIN locations dl
                ON bk_dlocationid = dl.`Id`
            LEFT OUTER JOIN drivers dr
                ON cr_driverid = dr.`Id`
            WHERE `cr_isreturntrip` = '1' AND ((('$FromLocalDate' between cr_fromdatetime AND cr_todatetime)  AND ('$ToLocalDate' between cr_fromdatetime AND cr_todatetime)) OR (( cr_fromdatetime between '$FromLocalDate' AND '$ToLocalDate')  AND 
                    (cr_todatetime between '$FromLocalDate' AND '$ToLocalDate'))) AND bk_id IS NOT NULL AND cn_bookingId IS NULL AND (bk_bidrate IS NULL OR bk_bidrate = 0)
            ORDER BY cr_id";
    $query = mysql_query($sql);
    Disconnect();
    return $query;
}

function GetSharedFree($FromLocalDate, $ToLocalDate) {
    Connect();
    $sql = "SELECT cablist.*, sl.`Name` AS lc_sourcename, dl.`Name` AS lc_destinationname, dr.`Name` AS dr_drivername, cm.Id as CarModelId, dr.`Phones` AS dr_phone, tbg.TotalBooking AS tbg_totalbooking, (cm_seatcapacity-IFNULL(tbg.TotalBooking,0)-1) AS cr_available, ct.`Name` AS Type, cm.`CarImage` AS cblink, cm.`DefaultAcKmRate` AS rate
            FROM cablist
            LEFT OUTER JOIN locations sl
                ON cr_source = sl.`Id`
            LEFT OUTER JOIN locations dl
                ON cr_destination = dl.`Id`
            JOIN carmodels cm
                ON cm.`Id` = `cm_Id` 
            LEFT OUTER JOIN cartypes ct
                ON cm.`CarTypeId` = ct.`Id`
            LEFT OUTER JOIN drivers dr
                ON cr_driverid = dr.`Id`
            LEFT OUTER JOIN (SELECT CabRouteId, SUM(IFNULL(TicketCount,0)) AS TotalBooking FROM bookings group by CabRouteId) tbg
                ON tbg.CabRouteId = cr_Id
            WHERE cr_status = '1' AND `cr_isreturntrip` = '1' AND ((('$FromLocalDate' between cr_fromdatetime AND cr_todatetime)  AND ('$ToLocalDate' between cr_fromdatetime AND cr_todatetime)) OR (( cr_fromdatetime between '$FromLocalDate' AND '$ToLocalDate')  AND 
                    (cr_todatetime between '$FromLocalDate' AND '$ToLocalDate'))) AND ((bk_Id IS NULL OR bk_Id = cn_bookingId) OR bk_bidrate = 0) AND ((cm_seatcapacity-IFNULL(tbg.TotalBooking,0)-1) > 0)
            ORDER BY cr_available";
    $query = mysql_query($sql);
    Disconnect();
    return $query;
}

function GetCharterBooking($FromLocalDate, $ToLocalDate) {
    Connect();
    $sql = "SELECT cablist.*, cl.Name AS cl_name, cl.Phones AS cl_phone, sl.`Name` AS lc_sourcename, dl.`Name` AS lc_destinationname, dr.`Name` AS dr_drivername, dr.`Phones` AS dr_phone
            FROM cablist
            LEFT OUTER JOIN clients cl
                ON bk_clientid = cl.Id
            LEFT OUTER JOIN locations sl
                ON cr_source = sl.`Id`
            LEFT OUTER JOIN locations dl
                ON cr_destination = dl.`Id`
            LEFT OUTER JOIN drivers dr
                ON cr_driverid = dr.`Id`
            WHERE cr_status = '1' AND `cr_isreturntrip` = '0' AND ((('$FromLocalDate' between cr_fromdatetime AND cr_todatetime)  AND ('$ToLocalDate' between cr_fromdatetime AND cr_todatetime)) OR (( cr_fromdatetime between '$FromLocalDate' AND '$ToLocalDate')  AND 
                    (cr_todatetime between '$FromLocalDate' AND '$ToLocalDate'))) AND bk_id IS NOT NULL AND cn_bookingId IS NULL AND cr_cabid IS NOT NULL";
    $query = mysql_query($sql);
    Disconnect();
    return $query;
}

function GetCharterFree($FromLocalDate, $ToLocalDate) {
    Connect();
    $sql = "SELECT cablist.*, sl.`Name` AS lc_sourcename, dl.`Name` AS lc_destinationname, dr.`Name` AS dr_drivername, dr.`Phones` AS dr_phone
            FROM cablist
            LEFT OUTER JOIN locations sl
                ON cr_source = sl.`Id`
            LEFT OUTER JOIN locations dl
                ON cr_destination = dl.`Id`
            LEFT OUTER JOIN drivers dr
                ON cr_driverid = dr.`Id`
            WHERE cr_status = '1' AND `cr_isreturntrip` = '0' AND ((('$FromLocalDate' between cr_fromdatetime AND cr_todatetime)  AND ('$ToLocalDate' between cr_fromdatetime AND cr_todatetime)) OR (( cr_fromdatetime between '$FromLocalDate' AND '$ToLocalDate')  AND 
                (cr_todatetime between '$FromLocalDate' AND '$ToLocalDate'))) AND (bk_id IS NULL OR bk_id = cn_bookingId)
            ORDER BY cr_fromdatetime";
    $query = mysql_query($sql);
    Disconnect();
    return $query;
    
}
function GetClientCharterFree($FromLocalDate, $ToLocalDate,$FromLocation,$NoPersons=0) {
    Connect();
    $sql = "SELECT cablist.*, sl.`Name` AS lc_sourcename, dl.`Name` AS lc_destinationname,cmd.`CarImage` AS cblink, ct.`Name` AS Type, `cb_AcRate` AS rate
            FROM cablist
            LEFT OUTER JOIN locations sl
                ON cr_source = sl.`Id`
            LEFT OUTER JOIN locations dl
                ON cr_destination = dl.`Id`
            LEFT OUTER JOIN carmodels cmd
            ON cmd.Id = cm_Id
            LEFT OUTER JOIN carmodels AS cm
          ON cm_Id = cm.Id
             LEFT OUTER JOIN cartypes ct
                ON cm.`CarTypeId` = ct.`Id`
            WHERE cr_status = '1' AND `cr_isreturntrip` = '0' AND ((('$FromLocalDate' between cr_fromdatetime AND cr_todatetime)  AND ('$ToLocalDate' between cr_fromdatetime AND cr_todatetime)) OR (( cr_fromdatetime between '$FromLocalDate' AND '$ToLocalDate')  AND 
                (cr_todatetime between '$FromLocalDate' AND '$ToLocalDate'))) AND (bk_id IS NULL OR bk_id = cn_bookingId)
            ORDER BY cr_fromdatetime";
    $query = mysql_query($sql);
    //echo $sql;
    Disconnect();
    return $query;
    
}

function GetVirtualBooking($FromLocalDate, $ToLocalDate) {
    Connect();
    $sql = "SELECT cablist.*, cl.Name AS cl_name, cl.Phones AS cl_phone, sl.`Name` AS lc_sourcename, dl.`Name` AS lc_destinationname, dr.`Name` AS dr_drivername, dr.`Phones` AS dr_phone
            FROM cablist
            LEFT OUTER JOIN clients cl
                ON bk_clientid = cl.Id
            LEFT OUTER JOIN locations sl
                ON cr_source = sl.`Id`
            LEFT OUTER JOIN locations dl
                ON cr_destination = dl.`Id`
            LEFT OUTER JOIN drivers dr
                ON cr_driverid = dr.`Id`
            WHERE cr_status = '1' AND `cr_isreturntrip` = '0' AND ((('$FromLocalDate' between cr_fromdatetime AND cr_todatetime)  AND ('$ToLocalDate' between cr_fromdatetime AND cr_todatetime)) OR (( cr_fromdatetime between '$FromLocalDate' AND '$ToLocalDate')  AND 
                    (cr_todatetime between '$FromLocalDate' AND '$ToLocalDate'))) AND bk_id IS NOT NULL AND cr_cabid IS NULL";
    $query = mysql_query($sql);
    //echo $sql;
    Disconnect();
    return $query;
}

function GetBidBooking($FromLocalDate, $ToLocalDate){
    $BidAwarddata = GetBidAwardedBooking($FromLocalDate, $ToLocalDate);
    $awardedId = "";
    while($br = mysql_fetch_assoc($BidAwarddata)){
        if(!empty($awardedId))
            $awardedId .=", ";
        $awardedId .= $br['cr_id'];
    }
    if(!empty($awardedId))
        $filterAwardedId = "AND cr_id NOT IN ($awardedId)";
    else
        $filterAwardedId = "";
    Connect();
    $sql = "SELECT distinct cablist.*, cl.Name AS cl_name, cl.Phones AS cl_phone, sl.`Name` AS lc_sourcename, dl.`Name` AS lc_destinationname, dr.`Name` AS dr_drivername, dr.`Phones` AS dr_phone
            FROM cablist
            LEFT OUTER JOIN clients cl
                ON bk_clientid = cl.Id
            LEFT OUTER JOIN locations sl
                ON cr_source = sl.`Id`
            LEFT OUTER JOIN locations dl
                ON cr_destination = dl.`Id`
            LEFT OUTER JOIN drivers dr
                ON cr_driverid = dr.`Id`
            WHERE cr_status = '1' AND `cr_isreturntrip` = '1' $filterAwardedId  AND ((('$FromLocalDate' between cr_fromdatetime AND cr_todatetime)  AND ('$ToLocalDate' between cr_fromdatetime AND cr_todatetime)) OR (( cr_fromdatetime between '$FromLocalDate' AND '$ToLocalDate')  AND 
                    (cr_todatetime between '$FromLocalDate' AND '$ToLocalDate'))) AND bk_id IS NOT NULL AND bk_bidrate IS NOT NULL AND bk_bidrate <> 0 AND bk_isbidawarded = 0";
    $query = mysql_query($sql);
    
    Disconnect();
    return $query;
}

function GetBidAwardedBooking($FromLocalDate, $ToLocalDate){
    Connect();
    $sql = "SELECT distinct cablist.*, cl.Name AS cl_name, cl.Phones AS cl_phone, sl.`Name` AS lc_sourcename, dl.`Name` AS lc_destinationname, dr.`Name` AS dr_drivername, dr.`Phones` AS dr_phone
            FROM cablist
            LEFT OUTER JOIN clients cl
                ON bk_clientid = cl.Id
            LEFT OUTER JOIN locations sl
                ON cr_source = sl.`Id`
            LEFT OUTER JOIN locations dl
                ON cr_destination = dl.`Id`
            LEFT OUTER JOIN drivers dr
                ON cr_driverid = dr.`Id`
            WHERE cr_status = '1' AND `cr_isreturntrip` = '1' AND ((('$FromLocalDate' between cr_fromdatetime AND cr_todatetime)  AND ('$ToLocalDate' between cr_fromdatetime AND cr_todatetime)) OR (( cr_fromdatetime between '$FromLocalDate' AND '$ToLocalDate')  AND 
                    (cr_todatetime between '$FromLocalDate' AND '$ToLocalDate'))) AND bk_id IS NOT NULL AND bk_bidrate IS NOT NULL AND bk_bidrate <> 0 AND bk_isbidawarded = 1";
    $query = mysql_query($sql);
    Disconnect();
    return $query;
}

function GetBidFree($FromLocalDate, $ToLocalDate, $FromLocation, $ToLocation){
    Connect();
    $freeListSql = "SELECT distinct cr_id, CASE WHEN bk_bidrate > 0 THEN 1 ELSE 0 END AS cr_isbidded
                    FROM cablist
                    JOIN carmodels cm
                        ON cm.`Id` = `cm_Id` 
                    LEFT OUTER JOIN (SELECT CabRouteId, SUM(IFNULL(TicketCount,0)) AS TotalBooking FROM bookings group by CabRouteId) tbg
                        ON tbg.CabRouteId = cr_Id
                    WHERE cr_status = '1' AND `cr_isreturntrip` = '1' AND cr_source = '$FromLocation' AND cr_destination = '$ToLocation' AND ((('$FromLocalDate' between cr_fromdatetime AND cr_todatetime)  AND ('$ToLocalDate' between cr_fromdatetime AND cr_todatetime)) OR (( cr_fromdatetime between '$FromLocalDate' AND '$ToLocalDate')  AND (cr_todatetime between '$FromLocalDate' AND '$ToLocalDate'))) AND ((((bk_Id IS NULL OR bk_Id = cn_bookingId) OR bk_bidrate = 0) AND ((cm_seatcapacity-IFNULL(tbg.TotalBooking,0)) = cm_seatcapacity)) OR (bk_id IS NOT NULL AND bk_bidrate IS NOT NULL AND bk_bidrate <> 0))
                    ORDER BY cr_isbidded DESC
                    LIMIT 0,5";
    $freeListQuery = mysql_query($freeListSql);
    $freeListArray = array();
    while($fr = mysql_fetch_assoc($freeListQuery)){
        $freeListArray[] = $fr['cr_id'];
    }
    $freeListId = implode(",", $freeListArray);
    if(!empty($freeListId)){
        $biddableCab = "SELECT cablist.*, sl.`Name` AS lc_sourcename, dl.`Name` AS lc_destinationname, cm.`Id` AS cmd, dr.`Name` AS dr_drivername, ct.`Name` AS Type, cm.Id as CarModelId, cm.`CarImage` AS cblink, dr.`Phones` AS dr_phone,cm.`DefaultAcKmRate` AS rate, CASE WHEN bk_bidrate > 0 THEN 1 ELSE 0 END AS cr_isbidded
                FROM cablist
                LEFT OUTER JOIN locations sl
                    ON cr_source = sl.`Id`
                LEFT OUTER JOIN locations dl
                    ON cr_destination = dl.`Id`
                JOIN carmodels cm
                    ON cm.`Id` = `cm_Id` 
                     LEFT OUTER JOIN cartypes ct
                    ON cm.`CarTypeId` = ct.`Id`
                LEFT OUTER JOIN drivers dr
                    ON cr_driverid = dr.`Id`
                WHERE cr_status = '1' AND  cr_id IN ($freeListId)
                ORDER BY cr_isbidded DESC";
        $query = mysql_query($biddableCab);
    }
    else
        $query = "";
    Disconnect();
    return $query;
}

function GetClientSharedFree($FromLocalDate, $ToLocalDate,$FromLocation,$ToLocation) {
    Connect();
    $sql = "SELECT cablist.*, sl.`Name` AS lc_sourcename, dl.`Name` AS lc_destinationname, dr.`Name` AS dr_drivername, cm.Id as CarModelId, dr.`Phones` AS dr_phone, tbg.TotalBooking AS tbg_totalbooking, (cm_seatcapacity-IFNULL(tbg.TotalBooking,0)-1) AS cr_available, ct.`Name` AS Type, cm.`CarImage` AS cblink, cm.`DefaultAcKmRate` AS rate
            FROM cablist
            LEFT OUTER JOIN locations sl
                ON cr_source = sl.`Id`
            LEFT OUTER JOIN locations dl
                ON cr_destination = dl.`Id`
            JOIN carmodels cm
                ON cm.`Id` = `cm_Id` 
            LEFT OUTER JOIN cartypes ct
                ON cm.`CarTypeId` = ct.`Id`
            LEFT OUTER JOIN drivers dr
                ON cr_driverid = dr.`Id`
            LEFT OUTER JOIN (SELECT CabRouteId, SUM(IFNULL(TicketCount,0)) AS TotalBooking FROM bookings group by CabRouteId) tbg
                ON tbg.CabRouteId = cr_Id
            WHERE cr_status = '1' AND `cr_isreturntrip` = '1'  AND cr_source = '$FromLocation' AND cr_destination = '$ToLocation' AND ((('$FromLocalDate' between cr_fromdatetime AND cr_todatetime)  AND ('$ToLocalDate' between cr_fromdatetime AND cr_todatetime)) OR (( cr_fromdatetime between '$FromLocalDate' AND '$ToLocalDate')  AND 
                    (cr_todatetime between '$FromLocalDate' AND '$ToLocalDate'))) AND ((bk_Id IS NULL OR bk_Id = cn_bookingId) OR bk_bidrate = 0) AND ((cm_seatcapacity-IFNULL(tbg.TotalBooking,0)-1) > 0)
            ORDER BY cr_available";
    $query = mysql_query($sql) or die('Error'.  mysql_error());
    //echo $sql;
    Disconnect();
    return $query;
    
}

function GetDriverSlip($cabRouteId){
    Connect();
    $sql = "SELECT cablist.*, cl.Name AS cl_name, cl.Phones AS cl_phone, sl.`Name` AS lc_sourcename, dl.`Name` AS lc_destinationname, dr.`Name` AS dr_drivername, dr.`Phones` AS dr_phone
            FROM cablist
            LEFT OUTER JOIN clients cl
                ON bk_clientid = cl.Id
            LEFT OUTER JOIN locations sl
                ON bk_slocationid = sl.`Id`
            LEFT OUTER JOIN locations dl
                ON bk_dlocationid = dl.`Id`
            LEFT OUTER JOIN drivers dr
                ON cr_driverid = dr.`Id`
            WHERE bk_status = '1' AND cr_id = '$cabRouteId'
            ORDER BY bk_id";
    $query = mysql_query($sql);
    Disconnect();
    return $query;
}

function SelectCarModel(){
    Connect();
        $sql  = "SELECT `Id`,`Name`,`CarImage` FROM carmodels";
        $exe = mysql_query($sql);
    Disconnect();
    return $exe;
}
/* by HArdik Vyas ON 18/10/2013 Friday  For Dearture Schedule*/
function GetClientAllSharedFree($FromLocalDate) {
    Connect();
    $sql = "SELECT cablist.*, sl.`Name` AS lc_sourcename, dl.`Name` AS lc_destinationname, dr.`Name` AS dr_drivername, cm.Id as CarModelId, dr.`Phones` AS dr_phone, tbg.TotalBooking AS tbg_totalbooking, (cm_seatcapacity-IFNULL(tbg.TotalBooking,0)-1) AS cr_available, ct.`Name` AS Type, cm.`CarImage` AS cblink, cm.`DefaultAcKmRate` AS rate
            FROM cablist
            LEFT OUTER JOIN locations sl
                ON cr_source = sl.`Id`
            LEFT OUTER JOIN locations dl
                ON cr_destination = dl.`Id`
            JOIN carmodels cm
                ON cm.`Id` = `cm_Id` 
            LEFT OUTER JOIN cartypes ct
                ON cm.`CarTypeId` = ct.`Id`
            LEFT OUTER JOIN drivers dr
                ON cr_driverid = dr.`Id`
            LEFT OUTER JOIN (SELECT CabRouteId, SUM(IFNULL(TicketCount,0)) AS TotalBooking FROM bookings group by CabRouteId) tbg
                ON tbg.CabRouteId = cr_Id
            WHERE cr_status = '1' AND `cr_isreturntrip` = '1'  AND ('$FromLocalDate' between cr_fromdatetime AND cr_todatetime)  
                     AND ((bk_Id IS NULL OR bk_Id = cn_bookingId) OR bk_bidrate = 0) AND ((cm_seatcapacity-IFNULL(tbg.TotalBooking,0)-1) > 0)
            ORDER BY cr_available";
    $query = mysql_query($sql) or die('Error'.  mysql_error());
    //echo $sql;
    Disconnect();
    return $query;
}
?>
