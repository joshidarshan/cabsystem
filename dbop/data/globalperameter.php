<?php

if ($_SERVER['HTTP_HOST'] == "localhost" || $_SERVER['HTTP_HOST'] == "localhost:8080")
    include_once $_SERVER['DOCUMENT_ROOT'] . '/cabsystem/dbop/dbconnect.php';
else
    include_once $_SERVER['DOCUMENT_ROOT'] . '/dbop/dbconnect.php';

function AddGlobalParameter($mh_percentage, $mh_multipleof, $cp_minimunkm, $cp_point, $cp_maxpoints, $cp_point_to_rupee_rate, $cp_discount_percent, $cab_freehours, $cab_nearbykm, $cab_average_speed, $cp_redeemtion_level, $recordperpage, $virtual_cab_per_model, $booking_confirmation_time) {
    Connect();
    $sql = "INSERT INTO
            globalparameters(mh_percentage, mh_multipleof, cp_minimunkm, cp_point, cp_maxpoints, cp_point_to_rupee_rate, cp_discount_percent, cab_freehours, cab_nearbykm, cab_average_speed, cp_redeemtion_level, recordperpage, virtual_cab_per_model, booking_confirmation_time)
            VALUES ('$mh_percentage', '$mh_multipleof', '$cp_minimunkm', '$cp_point', '$cp_maxpoints', '$cp_point_to_rupee_rate', '$cp_discount_percent', '$cab_freehours', '$cab_nearbykm', '$cab_average_speed', '$cp_redeemtion_level', '$recordperpage', '$virtual_cab_per_model', '$booking_confirmation_time')";
    mysql_query($sql) or die('Error'.  mysql_error());
    $ReturnId = mysql_insert_id();
    Disconnect();

    return $ReturnId;
}

function EditGlobalParameter($Id, $mh_percentage, $mh_multipleof, $cp_minimunkm, $cp_point, $cp_maxpoints, $cp_point_to_rupee_rate, $cp_discount_percent, $cab_freehours, $cab_nearbykm, $cab_average_speed, $redeemtion_level, $recordperpage, $virtual_cab_per_model, $booking_confirmation_time) {
    Connect();
    $sql = "UPDATE globalparameters 
            SET mh_percentage='$mh_percentage',mh_multipleof='$mh_multipleof',cp_minimunkm='$cp_minimunkm',cp_point = '$cp_point',cp_maxpoints='$cp_maxpoints',cp_point_to_rupee_rate='$cp_point_to_rupee_rate',cp_discount_percent='$cp_discount_percent',cab_freehours='$cab_freehours',cab_nearbykm='$cab_nearbykm',cab_average_speed='$cab_average_speed', cp_redeemtion_level = '$redeemtion_level', recordperpage = '$recordperpage', virtual_cab_per_model = '$virtual_cab_per_model', booking_confirmation_time = '$booking_confirmation_time'
            WHERE Id = '$Id'";
    mysql_query($sql);
    $ReturnRows = mysql_affected_rows();
    Disconnect();

    return $ReturnRows;
}

function GlobalParametersPagination() {
    Connect();
    $sql = "SELECT COUNT(`Id`) AS total 
            FROM globalparameters";
    $exe = mysql_query($sql);
    $fetch = mysql_fetch_array($exe);
    $no = $fetch['total'];
    Disconnect();

    return $no;
}

/*
 * Author: Harik Vyas
 * Des: Set Limit Of Record Per Page According Number Of Table Records.
 * ChangeBy: Darshan Joshi
 * Changes: Added modified fields.
 * changedOn: 6-9-2013,SaturDay
 */

function GetAllGlobalParameters($start = 0, $rpp = 500) {
    if ($rpp == 500) {
        $no = GlobalParametersPagination();
        $rpp = $no;
    }
    Connect();
    $sql = "SELECT `Id`, mh_percentage,mh_multipleof ,cp_minimunkm, cp_point, cp_maxpoints, cp_point_to_rupee_rate, cp_discount_percent, cab_freehours, cab_nearbykm, cab_average_speed, cp_redeemtion_level, recordperpage, virtual_cab_per_model, booking_confirmation_time
            FROM globalparameters 
            LIMIT $start,$rpp";
    $exe = mysql_query($sql);
    Disconnect();

    return $exe;
}

function GetGlobalParameter($Id) {
    Connect();
    $sql = "SELECT `Id`, mh_percentage, mh_multipleof, cp_minimunkm, cp_point, cp_maxpoints, cp_point_to_rupee_rate, cp_discount_percent, cab_freehours, cab_nearbykm, cab_average_speed,cp_redeemtion_level, recordperpage, virtual_cab_per_model, booking_confirmation_time
            FROM globalparameters 
            WHERE Id = '$Id'";
    $exe = mysql_query($sql) or die ('Error'. mysql_error());
    Disconnect();

    return $exe;
}

function GetVirtualCabPerModel(){
    $globalQuery = GetAllGlobalParameters();
    $globalData = mysql_fetch_assoc($globalQuery);
    $virtualCabPerModel = $globalData['virtual_cab_per_model'];
    return $virtualCabPerModel;
}

function GetVirtualCabCount(){
    Connect();
    $globalQuery = "SELECT COUNT(Id) as TotalCabs
                    FROM carmodels
                    WHERE `IsVirtual` = '1' AND `Status` = '1'";
    $globaldataQuery = mysql_query($globalQuery);
    $globalData = mysql_fetch_assoc($globaldataQuery);
    $virtualCabCount = $globalData['TotalCabs'];
    Disconnect();
    return $virtualCabCount;
}

function GetMaharajaFare($fare){
    $data = GetAllGlobalParameters();
    $mhdata = mysql_fetch_assoc($data);
    $maharajaPercent = $mhdata['mh_percentage'];
    $maharajaMultiple = $mhdata['mh_multipleof'];
    
    $maharajaFare = $fare + (($fare * $maharajaPercent)/100) +$maharajaMultiple;
    $maharajaFare = intval($maharajaFare / $maharajaMultiple) * $maharajaMultiple;
    return $maharajaFare;
}
?>
