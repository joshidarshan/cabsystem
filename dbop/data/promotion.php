<?php

if ($_SERVER['HTTP_HOST'] == "localhost" || $_SERVER['HTTP_HOST'] == "localhost:8080")
    include_once $_SERVER['DOCUMENT_ROOT'] . '/cabsystem/dbop/dbconnect.php';
else
    include_once $_SERVER['DOCUMENT_ROOT'] . '/dbop/dbconnect.php';

function AddPromotion($Name, $Description, $Status) {
    Connect();
    $sql = "INSERT INTO
            promotions(`Name`, `Description`, `Status`)
            VALUES ('$Name',$Description','$Status')";
    mysql_query($sql);
    $ReturnId = mysql_insert_id();
    Disconnect();
    
    return $ReturnId;
}

function EditPromotion($Id, $Name, $Description, $Status) {
    Connect();
    $sql = "UPDATE promotions 
            SET `Name` = '$Name', `Description` = '$Description', `Status` = '$Status' 
            WHERE `Id` = '$Id'";
    mysql_query($sql) or die('Query Error' . mysql_error());
    $ReturnRows = mysql_affected_rows();
    Disconnect();
    
    return $ReturnRows;
}

function DeletePromotion($Id) {
    Connect();
    $sql = "UPDATE promotions 
            SET `Status` = 0 
            WHERE `Id` = '$Id'";
    mysql_query($sql);
    $ReturnRows = mysql_affected_rows();
    Disconnect();

    return $ReturnRows;
}

function PromotionsPagination() {
    Connect();
    $sql = "SELECT COUNT(`Id`) AS total 
            FROM promotions";
    $exe = mysql_query($sql);
    $fetch = mysql_fetch_array($exe);
    $no = $fetch['total'];
    Disconnect();
    
    return $no;
}

/*
 * Author: Harik Vyas
 * Des: Set Limit Of Record Per Page According Number Of Table Records.
 * ChangeBy: Hardik Vyas
 * Changes: Put A Condition For Limitation.
 * changedOn: 31-08-2013,SaturDay
 */
function GetAllPromotions($start = 0, $rpp = 500){
    if ($rpp == 500) {
        $no = PromotionsPagination();
        $rpp = $no;
    }
    Connect();
    $sql = "SELECT `Id`, `Name`, `Description`, `Status` 
            FROM promotions 
            LIMIT $start, $rpp";
    $exe = mysql_query($sql);
    Disconnect();

    return $exe;
}


function GetAllPromotionsData(){
    Connect();
    $sql = "SELECT `Id`,`Name`, `Description`, `Status` 
           FROM promotions";
    $qry = mysql_query($sql);
    Disconnect();
    
    return $qry;
}

function GetPromotion($Id) {
    Connect();
    $sql = "SELECT `Name`, `Description`, `Status` 
            FROM promotions WHERE `Id` = '$Id'";
    $exe = mysql_query($sql);
    Disconnect();

    return $exe;
}

?>
