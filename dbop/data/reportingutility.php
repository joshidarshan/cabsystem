<?php
if ($_SERVER['HTTP_HOST'] == "localhost" || $_SERVER['HTTP_HOST'] == "localhost:8080")
    include_once $_SERVER['DOCUMENT_ROOT'] . '/cabsystem/dbop/dbconnect.php';
else
    include_once $_SERVER['DOCUMENT_ROOT'] . '/dbop/dbconnect.php';

function GetRouteWiseBusinessSummary($fromDate, $toDate){
    Connect();
    $sql = "SELECT ts.RouteName,
            SUM(CASE WHEN ts.SharedNoTrips>0 THEN 1 ELSE 00000 END) AS SharedNoTrips, 
            SUM(ts.SharedFare) AS SharedFare, 
            SUM(ts.SharedAmountReceived) AS SharedAmountReceived, 
            SUM(CASE WHEN ts.CharteredNoTrips > 0 THEN 1 ELSE 00000 END) AS CharteredNoTrips, 
            SUM(ts.CharteredFare) As CharteredFare, 
            SUM(ts.CharteredAmountReceived) As CharteredAmountReceived, 
            COUNT(ts.Id) AS TotalTrips,
            SUM(ts.TotalFare) AS TotalFare, 
            SUM(ts.TotalAmountReceived) AS TotalAmountReceived
            FROM tripsummary ts
            WHERE ts.FromDate BETWEEN '$fromDate' AND '$toDate'
            GROUP BY ts.RouteName
            ORDER BY ts.RouteName";
    $data = mysql_query($sql);
    Disconnect();
    return $data;
}

function GetAgentWiseBusinessSummaryReport($fromDate, $toDate){
    Connect();
    $sql = "Select ag.Name AS AgentName, 
            SUM(CASE WHEN ts.SharedNoTrips>0 THEN 1 ELSE 00000 END) AS SharedNoTrips, 
            SUM(ts.SharedFare) AS SharedFare, 
            SUM(ts.SharedAmountReceived) AS SharedAmountReceived, 
            SUM(CASE WHEN ts.CharteredNoTrips > 0 THEN 1 ELSE 00000 END) AS CharteredNoTrips, 
            SUM(ts.CharteredFare) As CharteredFare, 
            SUM(ts.CharteredAmountReceived) As CharteredAmountReceived, 
            COUNT(ts.Id) AS TotalTrips,
            SUM(ts.TotalFare) AS TotalFare, 
            SUM(ts.TotalAmountReceived) AS TotalAmountReceived
            FROM tripsummary ts
            JOIN agents ag
                    ON ts.AgentId = ag.Id
            WHERE ts.FromDate BETWEEN '$fromDate' AND '$toDate'
            GROUP BY ag.Name
            ORDER BY ag.Name";
    $data = mysql_query($sql);
    Disconnect();
    return $data;
}

function GetAgentWiseSharedTrips($agentIdList, $fromDate, $toDate){
    Connect();
    $sql = "SELECT ag.Name AS ag_name, cl.cr_fromdatetime, cl.cb_NoPlate, lcs.Name AS lcs_sourceName, lcd.Name AS lcd_destinationName, cl.bk_fare, cl.bk_amountReceived
            FROM cablist cl
            JOIN locations lcs
                    ON cl.cr_source = lcs.Id
            JOIN locations lcd
                    ON cl.cr_destination = lcd.Id
            JOIN agents ag
                    ON cl.cr_agentid = ag.Id
            WHERE cl.cr_agentid IN ('$agentIdList') AND cl.cr_fromdatetime BETWEEN '$fromDate' AND '$toDate' AND cl.cr_isreturntrip = '1'
            ORDER BY cl.cr_agentid, cl.cr_fromdatetime;";
    $data = mysql_fetch_assoc($sql);
    Disconnect();
    return $data;
}

function GetAgentWiseCharterTrips($agentIdList, $fromDate, $toDate){
    Connect();
    $sql = "SELECT ag.Name AS ag_name, cl.cr_fromdatetime, cl.cb_NoPlate, lcs.Name AS lcs_sourceName, lcd.Name AS lcd_destinationName, cl.bk_fare, cl.bk_amountReceived
            FROM cablist cl
            JOIN locations lcs
                    ON cl.cr_source = lcs.Id
            JOIN locations lcd
                    ON cl.cr_destination = lcd.Id
            JOIN agents ag
                    ON cl.cr_agentid = ag.Id
            WHERE cl.cr_agentid IN ('$agentIdList') AND cl.cr_fromdatetime BETWEEN '$fromDate' AND '$toDate' AND cl.cr_isreturntrip = '0'
            ORDER BY cl.cr_agentid, cl.cr_fromdatetime;";
    $data = mysql_fetch_assoc($sql);
    Disconnect();
    return $data;
}

function GetRouteWiseSharedTrips($fromDate, $toDate){
    Connect();
    $sql = "SELECT ag.Name AS ag_name, cl.cr_fromdatetime, cl.cb_NoPlate, lcs.Name AS lcs_sourceName, lcd.Name AS lcd_destinationName, cl.bk_fare, cl.bk_amountReceived
            FROM cablist cl
            JOIN locations lcs
                    ON cl.cr_source = lcs.Id
            JOIN locations lcd
                    ON cl.cr_destination = lcd.Id
            JOIN agents ag
                    ON cl.cr_agentid = ag.Id
            WHERE cl.cr_fromdatetime BETWEEN '$fromDate' AND '$toDate' AND cl.cr_isreturntrip = '1'
            ORDER BY cl.cr_source, cl.cr_destination, cl.cr_fromdatetime";
    $data = mysql_query($sql);
    Disconnect();
    return $data;
}


?>
