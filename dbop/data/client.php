<?php

if ($_SERVER['HTTP_HOST'] == "localhost" || $_SERVER['HTTP_HOST'] == "localhost:8080")
    include_once $_SERVER['DOCUMENT_ROOT'] . '/cabsystem/dbop/dbconnect.php';
else
    include_once $_SERVER['DOCUMENT_ROOT'] . '/dbop/dbconnect.php';
function UPath() {
    $path = $_SERVER['DOCUMENT_ROOT'];
    // Differenciate the path for local and web host. Includes database connect file.
    if ($_SERVER['HTTP_HOST'] == "localhost" || $_SERVER['HTTP_HOST'] == "localhost:8080")
        $path .= '/Cabsystem';
    return $path;
}

function ClientAdd($Name, $Email, $Phones, $Address, $Milestone, $Note, $CreditKm, $CreditPoint, $Status) {
    Connect();
    $sql = "INSERT INTO clients(`Name`, `Email`, `Phones`, `Address`, `Milestone`, `Note`, `CreditKm`, `CreditPoints`, `Status`)
            VALUES('$Name','$Email','$Phones','$Address','$Milestone','$Note','$CreditKm','$CreditPoint','$Status')";
    mysql_query($sql);
//    echo $sql;
    $ReturnId = mysql_insert_id();
    Disconnect();

    return $ReturnId;
}

function ClientEdit($Id, $Name, $Email, $Phones, $Address, $Milestone, $Note, $CreditKm, $CreditPoint, $Status) {
    Connect();
    $sql = "UPDATE clients
            SET `Name` = '$Name',`Email` = '$Email', `Phones` = '$Phones', `Address` = '$Address', `Milestone` = '$Milestone', `Note` = '$Note', `CreditKm` = '$CreditKm', `CreditPoints` = '$CreditPoint', `Status` = '$Status' 
            WHERE `Id` = '$Id'";
    mysql_query($sql);
//    echo $sql;
    $ReturnRows = mysql_affected_rows();
    Disconnect();
    return $ReturnRows;
}

function ClientPagination() {
    Connect();
    $sql = "SELECT COUNT(`Id`) AS total 
            FROM clients ";
    $exe = mysql_query($sql);
    $num = mysql_fetch_array($exe);
    $no = $num['total'];
    Disconnect();
    return $no;
}

/*
 * Author: Harik Vyas
 * Des: Set Limit Of Record Per Page According Number Of Table Records.
 * ChangeBy: Hardik Vyas
 * Changes: Put A Condition For Limitation.
 * changedOn: 31-08-2013,SaturDay
 */

function GetAllClient($clientStart=0, $clientRpp=500) {
    if ($clientRpp == 500) {
        $no = ClientPagination();
        $clientRpp = $no;
    }

    Connect();
    $sql = "SELECT `Id`, `Name`, `Email`, `Phones`, `Address`, `Milestone`, `Note`, `CreditKm`, `CreditPoints`, `Status` 
            FROM clients 
            LIMIT $clientStart,$clientRpp";
    $exe = mysql_query($sql);
    Disconnect();

    return $exe;
}

function GetClient($Id) {
    Connect();
    $sql = "SELECT `Name`, `Email`, `Phones`, `Address`, `Milestone`, `Note`, `CreditKm`, `CreditPoints`, `Status`
            FROM clients 
            WHERE Id = '$Id'";
    $exe = mysql_query($sql);
    Disconnect();

    return $exe;
}

/*
 * Author: Harik
 * Des: Will delete client from system.
 * ChangeBy: Darshan
 * Changes: Modify query.
 * changedOn: 22-08-2013
 */

function ClientDelete($Id) {
    Connect();
    $sql = "UPDATE clients
            SET Status='0' 
            WHERE Id = '$Id'";
    mysql_query($sql);
    $ReturnRows = mysql_affected_rows();
    Disconnect();

    return $ReturnRows;
}
/*
 * Author: Harik Vyas
 * Des: Made for  Check Availablity of Contact No with Ajax.
 * ChangeBy: Hardik Vyas
 * Changes: Return Number Of rows With Select Query.
 * changedOn: 2-09-2013,MondayDay
 */
function CheckClientPhone($Contact){
    Connect();
                $sql = "select 
                        Id from 
                        clients where Phones = '$Contact'";
                $exe = mysql_query($sql);
                $rows = mysql_num_rows($exe);
   Disconnect();
   return $rows;
   
}

/*
 * Author: Harik Vyas
 * Des: Made for  Check Availablity of Email with Ajax.
 * ChangeBy: Hardik Vyas
 * Changes: Return Number Of rows With Select Query.
 * changedOn: 2-09-2013,MondayDay
 */
function CheckClientEmail($Email){
    Connect();
                $sql = "select 
                        Id from 
                        clients where Email = '$Email'";
                $exe = mysql_query($sql);
                $rows = mysql_num_rows($exe);
   Disconnect();
   return $rows;
}

/** 
 * GetUserBYPhoneEmail
 *
 * Gets the ClientId by finding or creating email or phone
 *
 * @param string $email the unique email of user
 * @param string $phone the unique phone of user
 * @param string $name the name of client. Needed if required to add user to system
 */

function GetUserByPhoneEmail($email, $phone, $name){
    Connect();
    
    // Trying to select user
    $getUserSql = "SELECT `Id`
            FROM clients
            WHERE `Email` = '$email' OR `Phones` = '$phone'";
    $getUserQuery = mysql_query($getUserSql);
    Disconnect();
    $getUserData = mysql_fetch_assoc($getUserQuery);
    $clientId = 0;
    if(!$getUserData){
        $status = 1;
        //$Name, $Email, $Phones, $Address, $Milestone, $Note, $CreditKm, $CreditPoint, $Status
        $clientId = ClientAdd($name, $email, $phone, NULL, NULL, NULL, 0, 0, $status);
    }
    else
        $clientId = $getUserData['Id'];
    return $clientId;
}
function CheckUser($Id,$email, $phone, $name,$Pwd, $RoleId){
    Connect();
//    $sql = "SELECT clients.`Id` AS CId,users.`Id` AS UId ON clients.Id = users.Id FROM clients JOIN users WHERE `Id` = '$Id' AND (`Email` = '$email' OR `Phones` = '$phone')";
    $sql = "SELECT `Id` FROM clients WHERE `Id` = '$Id' AND (`Email` = '$email' OR `Phones` = '$phone')";
    $exe = mysql_query($sql);
       $GetData = mysql_fetch_assoc($exe);
    Disconnect();
    $ReturnId = 0;
     include_once UPath() . '/dbop/data/user.php';
    if($GetData){
        ClientEdit($Id, $name, $email, $phone, NULL, NULL, NULL, 0, 0, 1);
        $ReturnId = $GetData['Id'];
//        EditUser($ReturnId, $email, $phone, $Pwd, $RoleId, 1);
        
                
    }
    else{
        $ReturnId = ClientAdd($name, $email, $phone, NULL, NULL, NULL, 0, 0, 1);
       
        AddUser($ReturnId, $email, $phone, $Pwd, $RoleId, 1);
    }
    return $ReturnId;
}
function ClientProfileInsert ($Id,$ClientId,$TravelerType,$Gander,$Designation,$Occupation,$Department,$Company){
   Connect();
    $sql = "SELECT `ClientId` FROM clientprofile WHERE `ClientId` = '$Id'";
    $exe = mysql_query($sql);
    
    //echo "<br/> ClientProfile". $sql; 
    $GetClientProfile = mysql_fetch_assoc($exe); 
    Disconnect();   
    $ReturnId = 0;
    if($GetClientProfile){
        $ReturnId = ClientProfileEdit ($ClientId,$TravelerType,$Gander,$Designation,$Occupation,$Department,$Company);
        $ReturnId = $GetClientProfile['ClientId'];
    }
    else{
        $ReturnId = ClientProfileAdd($ClientId,$TravelerType,$Gander,$Designation,$Occupation,$Department,$Company);
//         AddUser($ReturnId, $email, $phone, $Pwd, $RoleId, 1);
        }
    return $ReturnId;   
}
function ClientInfoById($Id,$Email,$Phone){
     Connect();
    $sql = "SELECT clt.`Name` AS CName, clt.`Email` AS CEmail, clt.`Phones` AS CPhone, cltpro.`TravelerType` AS CPTravel, cltpro.`Gander` AS CPGander,cltpro.`Designation` AS CPDes,cltpro.`Occupation` AS CPOccup,cltpro.`Department` AS CPDept,cltpro.`Company` AS CPComp
            FROM clients AS clt 
            JOIN clientprofile cltpro
            ON clt.`Id` = cltpro.`ClientId`
            WHERE clt.Id = '$Id' AND (clt.`Email`= '$Email' OR clt.`Phones` = '$Phone')";
    $exe = mysql_query($sql);
    //echo $sql;
    Disconnect();

    return $exe;
}
function ClientProfileAdd($ClientId,$TravelerType,$Gander,$Designation,$Occupation,$Department,$Company) {
    Connect();
    $sql = "INSERT INTO clientprofile(`ClientId`, `TravelerType`, `Gander`, `Designation`, `Occupation`, `Department`, `Company`)
            VALUES('$ClientId','$TravelerType','$Gander','$Designation','$Occupation','$Department','$Company')";
    mysql_query($sql);
//    echo $sql;
    $ReturnId = mysql_insert_id();
    Disconnect();

    return $ReturnId;
}
function ClientProfileEdit ($ClientId,$TravelerType,$Gander,$Designation,$Occupation,$Department,$Company){
    Connect();
    $sql = "UPDATE clientprofile SET `TravelerType` = '$TravelerType', `Gander`='$Gander', `Designation`='$Designation', `Occupation`='$Occupation', `Department`='$Department', `Company`='$Company' WHERE `ClientId` = '$ClientId'";
    $exe = mysql_query($sql);
//    echo $sql;
    $ReturnId = mysql_affected_rows();
    Disconnect();
    return $ReturnId;
}    

function ClientInfoByClientId($Id){
    Connect();
    $sql = "select * from clientprofile where `Id` = '$Id'";
    $exe = mysql_query($sql);
    Disconnect();
    return $exe;
}
?>
