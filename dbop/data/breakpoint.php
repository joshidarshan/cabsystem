<?php

if ($_SERVER['HTTP_HOST'] == "localhost" || $_SERVER['HTTP_HOST'] == "localhost:8080")
    include_once $_SERVER['DOCUMENT_ROOT'] . '/cabsystem/dbop/dbconnect.php';
else
    include_once $_SERVER['DOCUMENT_ROOT'] . '/dbop/dbconnect.php';

function InsertBreakpoint($slocationId, $dlocationId, $name, $description, $maplocation, $Status) {
    Connect();
    $sql = "INSERT INTO breakpoints(`SLocationId`, `DLocationId`, `Name`, `Description`, `Maplocation`, `Status`)
            VALUES('$slocationId', '$dlocationId', '$name', '$description', '$maplocation', '$Status')";
    $query = mysql_query($sql);
    $insertId = mysql_insert_id();
    Disconnect();
    return $insertId;
}

function UpdateBreakpoint($Id, $SlocationId, $DlocationId, $Name, $Description, $Maplocation, $Status) {
    Connect();
    $sql = "UPDATE breakpoints
            SET `SLocationId` = '$SlocationId', `DLocationId` = '$DlocationId', `Name` = '$Name', `Description` = '$Description', `Maplocation` = '$Maplocation', `Status` = '$Status'
            WHERE `Id` = '$Id'";
    $query = mysql_query($sql);
    $affectedRows = mysql_affected_rows();
    Disconnect();
    return $affectedRows;
}

function DeleteBreakpoint($id) {
    Connect();
    $sql = "UPDATE breakpoints
            SET `Status` = '0'
            WHERE `Id` = '$id'";
    mysql_query($sql);
    $affectedRows = mysql_affected_rows();
    Disconnect();

    return $affectedRows;
}

function BreakPointsPagination() {
    Connect();
    $sql = "SELECT COUNT(Id) AS total 
            FROM breakpoints";
    $exe = mysql_query($sql);
    $fetch = mysql_fetch_array($exe);
    $no = $fetch['total'];
    Disconnect();

    return $no;
}

/*
 * Author: Harik Vyas
 * Des: Set Limit Of Record Per Page According Number Of Table Records.
 * ChangeBy: Hardik Vyas
 * Changes: Put A Condition For Limitation.
 * changedOn: 31-08-2013,SaturDay
 */
function GetBreakpoint($start = 0, $range = 50) {
    if ($range == 500) {
        $no = BreakPointsPagination();
        $range = $no;
    }

    Connect();
    $sql = "SELECT bk.`Id`, bk.`SLocationId`, bk.`DLocationId`, bk.`Name`, bk.`Description`, bk.`Maplocation`, bk.`Status`, sl.Name as SrcName, dl.Name as DstName
            FROM breakpoints bk
            JOIN locations sl
                ON bk.`SLocationId` = sl.`Id` 
            JOIN locations dl
                ON bk.`DLocationId` = dl.`Id`
            LIMIT $start,$range";
    $query = mysql_query($sql);
    Disconnect();
    return $query;
}

function GetBreakpointByLocation($slocationId, $dlocationId) {
    Connect();
    $sql = "SELECT `Id`, `SLocationId`, `DLocationId`, `Name`, `Description`, `Maplocation`, `Status`
            FROM breakpoints
            WHERE `SLocationId` = '$slocationId' AND `DLocationId` = '$dlocationId'";
    $query = mysql_query($sql);
    Disconnect();
    return $query;
}

function GetBreakpointById($Id) {
    Connect();
    $sql = "SELECT `Id`, `SLocationId`, `DLocationId`, `Name`, `Description`, `Maplocation`, `Status`
            FROM breakpoints
            WHERE Id = '$Id'";
    $query = mysql_query($sql);
    Disconnect();
    return $query;
}

function BreakPointPaginationByLocation($slocationId, $dlocationId) {
    Connect();
    $sql = "SELECT COUNT(Id) AS total 
            FROM breakpoints
            WHERE `SLocationId` = '$slocationId' AND `DLocationId` = '$dlocationId'";
    $query = mysql_query($sql);
    $data = mysql_fetch_assoc($query);
    $rows = $data['total'];
    Disconnect();

    return $rows;
}

?>
