<?php

if ($_SERVER['HTTP_HOST'] == "localhost" || $_SERVER['HTTP_HOST'] == "localhost:8080")
    include_once $_SERVER['DOCUMENT_ROOT'] . '/cabsystem/dbop/dbconnect.php';
else
    include_once $_SERVER['DOCUMENT_ROOT'] . '/dbop/dbconnect.php';

function InsertBid($ClientId,$SLocationId,$DLocationId,$Date1,$Date2,$Date3,$NoPerson,$ModelBid,$IsAwarded,$Status){
    Connect();
    $sql = "INSERT INTO `bidcab` 
            (`ClientId`,`SLocationId`,`DLocationId`,`DateTime-1`,`DateTime-2`,`DateTime-3`,`NoPassenger`,`ModelBid`,`IsAwarded`,`Status`)
             VALUES ('$ClientId','$SLocationId','$DLocationId','$Date1','$Date2','$Date3','$NoPerson','$ModelBid','$IsAwarded','$Status')";
    $exe = mysql_query($sql) or die ('Error'.  mysql_error());
//    echo $sql;
    $ReturnId = mysql_insert_id();
    Disconnect();
    return $ReturnId;
}

function GetNewBidBookings($FromDateTime, $ToDateTime){
    Connect();
    $sql = "SELECT DISTINCT cablist.*, cl.`Name` AS cl_name, cl.`Phones` AS cl_phone, sl.`Name` as lc_sourcename, dl.`Name` as lc_destinationname, bc.`ModelBid` as bc_bidData, dr.`Name` as dr_drivername, dr.`Phones` as dr_phone"
            . " From cablist"
            . " LEFT OUTER JOIN clients cl"
            . "     ON cl.`Id` = bk_clientid"
            . " LEFT OUTER JOIN bidcab bc"
            . "     ON bc.`ClientId` = cl.`Id`"
            . " LEFT OUTER JOIN locations sl"
            . "     ON bc.`SLocationId` = sl.`Id`"
            . " LEFT OUTER JOIN locations dl"
            . "     ON bc.`DLocationId` = dl.`Id`"
            . " LEFT OUTER JOIN drivers dr"
            . "     ON cr_driverid = dr.`Id`"
            . " WHERE cr_status = '1' AND ((bc.`DateTime-1` BETWEEN '$FromDateTime' AND '$ToDateTime') OR (bc.`DateTime-2` BETWEEN '$FromDateTime' AND '$ToDateTime')) ";
    $query = mysql_query($sql);
    Disconnect();
    return $query;
}

function GetBidBookingTemp($FromDateTime, $ToDateTime){
    Connect();
    $sql = "SELECT DISTINCT cl.Name as client, sl.Name as Source, dl.Name as Destination, bc.`DateTime-1` as DT1, bc.`DateTime-2` as DT2, bc.NoPassenger as NoPassengers, bc.ModelBid as ModelBid
            FROM bidcab bc
            JOIN clients cl
                ON bc.ClientId = cl.Id
            JOIN locations sl
                ON bc.SLocationId = sl.Id
            JOIN locations dl
                ON bc.DLocationId = dl.Id
            WHERE bc.Status = '1' AND ((bc.`DateTime-1` BETWEEN '$FromDateTime' AND '$ToDateTime') OR (bc.`DateTime-2` BETWEEN '$FromDateTime' AND '$ToDateTime'))";
    $query = mysql_query($sql);
    Disconnect();
    return $query;
}
        
?>
