<?php

if ($_SERVER['HTTP_HOST'] == "localhost" || $_SERVER['HTTP_HOST'] == "localhost:8080")
    include_once $_SERVER['DOCUMENT_ROOT'] . '/cabsystem/dbop/dbconnect.php';
else
    include_once $_SERVER['DOCUMENT_ROOT'] . '/dbop/dbconnect.php';

function AddCancelation($ClientId, $BookingId, $RewardAmount, $Point) {
    Connect();
    $sql = "INSERT INTO
            cancelations(`ClientId`, `BookingId`, `RewardAmount`, `RewardPoints`, `Status`)
            VALUES ('$ClientId','$BookingId','$RewardAmount','$Point', '1')";
    mysql_query($sql);
    $ReturnId = mysql_insert_id();
    Disconnect();

    return $ReturnId;
}

function AddSimpleCancellation($ClientId, $BookingId){
    Connect();
    $sql = "INSERT INTO
            cancelations(`ClientId`, `BookingId`)
            VALUES('$ClientId', '$BookingId')";
    mysql_query($sql);
    $cancelId = mysql_insert_id();
    Disconnect();
    
    return $cancelId;
}

function EditCancelation($Id, $ClientId, $BookingId, $RewardAmount, $Point) {
    Connect();
    $sql = "UPDATE cancelations 
            SET `ClientId` = '$ClientId', `BookingId` = '$BookingId', `RewardAmount` = '$RewardAmount', `RewardPoints` = '$Point' 
            WHERE Id='$Id'";

    mysql_query($sql);
    $ReturnRows = mysql_affected_rows();
    Disconnect();
    return $ReturnRows;
}

function DeleteCancelation($Id) {
    Connect();
    $sql = "UPDATE cancelations 
            SET Status = 0 
            WHERE Id= '$Id' ";
    $exe = mysql_query($sql);
    $ReturnRows = mysql_affected_rows();
    Disconnect();

    return $ReturnRows;
}

function CancelationPagination() {
    Connect();
    $sql = "SELECT 
            COUNT(Id) AS total 
            FROM cancelations";
    $exe = mysql_query($sql);
    $fetch = mysql_fetch_array($exe);
    $no = $fetch['total'];
    Disconnect();

    return $no;
}

/*
 * Author: Harik Vyas
 * Des: Set Limit Of Record Per Page According Number Of Table Records.
 * ChangeBy: Hardik Vyas
 * Changes: Put A Condition For Limitation.
 * changedOn: 31-08-2013,SaturDay
 */
function GetAnllCancelation($start, $rpp) {
    if ($rpp == 500) {
        $no = CancelationPagination();
        $rpp = $no;
    }

    Connect();
    $sql = "SELECT `Id`, `ClientId`, `BookingId`, `RewardAmount`, `RewardPoints` 
            FROM cancelations 
            LIMIT $start, $rpp";
    $exe = mysql_query($sql);

    Disconnect();
    return $exe;
}

function GetCancelation($Id) {
    Connect();
    $sql = "SELECT `ClientId`, `BookingId`, `RewardAmount`, `RewardPoints` 
            FROM cancelations 
            WHERE Id = '$Id'";
    $exe = mysql_query($sql);
    Disconnect();

    return $exe;
}

?>
