<?php

if ($_SERVER['HTTP_HOST'] == "localhost" || $_SERVER['HTTP_HOST'] == "localhost:8080")
    include_once $_SERVER['DOCUMENT_ROOT'] . '/cabsystem/dbop/dbconnect.php';
else
    include_once $_SERVER['DOCUMENT_ROOT'] . '/dbop/dbconnect.php';

function AddClientCreditPoint($ClientId, $Points, $ByCancel, $DateTime, $JobId) {
    Connect();
    $sql = "INSERT INTO clientcreditpoints(`ClientId`, `Points`, `ByCancel`, `DateTime`, `JobId`)
            VALUES ('$ClientId','$Points','$ByCancel','$DateTime','$JobId')";
    mysql_query($sql);
    $ReturnId = mysql_insert_id();
    Disconnect();

    return $ReturnId;
}

function EditClientCreditPoint($Id, $ClientId, $BookingId, $RewardAmount, $Point) {
    Connect();
    $sql = "UPDATE clientcreditpoints 
            SET `ClientId` = '$ClientId', `Points` = '$Points', `ByCancel` = '$ByCancel', `DateTime` = '$DateTime', `JobId` = '$JobId' 
            WHERE Id='$Id'";
    mysql_query($sql);
    $ReturnRows = mysql_affected_rows();
    Disconnect();

    return $ReturnRows;
}

function ClientCreditPointsPagination() {
    Connect();
    $sql = "SELECT COUNT(Id) AS total 
            FROM clientcreditpoints";
    $exe = mysql_query($sql);
    $fetch = mysql_fetch_array($exe);
    $no = $fetch['total'];
    Disconnect();

    return $no;
}

/*
 * Author: Harik Vyas
 * Des: Set Limit Of Record Per Page According Number Of Table Records.
 * ChangeBy: Hardik Vyas
 * Changes: Put A Condition For Limitation.
 * changedOn: 31-08-2013,SaturDay
 */

function GetAnllClientCreditPoints($start = 0, $rpp = 500) {
    if ($rpp == 500) {
        $no = ClientCreditPointsPagination();
        $rpp = $no;
    }

    Connect();
    $sql = "SELECT `Id`, `ClientId`, `Points`, `ByCancel`, `DateTime`, `JobId` 
            FROM clientcreditpoints 
            LIMIT $start, $rpp";
    $exe = mysql_query($sql);

    Disconnect();
    return $exe;
}

function GetClientCreditPoint($Id) {
    Connect();
    $sql = "SELECT `ClientId`, `Points`, `ByCancel`, `DateTime`, `JobId` 
            FROM clientcreditpoints 
            WHERE Id = '$Id'";
    $exe = mysql_query($sql);
    Disconnect();

    return $exe;
}

?>
