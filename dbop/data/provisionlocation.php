<?php

if ($_SERVER['HTTP_HOST'] == "localhost" || $_SERVER['HTTP_HOST'] == "localhost:8080")
    include_once $_SERVER['DOCUMENT_ROOT'] . '/cabsystem/dbop/dbconnect.php';
else
    include_once $_SERVER['DOCUMENT_ROOT'] . '/dbop/dbconnect.php';

function AddProvisionLocation($SLocation, $SState, $SDistrict, $DLocation, $DState, $DDistrict, $Status) {
    Connect();
    $sql = "INSERT INTO
            provisionlocations(`SLocation`, `SState`, `SDistrict`, `DLocation`, `DState`, `DDistrict`, `Status`)
            VALUES ('$SLocation','$SState','$SDistrict','$DLocation','$DState','$DDistrict','$Status')";
    mysql_query($sql) or die('Query Error' . mysql_error());
    $ReturnId = mysql_insert_id();
    Disconnect();

    return $ReturnId;
}

function EditProvisionLocation($Id, $SLocation, $SState, $SDistrict, $DLocation, $DState, $DDistrict, $Status) {
    Connect();
    $sql = "UPDATE provisionlocations 
            SET SLocation='$SLocation',SState='$SState',SDistrict='$SDistrict',DLocation='$DLocation',DState='$DState',DDistrict='$DDistrict',Status='$Status' WHERE Id = '$Id'";
    mysql_query($sql);
    $ReturnRows = mysql_affected_rows();
    Disconnect();

    return $ReturnRows;
}

function DeleteProvisionLocation($Id) {
    Connect();
    $sql = "UPDATE provisionlocations 
            SET `Status` = 0 
            WHERE `Id` = '$Id' ";
    mysql_query($sql);
    $ReturnRows = mysql_affected_rows();
    Disconnect();

    return $ReturnRows;
}

function ProvisionLocationsPagination() {
    Connect();
    $sql = "SELECT COUNT(`Id`) AS total 
            FROM provisionlocations";
    $exe = mysql_query($sql);
    $fetch = mysql_fetch_array($exe);
    $no = $fetch['total'];
    Disconnect();

    return $no;
}

/*
 * Author: Harik Vyas
 * Des: Set Limit Of Record Per Page According Number Of Table Records.
 * ChangeBy: Hardik Vyas
 * Changes: Put A Condition For Limitation.
 * changedOn: 31-08-2013,SaturDay
 */

function GetAllProvisionLocations($start = 0, $rpp = 500) {
    if ($rpp == 500) {
        $no = ProvisionLocationsPagination();
        $rpp = $no;
    }

    Connect();
    $sql = "SELECT `Id`, `SLocation`, `SState`, `SDistrict`, `DLocation`, `DState`, `DDistrict`, `Status` 
            FROM provisionlocations 
            LIMIT $start,$rpp";
    $exe = mysql_query($sql);
    Disconnect();

    return $exe;
}

function GetProvisionLocation($Id) {
    Connect();
    $sql = "SELECT `Id`, `SLocation`, `SState`, `SDistrict`, `DLocation`, `DState`, `DDistrict`, `Status` 
            FROM provisionlocations 
            WHERE `Id` = '$Id'";
    $exe = mysql_query($sql);
    Disconnect();

    return $exe;
}

?>
