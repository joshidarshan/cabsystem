<?php

if ($_SERVER['HTTP_HOST'] == "localhost" || $_SERVER['HTTP_HOST'] == "localhost:8080")
    include_once $_SERVER['DOCUMENT_ROOT'] . '/cabsystem/dbop/dbconnect.php';
else
    include_once $_SERVER['DOCUMENT_ROOT'] . '/dbop/dbconnect.php';
/*
 * Author: Harik Vyas
 * ChangeBy: Hardik Vyas
 * Changes:Add A Field Named 'ISVia'.
 * changedOn: 10-09-2013,TuesDay
 */

function AddCabRoute($CabId, $FromDateTime, $Source, $Destination, $ToDateTime, $OriginalLocation, $IsReturnTrip, $AgentId, $DriverId, $IsVia, $CarModelId, $Status) {
    Connect();
    $sql = "INSERT INTO
            cabroutes(`CabId`, `FromDateTime`, `Source`, `Destination`, `ToDateTime`,`OriginalLocation`, `IsReturnTrip`, `AgentId`, `DriverId`, `IsVia`, `CarModelId`,`Status`)
            VALUES ('$CabId','$FromDateTime','$Source','$Destination','$ToDateTime','$OriginalLocation','$IsReturnTrip','$AgentId','$DriverId','$IsVia','$CarModelId','$Status')";
    mysql_query($sql);
    $ReturnId = mysql_insert_id();
    Disconnect();

    return $ReturnId;
}

function AddVirtualCabRoute($fromDate, $source, $destination, $toDate, $isReturnTrip, $carModelId, $status){
    Connect();
    $sql = "INSERT INTO 
            cabroutes(`FromDateTime`, `Source`, `Destination`, `ToDateTime`, `IsReturnTrip`, `CarModelId`, `Status`)
            VALUES('$fromDate', '$source', '$destination', '$toDate', '$isReturnTrip', '$carModelId', '$status')";
    mysql_query($sql);
    $returnId = mysql_insert_id();
    Disconnect();
    return $returnId;
}

function EditCabRoute($Id, $CabId, $FromDateTime, $Source, $Destination, $ToDateTime, $OriginalLocation, $IsReturnTrip, $AgentId, $DriverId, $IsVia, $CarModelId, $Status) {
    Connect();
    $sql = "UPDATE cabroutes 
            SET CabId='$CabId',FromDateTime='$FromDateTime',Source='$Source',Destination='$Destination',ToDateTime='$ToDateTime',`OriginalLocation`= '$OriginalLocation',IsReturnTrip='$IsReturnTrip',AgentId='$AgentId',DriverId='$DriverId',`IsVia` = '$IsVia', `CarModelId`='$CarModelId', Status = '$Status' 
            WHERE Id = '$Id'";
    mysql_query($sql) or die('Error' . mysql_error());
    $ReturnRows = mysql_affected_rows();
    Disconnect();

    return $ReturnRows;
}

function EditCabRouteCornJob($Id, $toDate, $status){
    Connect();
    if($status > 0){
        $sql = "UPDATE cabroutes
                SET `ToDateTime` = '$toDate', `Destination` = 0
                WHERE `Id` = '$Id'";
    }
    else{
        $sql = "UPDATE cabroutes
                SET `Status` = 0
                WHERE `Id` = '$Id'";
    }
    $query = mysql_query($sql);
    $affectedRows= mysql_affected_rows();
    Disconnect();
    return $affectedRows;
}

function DeleteCabRoute($Id) {
    Connect();
    $sql = "UPDATE cabroutes 
            SET Status = 0 
            WHERE Id= '$Id' ";
    mysql_query($sql);
    $ReturnRows = mysql_affected_rows();
    Disconnect();

    return $ReturnRows;
}

function CabRoutesPagination($val1,$val2) {
    Connect();
    $sql = "SELECT COUNT(Id) AS total 
            FROM cabroutes WHERE (`FromDateTime` BETWEEN '$val1' AND '$val2') AND (`ToDateTime` BETWEEN '$val1' and '$val2')";
    $exe = mysql_query($sql);
    $fetch = mysql_fetch_array($exe);
    $no = $fetch['total'];
    Disconnect();

    return $no;
}

/*
 * Author: Harik Vyas
 * Des: Set Limit Of Record Per Page According Number Of Table Records.
 * ChangeBy: Hardik Vyas
 * Changes: Put A Condition For Limitation.
 * changedOn: 31-08-2013,SaturDay
 */

function GetAllCabRoutes($val1, $val2,$start = 0, $rpp = 500) {
    if ($rpp == 500) {
        $no = CabRoutesPagination($val1,$val2);
        $rpp = $no;
    }

    Connect();
    $sql = "SELECT cr.`Id`, cr.`CabId`, cr.`FromDateTime`, cr.`Source`, cr.`Destination`, cr.`ToDateTime`, cr.`OriginalLocation`,`IsReturnTrip`, cr.`AgentId`, cr.`DriverId`,cr.`IsVia`, cr.`Status`, a.Name AS ANam ,l.Name AS LName, dl.`Name` as DsName, c.NoPlate AS CName, d.Name AS DName,cm.Name AS ModelName
            FROM cabroutes cr 
            JOIN agents a
                ON cr.AgentId = a.Id
            JOIN drivers d
                ON cr.DriverId = d.Id
            JOIN locations l
                ON cr.Source = l.Id 
            JOIN locations  dl
                ON cr.`Destination` = dl.`Id`
            JOIN cabs c
                ON cr.CabId = c.Id
                JOIN carmodels cm
                ON cr.CarModelId = cm.Id
               WHERE (`FromDateTime` BETWEEN '$val1' AND '$val2') AND (`ToDateTime` BETWEEN '$val1' and '$val2')
            LIMIT $start,$rpp ";
    $query = mysql_query($sql) or die('Error'. mysql_error());
    Disconnect();

    return $query;
}


function CabRoutePoint($Id) {
    Connect();
    $sql = "SELECT cr.`Id`, cr.`CabId`, cr.`FromDateTime`, cr.`Source`, cr.`Destination`, cr.`ToDateTime`,cr.`OriginalLocation`, cr.`IsReturnTrip`, cr.`AgentId`, cr.`DriverId`, cr.`IsVia`,cr.CarModelId,cr.`Status`,cb.NoPlate AS NoPlate, lacS.Name AS Source, lacD.Name AS Destination 
            FROM cabroutes cr
            JOIN cabs cb 
            ON cr.CabId = cb.Id
            JOIN locations lacS
            ON lacS.Id = cr.Source
            JOIN locations lacD
            ON lacD.Id = cr.Destination
            WHERE cr.Id = '$Id'";
    $exe = mysql_query($sql) or die('Error' . mysql_error());
    Disconnect();

    return $exe;
}

/**
 * AddCharterCabToRoute
 * Adds a charter cab to specific route. The route value will be of booking value.
 * @param int $cabRouteId cabRouteId to reconize the cab.
 * @param datetime $toDateTime toDateTime will be the one user have provided.
 * @param string $toLocation will be the one user have provided.
 */
function AddCharterCabToRoute($cabRouteId, $toDateTime, $toLocation){
    Connect();
    $sql = "UPDATE cabroutes
            SET `ToDateTime` = '$toDateTime', `Destination` = '$toLocation'
            WHERE `Id` = '$cabRouteId'";
    mysql_query($sql);
    $affectedRows = mysql_affected_rows();
    Disconnect();
    return $affectedRows;
}

/**
 * AddActualCab against the virtual cab
 * Adds a physical cab to registered virtual cab. The route value will be of booking value.
 * @param int $Id This will be cabRouteId to reconize the cab.
 * @param int $cabId cabId will be the one admin have provided.
 * @param int $agentId will be the one admin have provided.
 * @param int $driverId will be the one admin have provided.
 */
function AddPhysicalCabToVirtual($Id, $cabId, $agentId, $driverId){
    Connect();
    $sql = "UPDATE cabroutes
            SET `CabId` = '$cabId', `AgentId` = '$agentId', `DriverId` = '$driverId'
            WHERE `Id` = '$Id'";
    mysql_query($sql);
    $affectedRows = mysql_affected_rows();
    Disconnect();
    return $affectedRows;
}
function CabRoutesPaginationByAgent($val1,$val2,$AgentId) {
    Connect();
    $sql = "SELECT COUNT(Id) AS total 
            FROM cabroutes WHERE (`FromDateTime` BETWEEN '$val1' AND '$val2') AND (`ToDateTime` BETWEEN '$val1' and '$val2') AND `AgentId` = '$AgentId'";
    $exe = mysql_query($sql);
    $fetch = mysql_fetch_array($exe);
    $no = $fetch['total'];
    Disconnect();

    return $no;
}
function GetAllCabRoutesByAgentId($AgentId) {
  

    Connect();
     
    $sql = "SELECT cr.Id, cb.NoPlate, cr.FromDateTime, cr.ToDateTime, sl.Name as Source, dl.Name as Destination, dr.Name as Driver, bk.Status as BookingStatus, cr.Status as CabRouteStatus
            FROM cabroutes cr
            JOIN cabs cb
                ON cr.cabId = cb.Id
            JOIN agents ag
                ON cr.AgentId = ag.Id
            JOIN drivers dr
                ON dr.AgentId = ag.Id
            JOIN bookings bk
                ON cr.Id = bk.CabRouteId
            JOIN locations sl
                ON cr.Source = sl.Id
            JOIN locations dl
                ON cr.Destination = dl.Id
            WHERE ag.Id = '$AgentId'";
            
    $query = mysql_query($sql) or die('Error'. mysql_error());
    //echo $sql;
    Disconnect();

    return $query;
}

function damiGetAllCabRoutesByAgentId($val1, $val2,$start, $rpp,$AgentId) {
  

    Connect();
    $sql = "SELECT cr.`Id`, cr.`CabId`, cr.`FromDateTime`, cr.`Source`, cr.`Destination`, cr.`ToDateTime`, cr.`OriginalLocation`,`IsReturnTrip`, cr.`AgentId`, cr.`DriverId`,cr.`IsVia`, cr.`Status`, a.Name AS ANam ,l.Name AS LName, dl.`Name` as DsName, c.NoPlate AS CName, d.Name AS DName,cm.Name AS ModelName,book.Status AS booked
            FROM cabroutes cr 
            JOIN agents a
                ON cr.AgentId = a.Id
            JOIN drivers d
                ON cr.DriverId = d.Id
            JOIN locations l
                ON cr.Source = l.Id 
            JOIN locations  dl
                ON cr.`Destination` = dl.`Id`
            JOIN cabs c
                ON cr.CabId = c.Id
                JOIN carmodels cm
                ON cr.CarModelId = cm.Id
           JOIN bookings book
                ON cr.`Id` = book.`CabRouteId`
         WHERE (`FromDateTime` BETWEEN '$val1' AND '$val2') AND (`ToDateTime` BETWEEN '$val1' and '$val2') AND cb.`AgentId` = '$AgentId'
            LIMIT $start,$rpp ";
    $query = mysql_query($sql) or die('Error'. mysql_error());
    //echo $sql;
    Disconnect();

    return $query;
}
function GetAllPandingCabRoutes($val1, $val2,$start = 0, $rpp = 500) {
    if ($rpp == 500) {
        $no = CabRoutesPagination($val1,$val2);
        $rpp = $no;
    }

    Connect();
    $sql = "SELECT cr.`Id`, cr.`CabId` AS CabId, cr.`FromDateTime`, cr.`Source`, cr.`Destination`, cr.`ToDateTime`, cr.`OriginalLocation`,`IsReturnTrip`, cr.`AgentId`, cr.`DriverId`,cr.`IsVia`, cr.`Status`, a.Name AS ANam ,l.Name AS LName, dl.`Name` as DsName, c.NoPlate AS CName, d.Name AS DName,cm.Name AS ModelName
            FROM cabroutes cr 
            JOIN agents a
                ON cr.AgentId = a.Id
            JOIN drivers d
                ON cr.DriverId = d.Id
            JOIN locations l
                ON cr.Source = l.Id 
            JOIN locations  dl
                ON cr.`Destination` = dl.`Id`
            JOIN cabs c
                ON cr.CabId = c.Id
                JOIN carmodels cm
                ON cr.CarModelId = cm.Id
               WHERE (`FromDateTime` BETWEEN '$val1' AND '$val2') AND (`ToDateTime` BETWEEN '$val1' and '$val2')AND cr.`Status` = 2
            LIMIT $start,$rpp ";
    $query = mysql_query($sql) or die('Error'. mysql_error());
//    echo $sql;
    Disconnect();

    return $query;
}
function CabRouteConform($Id){
    Connect();
        $sql ="UPDATE cabroutes SET `Status` = 1 WHERE `Id` = '$Id'";
        $exe = mysql_query($sql);
        $Rows = mysql_affected_rows();
    Disconnect();
    return $Rows;
}
?>
