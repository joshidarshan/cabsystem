<?php

if ($_SERVER['HTTP_HOST'] == "localhost" || $_SERVER['HTTP_HOST'] == "localhost:8080")
    include_once $_SERVER['DOCUMENT_ROOT'] . '/cabsystem/dbop/dbconnect.php';
else
    include_once $_SERVER['DOCUMENT_ROOT'] . '/dbop/dbconnect.php';

function CarTypeAdd($CarName, $CarStatus) {

    Connect();
    $sql = "INSERT  INTO cartypes(`Name`,`Status`)
            values('$CarName','$CarStatus')";
    mysql_query($sql);
    $returnId = mysql_insert_id();
    Disconnect();

    return $returnId;
}

function CarTypeEdit($Id, $CarName, $CarStatus) {

    Connect();
    $sql = "UPDATE cartypes 
            SET `Name` = '$CarName', `Status` = '$CarStatus' 
            WHERE `Id` = '$Id'";
    mysql_query($sql);
    $returnRows = mysql_affected_rows();
    Disconnect();

    return $returnRows;
}

function CarTypesPagination() {
    Connect();
    $sql = "SELECT COUNT(`Id`) AS total 
            FROM cartypes";
    $select = mysql_query($sql);
    $num = mysql_fetch_array($select);
    $no = $num['total'];
    Disconnect();

    return $no;
}

/*
 * Author: Harik Vyas
 * Des: Set Limit Of Record Per Page According Number Of Table Records.
 * ChangeBy: Hardik Vyas
 * Changes: Put A Condition For Limitation.
 * changedOn: 31-08-2013,SaturDay
 */
function GetCarTypesListing($CarStart = 0, $CarRpp = 500) {
    if ($CarRpp == 500) {
        $no = CarTypesPagination();
        $CarRpp = $no;
    }

    Connect();
    $sql = "SELECT `Id`, `Name`, `Status`
            FROM cartypes 
            LIMIT $CarStart,$CarRpp";
    $exe = mysql_query($sql);
    Disconnect();
    return $exe;
}

function GetCarTypesSelect($Id) {
    Connect();
    $sql = "SELECT `Name`, `Status`
            FROM cartypes 
            WHERE `Id` = '$Id'";
    $exe = mysql_query($sql);
    Disconnect();
    return $exe;
}

function DeleteCarType($Id) {
    Connect();
    $sql = "UPDATE cartypes 
            SET `Status` = '0' 
            WHERE `Id` = '$Id'";
    $exe = mysql_query($sql);
    $returnId = mysql_affected_rows();
    Disconnect();
    return $returnId;
}
/*
 * Author: Harik Vyas
 * Des: Made for  Check Availablity of Type with Ajax.
 * ChangeBy: Hardik Vyas
 * Changes: Return Number Of rows With Select Query.
 * changedOn: 4-09-2013,WednesDay
 */
function CheckCarType($CarType){
    Connect();
                $sql = "select 
                        Id from 
                        cartypes where Name = '$CarType'";
                $exe = mysql_query($sql);
                $rows = mysql_num_rows($exe);
   Disconnect();
   return $rows;
}

?>