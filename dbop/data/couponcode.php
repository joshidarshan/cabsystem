<?php

if ($_SERVER['HTTP_HOST'] == "localhost" || $_SERVER['HTTP_HOST'] == "localhost:8080")
    include_once $_SERVER['DOCUMENT_ROOT'] . '/cabsystem/dbop/dbconnect.php';
else
    include_once $_SERVER['DOCUMENT_ROOT'] . '/dbop/dbconnect.php';

function AddCouponcode($Name, $ValidFrom, $ValidTo, $LimitCount, $DiscountAmount, $DiscountPercent, $Status) {
    Connect();
    $sql = "INSERT INTO
            couponcodes(`Name`, `ValidFrom`, `ValidTo`, `LimitCount`, `DiscountAmount`, `DiscountPercent`, `Status`)
            VALUES ('$Name','$ValidFrom','$ValidTo','$LimitCount','$DiscountAmount','$DiscountPercent','$Status')";
    mysql_query($sql);
    $ReturnId = mysql_insert_id();
    Disconnect();

    return $ReturnId;
}

function EditCouponcode($Id, $Name, $ValidFrom, $ValidTo, $LimitCount, $DiscountAmount, $DiscountPercent, $Status) {
    Connect();
    $sql = "UPDATE couponcodes 
            SET `Name` = '$Name', `ValidFrom` = '$ValidFrom', `ValidTo` = '$ValidTo', `LimitCount` = '$LimitCount', `DiscountAmount` = '$DiscountAmount', `DiscountPercent` = '$DiscountPercent', `Status` = '$Status' 
            WHERE Id='$Id'";
    mysql_query($sql);
    $ReturnRows = mysql_affected_rows();
    Disconnect();

    return $ReturnRows;
}

function DeleteCouponcode($Id) {
    Connect();
    $sql = "UPDATE couponcodes 
            SET `Status` = 0 
            WHERE `Id` = '$Id' ";
    mysql_query($sql);
    $ReturnRows = mysql_affected_rows();
    Disconnect();

    return $ReturnRows;
}

function CouponcodePagination() {
    Connect();
    $sql = "SELECT COUNT(Id) AS total 
            FROM couponcodes";
    $exe = mysql_query($sql);
    $fetch = mysql_fetch_array($exe);
    $no = $fetch['total'];
    Disconnect();

    return $no;
}

/*
 * Author: Harik Vyas
 * Des: Set Limit Of Record Per Page According Number Of Table Records.
 * ChangeBy: Hardik Vyas
 * Changes: Put A Condition For Limitation.
 * changedOn: 31-08-2013,SaturDay
 */

function GetAllCouponcode($start = 0, $rpp = 500) {
    if ($rpp == 500) {
        $no = CouponcodePagination();
        $rpp = $no;
    }
    Connect();
    $sql = "SELECT `Id`, `Name`, `ValidFrom`, `ValidTo`, `LimitCount`, `DiscountAmount`, `DiscountPercent`, `Status` 
            FROM couponcodes 
            LIMIT $start, $rpp";
    $exe = mysql_query($sql);
    Disconnect();

    return $exe;
}

function GetCouponcode($Id) {
    Connect();
    $sql = "SELECT `Name`, `ValidFrom`, `ValidTo`, `LimitCount`, `DiscountAmount`, `DiscountPercent`, `Status` 
            FROM couponcodes 
            WHERE Id = '$Id'";
    $exe = mysql_query($sql);
    Disconnect();

    return $exe;
}

?>
