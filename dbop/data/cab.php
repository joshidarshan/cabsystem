<?php

if ($_SERVER['HTTP_HOST'] == "localhost" || $_SERVER['HTTP_HOST'] == "localhost:8080")
    include_once $_SERVER['DOCUMENT_ROOT'] . '/cabsystem/dbop/dbconnect.php';
else
    include_once $_SERVER['DOCUMENT_ROOT'] . '/dbop/dbconnect.php';

function InsertCab($CarModelId, $NoPlate, $Rating, $MadeYear, $AgentId, $KmRate, $AcKmRate, $DocLink, $Status) {
    Connect();
    $sql = "INSERT INTO cabs(`CarModelId`, `NoPlate`, `Rating`, `MadeYear`, `AgentId`, `KmRate`,`AcKmRate`, `DocLink`, `Status`)
            VALUES('$CarModelId', '$NoPlate', '$Rating', '$MadeYear', '$AgentId', '$KmRate','$AcKmRate', '$DocLink', '$Status')";
    mysql_query($sql);
    $insertedId = mysql_insert_id();
    Disconnect();

    return $insertedId;
}

function UpdateCabs($Id, $CarModelId, $NumberPlate, $Rating, $MadeYear, $AgentId, $KmRate, $AcKmRate, $DocumentLink, $Status) {
    Connect();
    $sql = "UPDATE cabs 
            SET `CarModelId` = '$CarModelId', `NoPlate` = '$NumberPlate', `Rating` = '$Rating', `MadeYear` = '$MadeYear', 
                `AgentId` = '$AgentId', `KmRate` = '$KmRate',`AcKmRate`='$AcKmRate', 
                `DocLink` = '$DocumentLink', `Status` = '$Status'
            WHERE Id='$Id'";
    mysql_query($sql);
    $ReturnRows = mysql_affected_rows();
    Disconnect();

    return $ReturnRows;
}

function CabPagination() {
    Connect();
    $sql = "SELECT COUNT(Id) AS total FROM cabs";
    $exe = mysql_query($sql);
    $fetch = mysql_fetch_array($exe);
    $no = $fetch['total'];
    Disconnect();
    return $no;
}

/*
 * Author: Harik Vyas
 * Des: Set Limit Of Record Per Page According Number Of Table Records.
 * ChangeBy: Hardik Vyas
 * Changes: Put A Condition For Limitation.
 * changedOn: 31-08-2013,SaturDay
 */
function GetAllCab($start = 0, $rpp = 500) {
    if ($rpp == 500) {
        $no = CabPagination();
        $rpp = $no;
    }


    Connect();
    $sql = "SELECT cb.`Id`, cb.`CarModelId`, cb.`NoPlate`, cb.`Rating`, cb.`MadeYear`, cb.`AgentId`, cb.`KmRate`, cb.`AcKmRate`,cb.`DocLink`, cm.`Name` as Model,cb.`Status`, ag.Name AS Agent
            FROM cabs cb
            JOIN carmodels cm
                ON cb.`CarModelId` = cm.`Id`
            JOIN agents ag
                ON cb.`AgentId` = ag.`Id`
            LIMIT $start,$rpp";
    $query = mysql_query($sql);
    Disconnect();

    return $query;
}

function GetCabById($Id) {
    Connect();
    $sql = "SELECT `CarModelId`, `NoPlate`, `Rating`, `MadeYear`, `AgentId`, `KmRate`,`AcKmRate`, `DocLink`, `Status`
            FROM cabs WHERE Id = '$Id'";
    $query = mysql_query($sql);
    Disconnect();

    return $query;
}

function DeleteCab($Id) {
    Connect();
    $sql = "UPDATE cabs SET Status = 0 WHERE Id= '$Id' ";
    $exe = mysql_query($sql);
    $ReturnRows = mysql_affected_rows();
    Disconnect();

    return $ReturnRows;
}

function GetCabByNoPlate($No){
    Connect();
    $sql = "SELECT `Id`, `CarModelId`, `NoPlate`, `Rating`, `MadeYear`, `AgentId`, `KmRate`, `AcKmRate`, `Status`
            FROM cabs
            WHERE `NoPlate` = '$No'";
    $query = mysql_query($sql);
    Disconnect();
    return $query;
}

function GetCabRateByCabRouteId($cabRouteId, &$acKmRate, &$kmRate){
    Connect();
    $sql = "SELECT ca.`KmRate`, ca.`AcKmRate`
            FROM cabs ca
            JOIN cabroutes cr
                ON ca.`CarModelId` = cr.`CarModelId` AND cr.`Id` = '$cabRouteId'";
    $query = mysql_query($sql);
    Disconnect();
    $data = mysql_fetch_assoc($query);
    $acKmRate = $data['AcKmRate'];
    $kmRate = $data['KmRate'];
}

function GetVirtualCabRateByModelId($modelId, &$acRate, &$nonAcRate){
    Connect();
    $sql = "SELECT `DefaultKmRate`, `DefaultAcKmRate`
            FROM carmodels
            where `Id` = '$modelId'";
    $query = mysql_query($sql);
    Disconnect();
    $data = mysql_fetch_assoc($query);
    $acRate = $data['DefaultAcKmRate'];
    $nonAcRate = $data['DefaultKmRate'];
}

function CheckCabByNoPlate($NoPlate){
    Connect();
    $sql = "SELECT `Id`
            FROM cabs
            WHERE `NoPlate` = '$NoPlate'";
    $query = mysql_query($sql) or die('Error'.  mysql_error());
    $rows = mysql_num_rows($query);
    Disconnect();
    return $rows;
}

function CheckCabByNoPlateAgentId($NoPlate,$AgentId){
    Connect();
    $sql = "SELECT `Id`
            FROM cabs
            WHERE `NoPlate` = '$NoPlate' AND `AgentId` = '$AgentId'";
    $query = mysql_query($sql) or die('Error'.  mysql_error());
    $rows = mysql_num_rows($query);
    Disconnect();
    return $rows;
}
function CabPaginationByAgentId($AgentId) {
    Connect();
    $sql = "SELECT COUNT(Id) AS total FROM cabs WHERE `AgentId` = '$AgentId'";
    $exe = mysql_query($sql);
    $fetch = mysql_fetch_array($exe);
    $no = $fetch['total'];
    Disconnect();
    return $no;
}
function GetAllCabByAgentId($AgentId,$start,$rpp) {
    Connect();
  
    $sql = "SELECT cb.`Id`, cb.`CarModelId`, cb.`NoPlate`, cb.`Rating`, cb.`MadeYear`, cb.`AgentId`, cb.`KmRate`, cb.`AcKmRate`,cb.`DocLink`, cm.`Name` as Model,cb.`Status`, ag.Name AS Agent
            FROM cabs cb
            JOIN carmodels cm
                ON cb.`CarModelId` = cm.`Id`
            JOIN agents ag
                ON cb.`AgentId` = ag.`Id` 
                WHERE cb.`AgentId` = '$AgentId' LIMIT $start,$rpp";
            
    $query = mysql_query($sql);
//    echo $sql;
    Disconnect();

    return $query;
}

function GetCabNoByAgentId($AgentId) {
    Connect();
    $sql = "SELECT cb.`Id`, cb.`CarModelId`, cb.`NoPlate`, cb.`Rating`, cb.`MadeYear`, cb.`AgentId`, cb.`KmRate`, cb.`AcKmRate`,cb.`DocLink`, cm.`Name` as Model,cb.`Status`, ag.Name AS Agent
            FROM cabs cb
            JOIN carmodels cm
                ON cb.`CarModelId` = cm.`Id`
            JOIN agents ag
                ON cb.`AgentId` = ag.`Id` 
                WHERE cb.`AgentId` = '$AgentId'";
            
    $query = mysql_query($sql);
//    echo $sql;
    Disconnect();

    return $query;
}
?>
