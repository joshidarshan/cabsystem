<?php

if ($_SERVER['HTTP_HOST'] == "localhost" || $_SERVER['HTTP_HOST'] == "localhost:8080")
    include_once $_SERVER['DOCUMENT_ROOT'] . '/cabsystem/dbop/dbconnect.php';
else
    include_once $_SERVER['DOCUMENT_ROOT'] . '/dbop/dbconnect.php';

function AddCreditKm($BookingId, $km, $IsRedeem, $DateTime, $JobId) {
    Connect();
    $sql = "INSERT INTO
            creditkms(`BookingId`, km, `IsRedeem`, `DateTime`, `JobId`)
            VALUES ('$BookingId','$km','$IsRedeem','$DateTime','$JobId')";
    mysql_query($sql);
    $ReturnId = mysql_insert_id();
    Disconnect();

    return $ReturnId;
}

function EditCreditKm($Id, $BookingId, $km, $IsRedeem, $DateTime, $JobId) {
    Connect();
    $sql = "UPDATE creditkms 
            SET `BookingId`='$BookingId',km='$km',`IsRedeem`='$IsRedeem',`DateTime`='$DateTime',`JobId`='$JobId' 
            WHERE `Id` = '$Id'";
    mysql_query($sql);
    $ReturnRows = mysql_affected_rows();
    Disconnect();

    return $ReturnRows;
}

function CreditKmsPagination() {
    Connect();
    $sql = "SELECT COUNT(Id) AS total 
            FROM creditkms";
    $exe = mysql_query($sql);
    $fetch = mysql_fetch_array($exe);
    $no = $fetch['total'];
    Disconnect();

    return $no;
}

/*
 * Author: Harik Vyas
 * Des: Set Limit Of Record Per Page According Number Of Table Records.
 * ChangeBy: Hardik Vyas
 * Changes: Put A Condition For Limitation.
 * changedOn: 31-08-2013,SaturDay
 */

function GetAllCreditKms($start = 0, $rpp = 500) {
    if ($rpp == 500) {
        $no = CreditKmsPagination();
        $rpp = $no;
    }
    Connect();
    $sql = "SELECT `Id`, `BookingId`, km, `IsRedeem`, `DateTime`, `JobId` 
            FROM creditkms 
            LIMIT $start,$rpp";
    $exe = mysql_query($sql);
    Disconnect();

    return $exe;
}

function GetCreditKm($Id) {
    Connect();
    $sql = "SELECT `Id`, `BookingId`, km, `IsRedeem`, `DateTime`, `JobId` 
            FROM creditkms 
            WHERE `Id` = '$Id'";
    $exe = mysql_query($sql);
    Disconnect();

    return $exe;
}

?>
