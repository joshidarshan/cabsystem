<?php

if ($_SERVER['HTTP_HOST'] == "localhost" || $_SERVER['HTTP_HOST'] == "localhost:8080")
    include_once $_SERVER['DOCUMENT_ROOT'] . '/cabsystem/dbop/dbconnect.php';
else
    include_once $_SERVER['DOCUMENT_ROOT'] . '/dbop/dbconnect.php';

function AddDriver($adAgentId, $adName, $adPhones, $adNotes, $docLink, $adStatus) {
    Connect();
    $sql = "INSERT INTO 
            drivers(`AgentId`, `Name`, `Phones`, `Note`, `DocumentLink`, `Status`) 
            VALUES ('$adAgentId', '$adName', '$adPhones', '$adNotes', '$docLink', '$adStatus')";
    mysql_query($sql);
    $returnId = mysql_insert_id();
    Disconnect();

    return $returnId;
}

/*
 * Author: Harik Vyas
 * Des: Set Limit Of Record Per Page According Number Of Table Records.
 * ChangeBy: Hardik Vyas
 * Changes: Put A Condition For Limitation.
 * changedOn: 31-08-2013,SaturDay
 */

function GetAgentsListing($Id, $driverStart = 0, $driverRpp = 500) {
    if ($driverRpp == 500) {
        $no = PaginationDriver($Id);
        $driverRpp = $no;
    }
    Connect();
    $sql = "select dr.`Id`, dr.`AgentId`, dr.`Name`, dr.`Phones`, dr.`Note`,dr.`DocumentLink`, dr.`Status`, ag.Name AS AgName 
            FROM drivers dr
            JOIN agents ag
            ON dr.AgentId = ag.Id
            WHERE `AgentId` = '$Id' 
            LIMIT $driverStart,$driverRpp";
    $fire = mysql_query($sql);
    Disconnect();

    return $fire;
}

function PaginationDriver($Id) {
    Connect();
    $sql = "SELECT COUNT(`Id`) AS total 
            FROM drivers 
            WHERE AgentId = '$Id'";
    $select = mysql_query($sql);
    $num = mysql_fetch_array($select);
    $no = $num['total'];

    Disconnect();
    return $no;
}

function GetDriver($Id) {
    Connect();
    $sql = "SELECT `Id`, `AgentId`, `Name`, `Phones`, `Note`, `DocumentLink`,`Status` 
            FROM Drivers 
            WHERE `Id` = '$Id'";
    $fire = mysql_query($sql);
    Disconnect();

    return $fire;
}

function UpdateDriver($Id, $Name, $Phones, $Notes, $DocumentLink, $Status) {
    Connect();
    $Update = "UPDATE drivers 
                SET `Name` = '$Name', Phones = '$Phones', `Note` = '$Notes', `DocumentLink` = '$DocumentLink', `Status` = '$Status' WHERE Id = '$Id'";
    $fire = mysql_query($Update) or die('Query Error' . mysql_error());
    $rows = mysql_affected_rows();
    Disconnect();

    return $rows;
}

function DeleteDriver($Id) {
    Connect();
    $Delete = "UPDATE drivers 
                SET `Status` = 0 
                WHERE Id = '$Id'";
    $fire = mysql_query($Delete);
    $rows = mysql_affected_rows();
    Disconnect();

    return $rows;
}

function GetDriverFromAgentId($agentId) {
    Connect();
    $sql = "SELECT `Id`, `Name`, `Phones`
            FROM drivers
            WHERE `AgentId` = '$agentId'";
    $query = mysql_query($sql);
    Disconnect();
    return $query;
}

function GetOneWayDriver() {
    Connect();
    $sql = "select 
                        Id from 
                        drivers where Name = 'OneWay'";
    $exe = mysql_query($sql);
    Disconnect();
    return $exe;
}

function AddOneWayDriver($AgentId) {
    Connect();
    $sql = "INSERT INTO drivers (`AgentId`,`Name`) VALUES('$AgentId','OneWay')";
    $exe = mysql_query($sql);
    return mysql_insert_id();
    Disconnect();
}

?>
