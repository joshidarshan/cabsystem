<?php

// Define path for the files from the root. Sepereate based on host.
$path = "";
if($_SERVER['HTTP_HOST'] == "localhost" || $_SERVER['HTTP_HOST'] == "localhost:8080")
    $path = $_SERVER['DOCUMENT_ROOT'].'/cabsystem/dbop/data';
else
    $path = $_SERVER['DOCUMENT_ROOT'].'/dbop/data';

// Database file inclusion.
// Includes: couponcode, discountscheme, couponused, discountused, promotion
include_once $path.'/couponcode.php';
include_once $path.'/discountscheme.php';
include_once $path.'/couponused.php';
include_once $path.'/discountused.php';
include_once $path.'/promotion.php';
?>
