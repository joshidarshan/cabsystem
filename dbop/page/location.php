<?php

// Define path for the files from the root. Sepereate based on host.
$path = "";
if($_SERVER['HTTP_HOST'] == "localhost" || $_SERVER['HTTP_HOST'] == "localhost:8080")
    $path = $_SERVER['DOCUMENT_ROOT'].'/cabsystem/dbop/data';
else
    $path = $_SERVER['DOCUMENT_ROOT'].'/dbop/data';

// Database file inclusion.
// Includes: location, breakpoint, provisionlocation
include_once $path.'/location.php';
include_once $path.'/breakpoint.php';
include_once $path.'/provisionlocation.php';


?>
