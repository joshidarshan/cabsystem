<?php

// Define path for the files from the root. Sepereate based on host.
$path = "";
if($_SERVER['HTTP_HOST'] == "localhost" || $_SERVER['HTTP_HOST'] == "localhost:8080")
    $path = $_SERVER['DOCUMENT_ROOT'].'/cabsystem/dbop/data';
else
    $path = $_SERVER['DOCUMENT_ROOT'].'/dbop/data';

// Database file inclusion.
// Includes: carmake, carmodel, cartype
include_once $path.'/carmake.php';
include_once $path.'/carmodel.php';
include_once $path.'/cartype.php';
?>
