<?php require_once './templates/header.php'; ?>
<script type="text/javascript" src="js/jquery.cycle.js"></script>
<div class="container">
    <div class="marketing">
        <div id="tabs" class="hs_tabs span9" style="float: left; height: 100px; width: 200px;">
            <div class="span2"><a href="#chartred_cab">Chartred Cab</a></div>
        </div>
        <div id="banner0" class="banner" style="float: left; max-width: 600px;">
            <div style="width: 100%;">
            <h1>
                <img src="Upload/SliderImages/525e7dfd42fd6-Desert.jpg" class="img-responsive">
                <p>1470Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident.</p>
            </h1>
            </div>
        </div>
        <div id="tabs" class="hs_tabs span9" style="float: left; height: 100px; width: 200px;">
            <div class="span2"><a href="#shared_cab">Shared Cab</a></div>
        </div>
        <script type="text/javascript"><!--
        $(document).ready(function() {
                $('#banner0 div:first-child').css('display', 'block');
            });

            var banner = function() {
                $('#banner0').cycle({
                    before: function(current, next) {
                        $(next).parent().height($(next).outerHeight());
                    }
                });
            }

            //setTimeout(banner, 2000);
//--></script>
        <br>
        <div id="tabs" class="hs_tabs span9">
<!--            <div class="span2"><a href="#oneway_cab">Oneway Cab</a></div>
            <div class="span2"><a href="#bid_a_cab">Bid a Cab</a></div>-->
        </div>
        <div class="row-fluid">
            <div class="span4" id="chartred_cab">
                <hr>
                <h1>Chartred Cab content goes here.</h1>
            </div>
            <div class="span4" id="shared_cab">
                <hr>
                <h1>Shared Cab content goes here.</h1>
            </div>
<!--            <div class="span4" id="oneway_cab">
                <hr>
                <h1>Oneway Cab content goes here.</h1>
            </div>
            <div class="span4" id="bid_a_cab">
                <hr>
                <h1>Bid a content goes here.</h1>
            </div>-->
        </div>
        <script type="text/javascript"><!--
                    $('#tabs a').tabs();
//--></script>
    </div>
</div>
<?php require_once 'includes/footer.php'; ?>