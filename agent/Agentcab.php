<?php
if (!isset($_SESSION))
    session_start();
$_SESSION['cat'] = "Route";

function AgentPath() {
    $path = $_SERVER['DOCUMENT_ROOT'];
    // Differenciate the path for local and web host. Includes database connect file.
    if ($_SERVER['HTTP_HOST'] == "localhost" || $_SERVER['HTTP_HOST'] == "localhost:8080")
        $path .= '/Cabsystem';
    return $path;
}

include_once AgentPath() . '/dbop/data/location.php';
include_once AgentPath() . '/dbop/data/cab.php';
include_once AgentPath() . '/templates/agentheader.php';

$page = 1;
$start = 0;
$rpp = 10;
$AgentId = $_SESSION['AgentId'];
$no = CabPaginationByAgentId($AgentId);
$totalpage = ceil($no / $rpp);

if (isset($_GET['action'])) {
    if ($_GET['action'] == "delete") {

        $Id = $_GET['Id'];
        DeleteCab($Id);
        echo "<script> alert('Your Record Delete SuccessFully');</script>";
    }
}
if (isset($_GET['page'])) {
    $page = $_REQUEST['page'];
    $a = $page * $rpp;
    $start = $a - $rpp;
}
$GetAllCab = GetAllCabByAgentId($AgentId,$start,$rpp);
?>
<script language="javascript">
    function del() {
        var r = confirm("Really Want to Delete A Break Point");
        if (!r)
            return false;
    }
</script>
<form action="Agentcab.php" method="post">
    <h1 align="center">Car Listing</h1>
    <table align='center' class="table table-striped">
        <tr>
            <th>Action</th>
            <th>Car Model</th>
            <th>No-Plate</th>
            <th>Rating</th>
            <th>Made Year</th>
            <th>Km Rate</th>
            <th>Ac Km Rate</th>
            <th>Document Link</th>
            <th>Status</th>
        </tr>
        <?php
        while ($FetchGetAllCab = mysql_fetch_array($GetAllCab)) {
            $Doc = substr($FetchGetAllCab['DocLink'], (stripos($FetchGetAllCab['DocLink'], '-') + 1), strlen($FetchGetAllCab['DocLink']));
            ?>
            <tr>
                <td>
                    <a href="AgentcabEdit.php?Id=<?php echo $FetchGetAllCab['Id']; ?>">Edit</a> &nbsp; | &nbsp;
                    <a href="Agentcab.php?action=delete&Id=<?php echo $FetchGetAllCab['Id']; ?>" onClick="return del();">Delete</a>
                </td>
                <td><?php echo $FetchGetAllCab['Model']; ?></td>
                <td><?php echo $FetchGetAllCab['NoPlate']; ?></td>
                <td><?php echo $FetchGetAllCab['Rating']; ?></td>
                <td><?php echo $FetchGetAllCab['MadeYear']; ?></td>
                <td><?php echo $FetchGetAllCab['KmRate']; ?></td>
                <td><?php echo $FetchGetAllCab['AcKmRate']; ?></td>
                <td><?php echo $Doc; ?></td>
                <td><?php echo $FetchGetAllCab['Status']; ?></td>
            </tr>
            <?php
        }
        ?>
    </table>
    <div class="pagination pagination-centered">
        <ul>
            <?php
            for ($i = 1; $i <= $totalpage; $i++) {
                if ($i == $page)
                    echo '<li class = "active"><a href="Agentcab.php?page=' . $i . '">' . $i . '</a></li>';
                else
                    echo '<li><a href="Agentcab.php?page=' . $i . '">' . $i . '</a></li>';
            }
            ?>
        </ul>
    </div>
</form>
