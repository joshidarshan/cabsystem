<?php
if (!isset($_SESSION))
    session_start();

function AgentPath() {
    $path = $_SERVER['DOCUMENT_ROOT'];
    // Differenciate the path for local and web host. Includes database connect file.
    if ($_SERVER['HTTP_HOST'] == "localhost" || $_SERVER['HTTP_HOST'] == "localhost:8080")
        $path .= '/Cabsystem';
    return $path;
}

//include_once Path30() . '/templates/header.php';
//include_once Path30() . '/bizop/views/BookedSeatLayout.php';
//echo $_POST['Book'];
//if(empty($_POST['Book'])){
include_once AgentPath() . '/templates/Agentheader.php';
//}
$PreAgentId =  $_SESSION['AgentId'];
?>
<?php
if (function_exists('date_default_timezone_set'))
{
  date_default_timezone_set('Asia/Calcutta');
}
$fromYear = date('Y');
$tday = date('d');
$tmonth = date('m');
$toYear = date('Y', strtotime('+1 year'));
$crid = 0;

include_once AgentPath() . '/dbop/data/CabRoute.php';
include_once AgentPath() . '/dbop/data/cab.php';
include_once AgentPath() . '/dbop/data/carmodel.php';
include_once AgentPath() . '/ajax/car.php';

if(isset($_GET) && !empty($_GET)){
    if(isset($_GET['crid']) && !empty($_GET['crid'])){
        $crid=$_GET['crid'];
    }
}

if (isset($_POST['sub'])) {
    
    // check if need to add new cab or not.
    if(isset($_POST['isnew']) && !empty($_POST['isnew']) && $_POST['isnew'] == 1){
        $cabNumberPlate = $_POST['ecab'];
        $cabModel = $_POST['model'];
        $cabMoadelYear = $_POST['madeyear'];
        $cabAcKmRate = $_POST['ackmrate'];
        $cabNonAcKmRate = $_POST['nackmrate'];
        $cabAgentId = $_POST['AgentId'];
        $cabStatus = 2;
        
        $insertedCabId = InsertCab($cabModel, $cabNumberPlate, '', $cabMoadelYear, $cabAgentId, $cabNonAcKmRate, $cabAcKmRate, '', $cabStatus);
    }
    
    if($_POST['isnew'] == 1)
        $crCabId = $insertedCabId;
    else 
        $crCabId = $_POST['CabId'];
    
    $cabModelId = $_POST['model'];
    $crFromDt = $_POST['FromDateTime'];
    
    if(isset($_POST['ToDateTime']) && !empty($_POST['ToDateTime']))
        $crToDt = $_POST['ToDateTime'];
    else
        $crToDt = date('Y-m-d', strtotime($crFromDt . '+ 3 day'))." 23:59:59";
    
    $OriginalLocation = $_POST['OriginalLocation'];
    $crSourceLocation = $_POST['hsource']; //$_POST['Source'];
    if(isset($_POST['hdestination']) && !empty($_POST['hdestination']))
        $crDestinationLocation = $_POST['hdestination']; //$_POST['Destination'];
    else
        $crDestinationLocation = 0; //$_POST['Destination'];
    $crIsReturn = 0;
    if (isset($_POST['IsReturnTrip']) && !empty($_POST['IsReturnTrip']))
        $crIsReturn = 1;
    $crAgentId = $PreAgentId;
    $crDriverId = $_POST['DriverId'];
       $IsVia = 0;
    if (isset($_POST['IsVia']) && !empty($_POST['IsVia']))
        $IsVia = 1;

    $crStatus = 2;
    
    // if crid present it means, admin want to add physical cab to virtual cab
    echo $_POST['crid'];
    if(isset($_POST['crid']) && !empty($_POST['crid'])){
        $crIsEdited = AddPhysicalCabToVirtual($_POST['crid'], $crCabId, $crAgentId, $crDriverId);
        if($crIsEdited)
            echo "<div class='alert alert-success'><a class='close' data-dismiss='alert'>x</a><strong>Physical cab added to virtual cab!</strong></div>";
        else
            echo "<div class='alert alert-error'><a class='close' data-dismiss='alert'>x</a><strong>Operation failed! Kindly try after some time.</strong></div>";
    }
    // else admin wants to add new/existing cab to system.
    else{
        $crIsInserted = AddCabRoute($crCabId, $crFromDt, $crSourceLocation, $crDestinationLocation, $crToDt, $OriginalLocation,$crIsReturn, $crAgentId, $crDriverId,$IsVia, $cabModelId,$crStatus);
        if ($crIsInserted)
            echo "<div class='alert alert-success'><a class='close' data-dismiss='alert'>x</a><strong>Cab Added for the route!</strong></div>";
        else
            echo "<div class='alert alert-error'><a class='close' data-dismiss='alert'>x</a><strong>Operation failed! Kindly try after some time.</strong></div>";
    }
}

?>

<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>

<!-- Datetime picker -->
<style>
    .td
    TD
    {
        text-align:center;
    }
</style>
<form action="Agentcabrouteadd.php" method="post" name="cabrouteadd" id="cabrouteadd">
    <h1 style="text-align: center;">Cabroute Add</h1>
    <table align="center" class="table table-striped"  >
        <input type="text" name="PreAgentId" id="PreAgentId" value="<?php echo $PreAgentId; ?>"/>
        <tr>
            <td class="td">Cab Number</td>
            <td><input type="text" id="ecab" name="ecab" onblur="return getCarInfo();" /></td>
        </tr>
        
        <!--  -->
        <tr>
            <td>Car Model</td>
            <td>
                <select id="model" name="model">
                    <?php
                        include_once AgentPath().'/dbop/data/carmodel.php';
                        $carModelData = GetCarModelListing();
                        while($carModelRow = mysql_fetch_assoc($carModelData)){
                            echo "<option value='$carModelRow[Id]'>$carModelRow[Name] - $carModelRow[NoSeats]  Seater</option>";
                        }
                    ?>
                </select>
            </td>
        </tr>

        <tr>
            <td>Made Year</td>
            <td><input type="text" id="madeyear" name="madeyear" /></td>
        </tr>
        
        <tr>
            <td>AcKmRate</td>
            <td><input type="text" id="ackmrate" name="ackmrate" /></td>
        </tr>
        
        <tr>
            <td>NonAc-KmRate</td>
            <td><input type="text" id="nackmrate" name="nackmrate" /></td>
        </tr>
        
        <tr>
            <td>From Date & Time</td>
            <td><input type="text" name="FromDateTime" id="FromDateTime" class="dttm" required/></td>
        </tr>

        <tr>
            <td>Select Source</td>
            <td><input type="text" name="Source" id="Source" onblur="return SetSource();" required/></td>
        </tr>
        <tr>
            <td>Select Destination</td>
            <td><input type="text" name="Destination" id="Destination" onblur="return SetDestination();" /></td>
        </tr>

        <tr>
            <td>To Date & Time</td>
            <td><input type="text" id="ToDateTime" name="ToDateTime" class="dttm" /></td>
        </tr>
        <tr>
            <td>Proper Location</td>
            <td><input type="text" name="OriginalLocation" class="dttm" /></td>
        </tr>

        <tr>
            <td>Is It Return Trip?</td>
            <td>Click For Return Trip&nbsp;&nbsp;=>&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" name="IsReturnTrip" /></td>
        </tr>
<!--        <tr>
            <td>Select Agent Id</td>
            <td>
                <select id="AgentId" name="AgentId" onchange="return GetDriver();">
                    <?php
                    include_once AgentPath() . '/dbop/data/Agent.php';
                    $GetAllAgents = GetAllAgents($start = 0, $rpp = 500);
                    echo "<option>Select Agent</option>";
                    while ($fetchAllAgents = mysql_fetch_array($GetAllAgents)) {
                        ?>
                        <option value="<?php echo $fetchAllAgents['Id']; ?>"><?php echo $fetchAllAgents['Name']; ?></option>
                    <?php } ?>
                </select>
            </td>
        </tr>-->
        <tr>
            <td>Select Driver Id</td>
            <td>
                <select id="DriverId" name="DriverId"  onfocus="return GetDriver();">
                </select>
            </td>
        </tr>
        <tr>
            <td>Make Sure Is Via Or Not</td>
            <td>Click For Via&nbsp;&nbsp;=>&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" name="IsVia" value="1" /></td>
        </tr>
       
        <tr>
            <td colspan="2" style="text-align: center;"><input type="submit" name="sub" value="Add" class="btn-large"/></td>
        </tr>
        <tr>
            <td colspan="2" style="text-align: center;"><a href="CabAdd.php">Cab not listed? Register new cab here</a></td>
        </tr>
    </table>
    <input type="hidden" name="isnew" id="isnew" value="0" />
    <input type="hidden" name="hsource" id="hsource" value="" />
    <input type="hidden" name="hdestination" id="hdestination" value="" />
    <input type="hidden" name="CabId" id="CabId" value="" />
    <?php if(isset($crid) && !empty($crid)) echo "<input type='hidden' name='crid' id='crid' value='$crid' />"; else echo "<input type='hidden' name='crid' id='crid' />"; ?>
</form>


<script>
                    function GetDriver() {
                        var agentId = $("#PreAgentId").val();
                        $.ajax({
                            type: "GET",
                            url: "../ajax/car.php?type=cabroute&required=drivers&agentId=" + agentId,
                            //data: "std=" + std,
                            success: function(data) {
                                //alert(data);
                                $("#DriverId").find('option').remove().end();
                                $("#DriverId").html(data);
                            }
                        });
                    }

                    $(function() {
                        var AgentId = $("#PreAgentId").val();
                        $.ajax({
                            type: "GET",
                            url: "../ajax/car.php?type=cabroute&AgentId=" + AgentId,
                            data: "required=Agentcablist",
                            success: function(data) {
                                obtainData = JSON.parse(data);
                                var availableCabs = obtainData;
                                $("#ecab").autocomplete({
                                    source: availableCabs
                                });
                            }
                        });
                    });
                    
                    // Location
                    $(function() {
                        $.ajax({
                            type: "GET",
                            url: "../ajax/location.php?type=location",
                            data: "required=locationlist",
                            success: function(data) {
                                //alert(data);
                                obtainData = JSON.parse(data);
                                var availableRoutes = obtainData;
                                $("#Source").autocomplete({
                                    source: availableRoutes
                                });
                                $("#Destination").autocomplete({
                                   source: availableRoutes
                                });
                            }
                        });
                    });
                    
                    function SetSource(){
                        source = $("#Source").val();
                       // alert(source);
                        
                        $.ajax({
                           type: "GET",
                           url: "../ajax/location.php?type=location&required=locationid&location="+source,
                           success: function(data){
                               //alert(data);
                               if(data){
                                   data = JSON.parse(data);
                                   $("#hsource").val(data['Id']);
                               }
                               else
                                   $("#hsource").val(0);
                           }
                        });
                    }
                    
                    function SetDestination(){
                        source = $("#Destination").val();
                       // alert(source);
                        
                        $.ajax({
                           type: "GET",
                           url: "../ajax/location.php?type=location&required=locationid&location="+source,
                           success: function(data){
                               //alert(data);
                               if(data){
                                   data = JSON.parse(data);
                                   $("#hdestination").val(data['Id']);
                               }
                               else
                                   $("#hdestination").val(0);
                           }
                        });
                    }
                    
                    // Datetime picker
                    $(function(){
				$('*[name=FromDateTime]').appendDtpicker({
					//"inline": true,
					"minuteInterval": 15,
                                        "current": "<?php echo $fromYear.'-'.$tmonth.'-'.$tday ?> 00:15"
				});
                                $('*[name=ToDateTime]').appendDtpicker({
					//"inline": true,
					"minuteInterval": 15
				});
			});
                        
                    // Car Info
                    function getCarInfo(){
                        var carno = $("#ecab").val();
                        $.ajax({
                            type: "GET",
                            url: "../ajax/car.php?type=cabs&required=cablist&carno=" + carno,
                            //data: "std=" + std,
                            success: function(data) {
                            data = JSON.parse(data);    
                            //alert(data);
                            //{"Id":"1","CarModelId":"1","NoPlate":"gj-03-av-1546","Rating":"10","MadeYear":"2012","AgentId":"1","KmRate":"12.00","AcKmRate":"14.00","Status":"1"}
                            $("#madeyear").val(data['MadeYear']);
                            $("#ackmrate").val(data['AcKmRate']);
                            $("#nackmrate").val(data['KmRate']);
                            $("#model").val(data['CarModelId']);
                            $("#AgentId").val(data['AgentId']);
                            if(data['Id']){
                                $("#isnew").val(0);
                                $("#CabId").val(data['Id']);
                            }
                            else
                                $("#isnew").val(1);
                        }
                        });
                    }
                    
                    $(document).ready(function(){
                        $("#ToDateTime").val("");
                    });
                    
</script>
