<?php
if (!isset($_SESSION))
    session_start();
if (isset($_SESSION['UID']) && !empty($_SESSION['UID']))
    header("location: index.php");

function LPath() {
    $path = $_SERVER['DOCUMENT_ROOT'];
    // Differenciate the path for local and web host. Includes database connect file.
    if ($_SERVER['HTTP_HOST'] == "localhost" || $_SERVER['HTTP_HOST'] == "localhost:8080")
        $path .= '/Cabsystem';
    return $path;
}

function ConstructUrl(){
    $path = $_SERVER['DOCUMENT_ROOT'];
    if ($_SERVER['HTTP_HOST'] == "localhost" || $_SERVER['HTTP_HOST'] == "localhost:8080")
        $path = $_SERVER['HTTP_HOST']."/cabsystem";
    else
        $path = $_SERVER['HTTP_HOST'];
    return $path;
}
?>
<html>
    <head>
        <link rel="stylesheet" href="../css/login.css" />
        <script src="../js/jquery-1.10.2.min.js" type="text/javascript"></script>
        <script src="../js/jquery.validate.min.js" type="text/javascript" ></script>
        <title>Agent Login | Khaligadi.com</title>
    </head>
    <body>
        <div id="wrapper">

            <form name="login-form" class="login-form" action="AgentLogin.php" method="post">

                <div class="header">
                    <h1 style="text-align: center;">Login</h1>
                    
                    <span id="error" style="font-size: 15px; color: red; display: none;">Username or password is incorrect</span>
                </div>

                <div class="content">
                    <input name="username" type="text" class="input username" placeholder="Username" required />
                    <div class="user-icon"></div>
                    <input name="password" type="password" class="input password" placeholder="Password" required />
                    <div class="pass-icon"></div>		
                </div>

                <div class="footer">
                    <input type="submit" name="submit" value="Login" class="button" />
                    <a href="AgentRegister.php"><img src="register_button.png" height="100px" width="100px" style="margin-left: 77px"/></a>
                </div>
                 
                     
                
            </form>

        </div>
        <div class="gradient"></div>
    </body>
</html>

<?php
if (isset($_POST['submit']) && !empty($_POST['submit'])) {
    include_once LPath() . '/dbop/data/user.php';

//    $username = mysql_real_escape_string($_POST['username']);
//    $password = mysql_real_escape_string($_POST['password']);
    $username = ($_POST['username']);
    $password = ($_POST['password']);

    $userData = LoginUserByUseridPassword($username, $password);
    var_dump($userData);
    if ($userData) {
        //UID, Role, RoleId, IsAdmin, IsAgent
        $_SESSION['UID'] = $userData['UID'];
        $_SESSION['Role'] = $userData['Role'];
        $_SESSION['IsAdmin'] = $userData['IsAdmin'];
        $_SESSION['IsAgent'] = $userData['IsAgent'];
        if($userData['IsAgent'] == 1){
//            $_SESSION['IsAgent'] = $_SESSION['AgentId'];
            $_SESSION['AgentId'] = $_SESSION['UID'];
            header("location: index.php");
        }
        else
            header("location: AgentLogout.php");
    }
    else
        echo "<script>$('#error').css('display','block');</script>";
}
?>