<?php
if (!isset($_SESSION))
    session_start();
$_SESSION['cat'] = "inventory";

function Path() {
    $path = $_SERVER['DOCUMENT_ROOT'];
    // Differenciate the path for local and web host. Includes database connect file.
    if ($_SERVER['HTTP_HOST'] == "localhost" || $_SERVER['HTTP_HOST'] == "localhost:8080")
        $path .= '/Cabsystem';
    return $path;
}

function DriverDocUrl() {
    $url = "http://" . $_SERVER['HTTP_HOST'];
    if ($_SERVER['HTTP_HOST'] == "localhost" || $_SERVER['HTTP_HOST'] == "localhost:8080")
        $url .= '/Cabsystem';

    return $url;
}

include_once Path() . '/templates/adminheader.php';
include_once Path() . '/dbop/data/driver.php';

if (isset($_POST['sub'])) {

    $Id = $_POST['Id'];
    $Name = $_POST['Name'];
    $Phones = $_POST['Phones'];
    $Notes = $_POST['Notes'];
    $DocumentLink = "";
    
    if (isset($_FILES['file']['name']) && !empty($_FILES['file']['name']) && $_FILES['file']['size'] > 0) {
        //$DocumentLink = $_FILES['DocLink'];
        $path = Path() . '/Upload/document/';
        $guid = uniqid();
        $fname = $guid . '-' . $_FILES['file']['name'];
        move_uploaded_file($_FILES["file"]["tmp_name"], $path . $fname);
        $DocumentLink = $fname;
    } else {
        if (isset($_POST['prevdoc']) && !empty($_POST['prevdoc']))
            $DocumentLink = $_POST['prevdoc'];
    }
    if (isset($_POST['status']) && !empty($_POST['status']))
        $Status = $_REQUEST['status'];
    else
        $Status = 0;
    UpdateDriver($Id, $Name, $Phones, $Notes, $DocumentLink, $Status);
    echo "<script> alert ('Your Update SuccessFully'); window.location='Agentdriver.php';</script>";
    }

$Id = $_REQUEST['Id'];
$result = GetDriver($Id);
$GetAgent = mysql_fetch_array($result);
$StatusChecked = $GetAgent['Status'];
?>
<h3>Driver Edit</h3>
<form action="DriverEdit.php" method="post" enctype="multipart/form-data">
    <table align="center" class="table table-striped" >
        <tr>
            <td>Enter Driver Id</td>
            <td><input type="text" name="Id" value="<?php echo $Id; ?>" /></td>
        </tr>
        <tr>
            <td>Enter Driver Name</td>
            <td><input type="text" name="Name" value="<?php echo $GetAgent['Name']; ?>"/></td>
        </tr>
        <tr>
            <td>Enter Driver Contact No</td>
            <td><textarea name="Phones" placeholder="Enter Contact No"><?php echo $GetAgent['Phones']; ?></textarea></td>
        </tr>
        <tr>
            <td>Enter Notes</td>
            <td><textarea name="Notes"><?php echo $GetAgent['Note']; ?></textarea></td>
        </tr>

        <tr>
            <td>Your Current Image</td>
            <td><?php $imgPath = DriverDocUrl()."/Upload/images/$GetAgent[DocumentLink]"; if(!empty($GetAgent['DocumentLink'])) echo "<img src='$imgPath' height='100px' width='100px'></img>"; else echo "No Image found";?></td>
        <tr>
            <td>Choose An Image</td>
            <td><input type="file" name="file"/></td>
        </tr>

        <tr>
            <td>Active Status</td>
            <?php if ($StatusChecked == 1) { ?>
                <td><input type="checkbox" name="status" value="1" checked="checked" /></td>
<?php } ?> <?php if ($StatusChecked == 0) { ?>

                <td><input type="checkbox" name="status" value="1" /></td>
<?php } ?>    

        </tr>
        <tr>
            <td colspan="2" style="text-align: center;"><input type="submit" name="sub" value="Add" class="btn-success btn"/></td>
        </tr>
    </table>
</form>

<?php
include_once Path() . '/templates/adminfooter.php';
?>