<?php

function Path() {
    $path = $_SERVER['DOCUMENT_ROOT'];
    // Differenciate the path for local and web host. Includes database connect file.
    if ($_SERVER['HTTP_HOST'] == "localhost" || $_SERVER['HTTP_HOST'] == "localhost:8080" || $_SERVER['HTTP_HOST'] == "192.168.1.100:8080")
        $path .= '/Cabsystem';
    return $path;
}

if (function_exists('date_default_timezone_set')) {
    date_default_timezone_set('Asia/Calcutta');
}
$fromYear = date('Y');
$tday = date('d');
$tmonth = date('m');
$toYear = date('Y', strtotime('+1 year'));
$crid = 0;

include_once Path() . '/templates/adminheader.php';
include_once Path() . '/dbop/data/CabRoute.php';
include_once Path() . '/dbop/data/cab.php';
include_once Path() . '/dbop/data/carmodel.php';
include_once Path() . '/dbop/data/location.php';
include_once Path() . '/dbop/data/agent.php';
include_once Path() . '/dbop/data/driver.php';
include_once Path() . '/ajax/car.php';

$locationArray = GetLocationArray();

if (isset($_POST['sub']) && isset($_POST['cab']) && !empty($_POST['cab'])) {

    $isNew = TRUE;
    $cabNoPlate = strtolower($_POST['cab']);
    $noToCabQuery = GetCabByNoPlate($cabNoPlate);
    $noToCabData = mysql_fetch_assoc($noToCabQuery);
    $cabId = 0;
    $cabModelId="";
    if ($noToCabData != null) {
        $cabId = $noToCabData['Id'];
        $cabModelId = $noToCabData['CarModelId'];
        $isNew = FALSE;
    }
    $oneWayAgentData = mysql_fetch_assoc(GetOneWayAgent());
    $oneWayAgentId = $oneWayAgentData['Id'];
    if(empty($oneWayAgentId))
        $oneWayAgentId = AddOneWayAgent();

    $oneWayDriverData = mysql_fetch_assoc(GetOneWayDriver());
    $oneWayDriverId = $oneWayDriverData['Id'];
    if(empty($oneWayDriverId))
        $oneWayDriverId = AddOneWayDriver ($oneWayAgentId);

    // check if need to add new cab or not.
    if ($isNew) {
        $cabNumberPlate = $_POST['cab'];
        $cabModel = $_POST['model'];
        $cabModelId = $_POST['model'];
        $cabMoadelYear = $_POST['madeyear'];
        $cabAcKmRate = $_POST['ackmrate'];
        $cabNonAcKmRate = $_POST['nackmrate'];
        $cabAgentId = $oneWayAgentId;
        $cabStatus = 1;

        $cabId = InsertCab($cabModel, $cabNumberPlate, '', $cabMoadelYear, $cabAgentId, $cabNonAcKmRate, $cabAcKmRate, '', $cabStatus);
        //echo "new cab insert: cabModel: $cabModel, NoPlate: $cabNumberPlate, ModelYear: $cabMoadelYear, AC: $cabAcKmRate, NAC: $cabNonAcKmRate, Agent: $cabAgentId, Status: $cabStatus";
    }


    
    $crFromDt = $_POST['FromDateTime'];

    if (isset($_POST['ToDateTime']) && !empty($_POST['ToDateTime']))
        $crToDt = $_POST['ToDateTime'];
    else
        $crToDt = date('Y-m-d', strtotime($crFromDt . '+ 3 day')) . " 23:59:59";

    $OriginalLocation = "";
    $crSourceLocation = $_POST['source'];
    $crDestinationLocation = $_POST['destination'];
    $crIsReturn = 1;
    $crAgentId = $oneWayAgentId;
    $crDriverId = $oneWayDriverId;
    $IsVia = 0;
    $crStatus = 2;

    //echo "Cabroute Add: CabId:$cabId, From: $crFromDt, Source: $crSourceLocation, Destination: $crDestinationLocation, To: $crToDt, OriginLoc: $OriginalLocation, IsReturn: $crIsReturn, Agent: $crAgentId, Driver: $crDriverId, Via: $IsVia, ModelId: $cabModelId, Status: $crStatus";
    $crIsInserted = AddCabRoute($crCabId, $crFromDt, $crSourceLocation, $crDestinationLocation, $crToDt, $OriginalLocation, $crIsReturn, $crAgentId, $crDriverId, $IsVia, $cabModelId, $crStatus);
    if ($crIsInserted)
        echo "<div class='alert alert-success'><a class='close' data-dismiss='alert'>x</a><strong>Cab Added for the route!</strong></div>";
    else
        echo "<div class='alert alert-error'><a class='close' data-dismiss='alert'>x</a><strong>Operation failed! Kindly try after some time.</strong></div>";
}
?>

<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>

<!-- Datetime picker -->
<style>
    .td
    TD
    {
        text-align:center;
    }
</style>

<form action="CabrouteAdd.php" method="post" name="cabrouteadd" id="cabrouteadd">
    <h1 style="text-align: center;">Cabroute Add</h1>
    <table align="center" class="table table-striped"  >
        <tr>
            <td class="td">Cab Number</td>
            <td><input type="text" id="cab" name="cab"/></td>
        </tr>

        <!--  -->
        <tr>
            <td>Car Model</td>
            <td>
                <select id="model" name="model">
                    <?php
                    include_once Path() . '/dbop/data/carmodel.php';
                    $carModelData = GetCarModelListing();
                    while ($carModelRow = mysql_fetch_assoc($carModelData)) {
                        echo "<option value='$carModelRow[Id]'>$carModelRow[Name] - $carModelRow[NoSeats]  Seater</option>";
                    }
                    ?>
                </select>
            </td>
        </tr>

        <tr>
            <td>Made Year</td>
            <td><input type="text" id="madeyear" name="madeyear" /></td>
        </tr>

        <tr>
            <td>AcKmRate</td>
            <td><input type="text" id="ackmrate" name="ackmrate" /></td>
        </tr>

        <tr>
            <td>NonAc-KmRate</td>
            <td><input type="text" id="nackmrate" name="nackmrate" /></td>
        </tr>

        <tr>
            <td>From Date & Time</td>
            <td><input type="text" name="FromDateTime" id="FromDateTime" class="dttm" required/></td>
        </tr>

        <tr>
            <td>Select Source</td>
            <td>
                <select id="source" name="source">
                    <?php
                    foreach ($locationArray as $lv) {
                        echo "<option value='$lv[Id]'>$lv[Name]</option>";
                    }
                    ?>
                </select>
            </td>
        </tr>
        <tr>
            <td>Select Destination</td>
            <td>
                <select id="destination" name="destination">
                    <?php
                    foreach ($locationArray as $lvd) {
                        echo "<option value='$lvd[Id]'>$lvd[Name]</option>";
                    }
                    ?>
                </select>
            </td>
        </tr>

        <tr>
            <td>To Date & Time</td>
            <td><input type="text" id="ToDateTime" name="ToDateTime" class="dttm" /></td>
        </tr>



        <tr>
            <td colspan="2" style="text-align: center;"><input type="submit" name="sub" value="Add" class="btn-large"/></td>
        </tr>

    </table>
</form>

<?php include_once Path() . "/templates/adminfooter.php"; ?>

<script>
    // Datetime picker
    $(function() {
        $('*[name=FromDateTime]').appendDtpicker({
            //"inline": true,
            "minuteInterval": 15,
            "current": "<?php echo $fromYear . '-' . $tmonth . '-' . $tday ?> 00:15"
        });
        $('*[name=ToDateTime]').appendDtpicker({
            //"inline": true,
            "minuteInterval": 15
        });
    });

    $(document).ready(function() {
        $("#ToDateTime").val("");
    });

</script>
