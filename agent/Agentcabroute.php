<?php
if (!isset($_SESSION))
    session_start();
$_SESSION['cat'] = "Route";

function AgentPath() {
    $path = $_SERVER['DOCUMENT_ROOT'];
    // Differenciate the path for local and web host. Includes database connect file.
    if ($_SERVER['HTTP_HOST'] == "localhost" || $_SERVER['HTTP_HOST'] == "localhost:8080")
        $path .= '/Cabsystem';
    return $path;
}


include_once AgentPath() . '/dbop/data/cabroute.php';
include_once AgentPath() . '/templates/agentheader.php';
$AgentId =  $_SESSION['AgentId'];
$totalpage = 0;
if (isset($_GET['action'])) {
    if ($_GET['action'] == "delete") {

        $Id = $_GET['Id'];
        DeleteCabRoute($Id);
        echo "<script> alert('Your Record Delete SuccessFully');</script>";
    }
}
?>

<script language="javascript">
    function del() {
        var r = confirm("Really Want to Delete A CabRoute");
        if (!r)
            return false;
    }
    
</script>
<form action="CabRoute.php" method="post">
    
    <h1 align="center">CabRoute Listing</h1>
    <table align='center' class="table table-striped">
        <tr>
            <th>Action</th>
            <th>Cab</th>
            <th>From - To DateTime</th>
            <th>Source</th>
            <th>Destination</th>
            <th>Driver Name</th>
            <th>Status</th>
        </tr>
        <?php
        $AllCabRoutes = GetAllCabRoutesByAgentId($AgentId);
        $previousCabRouteId="";
        while ($FetchAllCabRoutes = mysql_fetch_array($AllCabRoutes)) {
            if($previousCabRouteId == $FetchAllCabRoutes['Id'])
                continue;
            $previousCabRouteId = $FetchAllCabRoutes['Id'];
            ?>
        <tr><?php if($FetchAllCabRoutes['BookingStatus'] == 1){?>
            <td>
<!--                    <a href="#">Booked</a> &nbsp; | &nbsp;
                    <a href="#" onClick="return del();">Booked</a>-->
<?php echo $FetchAllCabRoutes['Id'];?>
                </td>

       <?php     
        }
        else
        {//SELECT cr.Id, cb.NoPlate, cr.FromDateTime, cr.ToDateTime, sl.Name as Source, dl.Name as Destination, dr.Name as Driver, bk.Status as BookingStatus
        ?>
                <td>
                    <a href="CabRouteEdit.php?Id=<?php echo $FetchAllCabRoutes['Id']; ?>">Edit</a> &nbsp; | &nbsp;
                    <a href="CabRoute.php?action=delete&Id=<?php echo $FetchAllCabRoutes['Id']; ?>" onClick="return del();">Delete</a>
                </td>
        <?php } ?>
                <td><?php echo $FetchAllCabRoutes['NoPlate']; ?></td>
                <td><?php echo $FetchAllCabRoutes['FromDateTime'] ."-". $FetchAllCabRoutes['ToDateTime']; ?></td>
                <td><?php echo $FetchAllCabRoutes['Source']; ?></td>
                <td><?php echo $FetchAllCabRoutes['Destination']; ?></td>
                <td><?php echo $FetchAllCabRoutes['Driver']; ?></td>
                <td><?php echo $FetchAllCabRoutes['CabRouteStatus']; ?></td>
            </tr>
            <?php
        }
        ?>
    
    </table>
    
    <div class="pagination pagination-centered">
        <ul>
            <?php
            for ($i = 1; $i <= $totalpage; $i++) {
                if ($i == $page)
                    echo '<li class = "active"><a href="CabRoute.php?page=' . $i . '">' . $i . '</a></li>';
                else
                    echo '<li><a href="CabRoute.php?page=' . $i . '">' . $i . '</a></li>';
            }
            ?>
        </ul>
    </div>
</form>

<?php 
//include_once Path() . '/templates/adminfooter.php'; 
?>
