<?php
if (!isset($_SESSION))
    session_start();

function BidPath() {
    $path = $_SERVER['DOCUMENT_ROOT'];
    // Differenciate the path for local and web host. Includes database connect file.
    if ($_SERVER['HTTP_HOST'] == "localhost" || $_SERVER['HTTP_HOST'] == "localhost:8080")
        $path .= '/Cabsystem';
    return $path;
}
include_once BidPath() . '/templates/header.php';
?>
<meta http-equiv="refresh" content="5; url=home.php">
<div style="width: 700px; height: 100px; border:2px solid; margin-left: 350px; margin-top: 20px">
    <h2 align="center">Thank You For Bid A Cab</h2>
    <p align="center" style="font-size: larger"><b> Thank you for registering Bid A Cab with Khaligadi.com. You will be updated about your bid status shortly in few hours</b></p>
 </div>
<?php include_once BidPath() . '/templates/footer.php'; ?>
