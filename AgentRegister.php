<?php

if (!isset($_SESSION))
    session_start();
$_SESSION['cat'] = "inventory";

function AgentRegPath() {
    $path = $_SERVER['DOCUMENT_ROOT'];
    // Differenciate the path for local and web host. Includes database connect file.
    if ($_SERVER['HTTP_HOST'] == "localhost" || $_SERVER['HTTP_HOST'] == "localhost:8080")
        $path .= '/Cabsystem';
    return $path;
}

include_once AgentRegPath() . '/templates/header.php';

if (isset($_POST['Sub'])) {
//    echo "<script>alert('Record');</script>";
    include_once AgentRegPath() . '/dbop/data/agent.php';
    include_once AgentRegPath() . '/dbop/data/user.php';

    $Name = $_REQUEST['Name'];
    $Address = $_REQUEST['Address'];
    $Phones = $_REQUEST['Phones'];
    $Email = $_REQUEST['Email'];
    $Notes = $_REQUEST['Notes'];
    $Status = 1;
    $RoleId = 2;
    $Password = $_POST['Password'];

    if (($_FILES["file"]["type"] == "image/gif") || ($_FILES["file"]["type"] == "image/jpeg") || ($_FILES["file"]["type"] == "image/png" )) {
        $path = AgentRegPath() . '/Upload/images/';
        $guid = uniqid();
        $fname = $guid . '-' . $_FILES['file']['name'];
        move_uploaded_file($_FILES["file"]["tmp_name"], $path . $fname);
        $imgnam = $fname;

        $Id = InsertAgent($Name, $Address, $Phones, $Email, $Notes, $imgnam, $Status);
        AddUser($Id, $Email, $Phones, $Password, $RoleId, $Status);
        echo "<script>alert('Register SuccessFully');window.location='agent/AgentLogin.php'</script>";
    } else {
        $Id = InsertAgent($Name, $Address, $Phones, $Email, $Notes, '', $Status);
        AddUser($Id, $Email, $Phones, $Password, $RoleId, $Status);
        echo "<div class='alert alert-success'><a class='close' data-dismiss='alert'>x</a><strong>Agent Added!</strong></div>";
    }
}
?>
<script>
    add_record_email = 0;
    add_record_no = 0;
    function CheckNumeric(e)
    {
        var key = e.which;
        if (key >= 48 && key <= 57 || key == 8)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    function CheckNumbers() {
        var Phones = $("#tel").val();
//        alert(Phones);
        if (Phones) {
            $.ajax({
                type: "GET",
                url: "./ajax/user.php?type=ContactNo&required=AgentContact&Contact=" + Phones,
                success: function(data) {
                    //alert(data);
                    message = "";
                    if (data == 0) {
                        message = "Already Exist";
                        $("#CheckContact").css({"color": "Red"});
                        add_record_no = 0;
                    }
                    else {
                        message = "Available";
                        $("#CheckContact").css({"color": "Green"});
                        add_record_no = 1;
                    }
                    $("#CheckContact").text(message);
                }
            });
        }

        else
        {
            return false;
        }

    }

    function ChkEmail() {
//        alert('Ram');
        var Email = $("#Email").val();
        if (Email) {
//            alert(Email);
            var isCorrect = CheckEmail();
            if (isCorrect) {
                $.ajax({
                    type: "GET",
                    url: "./ajax/user.php?type=EmailId&required=AgentEmail&Email=" + Email,
                    success: function(data) {
                        //alert(data);
                        message = "";
                        if (data == 0) {
                            message = "Already Exist";
                            $("#ChkEmail").css({"color": "Red"});
                            add_record_email = 0;
//                            alert('Already');
                        }
                        else {
                            message = "Available";
                            $("#ChkEmail").css({"color": "Green"});
                                             add_record_email = 1;   
//                                              alert('Already fcf');

                        }
                        $("#ChkEmail").text(message);
//                        alert('Available');
                    }
                });
            }
            else
                document.getElementById("ChkEmail").innerHTML = "Email Is Not Correct";
                add_record_email = 0;
        }
        else
        {
            return false;
        }
    }

    function CheckEmail() {
        var Email = $("#Email").val();
        var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
        if (reg.test(Email)) {
            return true;
        }
        else {
            return false;
        }
    }
   function Validation()
   {
//       alert(add_record_no);
       if (add_record_email == 0 || add_record_no == 0) {
            return false;
        }
        return true;
   }

</script>
<style>
  
</style>

    <script type="text/javascript">

 $(document).ready(function() {
/*
* In-Field Label jQuery Plugin
* http://fuelyourcoding.com/scripts/infield.html
*
* Copyright (c) 2009 Doug Neiner
* Dual licensed under the MIT and GPL licenses.
* Uses the same license as jQuery, see:
* http://docs.jquery.com/License
*
* @version 0.1
*/
(function($) { $.InFieldLabels = function(label, field, options) { var base = this; base.$label = $(label); base.$field = $(field); base.$label.data("InFieldLabels", base); base.showing = true; base.init = function() { base.options = $.extend({}, $.InFieldLabels.defaultOptions, options); base.$label.css('position', 'absolute'); var fieldPosition = base.$field.position(); base.$label.css({ 'left': fieldPosition.left, 'top': fieldPosition.top }).addClass(base.options.labelClass); if (base.$field.val() != "") { base.$label.hide(); base.showing = false; }; base.$field.focus(function() { base.fadeOnFocus(); }).blur(function() { base.checkForEmpty(true); }).bind('keydown.infieldlabel', function(e) { base.hideOnChange(e); }).change(function(e) { base.checkForEmpty(); }).bind('onPropertyChange', function() { base.checkForEmpty(); }); }; base.fadeOnFocus = function() { if (base.showing) { base.setOpacity(base.options.fadeOpacity); }; }; base.setOpacity = function(opacity) { base.$label.stop().animate({ opacity: opacity }, base.options.fadeDuration); base.showing = (opacity > 0.0); }; base.checkForEmpty = function(blur) { if (base.$field.val() == "") { base.prepForShow(); base.setOpacity(blur ? 1.0 : base.options.fadeOpacity); } else { base.setOpacity(0.0); }; }; base.prepForShow = function(e) { if (!base.showing) { base.$label.css({ opacity: 0.0 }).show(); base.$field.bind('keydown.infieldlabel', function(e) { base.hideOnChange(e); }); }; }; base.hideOnChange = function(e) { if ((e.keyCode == 16) || (e.keyCode == 9)) return; if (base.showing) { base.$label.hide(); base.showing = false; }; base.$field.unbind('keydown.infieldlabel'); }; base.init(); }; $.InFieldLabels.defaultOptions = { fadeOpacity: 0.5, fadeDuration: 300, labelClass: 'infield' }; $.fn.inFieldLabels = function(options) { return this.each(function() { var for_attr = $(this).attr('for'); if (!for_attr) return; var $field = $("input#" + for_attr + "[type='text']," + "input#" + for_attr + "[type='password']," + "input#" + for_attr + "[type='tel']," + "input#" + for_attr + "[type='email']," + "textarea#" + for_attr); if ($field.length == 0) return; (new $.InFieldLabels(this, $field[0], options)); }); }; })(jQuery);


    $("#RegisterUserForm label").inFieldLabels();
});

</script>

<style type="text/css">

/* Add whatever you need to your CSS reset */
html, body, h1, form, fieldset, input {
  margin: 0;
  padding: 0;
  border: none;
  }

body { font-family: Helvetica, Arial, sans-serif; font-size: 12px; }

        #registration {
			color: #fff;
            background: #2d2d2d;
            background: -webkit-gradient(
                            linear,
                            left bottom,
                            left top,
                            color-stop(0, rgb(60,60,60)),
                            color-stop(0.74, rgb(43,43,43)),
                            color-stop(1, rgb(60,60,60))
                        );
            background: -moz-linear-gradient(
                            center bottom,
                            rgb(60,60,60) 0%,
                            rgb(43,43,43) 74%,
                            rgb(60,60,60) 100%
                        );
            -moz-border-radius: 10px;
            -webkit-border-radius: 10px;
			border-radius: 10px;
            margin: 10px;
			width: 430px;
            }

 #registration a {
      color: #8c910b;
      text-shadow: 0px -1px 0px #000;
      }
	  
#registration fieldset {
      padding: 20px;
      }
	  
input.text {
      -webkit-border-radius: 15px;
      -moz-border-radius: 15px;
      border-radius: 15px;
      border:solid 1px #444;
      font-size: 14px;
      width: 90%;
      padding: 7px 8px 7px 30px;
      -moz-box-shadow: 0px 1px 0px #777;
      -webkit-box-shadow: 0px 1px 0px #777;
	  background: #ddd url('Upload/images/inputSprite.png') no-repeat 4px 5px;
	  background: url('Upload/images/inputSprite.png') no-repeat 4px 5px, -moz-linear-gradient(
           center bottom,
           rgb(225,225,225) 0%,
           rgb(215,215,215) 54%,
           rgb(173,173,173) 100%
           );
	  background:  url('Upload/images/inputSprite.png') no-repeat 4px 5px, -webkit-gradient(
          linear,
          left bottom,
          left top,
          color-stop(0, rgb(225,225,225)),
          color-stop(0.54, rgb(215,215,215)),
          color-stop(1, rgb(173,173,173))
          );
      color:#333;
      text-shadow:0px 1px 0px #FFF;
}	  

 input#email { 
 	background-position: 4px 5px; 
	background-position: 4px 5px, 0px 0px;
	}
	
 input#password { 
 	background-position: 4px -20px; 
	background-position: 4px -20px, 0px 0px;
	}
	
 input#name { 
 	background-position: 4px -46px; 
	background-position: 4px -46px, 0px 0px; 
	}
	
 input#tel { 
 	background-position: 4px -76px; 
	background-position: 4px -76px, 0px 0px; 
	}
	
#registration h2 {
	color: #fff;
	text-shadow: 0px -1px 0px #000;
	border-bottom: solid #181818 1px;
	-moz-box-shadow: 0px 1px 0px #3a3a3a;
	text-align: center;
	padding: 18px;
	margin: 0px;
	font-weight: normal;
	font-size: 24px;
	font-family: Lucida Grande, Helvetica, Arial, sans-serif;
	}
	
#registerNew {
	width: 203px;
	height: 40px;
	border: none;
	text-indent: -9999px;
	background: url('Upload/images/createAccountButton.png') no-repeat;
	cursor: pointer;
	float: right;
	}
	
	#registerNew:hover { background-position: 0px -41px; }
	#registerNew:active { background-position: 0px -82px; }
	
 #registration p {
      position: relative;
      }
	  
fieldset label.infield /* .infield label added by JS */ {
    color: #333;
    text-shadow: 0px 1px 0px #fff;
    position: absolute;
    text-align: left;
    top: 3px !important;
    left: 35px !important;
    line-height: 29px;
    }
    .simpletext{
        
        border-radius: 15px;width: 90%;
      padding: 7px 8px 7px 30px;
      -moz-box-shadow: 0px 1px 0px #777;
      -webkit-box-shadow: 0px 1px 0px #777; color:#333;
      text-shadow:0px 1px 0px #FFF;background: #ddd;
    }
</style>



    <div id="registration" style="margin-left: 435px">
 <h2>Create an User Account</h2>

 <form action="AgentRegister.php" method="post" enctype="multipart/form-data" name="Cabsystem" id="Cabsystem" onsubmit="return Validation();" >
 	<fieldset>
         <p>
            <label for="name">Name</label>
            <input id="name" name="Name" type="text" class="text" value="" />
         </p>
        
         <p>
            <label for="tel">Phone Number</label>
            <input id="tel" name="Phones"  type="tel" size="10" class="text" maxlength="10" onkeypress="return CheckNumeric(event);" onblur="return CheckNumbers();" placeholder="Enter Number Only" value="" />
         </p>
        
         <p>
            <label for="email">Email</label>
            <input id="Email" name="Email" type="email" class="text" value=""  required onblur="return ChkEmail();" /><span id="ChkEmail" style="color: red"></span><span id="val"></span>
         </p>
         <p>
            <label for="password">Password</label>
            <input id="password" name="Password" class="text" type="password" onfocus="return Email_filter();" />
         </p>
         <p>
            <label></label>
            <textarea name="Address" placeholder="Address" class="simpletext"></textarea>
         </p>
         <p>
            <textarea name="Notes" class="simpletext" placeholder="Note"></textarea>
         </p>
          <p>
              <label>Image/Document</label>
            <input type="file" name="file" class="btn" />
         </p>
                 <p>
             <input type="submit" name="Sub" />
            <!--<button id="registerNew" type="submit">Register</button>-->
         </p>
 	</fieldset>

 </form>
</div>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.1/jquery.min.js"></script>

<script type="text/javascript">

        $(document).ready(function() {
/*
* In-Field Label jQuery Plugin
* http://fuelyourcoding.com/scripts/infield.html
*
* Copyright (c) 2009 Doug Neiner
* Dual licensed under the MIT and GPL licenses.
* Uses the same license as jQuery, see:
* http://docs.jquery.com/License
*
* @version 0.1
*/
(function($) { $.InFieldLabels = function(label, field, options) { var base = this; base.$label = $(label); base.$field = $(field); base.$label.data("InFieldLabels", base); base.showing = true; base.init = function() { base.options = $.extend({}, $.InFieldLabels.defaultOptions, options); base.$label.css('position', 'absolute'); var fieldPosition = base.$field.position(); base.$label.css({ 'left': fieldPosition.left, 'top': fieldPosition.top }).addClass(base.options.labelClass); if (base.$field.val() != "") { base.$label.hide(); base.showing = false; }; base.$field.focus(function() { base.fadeOnFocus(); }).blur(function() { base.checkForEmpty(true); }).bind('keydown.infieldlabel', function(e) { base.hideOnChange(e); }).change(function(e) { base.checkForEmpty(); }).bind('onPropertyChange', function() { base.checkForEmpty(); }); }; base.fadeOnFocus = function() { if (base.showing) { base.setOpacity(base.options.fadeOpacity); }; }; base.setOpacity = function(opacity) { base.$label.stop().animate({ opacity: opacity }, base.options.fadeDuration); base.showing = (opacity > 0.0); }; base.checkForEmpty = function(blur) { if (base.$field.val() == "") { base.prepForShow(); base.setOpacity(blur ? 1.0 : base.options.fadeOpacity); } else { base.setOpacity(0.0); }; }; base.prepForShow = function(e) { if (!base.showing) { base.$label.css({ opacity: 0.0 }).show(); base.$field.bind('keydown.infieldlabel', function(e) { base.hideOnChange(e); }); }; }; base.hideOnChange = function(e) { if ((e.keyCode == 16) || (e.keyCode == 9)) return; if (base.showing) { base.$label.hide(); base.showing = false; }; base.$field.unbind('keydown.infieldlabel'); }; base.init(); }; $.InFieldLabels.defaultOptions = { fadeOpacity: 0.5, fadeDuration: 300, labelClass: 'infield' }; $.fn.inFieldLabels = function(options) { return this.each(function() { var for_attr = $(this).attr('for'); if (!for_attr) return; var $field = $("input#" + for_attr + "[type='text']," + "input#" + for_attr + "[type='password']," + "input#" + for_attr + "[type='tel']," + "input#" + for_attr + "[type='email']," + "textarea#" + for_attr); if ($field.length == 0) return; (new $.InFieldLabels(this, $field[0], options)); }); }; })(jQuery);


        							$("#RegisterUserForm label").inFieldLabels();
								   });

</script>
<?php

include_once AgentRegPath() . '/templates/adminfooter.php';
?>



