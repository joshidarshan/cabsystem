<?php

function Path21() {
    $path21 = $_SERVER['DOCUMENT_ROOT'];
    // Differenciate the path for local and web host. Includes database connect file.
    if ($_SERVER['HTTP_HOST'] == "localhost" || $_SERVER['HTTP_HOST'] == "localhost:8080" || $_SERVER['HTTP_HOST'] == "192.168.1.100:8080")
        $path21 .= '/Cabsystem';
    return $path21;
}

include_once Path21() . '/dbop/data/listingutility.php';
include_once Path21() . '/dbop/data/sharedfare.php';
include_once Path21() . '/dbop/data/sublocation.php';
include_once Path21() . '/dbop/data/globalperameter.php';
include_once Path21() . '/bizop/VirtualCab.php';

function Pre_List_OneWay($List, $days_between, $FromLocation, $ToLocation, $SubId, $FromLocalDate, $ToLocalDate, $Distance) {
   if($days_between == "" || $days_between == 0)
       $days_between = 1;
    $CCount=0;
    $OneWayCmId = "";
    $Str = "";
//$SLocationId = 1;
//$DLocationId = 3;
    $PrvCabRouteId = 0;
    $Rate = GetFareByRoute($FromLocation, $ToLocation);
    $MaharajaRate = GetMaharajaFare($Rate);
    $CmIdArray = array();
    while ($CabListing = mysql_fetch_array($List)) {
        $CCount++;
        if ($Distance < 250) {
            if (empty($CabListing['rate']) || $CabListing['rate'] == 0) {
                //$a = $CabListing['rate'];
                $count =  250 * 10;
            } else {
                $a = $CabListing['rate'];
                $count =  250 * $a;
            }
        } else {
            if (empty($CabListing['rate']) || $CabListing['rate'] == 0) {
                //$a = $CabListing['rate'];
                $count = $Distance * 10;
            } else {
                $a = $CabListing['rate'];
                $count = $Distance * $a;
            }
        }
        if ($PrvCabRouteId != $CabListing['cr_id']) {
            $PrvCabRouteId = $CabListing['cr_id'];
            $CmIdArray[] = $CabListing['cm_Id'];
            if (empty($PrvCabRouteId))
                $PrvCabRouteId = $CabListing['cr_id'];
            if($CabListing['cm_Id']== $OneWayCmId)
                continue;
            else {
                
            
            $OneWayCmId = $CabListing['cm_Id'];
            $Str .= "
                    <tr>          
                    <td style='height:70px; width:200px'><img src='Upload/Images/$CabListing[cblink]' height='70' width='200'/></td>
                    <td style='width:35%'>$CabListing[Type]<br/><font color='#0000ff'>Model-&nbsp &nbsp$CabListing[cm_ModelName]</font></td>
                    <td style='width:15%' ><div class='capacity'><font color='Red'>$CabListing[cm_SeatCapacity]</font></div></td>
                        <td style='position: absolute;' class='price' style='width:80%'> $count <br/>
                        <div id='items'>
                            <div class='item'>
                                <a href='#'><h6>Fare BreakUp<br/></h6>
                                    <div class='info' style='z-index: 2;padding-left:15px'>
                                         A.c Rate :-<font color = 'Red'>$CabListing[rate]</font> <br/>Total Days :- <font color = 'Red'>$days_between</font><br/> Per Day Km :-<font color = 'Red'>250</font><br/><font color='orange'>____________</font> <br/>Total Price <br/>$CabListing[rate] * $days_between * 250 :-  <font color='Red'>$count</font>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </td>
                    <td>
                        <form method='post' action='booking.php'>
                        <input type='hidden' name='CabRouteId' value='$CabListing[cr_id]'>
                        <input type='hidden' name='CabId' value='$CabListing[cr_cabid]'>
                        <input type='hidden' name='Source' value='$FromLocation'>
                        <input type='hidden' name='Destination' value='$ToLocation'>
                        <input type='hidden' name='ToDateTime' value='$CabListing[cr_fromdatetime]'>
                        <input type='hidden' name='CarModelId' value='$CabListing[cm_Id]'>
                        <input type='hidden' name='FromDate' value='$FromLocalDate'>
                        <input type='hidden' name='ToDate' value='$ToLocalDate'>
                        <input type='hidden' name='Type' value='OneWay'>   
                        <input type='hidden' name='Rate' value='$count'>
                        <input type='hidden' name='CarImage' value='$CabListing[cblink]'>   
                        <input type='hidden' name='CarModel' value='$CabListing[cm_ModelName]'>
                        <input type='hidden' name='AcKmRate' value='$CabListing[rate]'>   
                        <input type='hidden' name='Days' value='$days_between'>   
                        <input type='hidden' name='Distance' value='$Distance'> 
                        <input type='hidden' name='Seats' value='$CabListing[cm_SeatCapacity]'> 
                        <input type='hidden' name='CabType' value='$CabListing[Type]'>   
                        <input type='submit' value='Book Now' name='Booking'>
                        </form>
                    </td>
                </tr>";
        }
    }
    }
    $virtulCabList = GetClientVirtualCab(GetVirtualCab(20-$CCount), $days_between,$FromLocation,$ToLocation,$FromLocalDate,$ToLocalDate,$CmIdArray); 
    $Str .= $virtulCabList;
    return $Str;
}
?>

<style>
    #items .item a .info{
        display: none;
        font-size: 12px;
        position: absolute;
        width : 160%;
        

    }

    #items .item a:hover  .info{
        display: block;
        color: #0A246A;
        background-color: #DDD;
    }
</style>
