<?php

function CityPath() {
    $path21 = $_SERVER['DOCUMENT_ROOT'];
    // Differenciate the path for local and web host. Includes database connect file.
    if ($_SERVER['HTTP_HOST'] == "localhost" || $_SERVER['HTTP_HOST'] == "localhost:8080" || $_SERVER['HTTP_HOST'] == "192.168.1.100:8080")
        $path21 .= '/Cabsystem';
    return $path21;
}

include_once CityPath() . '/dbop/data/listingutility.php';
include_once CityPath() . '/dbop/data/sharedfare.php';
include_once CityPath() . '/dbop/data/sublocation.php';
include_once CityPath() . '/dbop/data/hourcharges.php';
include_once CityPath() . '/dbop/data/globalperameter.php';
include_once CityPath() . '/bizop/VirtualCab.php';


function Pre_List_charted($List, $days_between, $FromLocation, $ToLocation,$PickupLocation,$FromLocalDate,$ToLocalDate,$ChargeId,&$CCount) {
    $Str = "";
    $GetChargeById = GetChargesById($ChargeId);
    $FetchCharge = mysql_fetch_array($GetChargeById);
    $Distance = $FetchCharge['Km'];
    $DifaultCharge = $FetchCharge['Charge'];
    $Hours = $FetchCharge['Hour'];
    
    $GetPickupLocation = GetSingleSubLocation($PickupLocation);
    $FetchPickupLocation = mysql_fetch_array($GetPickupLocation);
    $PickupLocation = $FetchPickupLocation['SubLocation']; 
//$SLocationId = 1;
//$DLocationId = 3;
    //$RatePerDay = 300;
    //$Distance = 250;
    $DefaultKmRate = 10;
    $PrvCabRouteId = 0;
    /*$Rate = GetFareByRoute($FromLocationId, $ToLocationId);
    $MaharajaRate = GetMaharajaFare($Rate);
    $ProperLocationRate = SubLocationFare($SubId);*/
    //$i = 0;
      $Pre_cm_Id ="";
        $CmIdArray = array();
       
    while ($CabListing = mysql_fetch_array($List)) {
        $CCount++;
      
        
        if (empty($CabListing['rate']) || $CabListing['rate'] == 0) {
            //$a = $CabListing['rate'];
            $count = 1 * $Distance * $DefaultKmRate;
        } else {
            $a = $CabListing['rate'];
            $count = 1 * $Distance * $a;
        }
        if ($PrvCabRouteId != $CabListing['cr_id'] && !in_array($CabListing['cm_Id'], $CmIdArray) ) {
            //$i++;
           $CmIdArray[] = $CabListing['cm_Id'];
            $PrvCabRouteId = $CabListing['cr_id'];
            if (empty($PrvCabRouteId))
                $PrvCabRouteId = $CabListing['cr_id'];
            if($CabListing['cm_Id'] == $Pre_cm_Id)
                continue;
            else
            {
                //$Pre_cm_Id = 1;
                $Pre_cm_Id = $CabListing['cm_Id'];
            $Str .= "
                    <tr>          
                    <td style='height:70px; width:200px'><img src='Upload/Images/$CabListing[cblink]' height='70' width='200'/></td>
                    <td style='width:35%'>$CabListing[Type]<br/><font color='#0000ff'>Model-&nbsp &nbsp$CabListing[cm_ModelName]</font></td>
                    <td style='width:15%' ><div class='capacity'><font color='Red'>$CabListing[cm_SeatCapacity]</font></div></td>
                    <td style='position: relative;' class='price' style='width:80%'> $count <br/>
                        <div id='items'>
                            <div class='item'>
                                <a href='#'><h6>Fare BreakUp<br/></h6>
                                    <div class='info' style='z-index: 2;padding-left:15px'>
                                         A.c Rate :-<font color = 'Red'>$CabListing[rate]</font> <br/>Total Days :- <font color = 'Red'>$days_between</font><br/> Total Km :-<font color = 'Red'>$Distance</font><br/><font color='orange'>____________</font> <br/>Total Price <br/>$CabListing[rate] * $Distance  :-  <font color='Red'>$count</font>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </td>
                    <td>
                        <form method='post' action='booking.php'>
                        <input type='hidden' name='CabRouteId' value='$CabListing[cr_id]'>
                        <input type='hidden' name='CabId' value='$CabListing[cr_cabid]'>
                        <input type='hidden' name='Source' value='$FromLocation'>
                        <input type='hidden' name='Destination' value='$ToLocation'>
                        <input type='hidden' name='ToDateTime' value='$CabListing[cr_fromdatetime]'>
                        <input type='hidden' name='CarModelId' value='$CabListing[cm_Id]'>
                        <input type='hidden' name='FromDate' value='$FromLocalDate'>
                        <input type='hidden' name='ToDate' value='$ToLocalDate'>
                        <input type='hidden' name='CarImage' value='$CabListing[cblink]'>
                        <input type='hidden' name='Type' value='chartered'>   
                        <input type='hidden' name='Rate' value='$count'>
                        <input type='hidden' name='CarModel' value='$CabListing[cm_ModelName]'>
                        <input type='hidden' name='AcKmRate' value='$CabListing[rate]'>   
                        <input type='hidden' name='Days' value='$days_between'>   
                        <input type='hidden' name='Seats' value='$CabListing[cm_SeatCapacity]'> 
                        <input type='hidden' name='CabType' value='$CabListing[Type]'>             
                        <input type='hidden' name='CabType' value='$CabListing[cb_NoPlate]'>                 
                        <input type='submit' value='Book Now' name='Booking' class='btn btn-large'>
                        </form>
                </td>   
                </tr>";
            }
        }
    }
    $virtulCabList = GetClientVirtualCab(GetVirtualCab(20-$CCount), $days_between,$FromLocation,$ToLocation,$FromLocalDate,$ToLocalDate,$CmIdArray); 
    $Str .= $virtulCabList;
    return $Str;
}
?>

<style>
    #items .item a .info{
        display: none;
        font-size: 12px;
        position: absolute;
        width: 160%;

    }

    #items .item a:hover  .info{
        display: block;
        color: #0A246A;
        background-color: #DDD;
    }
    
</style>
