<?php

function GetListingView($list, $IsBooked, &$cabCount) {
    $cabCount = 0;
    $finalView = "";
    $cabRowView = "";
    $lastCabNo = 0;
    $cabSeatCapacity = 0;
    $cabTicketCount = 0;
    $cabSourceName = "";
    $cabDestinationName = "";
    $cabSourceId = 0;
    $cabDestinationId = 0;
    $cabDriverName = "";
    $driverNumber = "";
    $cabNumberPlate = "";
    $passengerRowView = "";
    $maxBid = 0;
    $modelName = "";
    $carModelId = 0;
    $passengersView = "";
    $cabRouteId = 0;
    $xlgdt = "";
    $xygdt = "";
    $fromDateTime = "";
    $isShare = 0;
    
    
    $clRow = mysql_fetch_assoc($list);
    $lastCabNo = $clRow['cr_cabid'];
    while ($clRow) {
        if ($IsBooked)
            $passengerRowView = getPassangerRowView($clRow['cl_name'], $clRow['cl_phone'], $clRow['bk_ticketCount'], $clRow['bk_IsBookByClient'], $clRow['bk_ispaid'], $clRow['bk_id'], $clRow['bk_fare'], $clRow['bk_amountReceived'], $clRow['bk_bidrate']);

            $cabTicketCount += $clRow['bk_ticketCount'];
        if ($cabSeatCapacity == 0)
            $cabSeatCapacity = $clRow['cm_SeatCapacity'];
        if (!empty($clRow['bk_bidrate']) && $clRow['bk_bidrate'] > $maxBid)
            $maxBid = $clRow['bk_bidrate'];
        if (empty($cabSourceName))
            $cabSourceName = $clRow['lc_sourcename'];
        if (empty($cabDestinationName))
            $cabDestinationName = $clRow['lc_destinationname'];
        if (empty($cabDriverName))
            $cabDriverName = $clRow['dr_drivername'];
        if(empty($driverNumber))
            $driverNumber = $clRow['dr_phone'];
        if (empty($cabNumberPlate))
            $cabNumberPlate = $clRow['cb_NoPlate'];
        if(empty($modelName))
            $modelName = $clRow['cm_ModelName'];
        if($cabRouteId == 0)
            $cabRouteId = $clRow['cr_id'];
        if(empty($xlgdt))
            $cFromDateTime = $clRow['cr_fromdatetime'];
        if(empty($xygdt))
            $cToDateTime = $clRow['cr_todatetime'];
        if($carModelId == 0)
            $carModelId = $clRow['cm_Id'];
        if($cabSourceId == 0)
            $cabSourceId = $clRow['cr_source'];
        if($cabDestinationId == 0)
            $cabDestinationId = $clRow['cr_destination'];
        if(empty($fromDateTime))
            $fromDateTime = $clRow['cr_fromdatetime'];
        if($isShare == 0)
            $isShare = $clRow['cr_isreturntrip'];

        $passengersView .= $passengerRowView;
        $passengerRowView="";

        $clRow = mysql_fetch_assoc($list);
        if(!$clRow)
            $hasRows = FALSE;
        if(!empty($clRow['cr_cabid']))
            if ($lastCabNo == $clRow['cr_cabid'] && $clRow)
                continue;
            
        $cabCount++;
        $passengerHeader="";
        
        if(isset($clRow['bk_bidrate']) && !empty($clRow['bk_bidrate']))
            $fareHeader = "BidRate";
        else
            $fareHeader = "Fare";
        
        $cabRowView = getCabRowView($lastCabNo, $cabNumberPlate, $cabSourceName, $cabDestinationName, $cabDriverName, $cabSeatCapacity, $cabTicketCount, $maxBid, $modelName, $cabRouteId, $IsBooked, $cFromDateTime, $cToDateTime, $carModelId, $driverNumber, $cabSourceId, $cabDestinationId, $fromDateTime, $isShare);
        if(!empty($passengersView))
            $passengerHeader.="<tr style='padding-left: 10px; font-weight: bold;'> <td>&nbsp;</td> <td colspan='3'>Passenger</td> <td>Mobile</td> <td>Seats Booked</td> <td>Booking By</td> <td>$fareHeader</td> <td>Amount Received</td> <td>Action</td></tr>";
        
        $cabview = $cabRowView.$passengerHeader.$passengersView;
        
        if(!empty($passengersView))
            $finalView .= $cabview ."<tr><td colspan='10'>&nbsp;</td></tr>";
        else
            $finalView .= $cabview;
        
        $cabview = "";
        $passengersView = "";

        $lastCabNo = $clRow['cr_cabid'];
        $cabSeatCapacity = $clRow['cm_SeatCapacity'];
        $cabTicketCount = $clRow['bk_ticketCount'];
        $cabSourceName = $clRow['lc_sourcename'];
        $cabDestinationName = $clRow['lc_destinationname'];
        $cabDriverName = $clRow['dr_drivername'];
        $driverNumber = $clRow['dr_phone'];
        $cabNumberPlate = $clRow['cb_NoPlate'];
        $cFromDateTime = $clRow['cr_fromdatetime'];
        $cToDateTime = $clRow['cr_todatetime'];
        $cabRouteId = $clRow['cr_id'];
        $cabSourceId = $clRow['cr_source'];
        $cabDestinationId = $clRow['cr_destination'];
        $maxBid = 0;
        $fromDateTime = $clRow['cr_fromdatetime'];
        $isShare = $clRow['cr_isreturntrip'];
        
    }
    return $finalView;
}

function getPassangerRowView($cl_name, $cl_phone, $bk_seatsBooked, $bk_isBookedByClient, $bk_ispaid, $bk_id, $bk_fare, $bk_amountReceived, $bidRate) {
    if(!empty($bk_id) && $bk_id != 0){
        if($bk_seatsBooked == NULL)
            $bk_seatsBooked = "Charter";
        if($bk_isBookedByClient == 1)
            $bk_isBookedByClient = "By Client";
        else
            $bk_isBookedByClient = "By Admin";
        if(!isset($bk_fare) || empty($bk_fare))
            $bk_fare = $bidRate;
    $PassengerRow = <<<ST
            <tr style='padding-left: 10px;'>
                <td>&nbsp;</td>
                <td colspan='3'>$cl_name</td>
                <td>$cl_phone</td>
                <td>$bk_seatsBooked</td>
                <td>$bk_isBookedByClient</td>
                <td>$bk_fare</td>
                <td>$bk_amountReceived</td>
                <td><a href="#?bookingId=$bk_id">Cancel</a></td>
            </tr>
ST;
    return $PassengerRow;
    }
}

function getCabRowView($lastCabNo, $cabNumberPlate, $cabSourceName, $cabDestinationName, $cabDriverName, $cabSeatCapacity, $cabTicketCount, $maxBid, $modelName, $cabRouteId, $isBooked, $dt1, $dt2, $carModelId, $driverNumber, $cabSourceId, $cabDestionationId, $fromDateTime, $isShare) {
    // for virtual cab
    if(empty($cabNumberPlate) && empty($lastCabNo))
        $cabRow = <<<CL
            <tr style='background-color: #F9F9F9;'>
                <td colspan='2'><a href='cabrouteadd.php?type=virtual&crid=$cabRouteId'>Add Physical Cab</a></td>
                <td>$fromDateTime</td>
                <td>$cabSourceName</td>
                <td>$cabDestinationName</td>
                <td>$modelName</td>
                <td>$cabSeatCapacity</td>
                <td colspan='3'>&nbsp;</td>
            </tr>
CL;
    
    else{
        // for free charter cab
        //else if (($lr['lc_destinationname'] == $lr['lc_sourcename'] || empty($lr['lc_destinationname']) || !empty($lr['lc_destinationname'])) && $lr['cr_isreturntrip'] == 0) {
        if(($cabDestinationName == $cabSourceName || empty($cabDestinationName) || !empty($cabDestinationName))  && $isShare == 0){
            $dt1 = date("d-m-Y H:i", strtotime($dt1));
            $dt2 = date("d-m-Y H:i", strtotime($dt2));
            $cabRow = <<<CL
                <tr style='background-color: #F9F9F9;'>
                    <td>$lastCabNo</td>
                    <td><a href="booking.php?type=chartered&cabRouteId=$cabRouteId&cabId=$lastCabNo">$cabNumberPlate</a></td>
                    <td>$fromDateTime</td>
                    <td>$cabSourceName</td>
                    <td>From: $dt1 - To: $dt2</td>
                    <td>$cabDriverName - $driverNumber</td>
                    <td>$modelName</td>
                    <td>$cabSeatCapacity</td>
                    <td>$cabTicketCount</td>
                    <td>$maxBid</td>
                </tr>
CL;
        }
        else{
            // Shared Cab
            if($cabDestinationName != $cabSourceName){
                $scap = $cabSeatCapacity - 1;
            $cabRow = <<<CP
                <tr style='background-color: #F9F9F9;'>
                    <td>$lastCabNo</td>
                    <td><a href="booking.php?type=shared&cabRouteId=$cabRouteId&cabId=$lastCabNo&source=$cabSourceId&destination=$cabDestionationId&travelDateTime=$dt1&carModelId=$carModelId">$cabNumberPlate</a></td>
                    <td>$fromDateTime</td>
                    <td>$cabSourceName</td>
                    <td>$cabDestinationName</td>
                    <td>$cabDriverName - $driverNumber</td>
                    <td>$modelName</td>
                    <td>$scap</td>
                    <td>$cabTicketCount</td>
                    <td>$maxBid</td>
                </tr>
CP;
            }
            // rest of type
            else{
            $cabRow = <<<CL
                <tr style='background-color: #F9F9F9;'>
                    <td>$lastCabNo</td>
                    <td><a href="booking.php?cabRouteId=$cabRouteId&cabId=$lastCabNo&source=$cabSourceName&destination=$cabDestinationName&travelDateTime=$dt1&carModelId=$carModelId">$cabNumberPlate</a></td>
                    <td>$fromDateTime</td>
                    <td>$cabSourceName</td>
                    <td>$cabDestinationName</td>
                    <td>$cabDriverName - $driverNumber</td>
                    <td>$modelName</td>
                    <td>$cabSeatCapacity</td>
                    <td>$cabTicketCount</td>
                    <td>$maxBid</td>
                </tr>
CL;
            }
        }
    }
    return $cabRow;
}

function NewGetListingView($list, $isBooked) {
    $isFirst = TRUE; $isFirstPassenger = TRUE;
    $maxBid = 0;
    $totalBooking = 0;
    $lastCabId = "";
    $cabView = "";
    $passengerView = "";
    $view = "";
    while ($lr = mysql_fetch_assoc($list)) {
        // preparing cabview.
        
        
        if ($lr['cr_id'] != $lastCabId || empty($lr['cr_id'])) {
            if(!empty($lastCabId)){
                $cabView = str_replace("maxBid", $maxBid, $cabView);
                $cabView = str_replace("totalBooking", $totalBooking, $cabView);
                $view .= $cabView.$passengerView;
                $cabView = ""; $passengerView = "";
                $totalBooking = 0;
                $maxBid = 0;
            }
            
            
            
            if(!$isFirst && $isBooked){
                $cabView .= "<tr><td colspan='10'>&nbsp;</td></tr>";
                $isFirstPassenger = TRUE;
            }
            else
                $isFirst = FALSE;
            $lastCabId = $lr['cr_id'];

            $driverSlip = "";
            if($isBooked)
                $driverSlip = " | <a href='driverslip.php?cabRouteId=$lr[cr_id]' target='_blank'>Driver Slip</a>";
            
            // Virtual cab
            if (empty($lr['cb_NoPlate'])) {
                $cabView .= "<tr style='background-color: #F9F9F9;'>
                            <td colspan='2'><a href='cabrouteadd.php?type=virtual&crid=$lr[cr_id]'>Add Physical Cab</a></td>
                            <td>$lr[cr_fromdatetime]</td>
                            <td colspan='2'>$lr[lc_sourcename] TO $lr[lc_destinationname]</td>
                            <td>$lr[cm_ModelName]</td>
                            <td>$lr[cm_SeatCapacity]</td>
                            <td colspan='3'>&nbsp;</td>
                        </tr>";
            }
            // Free charter cab
            else if (($lr['lc_destinationname'] == $lr['lc_sourcename'] || empty($lr['lc_destinationname']) || !empty($lr['lc_destinationname'])) && $lr['cr_isreturntrip'] == 0) {
                $dt1 = date("d-m-Y H:i", strtotime($lr['cr_fromdatetime']));
                $dt2 = date("d-m-Y H:i", strtotime($lr['cr_todatetime']));
                $cabView .= "<tr style='background-color: #F9F9F9;'>
                            <td>$lr[cr_cabid]</td>
                            <td><a href='booking.php?type=chartered&cabRouteId=$lr[cr_id]&cabId=$lr[cr_cabid]'>$lr[cb_NoPlate]</a> $driverSlip</td>
                            <td>$lr[cr_fromdatetime]</td>
                            <td>$lr[lc_sourcename]</td>
                            <td>$dt1 To $dt2</td>
                            <td>$lr[dr_drivername] - $lr[dr_phone]</td>
                            <td>$lr[cm_ModelName]</td>
                            <td>$lr[cm_SeatCapacity]</td>
                            <td>totalBooking</td>
                            <td>maxBid</td>
                        </tr>";
            }
            // Shared one way cab
            else if ($lr['lc_destinationname'] != $lr['lc_sourcename'] && $lr['cr_isreturntrip'] == 1) {
                $scap = $lr['cm_SeatCapacity'] - 1;
                $cabView .= "<tr style='background-color: #F9F9F9;'>
                            <td colspan='2'><a href='booking.php?type=shared&cabRouteId=$lr[cr_id]&cabId=$lr[cr_cabid]&source=$lr[cr_source]&destination=$lr[cr_destination]&travelDateTime=$lr[cr_fromdatetime]&carModelId=$lr[cm_Id]'>$lr[cb_NoPlate]</a> $driverSlip</td>
                            <td>$lr[cr_fromdatetime]</td>
                            <td colspan='2'>$lr[lc_sourcename] To $lr[lc_destinationname]</td>
                            <td>$lr[dr_drivername] - $lr[dr_phone]</td>
                            <td>$lr[cm_ModelName]</td>
                            <td>$scap</td>
                            <td>totalBooking</td>
                            <td>maxBid</td>
                        </tr>";
            }
            // all other type
            else {
                $scap = $lr['cm_SeatCapacity'] - 1;
                $cabView .= "<tr style='background-color: #F9F9F9;'>
                            <td>$lr[cr_cabid]</td>
                            <td><a href='booking.php?cabRouteId=$lr[cr_id]&cabId=$lr[cr_cabid]&source=$lr[cr_source]&destination=$lr[cr_destination]&travelDateTime=$lr[cr_fromdatetime]&carModelId=$lr[cm_Id]'>$lr[cb_NoPlate]</a> $driverSlip </td>
                            <td>$lr[cr_fromdatetime]</td>
                            <td>$lr[lc_sourcename]</td>
                            <td>$lr[lc_destinationname]</td>
                            <td>$lr[dr_drivername] - $lr[dr_phone]</td>
                            <td>$lr[cm_ModelName]</td>
                            <td>$scap</td>
                            <td>totalBooking</td>
                            <td>maxBid</td>
                        </tr>";
            }
        }
        
        if (!empty($lr['bk_bidrate']) && $lr['bk_bidrate'] > $maxBid)
            $maxBid = $lr['bk_bidrate'];
        
        if(!empty($lr['bk_ticketCount']))
            $totalBooking += $lr['bk_ticketCount'];
        
        if(!$isFirst && $isFirstPassenger && $isBooked){
            if(isset($lr['bk_bidrate']) && !empty($lr['bk_bidrate']) && $lr['bk_bidrate'] > 0)
                $bidHeading = "BidAmount";
            else
                $bidHeading = "Fare";
                $passengerView .= "<tr style='padding-left: 10px; font-weight: bold;'> <td>&nbsp;</td> <td colspan='3'>Passenger</td> <td>Mobile</td> <td>Seats Booked</td> <td>Booking By</td> <td>$bidHeading</td> <td>Amount Received</td> <td>Action</td></tr>";
            $isFirstPassenger = FALSE;
        }
        // Preparing passengers view
        //$clRow['cl_name'], $clRow['cl_phone'], $clRow['bk_ticketCount'], $clRow['bk_IsBookByClient'], $clRow['bk_ispaid'], $clRow['bk_id'], $clRow['bk_fare'], $clRow['bk_amountReceived']
        if($isBooked){
            if (!empty($lr['bk_id']) && $lr['bk_id'] != 0 && $lr['bk_status'] == 1) {
                if ($lr['bk_ticketCount'] == NULL)
                    $bk_seatsBooked = "-";
                else
                    $bk_seatsBooked = $lr['bk_ticketCount'];

                if ($lr['bk_IsBookByClient'] == 1)
                    $bk_isBookedByClient = "By Client";
                else
                    $bk_isBookedByClient = "By Admin";
                
                if(isset($lr['bk_bidrate']) && !empty($lr['bk_bidrate']) && $lr['bk_bidrate'] > 0){
                    $fare = $lr['bk_bidrate'];
                    $bidLink = "<a href='awardbid.php?bookingId=$lr[bk_id]&cabRouteId=$lr[cr_id]'>AwardBid</a>";
                }
                else{
                    $fare = $lr['bk_fare'];
                    $bidLink = "<a href='BookingCancellation.php?bookingId=$lr[bk_id]'>Cancel</a>";
                }
                $passengerView .= "<tr style='padding-left: 10px;'>
                            <td>&nbsp;</td>
                            <td colspan='3'>$lr[cl_name]</td>
                            <td>$lr[cl_phone]</td>
                            <td>$bk_seatsBooked</td>
                            <td>$bk_isBookedByClient</td>
                            <td>$fare</td>
                            <td>$lr[bk_amountReceived]</td>
                            <td>$bidLink</td>
                        </tr>";
            }
        }
    }
    $cabView = str_replace("maxBid", $maxBid, $cabView);
    $cabView = str_replace("totalBooking", $totalBooking, $cabView);
    $view .= $cabView.$passengerView;
    $cabView = ""; $passengerView = "";
    $maxBid = 0; $totalBooking = 0;
    return $view;
}
?>
