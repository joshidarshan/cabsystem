<?php

function Path() {
    $path = $_SERVER['DOCUMENT_ROOT'];
    // Differenciate the path for local and web host. Includes database connect file.
    if ($_SERVER['HTTP_HOST'] == "localhost" || $_SERVER['HTTP_HOST'] == "localhost:8080" || $_SERVER['HTTP_HOST'] == "192.168.1.100:8080")
        $path .= '/Cabsystem';
    return $path;
}

include_once Path() . '/dbop/data/utility.php';

function SeatLayoutGenerator($cabRouteId, $cabModelId) {
    // Booking for cab
    $cabBooking = "SELECT bk.`SeatsBooked`
                    FROM bookings bk
                    LEFT JOIN cancelations cn
                        ON bk.`Id` = cn.`BookingId`
                    WHERE bk.`CabRouteId` = '$cabRouteId' AND bk.`Status` = '1' AND cn.`BookingId` IS NULL";
    $cabBookingData = GetData($cabBooking);

    // Seat capacity & layout
    $carModelQuery = "SELECT `SeatLayout`
                        FROM carmodels
                        WHERE `Id` = '$cabModelId'";
    $carModelData = GetData($carModelQuery);
    $carModelRow = mysql_fetch_assoc($carModelData);

    $carModelSeatLayoutSplit = explode(",", $carModelRow['SeatLayout']);
    $seatsArray = array();

    foreach ($carModelSeatLayoutSplit as $csr => $csd) {
        $csd = strtolower($csd);
        $seat = explode("x", $csd);
        $seatsArray[$seat[0]] = array($seat[1], 0);
    }

    while ($cbrow = mysql_fetch_assoc($cabBookingData)) {
        $cbSplit = explode(",", $cbrow['SeatsBooked']);
        foreach ($cbSplit as $cbr => $cbd) {
            $cbd = strtolower($cbd);
            $cbBookRow = explode("x", $cbd);

            $bval = $seatsArray[$cbBookRow[0]][1];
            $bval += $cbBookRow[1];
            $seatsArray[$cbBookRow[0]][1] = $bval;
        }
    }

    return $seatsArray;
}

function SeatViewGenerator($layout) {
    $seatLayout = "<div id='seatLayout'><table class='table table-striped'>";
    foreach ($layout as $lr => $ld) {
        $availableSeats = $ld[0] - $ld[1];
        if(strtolower($lr) == 'a'){
            $seatLayout .= "<tr>
                                <td style='width:200px;'><strong style='margin-right: 15px;'>Row - ".strtoupper($lr)."</strong> &nbsp;&nbsp;&nbsp;  Availale : <strong>$availableSeats</strong> / <strong>$ld[0]</strong></td>
                                <td><input type='text' class='maharajaseat' style='width:50px;' name='seat[]' id='seat-$lr' onblur=check('seat-$lr',$availableSeats) />
                            </tr>";
        }
        else{
            $seatLayout .= "<tr>
                                <td style='width:200px;'><strong style='margin-right: 15px;'>Row - ".strtoupper($lr)."</strong> &nbsp;&nbsp;&nbsp;  Availale : <strong>$availableSeats</strong> / <strong>$ld[0]</strong></td>
                                <td><input type='text' class='seat' style='width:50px;' name='seat[]' id='seat-$lr' onblur=check('seat-$lr',$availableSeats) />
                            </tr>";
        }
    }
    
    $seatLayout .= "</table></div>";
    return $seatLayout;
}

function SeatViewGeneratorClientSide($layout) {
    $seatLayout = "<div id='seatLayout'>";
    foreach ($layout as $lr => $ld) {
        $availableSeats = $ld[0] - $ld[1];
        if(strtolower($lr) == 'a'){
            $seatLayout .= "<div class='span6' style='width:223px; height:80px'>
                            
                            <div class='span7' style='text-align: left;'>Row - ".strtoupper($lr)."
                                
<input type='text' class='maharajaseat' style='width:40px;' name='seat[]' id='seat-$lr' onblur=check('seat-$lr',$availableSeats) /><span style='font-size:14px'>Availale :<strong>$availableSeats</strong>/<strong>$ld[0]</strong></span>
                                    </div></div><div class='span2' style='width:55px'>
            <img src='Upload/images/steering_wheel.png' width='50px'>
        </div>";
                            
        }
        else{
            $seatLayout .= "<div class='span11' style='clear:both; width:290px; height:80px'><div class='span7' style='text-align:left;margin-top:22px'>Row - ".strtoupper($lr)."
                                <input type='text' class='seat' style='width:50px;' name='seat[]' id='seat-$lr' onblur=check('seat-$lr',$availableSeats) />
                                    Availale :<strong>$availableSeats</strong>/<strong>$ld[0]</strong></div></div>";
        }
        
    }
   $seatLayout .= "</div>";
    return $seatLayout;
   
}

?>
            
          