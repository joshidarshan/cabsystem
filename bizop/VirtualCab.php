<?php
if (!isset($_SESSION))
    session_start();
$_SESSION['cat'] = "inventory";

function Path() {
    $path = $_SERVER['DOCUMENT_ROOT'];
    // Differenciate the path for local and web host. Includes database connect file.
    if ($_SERVER['HTTP_HOST'] == "localhost" || $_SERVER['HTTP_HOST'] == "localhost:8080")
        $path .= '/Cabsystem';
    return $path;
}

include_once Path().'/dbop/data/globalperameter.php';
include_once Path().'/dbop/data/carmodel.php';



function GetVirtualCab($noCabs = 20){
    //$noVirtualCabPerModel = GetVirtualCabPerModel();
    $noVirtualModel = GetVirtualCabCount();
    
    // Get the list of carmodel which are allowed in virtual mode.
    $virtualCabListQuery = GetVirtualCarModels();
    $virtualCabList = array();
    //`Id`, `Name`, `CarMakeId`, `CarTypeId`, `Seats`, `SeatLayout`
    while($virtualCabRow = mysql_fetch_array($virtualCabListQuery)){
        $virtualLocalData['Id'] = $virtualCabRow['Id'];
        $virtualLocalData['Name'] = $virtualCabRow['Name'];
        $virtualLocalData['Seats'] = $virtualCabRow['Seats'];
        $virtualLocalData['CarImage'] = $virtualCabRow['CarImage'];
        $virtualLocalData['Type'] = $virtualCabRow['Type'];
        if($virtualCabRow['DefaultKmRate'] <10)
            $virtualLocalData['DefaultKmRate'] = "0".$virtualCabRow['DefaultKmRate'];
        else
            $virtualLocalData['DefaultKmRate'] = $virtualCabRow['DefaultKmRate'];
        
        if($virtualCabRow['DefaultAcKmRate'] < 10)
            $virtualLocalData['DefaultAcKmRate'] = "0".$virtualCabRow['DefaultAcKmRate'];
        else
            $virtualLocalData['DefaultAcKmRate'] = $virtualCabRow['DefaultAcKmRate'];
        
        array_push($virtualCabList, $virtualLocalData);
    }
    
    // This might not be needed. Because we will need to send model list. The parent form will iterate the list according
    // to requirement.
    $cabListReturn = array();
//    echo "NoCabs: $noCabs, NoVirtualModel: $noVirtualModel";
    //echo round($noCabs/$noVirtualModel);
    $cabToGenerate = round($noCabs/$noVirtualModel);
    if($cabToGenerate == 0)
        $cabToGenerate = 1;
        
    $virtualCounter = 0;
    for($i=0; $i<$cabToGenerate; $i++){
        for($j=0; $j<count($virtualCabList); $j++){
            if($virtualCounter < $noCabs){
                array_push($cabListReturn, $virtualCabList[$j]);
                $virtualCounter++;
            }
            else
                break;
        }
    }
    
    return $cabListReturn;
}

function GetAdminVirtualCab(){
    $virtualCabListQuery = GetVirtualCarModels();
    $virtualCabList = array();
    while($virtualCabRow = mysql_fetch_array($virtualCabListQuery)){
        $virtualLocalData['Id'] = $virtualCabRow['Id'];
        $virtualLocalData['Name'] = $virtualCabRow['Name'];
        $virtualLocalData['Seats'] = $virtualCabRow['Seats'];
        if($virtualCabRow['DefaultKmRate'] <10)
            $virtualLocalData['DefaultKmRate'] = "0".$virtualCabRow['DefaultKmRate'];
        else
            $virtualLocalData['DefaultKmRate'] = $virtualCabRow['DefaultKmRate'];
        
        if($virtualCabRow['DefaultAcKmRate'] < 10)
            $virtualLocalData['DefaultAcKmRate'] = "0".$virtualCabRow['DefaultAcKmRate'];
        else
            $virtualLocalData['DefaultAcKmRate'] = $virtualCabRow['DefaultAcKmRate'];
        
        array_push($virtualCabList, $virtualLocalData);
    }
    return $virtualCabList;
}

function GetVirtualCabView(){
    $virtualCabData = GetAdminVirtualCab();
    $responseString = "";
    foreach($virtualCabData as $vk=>$vd){
        $responseString .= "<tr>
                                <td>
                                    <a href='booking.php?type=virtual&carModelId=$vd[Id]'>$vd[Name]</a>
                                </td>
                                <td>
                                    $vd[Seats]
                                </td>
                                <td>
                                    $vd[DefaultKmRate]
                                </td>
                                <td>
                                    $vd[DefaultAcKmRate]
                                </td>
                            </tr>";
    }
    return $responseString;
}

function GetClientVirtualCab($List, $days,$FromLocationId,$ToLocationId,$FromLocalDate,$ToLocalDate,$CmIdArray){
        include_once Path() . '/dbop/data/globalperameter.php';
        $GetMinRate = GetAllGlobalParameters();
    $FetchMinRate = mysql_fetch_array($GetMinRate);
    $RatePerDay = $FetchMinRate['cp_minimunkm'];
    if($days<2)
        $days = 2;
    $TotalMinKm =  2*$RatePerDay;
    if(isset($List) && !empty($List)){
        $Str = "";
        $PreCmId ="";
        $i = "";
        $uniqueModelIdList = array();
       foreach ($List as $Cab=>$Cb){
           if($days<2)
               $days = 2;
           if($Cb['DefaultKmRate']=="" ){
             $Total =   10 * $RatePerDay * $days;
             
           }
           else{
               $Rate = $Cb['DefaultKmRate'];
               $Total = $Rate * $RatePerDay * $days; 
           }
          
          /*$charterView = Pre_List_charted($List, $days_between, $FromLocationId, $ToLocationId, $SubId, $FromLocalDate, $ToLocalDate, &$CCount);
          $Cb = $Cb['Id'];
          while ($CabListing = mysql_fetch_array($List)){
              if($CabListing['cm_Id']==$Cb['Id'])
                  continue;
          }*/
           $virtualFlag = FALSE;
       foreach ($CmIdArray as $ck=>$cv){
           if(!$virtualFlag){
               if($Cb['Id'] == $cv)
                   $virtualFlag = TRUE;
           }
       }
           /*$i++;
           if($Cb['Id']==$CmIdArray[$i])
               continue;
           else{*/
               
           if(!$virtualFlag && !in_array($Cb['Id'], $uniqueModelIdList)){
                $virtualFlag = FALSE;
                $PreCmId = $Cb['Id'];
                $uniqueModelIdList[] = $Cb['Id'];
         $Str .= "
            <tbody> <tr>
                    <td style='height:70px; width:200px;'><img src='Upload/images/$Cb[CarImage]'height='70' width='200'/></td>
                    <td style='width:35%'>$Cb[Type] <br/><font color='#0000ff'>Model-&nbsp &nbsp $Cb[Name]</font></td>
                    <td style='width:15%;'><div class='capacity'><font color='red'>$Cb[Seats]</font></div></td>
                     <td style='position: relative;' class='price' style='width:80%'>$Total <br/> <div id='items'>  
                         <div class='item'>
                            <a href='#'><h6>Fare BreakUp<br/></h6>
                                <div class='info' style='z-index: 2;padding-left:15px'>
                                     A.c Rate :-<font color = 'Red'>$Cb[DefaultKmRate]</font> <br/>Total Days :- <font color = 'Red'>$days</font><br/> Per Day Km :-<font color = 'Red'>$RatePerDay</font><br/>Minimum Total Km :- <font color='Red'>$TotalMinKm </font><font color='orange'>__________________________</font> <br/>Total Price <br/>$Cb[DefaultKmRate] * $days * $RatePerDay :-  <font color='Red'>$Total</font>   
                                </div>
                             </a>
                        </div></td>
                        <td>
                        <form method='post' action='booking.php'>
                            <input type='hidden' name='CabRouteId' value=''>
                            <input type='hidden' name='Source' value='$FromLocationId'>
                            <input type='hidden' name='Destination' value='$ToLocationId'>
                            <input type='hidden' name='CarModelId' value='$Cb[Id]'>
                            <input type='hidden' name='FromDate' value='$FromLocalDate'>
                            <input type='hidden' name='ToDate' value='$ToLocalDate'>
                            <input type='hidden' name='CarImage' value='$Cb[CarImage]'>
                            <input type='hidden' name='Type' value='Virtualchartered'>   
                            <input type='hidden' name='Rate' value='$Total'>
                            <input type='hidden' name='CarModel' value='$Cb[Name]'>
                            <input type='hidden' name='AcKmRate' value='$Cb[DefaultKmRate]'>   
                            <input type='hidden' name='Days' value='$days'>     
                            <input type='hidden' name='Seats' value='$Cb[Seats]'> 
                            <input type='hidden' name='CabType' value='$Cb[Type]'>   
                            <input type='submit' value='Book Now' name='Booking' class='btn btn-large'>
                        </form>
                    </td>
           </tr><tbody>";
            }
       }
return $Str;
}
}
?>
<style>
    #items .item a .info{
        display: none;
        font-size: 12px;
        position: absolute;
        
        

    }

    #items .item a:hover  .info{
        display: block;
        color: #0A246A;
        background-color: #DDD;
        min-width: 120px;
        width: auto;
    }
    
</style>
