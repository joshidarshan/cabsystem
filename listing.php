<?php
if (!isset($_SESSION))
    session_start();

function LPath() {
    $path = $_SERVER['DOCUMENT_ROOT'];
    // Differenciate the path for local and web host. Includes database connect file.
    if ($_SERVER['HTTP_HOST'] == "localhost" || $_SERVER['HTTP_HOST'] == "localhost:8080")
        $path .= '/Cabsystem';
    return $path;
}

include_once LPath() . '/templates/header.php';
include_once LPath() . '/dbop/data/listingutility.php';
include_once LPath() . '/dbop/data/location.php';
?>
<script type="text/javascript" src="js/jquery.tablesorter.min.js"></script>
<link rel="stylesheet" href="css/blue/style.css" />    
<form action="booking.php" method="post">

    <?php
    $Source = "Not Selected";
    
    if(!empty($_POST['FromLocation'])){
    $Sid = $_POST['FromLocation'];
    $SLocation  =  GetSingleLocation($Sid);
    $FetchSource = mysql_fetch_array($SLocation);
    $Source = $FetchSource['Name'];
    }
    $Dest = "Not Selected";
    if(!empty($_POST['ToLocation'])){
    $Did = $_POST['ToLocation'];
    $DLocation  =  GetSingleLocation($Did);
    $FetchDest = mysql_fetch_array($DLocation);
    $Dest = $FetchDest['Name'];
    }
    else if(!empty ($_POST['FromLocation']) && (!isset ($_POST['ToLocation']) || empty($_POST['ToLocation']))){
        $Did = $Sid;
        $Dest = $Source;
    }
    $FromDate = $_POST['FromDate'];
    //$SourceName = $_POST['SourceName'];
    //$DestName = $_POST['DestName'];
  //  echo $SourceLocanId, $DestinationId;
    if (isset($_POST['outstation'])) {
        include_once LPath() . '/bizop/Views/ClientSideCabListing.php';
        $SubId = NULL;
        $FromLocalDate = $_POST['FromDate'];
        $ToLocalDate = $_POST['ToDate'];
        $FromLocation = $_POST['FromLocation'];
        $ToLocation = $_POST['FromLocation'];
      //  $SourceName = $_POST['SourceName'];
       // $DestName = $_POST['DestName'];
        $NoPersons = 1;
        if(!empty($_POST['NoPersons']))
            $NoPersons = $_POST['NoPersons'];
        $start = strtotime($FromLocalDate);
        $end = strtotime($ToLocalDate);
        $Distance = 0;
        $days_between = ceil(abs($end - $start) / 86400);
        if($days_between == 0)
            $days_between = 1;
        $List = GetClientCharterFree($FromLocalDate, $ToLocalDate, $FromLocation, $NoPersons);
        $CabCount = 0;
        $charterView = "";
// hiddenfields
        //echo "<input type='hidden' id='hFromDate' name='hFromDate' value='$FromLocalDate' />";
                                                    //Pre_List_charted($List, $days_between, $FromLocationId, $ToLocationId, $SubId, $FromLocalDate, $ToLocalDate, &$CCount) {
        if(isset($_POST['outstation'])){ $charterView = Pre_List_charted($List, $days_between, $FromLocation, $ToLocation,$SubId,$FromLocalDate,$ToLocalDate,$CabCount); }
        //echo $CabCount;
    }
    ?>
    <?php 
    if (isset($_POST['citycab'])) {
        include_once LPath() . '/bizop/Views/ClientSideCityCabListing.php';
        $SubId = NULL;
        $FromLocalDate = $_POST['FromDate'];
        $FromLocation = $_POST['FromLocation'];
        $ToLocalDate = $_POST['ToDate'];
        $NoPersons = 2;
      //  $SourceName = $_POST['SourceName'];
       // $DestName = $_POST['DestName'];
        //$Distance = 0;
        $days_between = 1;
        $ToLocation = $_POST['FromLocation'];
        $PickupLocation = $_POST['SubLocationId'];
        $ChargeId = $_POST['ChargeId'];
        $List = GetClientCharterFree($FromLocalDate, $ToLocalDate, $FromLocation, $NoPersons);
        $CabCount = 0;
        $charterView = "";
// hiddenfields
        //echo "<input type='hidden' id='hFromDate' name='hFromDate' value='$FromLocalDate' />";
                                                    //Pre_List_charted($List, $days_between, $FromLocationId, $ToLocationId, $SubId, $FromLocalDate, $ToLocalDate, &$CCount) {
        if(isset($_POST['citycab'])){ $charterView = Pre_List_charted($List, $days_between, $FromLocation, $ToLocation,$PickupLocation,$FromLocalDate,$ToLocalDate,$ChargeId,$CabCount); }
        //echo $CabCount;
    }
    ?>
    <?php
    if (isset($_POST['OneWay'])) {
        include_once LPath() . '/bizop/Views/ClientSideOneWayCabListing.php';
        $SubId = NULL;
        $FromLocalDate = $_POST['FromDate'];
        $ToLocalDate = $_POST['ToDate'];
        $FromLocation = $_POST['FromLocation'];
        $ToLocation = $_POST['ToLocation'];
        $NoPersons = 1;
        $start = strtotime($FromLocalDate);
        $SourceName = $_POST['SourceName'];
        $DestName = $_POST['DestName'];
        $end = strtotime($ToLocalDate);
        $Distance = $_POST['DistanceKm'];
        $days_between = ceil(abs($end - $start) / 86400);
        $List = GetClientCharterFree($FromLocalDate, $ToLocalDate, $FromLocation, $NoPersons);
        // hiddenfields
        //echo "<input type='hidden' id='hFromDate' name='hFromDate' value='$FromLocalDate' />";
    }
    ?>
    <?php
    if (isset($_POST['shared'])) {
        include_once LPath() . '/bizop/Views/ClientSideSharedCab.php';
        $FromLocalDate = $_POST['FromDate'];
        $ToLocalDate = $_POST['ToDate'];
        $FromLocation = $_POST['FromLocation'];
        $ToLocation = $_POST['ToLocation'];
        $SubId = $_POST['SubLocationId'];
        $SourceName = $_POST['SourceName'];
            $DestName = $_POST['DestName'];
        $Type = $_POST['type'];
        $days_between = NULL;
        $Distance = 0;
//$start = strtotime($FromLocalDate);
//$end = strtotime($ToLocalDate);
//$days_between = ceil(abs($end - $start) / 86400);
//$total = 250 * $days_between;
//$Rate = $AcKm * $total; 

        $List = GetClientSharedFree($FromLocalDate, $ToLocalDate, $FromLocation, $ToLocation);
        //echo "<input type='hidden' name='CabRouteId' id='CabrouteId' value='/>"
    }
    ?>
    
    <?php 
        if(isset($_POST['Bid']))
        {
            include_once LPath() . '/bizop/Views/ClientSideBidACab.php';
            $FromLocalDate = $_POST['FromDate'];
            $ToLocalDate = $_POST['ToDate'];
            $FromLocation = $_POST['FromLocation'];
            $ToLocation = $_POST['ToLocation'];
            $SourceName = $_POST['SourceName'];
            $DestName = "";
             $SubId = NULL;
            $Type = NULL;
            $days_between = NULL;
            $Distance = 0;
            $List = GetBidFree($FromLocalDate, $ToLocalDate, $FromLocation, $ToLocation);
            
        }
    ?>
    <div class="container" >
        <!--<marquee behavour="left"><img src="Upload/images/taxi-icon.png" height="100px" width="100px"/><br/><h3>Select a Car</h3></marquee>-->
        <div class="span10 searchliine" style="background-image: url('Upload/Images/main-bg.jpg'); margin-top: 2%"><p><b>  Your Search: </b><a><?php echo $Source;?></a> To <a><?php echo $Dest;?></a> on <a><?php echo $FromDate;?></a> <a href="index.php">(Modify Search)</a></p></div>
        <div class="row listingform span10">
            <table id="listing" class="table table-striped table-bordered  tablesorter" >
                <style>
                    #listing td{
        vertical-align: middle;
    }
                </style>
                <thead>
                <tr>
                    <th>
                        Cabs
                    </th>
                    <th>
                        Make
                    </th>
                    <th>
                        Capacity
                    </th>
                    <th>
                        Price
                    </th>
                    <th>&nbsp;

                    </th>
                </tr>
                </thead>
                <tbody>
                <tr>
                <?php 
                if(isset($_POST['Bid']) && !empty($List)){ echo Pre_List_Bid($List, $days_between, $FromLocation, $ToLocation,$SubId,$FromLocalDate,$ToLocalDate); }
                if(isset($_POST['shared'])){ echo Pre_List_shared($List, $days_between, $FromLocation, $ToLocation,$SubId,$FromLocalDate,$ToLocalDate,$Distance); }
                if(isset($_POST['outstation'])){ echo $charterView;}
                 if(isset($_POST['citycab'])){ echo $charterView;}
                if(isset($_POST['OneWay'])){ echo Pre_List_OneWay($List, $days_between, $FromLocation, $ToLocation,$SubId,$FromLocalDate,$ToLocalDate,$Distance); }
                
                ?>  
                </tr>
               <!-- <tr><td colspan="5"><div class="paging"><a>1</a> 2 3 ... 14 15 16</div></td></tr>-->
                </tbody>
            </table>
        </div>
    </div>
</form>
<script>
    var isCalled = false;
    setTimeout(function(){
        callSorter();
    }, 5000);
    
    if(!isCalled)
        
    $(document).ready(function(){ 
        $("#listing").tablesorter();
    }); 
    
    function callSorter(){
        $("table#listing").trigger("update");
        /*var sorting = [[0,0]];
        $("table#listing").trigger("sorton",[sorting]);*/
    }
    
    $(function(){
        if ($("table#listing tbody tr").length > 0)
            $("#listing").tablesorter();
    });
</script>
<?php
include_once LPath() . '/templates/footer.php';
?>

